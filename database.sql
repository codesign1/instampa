-- Productos

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (1,'Paquete 10 fotos','10 fotos','',10,30000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (2,'Paquete 12 fotos','12 fotos','',12,36000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (3,'Paquete de 16 fotos','16 fotos','',16,48000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (4,'Paquete de 20 fotos','20 fotos','',20,58000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (5,'Paquete de 24 fotos','24 fotos','',24,69000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (6,'Paquete de 32 fotos','32 fotos','',32,88000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (7,'Paquete de 40 fotos','40 fotos','',40,106000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (8,'Paquete de 50 fotos','50 fotos','',50,127000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (9,'Paquete de 60 fotos','60 fotos','',60,147000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (10,'Paquete de 70 fotos','70 fotos','',70,164000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (11,'Paquete de 80 fotos','80 fotos','',80,180000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (12,'Paquete de 90 fotos','90 fotos','',90,193000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (13,'Paquete de 100 fotos','100 fotos','',100,200000,1);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (14,'Marco de madera de 1 foto','Marco de madera 1 foto 19,5 x 19,5','',1,32000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (15,'Marco de madera de 2 fotos','Marco de madera de 2 x 1 Fotos 24,7 x 24,7','',2,43000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (16,'Marco de madera de 3 fotos','Marco de madera de 3 x 1 fotos 36 x 24','',3,48000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (17,'Marco de madera de 4 fotos','Marco de madera de 2 x 2 fotos 30 x 30','',4,53000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (18,'Marco de madera de 6 fotos','Marco de madera de 3 x 2 fotos 38 x 38','',6,62000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (19,'Marco de madera de 9 fotos','Marco de madera de 3 x 3 fotos 38 x 45','',9,72000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (20,'Marco de madera de 12 fotos','Marco de madera de 4 x 3 fotos 42 x 53','',12,92000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (21,'Marco de madera de 20 fotos','Marco de madera de 5 x 4 fotos 53 x 66','',20,130000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (22,'Marco de madera de 24 fotos','Marco de madera de 4 x 6 fotos 53 x 79','',24,150000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (23,'Marco de madera de 32 fotos','Marco de madera de 4 x 8 fotos 55 x 105','',32,195000,2);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (24,'Cuerda de lentajuelas de 1 m','Cuerda de lentejuelas de 1 m con 6 fotos','',6,30000,4);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (25,'Cuerda de lentejuelas 1.50m','Cuerda de lentejuelas 1.50m con 10 fotos','',10,35000,4);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (26,'Cuerda de lentejuelas 1.70m','Cuerda de lentejuelas 1.70m con 12 fotos','',12,40000,4);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (27,'Cuerda de lentejuelas 3m','Cuerda de lentejuelas 3m con 20 fotos','',20,62000,4);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (28,'Cuerda de lentejuelas 3.40m','Cuerda de lentejuelas 3.40m con 24 fotos','',24,72000,4);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (29,'Album','Album de fotografias','/img/albums/album.png',0,30000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (30,'Album + 12 Fotos','Album de fotografias + un set de 12 fotos','/img/albums/album.png',12,59000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (31,'Album + 16 Fotos','Album de fotografias + un set de 16 fotos','/img/albums/album.png',16,68000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (32,'Album + 20 Fotos','Album de fotografias + un set de 20 fotos','/img/albums/album.png',20,76000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (33,'Album + 24 Fotos','Album de fotografias + un set de 24 fotos','',24,83000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (34,'Album + 32 Fotos','Album de fotografias + un set de 32 fotos','',32,97000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (35,'Album + 40 Fotos','Album de fotografias + un set de 40 fotos','',40,110000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (36,'Album + 50 Fotos','Album de fotografias + un set de 50 fotos','',50,134000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (37,'Album + 60 Fotos','Album de fotografias + un set de 60 fotos','',60,159000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (38,'Album + 70 Fotos','Album de fotografias + un set de 70 fotos','',70,175000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (39,'Album + 80 Fotos','Album de fotografias + un set de 80 fotos','',80,189000,5);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (40,'1 Foto + 1 Minicaballete','1 Foto + 1 Minicaballete','',1,15000,3);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (41,'2 Fotos + 2 Minicaballetes','2 Fotos + 2 Minicaballetes','',2,25000,3);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (42,'3 Fotos + 3 Minicaballetes','3 Fotos + 3 Minicaballetes','',3,40000,3);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (43,'4 Fotos + 4 Minicaballetes','4 Fotos + 4 Minicaballetes','',4,55000,3);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (45,'Postal 1 Foto','Postal con 1 foto','img/postales/postal.jpg',1,6000,6);

INSERT INTO public.producto(
id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto)
VALUES (46,'Postal 2 Fotos','Postal con 2 fotos','img/postales/postal.jpg',2,8000,6);

-- Marco 1

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/unoxuno/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/unoxuno/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/unoxuno/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/unoxuno/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/unoxuno/cafe.png', 'Café', '#663300', 'Marco Café', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco fucsia', '/img/marcos/unoxuno/fucsia.png', 'Fucsia', '#FF00CC', 'Marco Fucsia', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/unoxuno/gris.png', 'Gris', '#999999', 'Marco Gris', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/unoxuno/morado.png', 'Morado', '#6600CC', 'Marco Morado', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/unoxuno/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/unoxuno/negro.png', 'Negro', '#FFFFFF', 'Marco Negro', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/unoxuno/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 14, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/unoxuno/verdebiche.png', 'Verde Biche', '#66FF00', 'Marco Verde Biche', 14, 0);

-- Dos por Uno

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/dosxuno/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/dosxuno/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/dosxuno/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/dosxuno/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/dosxuno/cafe.png', 'Café', '#663300', 'Marco Café', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco fucsia', '/img/marcos/dosxuno/fucsia.png', 'Fucsia', '#FF00CC', 'Marco Fucsia', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/dosxuno/gris.png', 'Gris', '#999999', 'Marco Gris', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/dosxuno/morado.png', 'Morado', '#6600CC', 'Marco Morado', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/dosxuno/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/dosxuno/negro.png', 'Negro', '#000000', 'Marco Negro', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/dosxuno/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/dosxuno/verdebiche.png', 'Verde Biche', '#66FF00', 'Marco Verde Biche', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/dosxuno/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 15, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/dosxuno/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 15, 0);

-- Tres por Uno

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/tresxuno/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/tresxuno/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/tresxuno/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/tresxuno/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/tresxuno/cafe.png', 'Café', '#663300', 'Marco Café', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco fucsia', '/img/marcos/tresxuno/fucsia.png', 'Fucsia', '#FF00CC', 'Marco Fucsia', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/tresxuno/gris.png', 'Gris', '#999999', 'Marco Gris', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/tresxuno/morado.png', 'Morado', '#6600CC', 'Marco Morado', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/tresxuno/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/tresxuno/negro.png', 'Negro', '#000000', 'Marco Negro', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/tresxuno/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/tresxuno/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 16, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/tresxuno/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 16, 0);

-- Dos por Dos

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/dosxdos/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/dosxdos/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/dosxdos/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/dosxdos/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/dosxdos/cafe.png', 'Café', '#663300', 'Marco Café', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/dosxdos/gris.png', 'Gris', '#999999', 'Marco Gris', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/dosxdos/morado.png', 'Morado', '#6600CC', 'Marco Morado', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/dosxdos/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/dosxdos/negro.png', 'Negro', '#000000', 'Marco Negro', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/dosxdos/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/dosxdos/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/dosxdos/verdebiche.png', 'Verde Biche', '#006633', 'Marco Verde Biche', 17, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/dosxdos/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 17, 0);

-- Tres por Dos

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/tresxdos/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/tresxdos/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/tresxdos/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/tresxdos/cafe.png', 'Café', '#663300', 'Marco Café', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco fucsia', '/img/marcos/tresxdos/fucsia.png', 'Fucsia', '#FF00CC', 'Marco Fucsia', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/tresxdos/gris.png', 'Gris', '#999999', 'Marco Gris', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/tresxdos/morado.png', 'Morado', '#6600CC', 'Marco Morado', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/tresxdos/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/tresxdos/negro.png', 'Negro', '#000000', 'Marco Negro', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/tresxdos/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/tresxdos/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 18, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/tresxdos/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 18, 0);

-- Tres por Tres

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/tresxtres/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/tresxtres/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/tresxtres/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/tresxtres/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/tresxtres/cafe.png', 'Café', '#663300', 'Marco Café', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/tresxtres/gris.png', 'Gris', '#999999', 'Marco Gris', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/tresxtres/morado.png', 'Morado', '#6600CC', 'Marco Morado', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/tresxtres/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/tresxtres/negro.png', 'Negro', '#000000', 'Marco Negro', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/tresxtres/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/tresxtres/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/tresxtres/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 19, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/tresxtres/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 19, 0);

-- Cuatro por Tres

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/cuatroxtres/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/cuatroxtres/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/cuatroxtres/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/cuatroxtres/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/cuatroxtres/cafe.png', 'Café', '#663300', 'Marco Café', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/cuatroxtres/gris.png', 'Gris', '#999999', 'Marco Gris', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/cuatroxtres/morado.png', 'Morado', '#6600CC', 'Marco Morado', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/cuatroxtres/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/cuatroxtres/negro.png', 'Negro', '#000000', 'Marco Negro', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/cuatroxtres/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/cuatroxtres/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/cuatroxtres/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/cuatroxtres/rojo.png', 'Rojo', '#FF0000', 'Marco Rojo', 20, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/cuatroxtres/blanco.png', 'Blanco', '#FFFFFF', 'Marco Verde Oscuro', 20, 0);

-- Cuatro por Cinco

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/cincoxcuatro/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/cincoxcuatro/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/cincoxcuatro/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/cincoxcuatro/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/cincoxcuatro/cafe.png', 'Café', '#663300', 'Marco Café', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/cincoxcuatro/gris.png', 'Gris', '#999999', 'Marco Gris', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/cincoxcuatro/morado.png', 'Morado', '#6600CC', 'Marco Morado', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/cincoxcuatro/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/cincoxcuatro/negro.png', 'Negro', '#000000', 'Marco Negro', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/cincoxcuatro/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/cincoxcuatro/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/cincoxcuatro/verdebiche.png', 'Verde Biche', '#66FF00', 'Marco Verde Biche', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/cincoxcuatro/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 21, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/cincoxcuatro/blanco.png', 'Blanco', '#FFFFFF', 'Marco Blanco', 21, 0);

-- Cuatro por sies

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/cuatroxseis/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/cuatroxseis/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/cuatroxseis/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/cuatroxseis/cafe.png', 'Café', '#663300', 'Marco Café', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/cuatroxseis/gris.png', 'Gris', '#999999', 'Marco Gris', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/cuatroxseis/morado.png', 'Morado', '#6600CC', 'Marco Morado', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/cuatroxseis/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/cuatroxseis/negro.png', 'Negro', '#000000', 'Marco Negro', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/cuatroxseis/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/cuatroxseis/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/cincoxcuatro/verdebiche.png', 'Verde Biche', '#66FF00', 'Marco Verde Biche', 22, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/cuatroxseis/blanco.png', 'Blanco', '#FFFFFF', 'Marco Blanco', 22, 0);

-- Cuatro por Ocho

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Amarillo', '/img/marcos/cuatroxocho/amarillo.png', 'Amarillo', '#FFFF00', 'Marco Amarillo', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Aguamarina', '/img/marcos/cuatroxocho/aguamarina.png', 'Aguamarina', '#00FFCC', 'Marco Aguamarina', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Oscuro', '/img/marcos/cuatroxocho/azuloscuro.png', 'Azul Oscuro', '#0033CC', 'Marco Azul Oscuro', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Azul Claro', '/img/marcos/cuatroxocho/azulclaro.png', 'Azul Claro', '#33CCFF', 'Marco Azul Claro', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Café', '/img/marcos/cuatroxocho/cafe.png', 'Café', '#663300', 'Marco Café', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Gris', '/img/marcos/cuatroxocho/gris.png', 'Gris', '#999999', 'Marco Gris', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Morado', '/img/marcos/cuatroxocho/morado.png', 'Morado', '#6600CC', 'Marco Morado', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Naranja', '/img/marcos/cuatroxocho/naranja.png', 'Naranja', '#FF9900', 'Marco Naranja', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Negro', '/img/marcos/cuatroxocho/negro.png', 'Negro', '#000000', 'Marco Negro', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rojo', '/img/marcos/cuatroxocho/rojo.png', 'Rojo', '#FFCCFF', 'Marco Rojo', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Oscuro', '/img/marcos/cuatroxocho/verdeoscuro.png', 'Verde Oscuro', '#006633', 'Marco Verde Oscuro', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Verde Biche', '/img/marcos/cuatroxocho/verdebiche.png', 'Verde Biche', '#66FF00', 'Marco Verde Biche', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Rosado', '/img/marcos/cuatroxocho/rosado.png', 'Rosado', '#FFCCFF', 'Marco Rosado', 23, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Marco Blanco', '/img/marcos/cuatroxocho/blanco.png', 'Blanco', '#FFFFFF', 'Marco Blanco', 23, 0);

-- Caballetes 1

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Amarillo', '/img/caballetes/amarilloCaballete.png', 'Amarillo', '#FFFF00', 'Caballete Amarillo', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Aguamarina', '/img/caballetes/aguamarinaCaballete.png', 'Aguamarina', '#00FFCC', 'Caballete Aguamarina', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Oscuro', '/img/caballetes/azuloscuroCaballete.png', 'Azul Oscuro', '#0033CC', 'Caballete Azul Oscuro', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Claro', '/img/caballetes/azulclaroCaballete.png', 'Azul Claro', '#33CCFF', 'Caballete Azul Claro', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Café', '/img/caballetes/cafeCaballete.png', 'Café', '#663300', 'Caballete Café', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Gris', '/img/caballetes/grisCaballete.png', 'Gris', '#999999', 'Caballete Gris', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Morado', '/img/caballetes/moradoCaballete.png', 'Morado', '#6600CC', 'Caballete Morado', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Naranja', '/img/caballetes/naranjaCaballete.png', 'Naranja', '#FF9900', 'Caballete Naranja', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Negro', '/img/caballetes/negroCaballete.png', 'Negro', '#000000', 'Caballete Negro', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rojo', '/img/caballetes/rojoCaballete.png', 'Rojo', '#FFCCFF', 'Caballete Rojo', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Oscuro', '/img/caballetes/verdeoscuroCaballete.png', 'Verde Oscuro', '#006633', 'Caballete Verde Oscuro', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Biche', '/img/caballetes/verdebicheCaballete.png', 'Verde Biche', '#66FF00', 'Caballete Verde Biche', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rosado', '/img/caballetes/rosadoCaballete.png', 'Rosado', '#FFCCFF', 'Caballete Rosado', 40, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Blanco', '/img/caballetes/blancoCaballete.png', 'Blanco', '#FFFFFF', 'Caballete Blanco', 40, 0);

-- Caballetes 2

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Amarillo', '/img/caballetes/amarilloCaballete.png', 'Amarillo', '#FFFF00', 'Caballete Amarillo', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Aguamarina', '/img/caballetes/aguamarinaCaballete.png', 'Aguamarina', '#00FFCC', 'Caballete Aguamarina', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Oscuro', '/img/caballetes/azuloscuroCaballete.png', 'Azul Oscuro', '#0033CC', 'Caballete Azul Oscuro', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Claro', '/img/caballetes/azulclaroCaballete.png', 'Azul Claro', '#33CCFF', 'Caballete Azul Claro', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Café', '/img/caballetes/cafeCaballete.png', 'Café', '#663300', 'Caballete Café', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Gris', '/img/caballetes/grisCaballete.png', 'Gris', '#999999', 'Caballete Gris', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Morado', '/img/caballetes/moradoCaballete.png', 'Morado', '#6600CC', 'Caballete Morado', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Naranja', '/img/caballetes/naranjaCaballete.png', 'Naranja', '#FF9900', 'Caballete Naranja', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Negro', '/img/caballetes/negroCaballete.png', 'Negro', '#000000', 'Caballete Negro', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rojo', '/img/caballetes/rojoCaballete.png', 'Rojo', '#FFCCFF', 'Caballete Rojo', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Oscuro', '/img/caballetes/verdeoscuroCaballete.png', 'Verde Oscuro', '#006633', 'Caballete Verde Oscuro', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Biche', '/img/caballetes/verdebicheCaballete.png', 'Verde Biche', '#66FF00', 'Caballete Verde Biche', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rosado', '/img/caballetes/rosadoCaballete.png', 'Rosado', '#FFCCFF', 'Caballete Rosado', 41, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Blanco', '/img/caballetes/blancoCaballete.png', 'Blanco', '#FFFFFF', 'Caballete Blanco', 41, 0);

-- Caballetes 3

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Amarillo', '/img/caballetes/amarilloCaballete.png', 'Amarillo', '#FFFF00', 'Caballete Amarillo', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Aguamarina', '/img/caballetes/aguamarinaCaballete.png', 'Aguamarina', '#00FFCC', 'Caballete Aguamarina', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Oscuro', '/img/caballetes/azuloscuroCaballete.png', 'Azul Oscuro', '#0033CC', 'Caballete Azul Oscuro', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Claro', '/img/caballetes/azulclaroCaballete.png', 'Azul Claro', '#33CCFF', 'Caballete Azul Claro', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Café', '/img/caballetes/cafeCaballete.png', 'Café', '#663300', 'Caballete Café', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Gris', '/img/caballetes/grisCaballete.png', 'Gris', '#999999', 'Caballete Gris', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Morado', '/img/caballetes/moradoCaballete.png', 'Morado', '#6600CC', 'Caballete Morado', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Naranja', '/img/caballetes/naranjaCaballete.png', 'Naranja', '#FF9900', 'Caballete Naranja', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Negro', '/img/caballetes/negroCaballete.png', 'Negro', '#000000', 'Caballete Negro', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rojo', '/img/caballetes/rojoCaballete.png', 'Rojo', '#FFCCFF', 'Caballete Rojo', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Oscuro', '/img/caballetes/verdeoscuroCaballete.png', 'Verde Oscuro', '#006633', 'Caballete Verde Oscuro', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Biche', '/img/caballetes/verdebicheCaballete.png', 'Verde Biche', '#66FF00', 'Caballete Verde Biche', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rosado', '/img/caballetes/rosadoCaballete.png', 'Rosado', '#FFCCFF', 'Caballete Rosado', 42, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Blanco', '/img/caballetes/blancoCaballete.png', 'Blanco', '#FFFFFF', 'Caballete Blanco', 42, 0);

-- Caballetes 4

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Amarillo', '/img/caballetes/amarilloCaballete.png', 'Amarillo', '#FFFF00', 'Caballete Amarillo', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Aguamarina', '/img/caballetes/aguamarinaCaballete.png', 'Aguamarina', '#00FFCC', 'Caballete Aguamarina', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Oscuro', '/img/caballetes/azuloscuroCaballete.png', 'Azul Oscuro', '#0033CC', 'Caballete Azul Oscuro', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Azul Claro', '/img/caballetes/azulclaroCaballete.png', 'Azul Claro', '#33CCFF', 'Caballete Azul Claro', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Café', '/img/caballetes/cafeCaballete.png', 'Café', '#663300', 'Caballete Café', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Gris', '/img/caballetes/grisCaballete.png', 'Gris', '#999999', 'Caballete Gris', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Morado', '/img/caballetes/moradoCaballete.png', 'Morado', '#6600CC', 'Caballete Morado', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Naranja', '/img/caballetes/naranjaCaballete.png', 'Naranja', '#FF9900', 'Caballete Naranja', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Negro', '/img/caballetes/negroCaballete.png', 'Negro', '#000000', 'Caballete Negro', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rojo', '/img/caballetes/rojoCaballete.png', 'Rojo', '#FFCCFF', 'Caballete Rojo', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Oscuro', '/img/caballetes/verdeoscuroCaballete.png', 'Verde Oscuro', '#006633', 'Caballete Verde Oscuro', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Verde Biche', '/img/caballetes/verdebicheCaballete.png', 'Verde Biche', '#66FF00', 'Caballete Verde Biche', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Rosado', '/img/caballetes/rosadoCaballete.png', 'Rosado', '#FFCCFF', 'Caballete Rosado', 43, 0);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Caballete Blanco', '/img/caballetes/blancoCaballete.png', 'Blanco', '#FFFFFF', 'Caballete Blanco', 43, 0);


-- Cuerda 1 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Amarillo', '', 'Amarillo', '#FFFF00', 'Cuerda Amarillo', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Aguamarina', '', 'Aguamarina', '#00FFCC', 'Cuerda Aguamarina', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Cuerda Azul Oscuro', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Claro', '', 'Azul Claro', '#33CCFF', 'Cuerda Azul Claro', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Café', '', 'Café', '#663300', 'Cuerda Café', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Gris', '', 'Gris', '#999999', 'Cuerda Gris', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Morado', '', 'Morado', '#6600CC', 'Cuerda Morado', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Naranja', '', 'Naranja', '#FF9900', 'Cuerda Naranja', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Negro', '', 'Negro', '#000000', 'Cuerda Negro', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rojo', '', 'Rojo', '#FFCCFF', 'Cuerda Rojo', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Cuerda Verde Oscuro', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Biche', '', 'Verde Biche', '#66FF00', 'Cuerda Verde Biche', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rosado', '', 'Rosado', '#FFCCFF', 'Cuerda Rosado', 24, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Blanco', '', 'Blanco', '#FFFFFF', 'Cuerda Blanco', 24, 1);

-- Cuerda 1.5 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Amarillo', '', 'Amarillo', '#FFFF00', 'Cuerda Amarillo', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Aguamarina', '', 'Aguamarina', '#00FFCC', 'Cuerda Aguamarina', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Cuerda Azul Oscuro', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Claro', '', 'Azul Claro', '#33CCFF', 'Cuerda Azul Claro', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Café', '', 'Café', '#663300', 'Cuerda Café', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Gris', '', 'Gris', '#999999', 'Cuerda Gris', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Morado', '', 'Morado', '#6600CC', 'Cuerda Morado', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Naranja', '', 'Naranja', '#FF9900', 'Cuerda Naranja', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Negro', '', 'Negro', '#000000', 'Cuerda Negro', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rojo', '', 'Rojo', '#FFCCFF', 'Cuerda Rojo', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Cuerda Verde Oscuro', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Biche', '', 'Verde Biche', '#66FF00', 'Cuerda Verde Biche', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rosado', '', 'Rosado', '#FFCCFF', 'Cuerda Rosado', 25, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Blanco', '', 'Blanco', '#FFFFFF', 'Cuerda Blanco', 25, 1);

-- Cuerda 1.7 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Amarillo', '', 'Amarillo', '#FFFF00', 'Cuerda Amarillo', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Aguamarina', '', 'Aguamarina', '#00FFCC', 'Cuerda Aguamarina', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Cuerda Azul Oscuro', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Claro', '', 'Azul Claro', '#33CCFF', 'Cuerda Azul Claro', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Café', '', 'Café', '#663300', 'Cuerda Café', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Gris', '', 'Gris', '#999999', 'Cuerda Gris', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Morado', '', 'Morado', '#6600CC', 'Cuerda Morado', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Naranja', '', 'Naranja', '#FF9900', 'Cuerda Naranja', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Negro', '', 'Negro', '#000000', 'Cuerda Negro', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rojo', '', 'Rojo', '#FFCCFF', 'Cuerda Rojo', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Cuerda Verde Oscuro', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Biche', '', 'Verde Biche', '#66FF00', 'Cuerda Verde Biche', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rosado', '', 'Rosado', '#FFCCFF', 'Cuerda Rosado', 26, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Blanco', '', 'Blanco', '#FFFFFF', 'Cuerda Blanco', 26, 1);

-- Cuerda 3 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Amarillo', '', 'Amarillo', '#FFFF00', 'Cuerda Amarillo', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Aguamarina', '', 'Aguamarina', '#00FFCC', 'Cuerda Aguamarina', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Cuerda Azul Oscuro', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Claro', '', 'Azul Claro', '#33CCFF', 'Cuerda Azul Claro', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Café', '', 'Café', '#663300', 'Cuerda Café', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Gris', '', 'Gris', '#999999', 'Cuerda Gris', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Morado', '', 'Morado', '#6600CC', 'Cuerda Morado', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Naranja', '', 'Naranja', '#FF9900', 'Cuerda Naranja', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Negro', '', 'Negro', '#000000', 'Cuerda Negro', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rojo', '', 'Rojo', '#FFCCFF', 'Cuerda Rojo', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Cuerda Verde Oscuro', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Biche', '', 'Verde Biche', '#66FF00', 'Cuerda Verde Biche', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rosado', '', 'Rosado', '#FFCCFF', 'Cuerda Rosado', 27, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Blanco', '', 'Blanco', '#FFFFFF', 'Cuerda Blanco', 27, 1);

-- Cuerda 3.4 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Amarillo', '', 'Amarillo', '#FFFF00', 'Cuerda Amarillo', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Aguamarina', '', 'Aguamarina', '#00FFCC', 'Cuerda Aguamarina', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Cuerda Azul Oscuro', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Azul Claro', '', 'Azul Claro', '#33CCFF', 'Cuerda Azul Claro', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Café', '', 'Café', '#663300', 'Cuerda Café', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Gris', '', 'Gris', '#999999', 'Cuerda Gris', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Morado', '', 'Morado', '#6600CC', 'Cuerda Morado', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Naranja', '', 'Naranja', '#FF9900', 'Cuerda Naranja', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Negro', '', 'Negro', '#000000', 'Cuerda Negro', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rojo', '', 'Rojo', '#FFCCFF', 'Cuerda Rojo', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Cuerda Verde Oscuro', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Verde Biche', '', 'Verde Biche', '#66FF00', 'Cuerda Verde Biche', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Rosado', '', 'Rosado', '#FFCCFF', 'Cuerda Rosado', 28, 1);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Cuerda Blanco', '', 'Blanco', '#FFFFFF', 'Cuerda Blanco', 28, 1);

-- Pinza 2 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Amarillo', '', 'Amarillo', '#FFFF00', 'Pinza Amarillo', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Aguamarina', '', 'Aguamarina', '#00FFCC', 'Pinza Aguamarina', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Pinza Azul Oscuro', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Claro', '', 'Azul Claro', '#33CCFF', 'Pinza Azul Claro', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Café', '', 'Café', '#663300', 'Pinza Café', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Gris', '', 'Gris', '#999999', 'Pinza Gris', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Morado', '', 'Morado', '#6600CC', 'Pinza Morado', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Naranja', '', 'Naranja', '#FF9900', 'Pinza Naranja', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Negro', '', 'Negro', '#000000', 'Pinza Negro', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rojo', '', 'Rojo', '#FFCCFF', 'Pinza Rojo', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Pinza Verde Oscuro', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Biche', '', 'Verde Biche', '#66FF00', 'Pinza Verde Biche', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rosado', '', 'Rosado', '#FFCCFF', 'Pinza Rosado', 24, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Blanco', '', 'Blanco', '#FFFFFF', 'Pinza Blanco', 24, 2);

-- Pinza 2.5 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Amarillo', '', 'Amarillo', '#FFFF00', 'Pinza Amarillo', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Aguamarina', '', 'Aguamarina', '#00FFCC', 'Pinza Aguamarina', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Pinza Azul Oscuro', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Claro', '', 'Azul Claro', '#33CCFF', 'Pinza Azul Claro', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Café', '', 'Café', '#663300', 'Pinza Café', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Gris', '', 'Gris', '#999999', 'Pinza Gris', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Morado', '', 'Morado', '#6600CC', 'Pinza Morado', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Naranja', '', 'Naranja', '#FF9900', 'Pinza Naranja', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Negro', '', 'Negro', '#000000', 'Pinza Negro', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rojo', '', 'Rojo', '#FFCCFF', 'Pinza Rojo', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Pinza Verde Oscuro', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Biche', '', 'Verde Biche', '#66FF00', 'Pinza Verde Biche', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rosado', '', 'Rosado', '#FFCCFF', 'Pinza Rosado', 25, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Blanco', '', 'Blanco', '#FFFFFF', 'Pinza Blanco', 25, 2);

-- Pinza 2.7 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Amarillo', '', 'Amarillo', '#FFFF00', 'Pinza Amarillo', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Aguamarina', '', 'Aguamarina', '#00FFCC', 'Pinza Aguamarina', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Pinza Azul Oscuro', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Claro', '', 'Azul Claro', '#33CCFF', 'Pinza Azul Claro', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Café', '', 'Café', '#663300', 'Pinza Café', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Gris', '', 'Gris', '#999999', 'Pinza Gris', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Morado', '', 'Morado', '#6600CC', 'Pinza Morado', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Naranja', '', 'Naranja', '#FF9900', 'Pinza Naranja', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Negro', '', 'Negro', '#000000', 'Pinza Negro', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rojo', '', 'Rojo', '#FFCCFF', 'Pinza Rojo', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Pinza Verde Oscuro', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Biche', '', 'Verde Biche', '#66FF00', 'Pinza Verde Biche', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rosado', '', 'Rosado', '#FFCCFF', 'Pinza Rosado', 26, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Blanco', '', 'Blanco', '#FFFFFF', 'Pinza Blanco', 26, 2);

-- Pinza 3 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Amarillo', '', 'Amarillo', '#FFFF00', 'Pinza Amarillo', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Aguamarina', '', 'Aguamarina', '#00FFCC', 'Pinza Aguamarina', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Pinza Azul Oscuro', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Claro', '', 'Azul Claro', '#33CCFF', 'Pinza Azul Claro', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Café', '', 'Café', '#663300', 'Pinza Café', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Gris', '', 'Gris', '#999999', 'Pinza Gris', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Morado', '', 'Morado', '#6600CC', 'Pinza Morado', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Naranja', '', 'Naranja', '#FF9900', 'Pinza Naranja', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Negro', '', 'Negro', '#000000', 'Pinza Negro', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rojo', '', 'Rojo', '#FFCCFF', 'Pinza Rojo', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Pinza Verde Oscuro', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Biche', '', 'Verde Biche', '#66FF00', 'Pinza Verde Biche', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rosado', '', 'Rosado', '#FFCCFF', 'Pinza Rosado', 27, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Blanco', '', 'Blanco', '#FFFFFF', 'Pinza Blanco', 27, 2);

-- Pinza 3.4 m

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Amarillo', '', 'Amarillo', '#FFFF00', 'Pinza Amarillo', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Aguamarina', '', 'Aguamarina', '#00FFCC', 'Pinza Aguamarina', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Oscuro', '', 'Azul Oscuro', '#0033CC', 'Pinza Azul Oscuro', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Azul Claro', '', 'Azul Claro', '#33CCFF', 'Pinza Azul Claro', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Café', '', 'Café', '#663300', 'Pinza Café', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Gris', '', 'Gris', '#999999', 'Pinza Gris', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Morado', '', 'Morado', '#6600CC', 'Pinza Morado', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Naranja', '', 'Naranja', '#FF9900', 'Pinza Naranja', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Negro', '', 'Negro', '#000000', 'Pinza Negro', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rojo', '', 'Rojo', '#FFCCFF', 'Pinza Rojo', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Oscuro', '', 'Verde Oscuro', '#006633', 'Pinza Verde Oscuro', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Verde Biche', '', 'Verde Biche', '#66FF00', 'Pinza Verde Biche', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Rosado', '', 'Rosado', '#FFCCFF', 'Pinza Rosado', 28, 2);

INSERT INTO public.accesorioproducto(
 nombre, imagen, color, color_code, detalle, id_producto, tipo)
VALUES ('Pinza Blanco', '', 'Blanco', '#FFFFFF', 'Pinza Blanco', 28, 2);

---PAQUETES DE FOTOS ---
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,1);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,1);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,1);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,1);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,2);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,2);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,2);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,2);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,3);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,3);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,3);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,3);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,4);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,4);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,4);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,4);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,5);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,5);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,5);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,5);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,6);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,6);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,6);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,6);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,7);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,7);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,7);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,7);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,8);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,8);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,8);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,8);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,9);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,9);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,9);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,9);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,10);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,10);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,10);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,10);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,11);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,11);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,11);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,11);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,12);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,12);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,12);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,12);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,13);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,13);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,13);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,13);

--- MARCOS ---

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,14);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,14);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',12000,14);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,14);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,15);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,15);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',12000,15);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,15);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,16);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,16);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',12000,16);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,16);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,17);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,17);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',12000,17);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,17);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',10000,18);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',15000,18);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',18000,18);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',18000,18);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',10000,19);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',15000,19);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',18000,19);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',18000,19);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',12000,20);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',30000,20);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',40000,20);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',23000,20);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',12000,21);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',30000,21);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',40000,21);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',23000,21);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',15000,22);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',32000,22);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',42000,22);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',25000,22);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',15000,23);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',32000,23);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',42000,23);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',25000,23);

--- CUERDA ---

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,24);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,24);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,24);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,24);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,25);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,25);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,25);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,25);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,26);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,26);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,26);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,26);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,27);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,27);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,27);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,27);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,28);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,28);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,28);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,28);

--- ALBUMES ---

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,29);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,29);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,29);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,29);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,30);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,30);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,30);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,30);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,31);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,31);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,31);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,31);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,32);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,32);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,32);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,32);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,33);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,33);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,33);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,33);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,34);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,34);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,34);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,34);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,35);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,35);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,35);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,35);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,36);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,36);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,36);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,36);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,37);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,37);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,37);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,37);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,38);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,38);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,38);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,38);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,39);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,39);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,39);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,39);

--- CABALLETE ---

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,40);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,40);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,40);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,40);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,41);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,41);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,41);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,41);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,42);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,42);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,42);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,42);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',8000,43);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',10000,43);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',10000,43);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',10000,43);

--- POSTALES ---

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,45);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,45);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,45);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,45);

INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('bogota',6000,46);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('costa',9000,46);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('valledupar/Monteria',9000,46);
INSERT INTO public.costoenvio(
zona, precio, id_producto)
VALUES ('otra',9000,46);