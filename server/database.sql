CREATE TABLE IF NOT EXISTS tipoproducto (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  slug TEXT NULL,
  imagen TEXT NOT NULL,
  descripcion TEXT NOT NULL,
  disclaimer TEXT NULL
);

CREATE TABLE IF NOT EXISTS empaque (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  precio INTEGER NOT NULL,
  color TEXT NULL,
  color_code TEXT NULL,
  descripcion TEXT NOT NULL,
  imagen TEXT NULL,
  habilitado BOOLEAN NULL,
  tipo_empaque INTEGER NULL,
  id_tipoproducto INTEGER REFERENCES tipoproducto(id)
);

CREATE TABLE IF NOT EXISTS producto (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  descripcion TEXT NOT NULL,
  imagen TEXT NULL,
  numfotos INTEGER NOT NULL,
  precio INTEGER NOT NULL,
  habilitado BOOLEAN NULL,
  id_tipoproducto INTEGER NOT NULL REFERENCES tipoproducto(id)
);

CREATE TABLE IF NOT EXISTS accesorioproducto (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  imagen TEXT NULL,
  color TEXT NOT NULL,
  color_code TEXT NOT NULL,
  detalle TEXT NOT NULL,
  tipo INTEGER NULL,
  id_producto INTEGER NOT NULL REFERENCES producto(id)
);

CREATE TABLE IF NOT EXISTS costoenvio (
  id SERIAL NOT NULL PRIMARY KEY,
  zona TEXT NOT NULL,
  precio INTEGER NOT NULL,
  id_producto INTEGER NOT NULL REFERENCES producto(id)
);

CREATE TABLE IF NOT EXISTS pedido (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  cedula INTEGER NOT NULL,
  direccion TEXT NOT NULL,
  direccionenvio TEXT NOT NULL,
  telefono INTEGER NOT NULL,
  politicas BOOLEAN NOT NULL,
  usuarioinstagram TEXT NULL,
  pago TEXT NOT NULL,
  estado TEXT NOT NULL,
  fecharecepcion TIMESTAMP NOT NULL,
  precio INTEGER NOT NULL,
  iva INTEGER NOT NULL,
  total INTEGER NOT NULL,
  costoenvio INTEGER NOT NULL,
  id_user TEXT NOT NULL,
  referencecode TEXT NOT NULL,
  envioexpress BOOLEAN NOT NULL,
  bday TEXT
);

CREATE TABLE IF NOT EXISTS productopedido (
  id SERIAL NOT NULL PRIMARY KEY,
  id_producto INTEGER NOT NULL REFERENCES producto(id),
  id_empaque INTEGER NOT NULL REFERENCES empaque(id),
  id_pedido INTEGER NOT NULL REFERENCES pedido(id)
);

CREATE TABLE IF NOT EXISTS accesorioproductopedido (
  id SERIAL NOT NULL PRIMARY KEY,
  id_productopedido INTEGER NOT NULL REFERENCES productopedido(id),
  id_accesorioproducto INTEGER NOT NULL REFERENCES accesorioproducto(id)
);

CREATE TABLE IF NOT EXISTS foto(
  id SERIAL NOT NULL PRIMARY KEY,
  imagen TEXT NOT NULL,
  texto TEXT NULL,
  color TEXT NULL,
  formato BOOLEAN,
  id_productopedido INTEGER REFERENCES productopedido(id)
);

CREATE TABLE IF NOT EXISTS politica (
  id SERIAL NOT NULL PRIMARY KEY,
  nombre TEXT NOT NULL,
  descripcion TEXT NOT NULL,
  obligatorio BOOLEAN
);
