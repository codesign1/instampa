'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var instagram = require('./boot/instagram');
var mailing = require('./boot/mailing');
var zip = require('./boot/zip');

var app = module.exports = loopback();

// Instagram

app.get('/api/instagram/step1', instagram.step1Login);
app.get('/api/instagram/step2', instagram.step2Login);
app.get('/api/instagram/getselfmedia', instagram.getMedia);

// Mailing

app.get('/api/mailing/nuevoPedido', mailing.nuevoPedido);
app.get('/api/mailing/pedidoRecibido', mailing.pedidoRecibido);
app.get('/api/mailing/mensajeContacto', mailing.mensajeContacto);
app.get('/api/mailing/pedidoEnviado', mailing.pedidoEnviado);

// Zip

app.post('/api/zip/createZip', zip.zipPhoto);

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
