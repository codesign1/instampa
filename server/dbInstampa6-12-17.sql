--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accesorioproducto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accesorioproducto (
    id integer NOT NULL,
    nombre text NOT NULL,
    imagen text,
    color text NOT NULL,
    color_code text NOT NULL,
    detalle text NOT NULL,
    id_producto integer NOT NULL,
    tipo integer DEFAULT 0
);


ALTER TABLE accesorioproducto OWNER TO postgres;

--
-- Name: accesorioproducto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE accesorioproducto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE accesorioproducto_id_seq OWNER TO postgres;

--
-- Name: accesorioproducto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE accesorioproducto_id_seq OWNED BY accesorioproducto.id;


--
-- Name: accesorioproductopedido; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE accesorioproductopedido (
    id integer NOT NULL,
    id_productopedido integer NOT NULL,
    id_accesorioproducto integer NOT NULL
);


ALTER TABLE accesorioproductopedido OWNER TO postgres;

--
-- Name: accesorioproductopedido_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE accesorioproductopedido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE accesorioproductopedido_id_seq OWNER TO postgres;

--
-- Name: accesorioproductopedido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE accesorioproductopedido_id_seq OWNED BY accesorioproductopedido.id;


--
-- Name: costoenvio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE costoenvio (
    id integer NOT NULL,
    zona text NOT NULL,
    precio integer NOT NULL,
    id_producto integer NOT NULL
);


ALTER TABLE costoenvio OWNER TO postgres;

--
-- Name: costoenvio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE costoenvio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE costoenvio_id_seq OWNER TO postgres;

--
-- Name: costoenvio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE costoenvio_id_seq OWNED BY costoenvio.id;


--
-- Name: empaque; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empaque (
    id integer NOT NULL,
    nombre text NOT NULL,
    precio integer NOT NULL,
    color text,
    color_code text,
    descripcion text NOT NULL,
    imagen text,
    tipo_empaque integer,
    id_tipoproducto integer,
    habilitado boolean
);


ALTER TABLE empaque OWNER TO postgres;

--
-- Name: empaque_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empaque_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE empaque_id_seq OWNER TO postgres;

--
-- Name: empaque_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empaque_id_seq OWNED BY empaque.id;


--
-- Name: foto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE foto (
    id integer NOT NULL,
    imagen text NOT NULL,
    texto text,
    color text,
    formato boolean,
    id_productopedido integer
);


ALTER TABLE foto OWNER TO postgres;

--
-- Name: foto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE foto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE foto_id_seq OWNER TO postgres;

--
-- Name: foto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE foto_id_seq OWNED BY foto.id;


--
-- Name: pedido; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pedido (
    id integer NOT NULL,
    nombre text NOT NULL,
    cedula numeric(12,0) NOT NULL,
    direccion text NOT NULL,
    direccionenvio text NOT NULL,
    telefono numeric(12,0) NOT NULL,
    politicas boolean NOT NULL,
    pago text NOT NULL,
    estado text NOT NULL,
    fecharecepcion timestamp without time zone NOT NULL,
    precio integer NOT NULL,
    iva integer NOT NULL,
    total integer NOT NULL,
    id_user text NOT NULL,
    usuarioinstagram text,
    costoenvio integer NOT NULL,
    referencecode text NOT NULL,
    envioexpress boolean NOT NULL,
    bday text,
    ciudad text
);


ALTER TABLE pedido OWNER TO postgres;

--
-- Name: pedido_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pedido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pedido_id_seq OWNER TO postgres;

--
-- Name: pedido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pedido_id_seq OWNED BY pedido.id;


--
-- Name: politica; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE politica (
    id integer NOT NULL,
    nombre text NOT NULL,
    descripcion text NOT NULL,
    obligatorio boolean
);


ALTER TABLE politica OWNER TO postgres;

--
-- Name: politica_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE politica_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE politica_id_seq OWNER TO postgres;

--
-- Name: politica_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE politica_id_seq OWNED BY politica.id;


--
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE producto (
    id integer NOT NULL,
    nombre text NOT NULL,
    descripcion text NOT NULL,
    imagen text,
    numfotos integer NOT NULL,
    precio integer NOT NULL,
    id_tipoproducto integer NOT NULL,
    habilitado boolean
);


ALTER TABLE producto OWNER TO postgres;

--
-- Name: producto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE producto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE producto_id_seq OWNER TO postgres;

--
-- Name: producto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE producto_id_seq OWNED BY producto.id;


--
-- Name: productopedido; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productopedido (
    id integer NOT NULL,
    id_producto integer NOT NULL,
    id_empaque integer,
    id_pedido integer NOT NULL
);


ALTER TABLE productopedido OWNER TO postgres;

--
-- Name: productopedido_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productopedido_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE productopedido_id_seq OWNER TO postgres;

--
-- Name: productopedido_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productopedido_id_seq OWNED BY productopedido.id;


--
-- Name: tipoproducto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoproducto (
    id integer NOT NULL,
    nombre text NOT NULL,
    slug text,
    imagen text NOT NULL,
    descripcion text NOT NULL,
    disclaimer text
);


ALTER TABLE tipoproducto OWNER TO postgres;

--
-- Name: tipoproducto_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoproducto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipoproducto_id_seq OWNER TO postgres;

--
-- Name: tipoproducto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoproducto_id_seq OWNED BY tipoproducto.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorioproducto ALTER COLUMN id SET DEFAULT nextval('accesorioproducto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorioproductopedido ALTER COLUMN id SET DEFAULT nextval('accesorioproductopedido_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY costoenvio ALTER COLUMN id SET DEFAULT nextval('costoenvio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empaque ALTER COLUMN id SET DEFAULT nextval('empaque_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY foto ALTER COLUMN id SET DEFAULT nextval('foto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedido ALTER COLUMN id SET DEFAULT nextval('pedido_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY politica ALTER COLUMN id SET DEFAULT nextval('politica_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto ALTER COLUMN id SET DEFAULT nextval('producto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productopedido ALTER COLUMN id SET DEFAULT nextval('productopedido_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoproducto ALTER COLUMN id SET DEFAULT nextval('tipoproducto_id_seq'::regclass);


--
-- Data for Name: accesorioproducto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accesorioproducto (id, nombre, imagen, color, color_code, detalle, id_producto, tipo) FROM stdin;
85	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	14	3
87	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	14	3
89	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/cafe.png	Café	#663300	Marco Café	14	3
90	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/fucsia.png	Fucsia	#FF00CC	Marco Fucsia	14	3
91	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/gris.png	Gris	#999999	Marco Gris	14	3
92	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/morado.png	Morado	#6600CC	Marco Morado	14	3
93	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/naranja.png	Naranja	#FF9900	Marco Naranja	14	3
94	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/negro.png	Negro	#000000	Marco Negro	14	3
96	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	14	3
97	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	15	3
98	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	15	3
100	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	15	3
102	Marco fucsia	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/fucsia.png	Fucsia	#FF00CC	Marco Fucsia	15	3
103	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/gris.png	Gris	#999999	Marco Gris	15	3
104	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/morado.png	Morado	#6600CC	Marco Morado	15	3
105	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/naranja.png	Naranja	#FF9900	Marco Naranja	15	3
106	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/negro.png	Negro	#000000	Marco Negro	15	3
107	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/rosado.png	Rosado	#FFCCFF	Marco Rosado	15	3
108	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	15	3
110	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/blanco.png	Blanco	#FFFFFF	Marco Blanco	15	3
145	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/negro.png	Negro	#000000	Marco Negro	18	3
111	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	16	3
113	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	16	3
117	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/gris.png	Gris	#999999	Marco Gris	16	3
121	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/rosado.png	Rosado	#FFCCFF	Marco Rosado	16	3
118	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/morado.png	Morado	#6600CC	Marco Morado	16	3
119	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/naranja.png	Naranja	#FF9900	Marco Naranja	16	3
135	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	17	3
120	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/negro.png	Negro	#000000	Marco Negro	16	3
124	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	17	3
126	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	17	3
127	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	17	3
128	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/cafe.png	Café	#663300	Marco Café	17	3
129	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/gris.png	Gris	#999999	Marco Gris	17	3
131	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/naranja.png	Naranja	#FF9900	Marco Naranja	17	3
132	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/negro.png	Negro	#000000	Marco Negro	17	3
136	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/blanco.png	Blanco	#FFFFFF	Marco Blanco	17	3
422	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/lila.png	Lila	#C299F6	Marco lila	17	3
137	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	18	3
138	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	18	3
140	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/cafe.png	Café	#663300	Marco Café	18	3
139	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	18	3
142	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/gris.png	Gris	#999999	Marco Gris	18	3
143	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/morado.png	Morado	#6600CC	Marco Morado	18	3
144	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/naranja.png	Naranja	#FF9900	Marco Naranja	18	3
146	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/rosado.png	Rosado	#FFCCFF	Marco Rosado	18	3
148	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/blanco.png	Blanco	#FFFFFF	Marco Blanco	18	3
149	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	19	3
150	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	19	3
217	Caballete Aguamarina	/img/fotos_pedidos/admin_fotos/caballetes/aguamarinaCaballete.png	Aguamarina	#00FFCC	Caballete Aguamarina	40	3
151	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	19	3
153	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/cafe.png	Café	#663300	Marco Café	19	3
154	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/gris.png	Gris	#999999	Marco Gris	19	3
155	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/morado.png	Morado	#6600CC	Marco Morado	19	3
156	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/naranja.png	Naranja	#FF9900	Marco Naranja	19	3
160	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/rosado.png	Rosado	#FFCCFF	Marco Rosado	19	3
161	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/blanco.png	Blanco	#FFFFFF	Marco Blanco	19	3
162	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	20	3
163	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	20	3
164	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	20	3
165	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	20	3
167	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/gris.png	Gris	#999999	Marco Gris	20	3
168	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/morado.png	Morado	#6600CC	Marco Morado	20	3
170	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/negro.png	Negro	#000000	Marco Negro	20	3
169	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/naranja.png	Naranja	#FF9900	Marco Naranja	20	3
181	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/gris.png	Gris	#999999	Marco Gris	21	3
195	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/morado.png	Morado	#6600CC	Marco Morado	22	3
210	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/negro.png	Negro	#000000	Marco Negro	23	3
171	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/rojo.png	Rojo	#FF0000	Marco Rojo	20	3
200	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	22	3
209	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/naranja.png	Naranja	#FF9900	Marco Naranja	23	3
211	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/rojo.png	Rojo	#FF0000	Marco Rojo	23	3
213	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	23	3
203	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	23	3
214	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/rosado.png	Rosado	#FFCCFF	Marco Rosado	23	3
215	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/blanco.png	Blanco	#FFFFFF	Marco Blanco	23	3
173	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/rosado.png	Rosado	#FFCCFF	Marco Rosado	20	3
175	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/blanco.png	Blanco	#FFFFFF	Marco Blanco	20	3
176	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	21	0
177	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	21	3
180	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/cafe.png	Café	#663300	Marco Café	21	3
179	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	21	3
182	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/morado.png	Morado	#6600CC	Marco Morado	21	3
183	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/naranja.png	Naranja	#FF9900	Marco Naranja	21	3
184	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/negro.png	Negro	#000000	Marco Negro	21	3
187	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/verdebiche.png	Verde Biche	#66FF00	Marco Verde Biche	21	3
188	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/rosado.png	Rosado	#FFCCFF	Marco Rosado	21	3
189	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/blanco.png	Blanco	#FFFFFF	Marco Blanco	21	3
190	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	22	3
193	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/cafe.png	Café	#663300	Marco Café	22	3
194	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/gris.png	Gris	#999999	Marco Gris	22	3
196	Marco Naranja	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/naranja.png	Naranja	#FF9900	Marco Naranja	22	3
197	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/negro.png	Negro	#000000	Marco Negro	22	3
199	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	22	3
201	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/blanco.png	Blanco	#FFFFFF	Marco Blanco	22	3
207	Marco Gris	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/gris.png	Gris	#999999	Marco Gris	23	3
206	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/cafe.png	Café	#663300	Marco Café	23	3
204	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	23	3
152	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	19	3
276	Cuerda Café		Café	#663300	Cuerda Café	24	1
216	Caballete Amarillo	/img/fotos_pedidos/admin_fotos/caballetes/amarilloCaballete.png	Amarillo	#FFFF00	Caballete Amarillo	40	3
218	Caballete Azul Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/azuloscuroCaballete.png	Azul Oscuro	#0033CC	Caballete Azul Oscuro	40	3
220	Caballete Café	/img/fotos_pedidos/admin_fotos/caballetes/cafeCaballete.png	Café	#663300	Caballete Café	40	3
221	Caballete Gris	/img/fotos_pedidos/admin_fotos/caballetes/grisCaballete.png	Gris	#999999	Caballete Gris	40	3
222	Caballete Morado	/img/fotos_pedidos/admin_fotos/caballetes/moradoCaballete.png	Morado	#6600CC	Caballete Morado	40	3
223	Caballete Naranja	/img/fotos_pedidos/admin_fotos/caballetes/naranjaCaballete.png	Naranja	#FF9900	Caballete Naranja	40	3
224	Caballete Negro	/img/fotos_pedidos/admin_fotos/caballetes/negroCaballete.png	Negro	#000000	Caballete Negro	40	3
226	Caballete Verde Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/verdeoscuroCaballete.png	Verde Oscuro	#006633	Caballete Verde Oscuro	40	3
225	Caballete Rojo	/img/fotos_pedidos/admin_fotos/caballetes/rojoCaballete.png	Rojo	#FF0000	Caballete Rojo	40	3
227	Caballete Verde Biche	/img/fotos_pedidos/admin_fotos/caballetes/verdebicheCaballete.png	Verde Biche	#66FF00	Caballete Verde Biche	40	3
228	Caballete Rosado	/img/fotos_pedidos/admin_fotos/caballetes/rosadoCaballete.png	Rosado	#FFCCFF	Caballete Rosado	40	3
267	Caballete Rojo	/img/fotos_pedidos/admin_fotos/caballetes/rojoCaballete.png	Rojo	#FF0000	Caballete Rojo	43	3
269	Caballete Verde Biche	/img/fotos_pedidos/admin_fotos/caballetes/verdebicheCaballete.png	Verde Biche	#66FF00	Caballete Verde Biche	43	3
270	Caballete Rosado	/img/fotos_pedidos/admin_fotos/caballetes/rosadoCaballete.png	Rosado	#FFCCFF	Caballete Rosado	43	3
271	Caballete Blanco	/img/fotos_pedidos/admin_fotos/caballetes/blancoCaballete.png	Blanco	#FFFFFF	Caballete Blanco	43	3
262	Caballete Café	/img/fotos_pedidos/admin_fotos/caballetes/cafeCaballete.png	Café	#663300	Caballete Café	43	3
229	Caballete Blanco	/img/fotos_pedidos/admin_fotos/caballetes/blancoCaballete.png	Blanco	#FFFFFF	Caballete Blanco	40	3
230	Caballete Amarillo	/img/fotos_pedidos/admin_fotos/caballetes/amarilloCaballete.png	Amarillo	#FFFF00	Caballete Amarillo	41	3
231	Caballete Aguamarina	/img/fotos_pedidos/admin_fotos/caballetes/aguamarinaCaballete.png	Aguamarina	#00FFCC	Caballete Aguamarina	41	3
232	Caballete Azul Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/azuloscuroCaballete.png	Azul Oscuro	#0033CC	Caballete Azul Oscuro	41	3
233	Caballete Azul Claro	/img/fotos_pedidos/admin_fotos/caballetes/azulclaroCaballete.png	Azul Claro	#33CCFF	Caballete Azul Claro	41	3
251	Caballete Naranja	/img/fotos_pedidos/admin_fotos/caballetes/naranjaCaballete.png	Naranja	#FF9900	Caballete Naranja	42	3
252	Caballete Negro	/img/fotos_pedidos/admin_fotos/caballetes/negroCaballete.png	Negro	#000000	Caballete Negro	42	3
255	Caballete Verde Biche	/img/fotos_pedidos/admin_fotos/caballetes/verdebicheCaballete.png	Verde Biche	#66FF00	Caballete Verde Biche	42	3
256	Caballete Rosado	/img/caballetes/rosadoCaballete.png	Rosado	#FFCCFF	Caballete Rosado	42	3
264	Caballete Morado	/img/fotos_pedidos/admin_fotos/caballetes/moradoCaballete.png	Morado	#6600CC	Caballete Morado	43	3
261	Caballete Azul Claro	/img/fotos_pedidos/admin_fotos/caballetes/azulclaroCaballete.png	Azul Claro	#33CCFF	Caballete Azul Claro	43	3
257	Caballete Blanco	/img/caballetes/blancoCaballete.png	Blanco	#FFFFFF	Caballete Blanco	42	3
259	Caballete Aguamarina	/img/fotos_pedidos/admin_fotos/caballetes/aguamarinaCaballete.png	Aguamarina	#00FFCC	Caballete Aguamarina	43	3
234	Caballete Café	/img/fotos_pedidos/admin_fotos/caballetes/cafeCaballete.png	Café	#663300	Caballete Café	41	3
235	Caballete Gris	/img/fotos_pedidos/admin_fotos/caballetes/grisCaballete.png	Gris	#999999	Caballete Gris	41	3
236	Caballete Morado	/img/fotos_pedidos/admin_fotos/caballetes/moradoCaballete.png	Morado	#6600CC	Caballete Morado	41	3
237	Caballete Naranja	/img/fotos_pedidos/admin_fotos/caballetes/naranjaCaballete.png	Naranja	#FF9900	Caballete Naranja	41	3
250	Caballete Morado	/img/fotos_pedidos/admin_fotos/caballetes/moradoCaballete.png	Morado	#6600CC	Caballete Morado	42	3
238	Caballete Negro	/img/fotos_pedidos/admin_fotos/caballetes/negroCaballete.png	Negro	#000000	Caballete Negro	41	3
240	Caballete Verde Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/verdeoscuroCaballete.png	Verde Oscuro	#006633	Caballete Verde Oscuro	41	3
241	Caballete Verde Biche	/img/fotos_pedidos/admin_fotos/caballetes/verdebicheCaballete.png	Verde Biche	#66FF00	Caballete Verde Biche	41	3
242	Caballete Rosado	/img/fotos_pedidos/admin_fotos/caballetes/rosadoCaballete.png	Rosado	#FFCCFF	Caballete Rosado	41	3
243	Caballete Blanco	/img/fotos_pedidos/admin_fotos/caballetes/blancoCaballete.png	Blanco	#FFFFFF	Caballete Blanco	41	3
245	Caballete Aguamarina	/img/fotos_pedidos/admin_fotos/caballetes/aguamarinaCaballete.png	Aguamarina	#00FFCC	Caballete Aguamarina	42	3
248	Caballete Café	/img/fotos_pedidos/admin_fotos/caballetes/cafeCaballete.png	Café	#663300	Caballete Café	42	3
249	Caballete Gris	/img/fotos_pedidos/admin_fotos/caballetes/grisCaballete.png	Gris	#999999	Caballete Gris	42	3
254	Caballete Verde Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/verdeoscuroCaballete.png	Verde Oscuro	#006633	Caballete Verde Oscuro	42	3
265	Caballete Naranja	/img/fotos_pedidos/admin_fotos/caballetes/naranjaCaballete.png	Naranja	#FF9900	Caballete Naranja	43	3
266	Caballete Negro	/img/fotos_pedidos/admin_fotos/caballetes/negroCaballete.png	Negro	#000000	Caballete Negro	43	3
157	Marco Negro	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/negro.png	Negro	#000000	Marco Negro	19	3
441	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/lila.png	Lila	#C299F6	Marco lila	23	3
280	Cuerda Negro		Negro	#000000	Cuerda Negro	24	1
281	Cuerda Rojo		Rojo	#f90315	Cuerda Rojo	24	1
284	Cuerda Rosado		Rosado	#FFCCFF	Cuerda Rosado	24	1
342	Pinza Amarillo		Amarillo	#FFFF00	Pinza Amarillo	24	2
343	Pinza Aguamarina		Aguamarina	#00FFCC	Pinza Aguamarina	24	2
344	Pinza Azul Oscuro		Azul Oscuro	#0033CC	Pinza Azul Oscuro	24	2
345	Pinza Azul Claro		Azul Claro	#33CCFF	Pinza Azul Claro	24	2
346	Pinza Café		Café	#663300	Pinza Café	24	2
347	Pinza Gris		Gris	#999999	Pinza Gris	24	2
348	Pinza Morado		Morado	#6600CC	Pinza Morado	24	2
349	Pinza Naranja		Naranja	#FF9900	Pinza Naranja	24	2
350	Pinza Negro		Negro	#000000	Pinza Negro	24	2
352	Pinza Verde Oscuro		Verde Oscuro	#006633	Pinza Verde Oscuro	24	2
353	Pinza Verde Biche		Verde Biche	#66FF00	Pinza Verde Biche	24	2
354	Pinza Rosado		Rosado	#FFCCFF	Pinza Rosado	24	2
355	Pinza Blanco		Blanco	#FFFFFF	Pinza Blanco	24	2
288	Cuerda Azul Oscuro		Azul Oscuro	#0033CC	Cuerda Azul Oscuro	25	1
290	Cuerda Café		Café	#663300	Cuerda Café	25	1
293	Cuerda Dorada		Dorado	#FFD500	Cuerda dorada	25	1
294	Cuerda Negro		Negro	#000000	Cuerda Negro	25	1
295	Cuerda Rojo		Rojo	#f90315	Cuerda Rojo	25	1
296	Cuerda Fucsia		Fucsia	#FF00CC	Cuerda fucsia	25	1
298	Cuerda Rosado		Rosado	#FFCCFF	Cuerda Rosado	25	1
299	Cuerda Plateada		Plata	#B9B9B9	Cuerda Plateada	25	1
356	Clips Amarillos		Amarillo	#FFFF00	Pinza Amarillo	25	2
357	Clips Aguamarina		Aguamarina	#00FFCC	Pinza Aguamarina	25	2
358	Clips Azul Oscuro		Azul Oscuro	#0033CC	Pinza Azul Oscuro	25	2
359	Clips Azul Claro		Azul Claro	#33CCFF	Pinza Azul Claro	25	2
360	Clips Cafés		Café	#663300	Pinza Café	25	2
340	Cuerda Rosado		Rosado	#FFCCFF	Cuerda Rosado	28	1
219	Caballete Azul Claro	/img/fotos_pedidos/admin_fotos/caballetes/azulclaroCaballete.png	Azul Claro	#33CCFF	Caballete Azul Claro	40	3
361	Clips Grises		Gris	#999999	Pinza Gris	25	2
362	Clips Morados		Morado	#6600CC	Pinza Morado	25	2
363	Clips Naranja		Naranja	#FF9900	Pinza Naranja	25	2
364	Clips Negros		Negro	#000000	Pinza Negro	25	2
365	Clips Rojos		Rojo	#f90315	Pinza Rojo	25	2
366	Clips Verde Oscuro		Verde Oscuro	#006633	Pinza Verde Oscuro	25	2
367	Clips Verde Biche		Verde Biche	#66FF00	Pinza Verde Biche	25	2
368	Clips Rosado		Rosado	#FFCCFF	Pinza Rosado	25	2
303	Cuerda Azul		Azul	#0033CC	Cuerda Azul	26	1
304	Cuerda Café		Café	#663300	Cuerda Café	26	1
316	Cuerda Azul Oscuro		Azul Oscuro	#0033CC	Cuerda Azul Oscuro	27	1
318	Cuerda Café		Café	#663300	Cuerda Café	27	1
319	Cuerda Plateada		Plateado	#B9B9B9	Cuerda Plateada	27	1
321	Cuerda Dorada		Dorado	#FFD500	Cuerda Dorada	27	1
307	Cuerda Dorada		Dorado	#FFD500	Cuerda dorada	26	1
305	Cuerda Plateada		Plateado	#B9B9B9	Cuerda Plateada	26	1
308	Cuerda Negro		Negro	#000000	Cuerda Negro	26	1
309	Cuerda Rojo		Rojo	#f90315	Cuerda Rojo	26	1
313	Cuerda Fucsia		Fucsia	#FF00CC	Cuerda Fucsia	26	1
312	Cuerda Rosado		Rosado	#FFCCFF	Cuerda Rosado	26	1
322	Cuerda Negro		Negro	#000000	Cuerda Negro	27	1
323	Cuerda Rojo		Rojo	#f90315	Cuerda Rojo	27	1
326	Cuerda Rosado		Rosado	#FFCCFF	Cuerda Rosado	27	1
327	Cuerda Fucsia		Fucsia	#FF00CC	Cuerda Fucsia	27	1
330	Cuerda Azul Oscuro		Azul Oscuro	#0033CC	Cuerda Azul Oscuro	28	1
332	Cuerda Café		Café	#663300	Cuerda Café	28	1
333	Cuerda Plateada		Plateado	#B9B9B9	Cuerda Plateada	28	1
335	Cuerda Dorada		Dorado	#FFD500	Cuerda Dorada	28	1
336	Cuerda Negro		Negro	#000000	Cuerda Negro	28	1
337	Cuerda Rojo		Rojo	#f90315	Cuerda Rojo	28	1
341	Cuerda Fucsia		Fucsia	#FF00CC	Cuerda Fucsia	28	1
95	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/rosado.png	Rosado	#FFCCFF	Marco Rosado	14	3
412	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/rojo.png	Rojo	#FF0000	Marco Rojo	14	3
414	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/verdeoscuro.png	Verde Oscuro	#006633	Marco verde oscuro	14	3
416	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/lila.png	Lila	#c299f6	Marco lila	15	3
401	Pinza Azul Claro		Azul Claro	#33CCFF	Pinza Azul Claro	28	2
389	Pinza Gris		Gris	#999999	Pinza Gris	27	2
415	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/lila.png	Lila	#C299F6	Marco Lila	14	3
158	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/rojo.png	Rojo	#FF0000	Marco Rojo	19	3
423	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/rosado.png	Rosado	#FFCCFF	Marco Rosado	17	3
417	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/rojo.png	Rojo	#FF0000	Marco Rojo	15	3
432	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/lila.png	Lila	#C299F6	Marco Lila	20	3
159	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	19	3
125	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	17	3
424	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	18	3
122	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	16	3
425	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/lila.png	Lila	#C299F6	Marco lila	18	3
418	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/lila.png	Lila	#c299f6	Marco lila	16	3
428	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/fucsia.png	Fucsia	#FF00CC	Marco fucsia	19	3
429	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/lila.png	Lila	#C299F6	Marco lila	19	3
372	Pinza Azul Oscuro		Azul Oscuro	#0033CC	Pinza Azul Oscuro	26	2
420	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/verdebiche.png	Verde Biche	#66FF00	Marco verde biche	16	3
133	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/rojo.png	Rojo	#FF0000	Marco Rojo	17	3
431	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/fucsia.png	Fucsia	#FF00CC	Marco fucsia	20	3
421	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/fucsia.png	Fucsia	#FF00CC	Marco fucsia	17	3
427	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/rojo.png	Rojo	#FF0000	Marco rojo	18	3
433	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/verdebiche.png	Verde Biche	#66FF00	Marco verde biche	20	3
191	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	22	3
351	Pinza Rojo		Rojo	#f90315	Pinza Rojo	24	2
260	Caballete Azul Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/azuloscuroCaballete.png	Azul Oscuro	#0033CC	Caballete Azul Oscuro	43	3
436	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/amarillo.png	Amarillo	#FFFF00	Marco amarillo	22	3
369	Clips Blanco		Blanco	#FFFFFF	Pinza Blanco	25	2
370	Pinza Amarillo		Amarillo	#FFFF00	Pinza Amarillo	26	2
371	Pinza Aguamarina		Aguamarina	#00FFCC	Pinza Aguamarina	26	2
373	Pinza Azul Claro		Azul Claro	#33CCFF	Pinza Azul Claro	26	2
374	Pinza Café		Café	#663300	Pinza Café	26	2
375	Pinza Gris		Gris	#999999	Pinza Gris	26	2
376	Pinza Morado		Morado	#6600CC	Pinza Morado	26	2
377	Pinza Naranja		Naranja	#FF9900	Pinza Naranja	26	2
379	Pinza Rojo		Rojo	#f90315	Pinza Rojo	26	2
380	Pinza Verde Oscuro		Verde Oscuro	#006633	Pinza Verde Oscuro	26	2
381	Pinza Verde Biche		Verde Biche	#66FF00	Pinza Verde Biche	26	2
382	Pinza Rosado		Rosado	#FFCCFF	Pinza Rosado	26	2
383	Pinza Blanco		Blanco	#FFFFFF	Pinza Blanco	26	2
384	Pinza Amarillo		Amarillo	#FFFF00	Pinza Amarillo	27	2
386	Pinza Azul Oscuro		Azul Oscuro	#0033CC	Pinza Azul Oscuro	27	2
387	Pinza Azul Claro		Azul Claro	#33CCFF	Pinza Azul Claro	27	2
388	Pinza Café		Café	#663300	Pinza Café	27	2
390	Pinza Morado		Morado	#6600CC	Pinza Morado	27	2
393	Pinza Rojo		Rojo	#f90315	Pinza Rojo	27	2
394	Pinza Verde Oscuro		Verde Oscuro	#006633	Pinza Verde Oscuro	27	2
395	Pinza Verde Biche		Verde Biche	#66FF00	Pinza Verde Biche	27	2
396	Pinza Rosado		Rosado	#FFCCFF	Pinza Rosado	27	2
397	Pinza Blanco		Blanco	#FFFFFF	Pinza Blanco	27	2
399	Pinza Aguamarina		Aguamarina	#00FFCC	Pinza Aguamarina	28	2
398	Pinza Amarillo		Amarillo	#FFFF00	Pinza Amarillo	28	2
400	Pinza Azul Oscuro		Azul Oscuro	#0033CC	Pinza Azul Oscuro	28	2
402	Pinza Café		Café	#663300	Pinza Café	28	2
403	Pinza Gris		Gris	#999999	Pinza Gris	28	2
404	Pinza Morado		Morado	#6600CC	Pinza Morado	28	2
405	Pinza Naranja		Naranja	#FF9900	Pinza Naranja	28	2
406	Pinza Negro		Negro	#000000	Pinza Negro	28	2
407	Pinza Rojo		Rojo	#f90315	Pinza Rojo	28	2
408	Pinza Verde Oscuro		Verde Oscuro	#006633	Pinza Verde Oscuro	28	2
409	Pinza Verde Biche		Verde Biche	#66FF00	Pinza Verde Biche	28	2
410	Pinza Rosado		Rosado	#FFCCFF	Pinza Rosado	28	2
411	Pinza Blanco		Blanco	#FFFFFF	Pinza Blanco	28	2
239	Caballete Rojo	/img/fotos_pedidos/admin_fotos/caballetes/rojoCaballete.png	Rojo	#FF0000	Caballete Rojo	41	3
452	Cuerda Plateada	\N	Plateado	#B9B9B9	Cuerda plateada	24	1
453	Pinzas Fucsia	\N	Fucsia	#FF00CC	Pinzas fucsia	24	2
454	Pinzas Lila	\N	Lila	#C299F6	Pinzas lila	24	2
455	Clips Lila	\N	Lila	#C299F6	Pinza lila	25	2
456	Clips Fucsia	\N	Fucsia	#FF00CC	Pinza fucsia	25	2
457	Pinza Lila	\N	Lila	#C299F6	Pinza lila	26	2
458	Pinza Fucsia	\N	Fucsia	#FF00CC	Pinza fucsia	26	2
385	Pinza Aguamarina		Aguamarina	#00FFCC	Pinza Aguamarina	27	2
391	Pinza Naranja		Naranja	#FF9900	Pinza Naranja	27	2
392	Pinza Negro		Negro	#000000	Pinza Negro	27	2
86	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	14	3
88	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	14	3
413	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/unoxuno/blanco.png	Blanco	#FFFFFF	Marco Blanco	14	3
460	Pinza Lila	\N	Lila	#C299F6	Pinza Lila	27	2
461	Pinza Fucsia	\N	Fucsia	#FF00CC	Pinza Fucsia	28	2
462	Pinza Lila	\N	Lila	#C299F6	Pinza Lila	28	2
442	Caballete Fucsia	/img/fotos_pedidos/admin_fotos/caballetes/fucsiaCaballete.png	Fucsia	#FF00CC	Caballete fucsia	40	3
274	Cuerda Azul Oscuro		Azul Oscuro	#0033CC	Cuerda Azul Oscuro	24	1
451	Cuerda Fucsia	\N	Fucsia	#FF00CC	Cuerda fucsia	24	1
443	Caballete Lila	/img/fotos_pedidos/admin_fotos/caballetes/lilaCaballete.png	Lila	#C299F6	Caballete lila	40	3
101	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/cafe.png	Café	#663300	Marco Café	15	3
109	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	15	3
116	Marco fucsia	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/fucsia.png	Fucsia	#FF00CC	Marco Fucsia	16	3
134	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	17	3
123	Marco Blanco	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/blanco.png	Blanco	#FFFFFF	Marco Blanco	16	3
419	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/rojo.png	Rojo	#FF0000	Marco rojo	16	3
130	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/dosxdos/morado.png	Morado	#6600CC	Marco Morado	17	3
141	Marco fucsia	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/fucsia.png	Fucsia	#FF00CC	Marco Fucsia	18	3
147	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	18	3
426	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/tresxdos/verdebiche.png	Verde Biche	#66FF00	Marco verde biche	18	3
178	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	21	3
166	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/cafe.png	Café	#663300	Marco Café	20	3
244	Caballete Amarillo	/img/fotos_pedidos/admin_fotos/caballetes/amarilloCaballete.png	Amarillo	#FFFF00	Caballete Amarillo	42	3
446	Caballete Fucsia	img/caballetes/fucsiaCaballete.png	Fucsia	#FF00CC	Caballete fucsia	42	3
449	Caballete Lila	/img/fotos_pedidos/admin_fotos/caballetes/lilaCaballete.png	Lila	#C299F6	Caballete lila	43	3
448	Caballete Fucsia	/img/fotos_pedidos/admin_fotos/caballetes/fucsiaCaballete.png	Fucsia	#FF00CC	Caballete fucsia	43	3
430	Marco Verde Biche	/img/fotos_pedidos/admin_fotos/marcos/tresxtres/verdebiche.png	Verde Biche	#66FF00	Marco verde biche	19	3
172	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxtres/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	20	3
434	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/fucsia.png	Fucsia	#FF00CC	Marco fucsia	21	3
435	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/lila.png	Lila	#C299F6	Marco Lila	21	3
438	Marco Lila	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/lila.png	Lila	#C299F6	Marco lila	22	3
439	Marco Rosado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/rosado.png	Rosado	#FFCCFF	Marco rosado	22	3
202	Marco Amarillo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/amarillo.png	Amarillo	#FFFF00	Marco Amarillo	23	3
205	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	23	3
192	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	22	3
444	Caballete Fucsia	/img/fotos_pedidos/admin_fotos/caballetes/fucsiaCaballete.png	Fucsia	#FF00CC	Caballete fucsia	41	3
445	Caballete Lila	/img/fotos_pedidos/admin_fotos/caballetes/lilaCaballete.png	Lila	#C299F6	Caballete lila	41	3
185	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/rojo.png	Rojo	#FF0000	Marco Rojo	21	3
186	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cincoxcuatro/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	21	3
198	Marco Rojo	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/rojo.png	Rojo	#FF0000	Marco Rojo	22	3
437	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/cuatroxseis/fucsia.png	Fucsia	#FF00CC	Marco fucsia	22	3
247	Caballete Azul Claro	/img/fotos_pedidos/admin_fotos/caballetes/azulclaroCaballete.png	Azul Claro	#33CCFF	Caballete Azul Claro	42	3
212	Marco Verde Oscuro	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/verdeoscuro.png	Verde Oscuro	#006633	Marco Verde Oscuro	23	3
208	Marco Morado	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/morado.png	Morado	#6600CC	Marco Morado	23	3
440	Marco Fucsia	/img/fotos_pedidos/admin_fotos/marcos/cuatroxocho/fucsia.png	Fucsia	#FF00CC	Marco fucsia	23	3
450	Cuerda Dorada	\N	Dorado	#FFD500	Cuerda dorada	24	1
459	Pinza Fucsia	\N	Fucsia	#FF00CC	Pinza Fucsia	27	2
246	Caballete Azul Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/azuloscuroCaballete.png	Azul Oscuro	#0033CC	Caballete Azul Oscuro	42	3
99	Marco Azul Oscuro	/img/fotos_pedidos/admin_fotos/marcos/dosxuno/azuloscuro.png	Azul Oscuro	#0033CC	Marco Azul Oscuro	15	3
112	Marco Aguamarina	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/aguamarina.png	Aguamarina	#00FFCC	Marco Aguamarina	16	3
114	Marco Azul Claro	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/azulclaro.png	Azul Claro	#33CCFF	Marco Azul Claro	16	3
115	Marco Café	/img/fotos_pedidos/admin_fotos/marcos/tresxuno/cafe.png	Café	#663300	Marco Café	16	3
378	Pinza Negro		Negro	#000000	Pinza Negro	26	2
253	Caballete Rojo	/img/fotos_pedidos/admin_fotos/caballetes/rojoCaballete.png	Rojo	#FF0000	Caballete Rojo	42	3
447	Caballete Lila	/img/fotos_pedidos/admin_fotos/caballetes/lilaCaballete.png	Lila	#C299F6	Caballete lila	42	3
258	Caballete Amarillo	/img/fotos_pedidos/admin_fotos/caballetes/amarilloCaballete.png	Amarillo	#FFFF00	Caballete Amarillo	43	3
263	Caballete Gris	/img/fotos_pedidos/admin_fotos/caballetes/grisCaballete.png	Gris	#999999	Caballete Gris	43	3
268	Caballete Verde Oscuro	/img/fotos_pedidos/admin_fotos/caballetes/verdeoscuroCaballete.png	Verde Oscuro	#006633	Caballete Verde Oscuro	43	3
\.


--
-- Name: accesorioproducto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('accesorioproducto_id_seq', 473, true);


--
-- Data for Name: accesorioproductopedido; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY accesorioproductopedido (id, id_productopedido, id_accesorioproducto) FROM stdin;
244	309	153
247	311	452
249	312	450
254	315	351
257	317	427
259	319	86
262	328	419
266	334	114
270	340	351
272	344	85
274	346	111
276	349	414
278	351	414
280	354	129
282	362	294
283	362	358
285	367	443
286	368	443
288	370	123
290	373	100
294	380	120
296	385	413
297	386	217
298	386	228
300	388	100
302	391	88
305	397	94
307	400	123
309	403	113
311	405	93
313	408	417
315	413	417
317	416	136
319	420	136
321	422	112
323	426	132
325	428	106
327	430	100
329	435	318
330	435	388
332	441	87
334	443	225
336	445	100
338	448	127
340	452	106
342	463	157
344	465	427
345	466	452
347	467	132
349	469	161
351	474	126
353	479	280
354	479	351
356	484	93
358	487	112
360	491	232
362	495	86
364	497	144
366	500	223
368	503	89
370	505	281
372	506	227
374	509	442
376	514	175
378	516	125
380	521	116
382	526	281
383	526	344
385	539	451
387	540	274
388	540	345
390	541	353
391	542	163
392	543	303
393	543	370
395	548	112
397	550	218
399	552	229
402	554	88
405	557	416
407	559	107
408	560	88
410	562	95
412	566	416
414	568	107
417	569	224
419	571	413
421	573	412
423	574	359
425	576	85
427	578	323
429	581	87
431	583	246
433	583	255
435	585	112
144	186	223
171	228	224
238	303	154
243	308	242
245	310	313
246	310	371
248	311	343
250	312	354
253	315	280
255	316	304
256	316	458
258	318	217
260	320	298
261	320	357
263	330	160
265	333	123
269	340	280
271	343	94
273	345	231
275	347	97
277	350	414
279	352	414
281	357	417
284	364	120
289	371	98
291	374	98
293	376	113
295	383	111
299	387	102
301	390	88
303	393	288
304	393	357
306	398	132
310	404	85
312	406	110
314	410	139
316	415	102
318	419	157
320	421	114
322	423	114
231	292	132
324	427	150
326	429	112
328	434	129
331	436	87
333	442	87
335	444	224
337	447	227
339	449	442
341	457	125
343	464	157
346	466	350
348	468	128
350	470	167
352	477	106
355	483	103
357	485	112
359	489	169
361	494	93
363	496	223
365	498	116
367	499	86
369	504	89
371	505	344
373	509	227
375	513	414
377	515	129
379	518	110
381	525	106
384	537	217
386	539	353
389	541	276
394	547	127
396	549	98
398	551	177
400	553	340
401	553	461
403	555	224
404	555	221
406	558	85
409	561	85
411	563	95
413	565	88
415	567	85
416	564	85
418	570	413
420	572	412
422	574	296
424	575	118
426	577	96
428	578	388
430	583	253
432	583	447
434	586	119
\.


--
-- Name: accesorioproductopedido_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('accesorioproductopedido_id_seq', 435, true);


--
-- Data for Name: costoenvio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY costoenvio (id, zona, precio, id_producto) FROM stdin;
165	bogota	8000	17
167	valledupar/Monteria	12000	17
154	costa	10000	14
168	otra	10000	17
169	bogota	10000	18
171	valledupar/Monteria	18000	18
162	costa	10000	16
159	valledupar/Monteria	12000	15
164	otra	10000	16
204	otra	9000	26
200	otra	9000	25
205	bogota	6000	27
160	otra	10000	15
172	otra	18000	18
173	bogota	10000	19
190	costa	32000	23
174	costa	15000	19
166	costa	10000	17
193	bogota	6000	24
175	valledupar/Monteria	18000	19
176	otra	18000	19
177	bogota	12000	20
101	bogota	6000	1
179	valledupar/Monteria	40000	20
180	otra	23000	20
178	costa	30000	20
181	bogota	12000	21
182	costa	30000	21
194	costa	9000	24
201	bogota	6000	26
198	costa	9000	25
183	valledupar/Monteria	40000	21
184	otra	23000	21
185	bogota	15000	22
186	costa	32000	22
187	valledupar/Monteria	42000	22
188	otra	25000	22
189	bogota	15000	23
191	valledupar/Monteria	42000	23
158	costa	10000	15
161	bogota	8000	16
163	valledupar/Monteria	12000	16
196	otra	9000	24
197	bogota	6000	25
199	valledupar/Monteria	9000	25
202	costa	9000	26
206	costa	9000	27
207	valledupar/Monteria	9000	27
208	otra	9000	27
209	bogota	6000	28
210	costa	9000	28
102	costa	9000	1
211	valledupar/Monteria	9000	28
106	costa	9000	2
212	otra	9000	28
217	bogota	8000	30
218	costa	10000	30
105	bogota	6000	2
107	valledupar/Monteria	9000	2
110	costa	9000	3
219	valledupar/Monteria	10000	30
220	otra	10000	30
221	bogota	8000	31
222	costa	10000	31
223	valledupar/Monteria	10000	31
224	otra	10000	31
226	costa	10000	32
227	valledupar/Monteria	10000	32
228	otra	10000	32
109	bogota	6000	3
231	valledupar/Monteria	10000	33
225	bogota	8000	32
111	valledupar/Monteria	9000	3
112	otra	9000	3
113	bogota	6000	4
114	costa	9000	4
115	valledupar/Monteria	9000	4
116	otra	9000	4
117	bogota	6000	5
118	costa	9000	5
125	bogota	6000	7
127	valledupar/Monteria	9000	7
121	bogota	6000	6
122	costa	9000	6
126	costa	9000	7
128	otra	9000	7
129	bogota	6000	8
130	costa	9000	8
131	valledupar/Monteria	9000	8
132	otra	9000	8
133	bogota	6000	9
134	costa	9000	9
135	valledupar/Monteria	9000	9
136	otra	9000	9
137	bogota	6000	10
138	costa	9000	10
140	otra	9000	10
141	bogota	6000	11
142	costa	9000	11
143	valledupar/Monteria	9000	11
144	otra	9000	11
145	bogota	6000	12
147	valledupar/Monteria	9000	12
146	costa	9000	12
148	otra	9000	12
149	bogota	6000	13
150	costa	9000	13
151	valledupar/Monteria	9000	13
152	otra	9000	13
156	otra	10000	14
157	bogota	8000	15
170	costa	15000	18
192	otra	25000	23
203	valledupar/Monteria	9000	26
232	otra	10000	33
103	valledupar/Monteria	9000	1
230	costa	10000	33
229	bogota	8000	33
233	bogota	8000	34
234	costa	10000	34
235	valledupar/Monteria	10000	34
238	costa	10000	35
239	valledupar/Monteria	10000	35
240	otra	10000	35
237	bogota	8000	35
246	costa	10000	37
247	valledupar/Monteria	10000	37
248	otra	10000	37
245	bogota	8000	37
244	otra	10000	36
250	costa	10000	38
249	bogota	8000	38
251	valledupar/Monteria	10000	38
241	bogota	8000	36
242	costa	10000	36
243	valledupar/Monteria	10000	36
123	valledupar/Monteria	9000	6
104	otra	9000	1
108	otra	9000	2
120	otra	9000	5
119	valledupar/Monteria	9000	5
124	otra	9000	6
139	valledupar/Monteria	9000	10
153	bogota	8000	14
155	valledupar/Monteria	12000	14
195	valledupar/Monteria	9000	24
257	bogota	8000	40
258	costa	10000	40
259	valledupar/Monteria	10000	40
260	otra	10000	40
261	bogota	8000	41
262	costa	10000	41
263	valledupar/Monteria	10000	41
264	otra	10000	41
265	bogota	8000	42
266	costa	10000	42
267	valledupar/Monteria	10000	42
268	otra	10000	42
269	bogota	8000	43
270	costa	10000	43
271	valledupar/Monteria	10000	43
272	otra	10000	43
236	otra	10000	34
252	otra	10000	38
253	bogota	8000	39
254	costa	10000	39
255	valledupar/Monteria	10000	39
256	otra	10000	39
273	bogota	6000	45
274	costa	9000	45
275	valledupar/Monteria	9000	45
276	otra	9000	45
\.


--
-- Name: costoenvio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('costoenvio_id_seq', 376, true);


--
-- Data for Name: empaque; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY empaque (id, nombre, precio, color, color_code, descripcion, imagen, tipo_empaque, id_tipoproducto, habilitado) FROM stdin;
8	Caja de Madera	5200	Aguamarina	#08f9d6	Caja de madera agumarina	img/fotos_pedidos/admin_fotos/aguamarina_cajas.png	2	1	t
26	Sobre Naranja	1000	Naranja	#FFA506	Sobre naranja	img/fotos_pedidos/admin_fotos/naranja_sobre.png	1	1	t
9	Caja de Madera	5200	Rojo	#f90315	Caja de madera roja	img/fotos_pedidos/admin_fotos/rojo_cajas.png	2	1	t
10	Caja de Madera	5200	Café	#513103	Caja de madera café	img/fotos_pedidos/admin_fotos/cafe_cajas.png	2	1	t
11	Caja de Madera	5200	Negro	#070707	Caja de madera negra	img/fotos_pedidos/admin_fotos/negro_cajas.png	2	1	t
12	Caja de Madera	5200	Gris	#939290	Caja de madera gris	img/fotos_pedidos/admin_fotos/gris_cajas.png	2	1	t
13	Caja de Madera	5200	Blanco	#FFFFFF	Caja de madera blanca	img/fotos_pedidos/admin_fotos/blanco_cajas.png	2	1	t
14	Caja de Madera	5200	Verde oscuro	#035e30	Caja de madera verde oscuro	img/fotos_pedidos/admin_fotos/verdeoscuro_cajas.png	2	1	t
15	Caja de Madera	5200	Verde biche	#67fc06	Caja de madera verde biche	img/fotos_pedidos/admin_fotos/verdebiche_cajas.png	2	1	t
16	Caja de Madera	5200	Fucsia	#f908d7	Caja de madera fucsia	img/fotos_pedidos/admin_fotos/fucsia_cajas.png	2	1	t
17	Caja de Madera	5200	Amarillo	#ffff00	Caja de madera amarilla	img/fotos_pedidos/admin_fotos/amarillo_cajas.png	2	1	t
19	Caja de Madera	5200	Morado	#7506c4	Caja de madera morada	img/fotos_pedidos/admin_fotos/morado_cajas.png	2	1	t
18	Caja de Madera	5200	Naranja	#ffa506	Caja de madera naranja	img/fotos_pedidos/admin_fotos/naranja_cajas.png	2	1	t
20	Sobre	0	Rojo	#f90315	Sobre rojo	/img/sobres/rojo_sobre.png	1	6	t
21	Sobre	0	Amarillo	#ffff00	Sobre amarillo	/img/sobres/amarillo_sobre.png	1	6	t
22	Sobre	0	Azul	#0640bf	Sobre azul	/img/sobres/azul_sobre.png	1	6	t
23	Sobre	0	Verde	#66FF00	Sobre verde	/img/sobres/verde_sobre.png	1	6	t
1	Sobre	800	Rojo	#f90315	Sobre rojo	img/fotos_pedidos/admin_fotos/rojo_sobre.png	1	1	t
2	Sobre	800	Amarillo	#ffff00	Sobre amarillo	img/fotos_pedidos/admin_fotos/amarillo_sobre.png	1	1	t
3	Sobre	800	Azul	#0640bf	Sobre azul	img/fotos_pedidos/admin_fotos/azul_sobre.png	1	1	t
4	Sobre	800	Verde	#67fc06	Sobre verde	img/fotos_pedidos/admin_fotos/verde_sobre.png	1	1	t
24	Sobre	0	Blanco	#FFFFFF	Sobre blanco	/img/sobres/blanco_sobre.png	1	6	t
27	Caja Lila	5200	Lila	#C299F6	Caja de madera lila	img/fotos_pedidos/admin_fotos/lila_cajas.png	2	1	t
5	Sobre	800	Blanco	#FFFFFF	Sobre blanco	img/fotos_pedidos/admin_fotos/blanco_sobre.png	1	1	t
29	Sobre Naranja	0	Naranja	#FFA506	Sobre Naranja	img/sobres/naranja_sobre.png	1	6	t
6	Caja de Madera	5200	Azul Oscuro	#0640bf	Caja de madera azul oscuro	img/fotos_pedidos/admin_fotos/azul_oscuro_cajas.png	2	1	t
28	Caja Rosada	5200	Rosado	#FFCCFF	Caja de madera rosada	img/fotos_pedidos/admin_fotos/rosado_cajas.png	2	1	t
7	Caja de Madera	5200	Azul Claro	#4bbff9	Caja de madera azul claro	img/fotos_pedidos/admin_fotos/azulclaro_cajas.png	2	1	t
\.


--
-- Name: empaque_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empaque_id_seq', 32, true);


--
-- Data for Name: foto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY foto (id, imagen, texto, color, formato, id_productopedido) FROM stdin;
1031	client/img/fotos_pedidos/fotos_pedidos/7-2-117-eogup782m-0.jpeg	a	a	t	295
1032	client/img/fotos_pedidos/fotos_pedidos/7-2-117-oucsboyro-1.jpeg	a	a	t	295
1033	client/img/fotos_pedidos/fotos_pedidos/7-2-117-lz3fm9yj0-2.jpeg	a	a	t	295
1035	client/img/fotos_pedidos/fotos_pedidos/7-2-117-fod8njtt5-4.jpeg	a	a	t	295
1037	client/img/fotos_pedidos/fotos_pedidos/7-2-117-ygcwa4810-6.jpeg	a	a	t	295
1039	client/img/fotos_pedidos/fotos_pedidos/7-2-117-dm1vwsxxu-7.jpeg	a	a	t	295
1041	client/img/fotos_pedidos/fotos_pedidos/7-2-117-572jczj58-10.jpeg	a	a	t	295
1043	client/img/fotos_pedidos/fotos_pedidos/7-2-117-p459iqiit-12.jpeg	a	a	t	295
1045	client/img/fotos_pedidos/fotos_pedidos/7-2-117-4kw883hmz-14.jpeg	a	a	t	295
1047	client/img/fotos_pedidos/fotos_pedidos/7-2-117-n1wrvn7su-16.jpeg	a	a	t	295
1049	client/img/fotos_pedidos/fotos_pedidos/7-2-117-gz97q1jnx-18.jpeg	a	a	t	295
1051	client/img/fotos_pedidos/fotos_pedidos/7-2-117-9pqne0mzq-20.jpeg	a	a	t	295
1053	client/img/fotos_pedidos/fotos_pedidos/7-2-117-1rje91m61-22.jpeg	a	a	t	295
1055	client/img/fotos_pedidos/fotos_pedidos/7-2-117-g8zn61qnq-24.jpeg	a	a	t	295
1057	client/img/fotos_pedidos/fotos_pedidos/7-2-117-svbwueecb-26.jpeg	a	a	t	295
1059	client/img/fotos_pedidos/fotos_pedidos/7-2-117-n35jobq2p-28.jpeg	a	a	t	295
1061	client/img/fotos_pedidos/fotos_pedidos/7-2-117-rfzz45sxl-30.jpeg	a	a	t	295
1137	client/img/fotos_pedidos/fotos_pedidos/7-6-117-kd9121od0-0.jpeg	a	a	f	305
1138	client/img/fotos_pedidos/fotos_pedidos/7-6-117-pb01g82nw-1.jpeg	a	a	f	305
1139	client/img/fotos_pedidos/fotos_pedidos/7-6-117-nbq4poatr-3.jpeg	a	a	f	305
1140	client/img/fotos_pedidos/fotos_pedidos/7-6-117-3y3toa5rn-2.jpeg	a	a	f	305
1142	client/img/fotos_pedidos/fotos_pedidos/7-6-117-592ksta1l-6.jpeg	a	a	f	305
1144	client/img/fotos_pedidos/fotos_pedidos/7-6-117-tyod5aofe-5.jpeg	a	a	f	305
1146	client/img/fotos_pedidos/fotos_pedidos/7-6-117-sciww3gnc-0.jpeg	a	a	f	305
1222	client/img/fotos_pedidos/fotos_pedidos/7-5-117-f9onhz92z-5.jpeg	a	a	t	317
1318	client/img/fotos_pedidos/fotos_pedidos/7-5-117-y9kq6oqr9-0.jpeg	a	a	f	329
1319	client/img/fotos_pedidos/fotos_pedidos/7-5-117-u7ls8jxw2-0.jpeg	a	a	f	329
1320	client/img/fotos_pedidos/fotos_pedidos/7-5-117-dr9uyqy5d-0.jpeg	a	a	f	329
1321	client/img/fotos_pedidos/fotos_pedidos/7-5-117-64dlooi3j-0.jpeg	a	a	f	329
1324	client/img/fotos_pedidos/fotos_pedidos/7-5-117-84njwqjm6-0.jpeg	a	a	f	329
1456	client/img/fotos_pedidos/fotos_pedidos/8-5-117-jkinuf42b-0.jpeg	a	a	f	345
1581	client/img/fotos_pedidos/fotos_pedidos/8-0-117-1ac8bw1f0-1.jpeg	a	a	f	360
1585	client/img/fotos_pedidos/fotos_pedidos/8-0-117-3ra7nv1ja-5.jpeg	a	a	f	360
1589	client/img/fotos_pedidos/fotos_pedidos/8-0-117-th414o4jm-9.jpeg	a	a	f	360
1593	client/img/fotos_pedidos/fotos_pedidos/8-0-117-0faztvzeb-3.jpeg	a	a	f	360
1597	client/img/fotos_pedidos/fotos_pedidos/8-0-117-dtyplt7h7-7.jpeg	a	a	f	360
1601	client/img/fotos_pedidos/fotos_pedidos/8-0-117-53zmkvdrl-1.jpeg	a	a	f	360
1605	client/img/fotos_pedidos/fotos_pedidos/8-5-117-6ow72zspr-1.jpeg	a	a	f	361
1609	client/img/fotos_pedidos/fotos_pedidos/8-5-117-hdawp7c4s-5.jpeg	a	a	f	361
1613	client/img/fotos_pedidos/fotos_pedidos/8-5-117-q4xz2scjq-9.jpeg	a	a	f	361
1732	client/img/fotos_pedidos/fotos_pedidos/8-6-117-re1rsxc8x-0.jpeg	a	a	f	380
1733	client/img/fotos_pedidos/fotos_pedidos/8-6-117-hfyod3z7g-1.jpeg	a	a	f	380
1734	client/img/fotos_pedidos/fotos_pedidos/8-6-117-ajfhqt72z-2.jpeg	a	a	f	380
1851	client/img/fotos_pedidos/fotos_pedidos/9-6-117-g0mn0eqfm-0.jpeg	a	a	f	395
1853	client/img/fotos_pedidos/fotos_pedidos/9-6-117-9sb63ejor-0.jpeg	a	a	f	395
1855	client/img/fotos_pedidos/fotos_pedidos/9-6-117-jwtnj0780-0.jpeg	a	a	f	395
1857	client/img/fotos_pedidos/fotos_pedidos/9-6-117-f5105wq2i-0.png	a	a	f	395
1859	client/img/fotos_pedidos/fotos_pedidos/9-6-117-l88h0bvch-0.jpeg	a	a	f	395
1948	client/img/fotos_pedidos/fotos_pedidos/9-2-117-2s9z482hm-0.jpeg	a	a	f	408
1949	client/img/fotos_pedidos/fotos_pedidos/9-2-117-klsi5q31d-0.jpeg	a	a	f	408
2063	client/img/fotos_pedidos/fotos_pedidos/9-6-117-1mdtuirdx-5.jpeg	a	a	f	425
2067	client/img/fotos_pedidos/fotos_pedidos/9-6-117-fj1rxbbd7-8.jpeg	a	a	f	425
2120	client/img/fotos_pedidos/fotos_pedidos/9-3-117-zjb49tll7-0.jpeg	a	a	f	433
2125	client/img/fotos_pedidos/fotos_pedidos/9-3-117-t0utxwrgm-0.jpeg	a	a	f	433
2131	client/img/fotos_pedidos/fotos_pedidos/9-3-117-88s3tcswx-0.jpeg	a	a	f	433
2137	client/img/fotos_pedidos/fotos_pedidos/9-3-117-skovrnufb-0.jpeg	a	a	f	433
2234	client/img/fotos_pedidos/fotos_pedidos/9-1-117-oawuzk3rw-0.jpeg	a	a	f	448
2319	client/img/fotos_pedidos/fotos_pedidos/10-2-117-5e9vcwk2i-11.jpeg	a	a	f	458
2426	client/img/fotos_pedidos/fotos_pedidos/10-3-117-gmmqzl68a-0.jpeg	a	a	f	470
2430	client/img/fotos_pedidos/fotos_pedidos/10-3-117-hsh2r84as-0.jpeg	a	a	f	470
2538	client/img/fotos_pedidos/fotos_pedidos/10-1-117-gqql5r3us-3.jpeg	a	a	t	479
2540	client/img/fotos_pedidos/fotos_pedidos/10-1-117-nv1yd5a8h-4.jpeg	a	a	t	479
2620	client/img/fotos_pedidos/fotos_pedidos/10-0-117-dk3vr71qv-4.jpeg	a	a	f	489
2623	client/img/fotos_pedidos/fotos_pedidos/10-0-117-cets3u2ro-6.jpeg	a	a	f	489
2625	client/img/fotos_pedidos/fotos_pedidos/10-0-117-fc55a7m01-9.jpeg	a	a	f	489
2628	client/img/fotos_pedidos/fotos_pedidos/10-0-117-cvktb733f-2.jpeg	a	a	f	489
2715	client/img/fotos_pedidos/fotos_pedidos/10-2-117-nwq42f2r1-1.jpeg	a	a	f	505
2853	client/img/fotos_pedidos/fotos_pedidos/11-2-117-v8sko8hv2-0.jpeg	a	a	f	515
2943	client/img/fotos_pedidos/fotos_pedidos/11-1-117-nysyh08n9-0.png	a	a	f	523
2946	client/img/fotos_pedidos/fotos_pedidos/11-1-117-aumk37fyu-0.jpeg	a	a	f	523
2949	client/img/fotos_pedidos/fotos_pedidos/11-1-117-fl46msabq-0.png	a	a	f	524
2952	client/img/fotos_pedidos/fotos_pedidos/11-1-117-ipz58io7n-0.jpeg	a	a	f	524
2955	client/img/fotos_pedidos/fotos_pedidos/11-1-117-4t337ehus-0.jpeg	a	a	f	524
3039	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ojselg1bl-0.jpeg	a	a	f	535
3040	client/img/fotos_pedidos/fotos_pedidos/11-3-117-t3c61019i-1.jpeg	a	a	f	535
1034	client/img/fotos_pedidos/fotos_pedidos/7-2-117-rkafu8uy6-3.jpeg	a	a	t	295
1036	client/img/fotos_pedidos/fotos_pedidos/7-2-117-lpi3b9chu-5.jpeg	a	a	t	295
1038	client/img/fotos_pedidos/fotos_pedidos/7-2-117-ngcpksv47-8.jpeg	a	a	t	295
1040	client/img/fotos_pedidos/fotos_pedidos/7-2-117-45k3lw3pe-9.jpeg	a	a	t	295
1042	client/img/fotos_pedidos/fotos_pedidos/7-2-117-0opxmnltk-11.jpeg	a	a	t	295
1044	client/img/fotos_pedidos/fotos_pedidos/7-2-117-x4sg9uf9x-13.jpeg	a	a	t	295
1046	client/img/fotos_pedidos/fotos_pedidos/7-2-117-ff989q13i-15.jpeg	a	a	t	295
1048	client/img/fotos_pedidos/fotos_pedidos/7-2-117-p0p24vce8-17.jpeg	a	a	t	295
1050	client/img/fotos_pedidos/fotos_pedidos/7-2-117-tlqz1bxwb-19.jpeg	a	a	t	295
1052	client/img/fotos_pedidos/fotos_pedidos/7-2-117-7ao9a6uqq-21.jpeg	a	a	t	295
1054	client/img/fotos_pedidos/fotos_pedidos/7-2-117-fqsuad3gn-23.jpeg	a	a	t	295
1056	client/img/fotos_pedidos/fotos_pedidos/7-2-117-6x8fsc5us-25.jpeg	a	a	t	295
1058	client/img/fotos_pedidos/fotos_pedidos/7-2-117-2qi1cvdb1-27.jpeg	a	a	t	295
1060	client/img/fotos_pedidos/fotos_pedidos/7-2-117-iuso979qu-29.jpeg	a	a	t	295
1062	client/img/fotos_pedidos/fotos_pedidos/7-2-117-gcfqbw0yf-31.jpeg	a	a	t	295
1141	client/img/fotos_pedidos/fotos_pedidos/7-6-117-kj81d8un0-4.jpeg	a	a	f	305
1143	client/img/fotos_pedidos/fotos_pedidos/7-6-117-nivdpt5gd-7.jpeg	a	a	f	305
1145	client/img/fotos_pedidos/fotos_pedidos/7-6-117-2wkutxxbx-8.jpeg	a	a	f	305
1223	client/img/fotos_pedidos/fotos_pedidos/7-0-117-sn3cvlso9-0.jpeg	a	a	f	318
1322	client/img/fotos_pedidos/fotos_pedidos/7-5-117-ajbsjyhm4-0.jpeg	a	a	f	329
1327	client/img/fotos_pedidos/fotos_pedidos/7-5-117-d2ew72ps3-0.jpeg	a	a	f	329
1457	client/img/fotos_pedidos/fotos_pedidos/8-5-117-hh6gcawdl-0.jpeg	a	a	f	346
1458	client/img/fotos_pedidos/fotos_pedidos/8-5-117-z0xe87xn6-2.jpeg	a	a	f	346
1582	client/img/fotos_pedidos/fotos_pedidos/8-0-117-dbmkg5tbh-2.jpeg	a	a	f	360
1586	client/img/fotos_pedidos/fotos_pedidos/8-0-117-pxuykjxt7-6.jpeg	a	a	f	360
1590	client/img/fotos_pedidos/fotos_pedidos/8-0-117-hoo9hckrw-0.jpeg	a	a	f	360
1594	client/img/fotos_pedidos/fotos_pedidos/8-0-117-nwk10f5gt-4.jpeg	a	a	f	360
1598	client/img/fotos_pedidos/fotos_pedidos/8-0-117-rhwjrlh9t-8.jpeg	a	a	f	360
1602	client/img/fotos_pedidos/fotos_pedidos/8-0-117-6v2x6xwu1-2.jpeg	a	a	f	360
1606	client/img/fotos_pedidos/fotos_pedidos/8-5-117-ons616to1-2.jpeg	a	a	f	361
1610	client/img/fotos_pedidos/fotos_pedidos/8-5-117-3gure7q2t-6.jpeg	a	a	f	361
1735	client/img/fotos_pedidos/fotos_pedidos/8-6-117-zhxjpu4z6-0.jpeg	a	a	f	381
1736	client/img/fotos_pedidos/fotos_pedidos/8-6-117-cwj0n9kq3-1.jpeg	a	a	f	381
1737	client/img/fotos_pedidos/fotos_pedidos/8-6-117-8nbshynvc-2.jpeg	a	a	f	381
1739	client/img/fotos_pedidos/fotos_pedidos/8-6-117-q2bwxkikf-4.jpeg	a	a	f	381
1740	client/img/fotos_pedidos/fotos_pedidos/8-6-117-j2sz6r6o6-5.jpeg	a	a	f	381
1742	client/img/fotos_pedidos/fotos_pedidos/8-6-117-6u0cn794o-7.jpeg	a	a	f	381
1744	client/img/fotos_pedidos/fotos_pedidos/8-6-117-rs4jzokjw-0.jpeg	a	a	f	381
1746	client/img/fotos_pedidos/fotos_pedidos/8-6-117-gqzu9hl60-2.jpeg	a	a	f	381
1752	client/img/fotos_pedidos/fotos_pedidos/8-6-117-8af69zb3j-8.jpeg	a	a	f	381
1758	client/img/fotos_pedidos/fotos_pedidos/8-6-117-u861e2wct-5.jpeg	a	a	f	381
1764	client/img/fotos_pedidos/fotos_pedidos/8-6-117-3tr8vii9r-1.jpeg	a	a	f	381
1860	client/img/fotos_pedidos/fotos_pedidos/9-6-117-zp712rc19-0.jpeg	a	a	f	396
1861	client/img/fotos_pedidos/fotos_pedidos/9-6-117-50zp0ajw7-0.jpeg	a	a	f	396
1862	client/img/fotos_pedidos/fotos_pedidos/9-6-117-92ask3hkw-0.jpeg	a	a	f	396
1863	client/img/fotos_pedidos/fotos_pedidos/9-6-117-0c3ekh7tz-0.jpeg	a	a	f	396
1864	client/img/fotos_pedidos/fotos_pedidos/9-6-117-qwyx30jyp-0.jpeg	a	a	f	396
1865	client/img/fotos_pedidos/fotos_pedidos/9-6-117-554scmhcb-0.jpeg	a	a	f	396
1866	client/img/fotos_pedidos/fotos_pedidos/9-6-117-i4aqhm2j5-0.jpeg	a	a	f	396
1867	client/img/fotos_pedidos/fotos_pedidos/9-6-117-9s8x2e6bt-0.jpeg	a	a	f	396
1868	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ymahwnsdp-0.jpeg	a	a	f	396
1869	client/img/fotos_pedidos/fotos_pedidos/9-6-117-6vhozck20-0.jpeg	a	a	f	396
1870	client/img/fotos_pedidos/fotos_pedidos/9-6-117-3tp7ew67f-0.jpeg	a	a	f	396
1871	client/img/fotos_pedidos/fotos_pedidos/9-6-117-h3gm0kphv-0.png	a	a	f	396
1950	client/img/fotos_pedidos/fotos_pedidos/9-2-117-cqe8kuxxm-0.jpeg	a	a	f	409
1951	client/img/fotos_pedidos/fotos_pedidos/9-2-117-yqmyflfes-1.jpeg	a	a	f	409
1952	client/img/fotos_pedidos/fotos_pedidos/9-2-117-j2lrh8zie-3.jpeg	a	a	f	409
1954	client/img/fotos_pedidos/fotos_pedidos/9-2-117-b9g9gh4rs-4.jpeg	a	a	f	409
1956	client/img/fotos_pedidos/fotos_pedidos/9-2-117-tvoctumz6-6.jpeg	a	a	f	409
1958	client/img/fotos_pedidos/fotos_pedidos/9-2-117-43fucocvf-8.jpeg	a	a	f	409
2064	client/img/fotos_pedidos/fotos_pedidos/9-6-117-a2p97vq4c-6.jpeg	a	a	f	425
2121	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ihnq2kp5z-0.jpeg	a	a	f	433
2126	client/img/fotos_pedidos/fotos_pedidos/9-3-117-65kwcxtul-0.jpeg	a	a	f	433
2132	client/img/fotos_pedidos/fotos_pedidos/9-3-117-bsexsj2wh-0.jpeg	a	a	f	433
2138	client/img/fotos_pedidos/fotos_pedidos/9-3-117-8ziqmbvcu-0.jpeg	a	a	f	433
2235	client/img/fotos_pedidos/fotos_pedidos/9-1-117-97cw8p4q3-0.jpeg	a	a	f	449
2324	client/img/fotos_pedidos/fotos_pedidos/10-4-117-twxhteksn-1.jpeg	a	a	f	459
2325	client/img/fotos_pedidos/fotos_pedidos/10-4-117-hqtmqrvbb-0.png	a	a	f	459
2326	client/img/fotos_pedidos/fotos_pedidos/10-4-117-3rywxlujg-2.jpeg	a	a	f	459
2327	client/img/fotos_pedidos/fotos_pedidos/10-4-117-ihqf53mtp-3.jpeg	a	a	f	459
2328	client/img/fotos_pedidos/fotos_pedidos/10-4-117-nczzx9cma-4.jpeg	a	a	f	459
2329	client/img/fotos_pedidos/fotos_pedidos/10-4-117-t2ervdgxl-5.jpeg	a	a	f	459
2330	client/img/fotos_pedidos/fotos_pedidos/10-4-117-6wnzdbq2o-6.jpeg	a	a	f	459
2331	client/img/fotos_pedidos/fotos_pedidos/10-4-117-sahdexs7o-7.jpeg	a	a	f	459
2332	client/img/fotos_pedidos/fotos_pedidos/10-4-117-piv9k70al-8.jpeg	a	a	f	459
1459	client/img/fotos_pedidos/fotos_pedidos/8-5-117-0wgdrj7yb-1.jpeg	a	a	f	346
1583	client/img/fotos_pedidos/fotos_pedidos/8-0-117-rr30nbjz7-3.jpeg	a	a	f	360
1587	client/img/fotos_pedidos/fotos_pedidos/8-0-117-mdx9uw181-7.jpeg	a	a	f	360
1591	client/img/fotos_pedidos/fotos_pedidos/8-0-117-b2z156vir-1.jpeg	a	a	f	360
1224	client/img/fotos_pedidos/fotos_pedidos/7-0-117-c2qsl3jmn-0.jpeg	a	a	f	319
1323	client/img/fotos_pedidos/fotos_pedidos/7-5-117-u0pgbzu2e-0.jpeg	a	a	f	329
1595	client/img/fotos_pedidos/fotos_pedidos/8-0-117-8g5us6wvw-5.jpeg	a	a	f	360
1599	client/img/fotos_pedidos/fotos_pedidos/8-0-117-2qh3108nz-9.jpeg	a	a	f	360
1603	client/img/fotos_pedidos/fotos_pedidos/8-0-117-ee2iy9h6x-3.png	a	a	f	360
1607	client/img/fotos_pedidos/fotos_pedidos/8-5-117-j178bbuqi-3.jpeg	a	a	f	361
1611	client/img/fotos_pedidos/fotos_pedidos/8-5-117-bup6ypaih-7.jpeg	a	a	f	361
1738	client/img/fotos_pedidos/fotos_pedidos/8-6-117-g0lif78qz-3.jpeg	a	a	f	381
1741	client/img/fotos_pedidos/fotos_pedidos/8-6-117-tmjxvcxnq-6.jpeg	a	a	f	381
1743	client/img/fotos_pedidos/fotos_pedidos/8-6-117-m3mjczsjx-8.jpeg	a	a	f	381
1745	client/img/fotos_pedidos/fotos_pedidos/8-6-117-dr2dihubn-1.jpeg	a	a	f	381
1751	client/img/fotos_pedidos/fotos_pedidos/8-6-117-ndt549kej-7.jpeg	a	a	f	381
1757	client/img/fotos_pedidos/fotos_pedidos/8-6-117-198hhvep9-4.jpeg	a	a	f	381
1763	client/img/fotos_pedidos/fotos_pedidos/8-6-117-cl6qcwyx8-2.jpeg	a	a	f	381
1872	client/img/fotos_pedidos/fotos_pedidos/9-6-117-1wsak9uss-0.jpeg	a	a	f	397
1953	client/img/fotos_pedidos/fotos_pedidos/9-2-117-66f26j816-2.jpeg	a	a	f	409
1955	client/img/fotos_pedidos/fotos_pedidos/9-2-117-oz7mlepno-5.jpeg	a	a	f	409
1957	client/img/fotos_pedidos/fotos_pedidos/9-2-117-njk3d6umu-7.jpeg	a	a	f	409
1959	client/img/fotos_pedidos/fotos_pedidos/9-2-117-5bwq4peod-9.jpeg	a	a	f	409
2068	client/img/fotos_pedidos/fotos_pedidos/9-0-117-g2vdln3et-0.jpeg	a	a	f	426
2070	client/img/fotos_pedidos/fotos_pedidos/9-0-117-69p7utl3g-1.jpeg	a	a	f	426
2122	client/img/fotos_pedidos/fotos_pedidos/9-3-117-58jvq4hy6-0.jpeg	a	a	f	433
2128	client/img/fotos_pedidos/fotos_pedidos/9-3-117-te80etw7t-0.jpeg	a	a	f	433
2134	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ntnjx8gqb-0.jpeg	a	a	f	433
2236	client/img/fotos_pedidos/fotos_pedidos/9-2-117-f4na7bigh-0.jpeg	a	a	f	450
2237	client/img/fotos_pedidos/fotos_pedidos/9-2-117-hv4io7wrb-2.jpeg	a	a	f	450
2238	client/img/fotos_pedidos/fotos_pedidos/9-2-117-xp9x60kej-5.jpeg	a	a	f	450
2239	client/img/fotos_pedidos/fotos_pedidos/9-2-117-bw5uhj2q9-3.jpeg	a	a	f	450
2243	client/img/fotos_pedidos/fotos_pedidos/9-2-117-n2n50j87x-8.jpeg	a	a	f	450
2333	client/img/fotos_pedidos/fotos_pedidos/10-4-117-aax241jww-0.jpeg	a	a	f	459
2334	client/img/fotos_pedidos/fotos_pedidos/10-4-117-o7fs1m6wk-9.jpeg	a	a	f	459
2335	client/img/fotos_pedidos/fotos_pedidos/10-4-117-huqqua0x2-3.jpeg	a	a	f	459
2336	client/img/fotos_pedidos/fotos_pedidos/10-4-117-p8s1lb9b6-2.jpeg	a	a	f	459
2337	client/img/fotos_pedidos/fotos_pedidos/10-4-117-5k0ctmnv7-4.jpeg	a	a	f	459
2338	client/img/fotos_pedidos/fotos_pedidos/10-4-117-g6qsil8ee-5.jpeg	a	a	f	459
2339	client/img/fotos_pedidos/fotos_pedidos/10-4-117-gzep65t8h-6.jpeg	a	a	f	459
2340	client/img/fotos_pedidos/fotos_pedidos/10-4-117-43rakewt1-7.jpeg	a	a	f	459
2341	client/img/fotos_pedidos/fotos_pedidos/10-4-117-afjqz5jjk-8.jpeg	a	a	f	459
2342	client/img/fotos_pedidos/fotos_pedidos/10-4-117-vgp6ez7xz-9.jpeg	a	a	f	459
2429	client/img/fotos_pedidos/fotos_pedidos/10-3-117-hg1iclpnu-0.png	a	a	f	470
2433	client/img/fotos_pedidos/fotos_pedidos/10-3-117-f7d7e2ni1-0.jpeg	a	a	f	470
2539	client/img/fotos_pedidos/fotos_pedidos/10-1-117-21i1mnanm-2.jpeg	a	a	t	479
2629	client/img/fotos_pedidos/fotos_pedidos/10-1-117-4hirogq3p-0.jpeg	a	a	f	490
2630	client/img/fotos_pedidos/fotos_pedidos/10-1-117-fvzlsmhw3-1.jpeg	a	a	f	490
2631	client/img/fotos_pedidos/fotos_pedidos/10-1-117-f8rbnkedl-2.jpeg	a	a	f	490
2632	client/img/fotos_pedidos/fotos_pedidos/10-1-117-m67bwkywk-5.png	a	a	f	490
2634	client/img/fotos_pedidos/fotos_pedidos/10-1-117-c8b1j6544-3.jpeg	a	a	f	490
2636	client/img/fotos_pedidos/fotos_pedidos/10-1-117-3k3mbmln0-7.jpeg	a	a	f	490
2639	client/img/fotos_pedidos/fotos_pedidos/10-1-117-u3fosdawt-10.jpeg	a	a	f	490
2642	client/img/fotos_pedidos/fotos_pedidos/10-1-117-te590ei4o-1.jpeg	a	a	f	490
2645	client/img/fotos_pedidos/fotos_pedidos/10-1-117-72livh19q-4.jpeg	a	a	f	490
2648	client/img/fotos_pedidos/fotos_pedidos/10-1-117-asxhj0pzq-1.png	a	a	f	490
2651	client/img/fotos_pedidos/fotos_pedidos/10-1-117-zd01y1d7i-4.jpeg	a	a	f	490
2716	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0kq8la5k6-2.jpeg	a	a	f	505
2854	client/img/fotos_pedidos/fotos_pedidos/11-2-117-t8zbvp6am-0.jpeg	a	a	f	515
2957	client/img/fotos_pedidos/fotos_pedidos/11-1-117-jn12dfjkb-0.jpeg	a	a	f	525
2958	client/img/fotos_pedidos/fotos_pedidos/11-1-117-w7i46lx10-0.jpeg	a	a	f	525
3041	client/img/fotos_pedidos/fotos_pedidos/11-3-117-bpe4nltxm-2.jpeg	a	a	f	535
3045	client/img/fotos_pedidos/fotos_pedidos/11-3-117-68mxqb36t-6.jpeg	a	a	f	535
3051	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7ekgyshu6-2.jpeg	a	a	f	535
3057	client/img/fotos_pedidos/fotos_pedidos/11-3-117-9nkks4rzx-8.jpeg	a	a	f	535
3063	client/img/fotos_pedidos/fotos_pedidos/11-3-117-uqz98i3hy-4.jpeg	a	a	f	535
3069	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7722k62jx-0.jpeg	a	a	f	535
3143	client/img/fotos_pedidos/fotos_pedidos/11-4-117-d1aii6d2w-0.jpeg	a	a	f	540
3151	client/img/fotos_pedidos/fotos_pedidos/11-4-117-r3josino9-0.jpeg	a	a	f	541
3155	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cpnnkaqyz-0.jpeg	a	a	f	542
3161	client/img/fotos_pedidos/fotos_pedidos/11-4-117-clpecit9i-0.jpeg	a	a	f	542
3255	client/img/fotos_pedidos/fotos_pedidos/11-4-117-qt1ogxt83-0.jpeg	a	a	f	546
3272	client/img/fotos_pedidos/fotos_pedidos/11-2-117-mf9nxkhs0-0.jpeg	a	a	f	551
3273	client/img/fotos_pedidos/fotos_pedidos/11-2-117-tnwypud2o-1.jpeg	a	a	f	551
3275	client/img/fotos_pedidos/fotos_pedidos/11-2-117-n2e4kg8wq-2.jpeg	a	a	f	551
1065	client/img/fotos_pedidos/fotos_pedidos/7-3-117-mvrpziinr-0.jpeg	a	a	f	298
1066	client/img/fotos_pedidos/fotos_pedidos/7-3-117-xvlxr4h2z-1.png	a	a	f	298
1067	client/img/fotos_pedidos/fotos_pedidos/7-3-117-qgtql90ee-2.png	a	a	f	298
1068	client/img/fotos_pedidos/fotos_pedidos/7-3-117-geriryvh2-3.jpeg	a	a	f	298
1072	client/img/fotos_pedidos/fotos_pedidos/7-3-117-gk3lpmzk5-7.png	a	a	f	298
1460	client/img/fotos_pedidos/fotos_pedidos/8-5-117-linsio0mj-1.jpeg	a	a	f	347
1461	client/img/fotos_pedidos/fotos_pedidos/8-5-117-g0615on9c-0.jpeg	a	a	f	347
1225	client/img/fotos_pedidos/fotos_pedidos/7-1-117-088i47qhw-1.jpeg	a	a	f	320
1229	client/img/fotos_pedidos/fotos_pedidos/7-1-117-5h43c8ejc-4.jpeg	a	a	f	320
1233	client/img/fotos_pedidos/fotos_pedidos/7-1-117-plgdq5tg9-6.jpeg	a	a	f	320
1325	client/img/fotos_pedidos/fotos_pedidos/7-5-117-vlthigsrg-0.jpeg	a	a	f	329
1424	client/img/fotos_pedidos/fotos_pedidos/8-3-117-vvxydx5e5-0.jpeg	a	a	f	340
1428	client/img/fotos_pedidos/fotos_pedidos/8-3-117-j3b5ns68f-4.jpeg	a	a	f	340
1614	client/img/fotos_pedidos/fotos_pedidos/8-0-117-xr5cgare1-0.jpeg	a	a	f	362
1615	client/img/fotos_pedidos/fotos_pedidos/8-0-117-7lemhbqja-0.jpeg	a	a	f	362
1616	client/img/fotos_pedidos/fotos_pedidos/8-0-117-njd5uhwy5-0.jpeg	a	a	f	362
1617	client/img/fotos_pedidos/fotos_pedidos/8-0-117-ie6mq1pbt-0.jpeg	a	a	f	362
1618	client/img/fotos_pedidos/fotos_pedidos/8-0-117-hjojq0x44-1.jpeg	a	a	f	362
1619	client/img/fotos_pedidos/fotos_pedidos/8-0-117-txdkarkvr-0.jpeg	a	a	f	362
1620	client/img/fotos_pedidos/fotos_pedidos/8-0-117-f810b0jxi-0.jpeg	a	a	f	362
1621	client/img/fotos_pedidos/fotos_pedidos/8-0-117-gkgiaotar-0.jpeg	a	a	f	362
1622	client/img/fotos_pedidos/fotos_pedidos/8-0-117-l33nvpnlo-0.jpeg	a	a	f	362
1623	client/img/fotos_pedidos/fotos_pedidos/8-0-117-8cajg46l9-1.jpeg	a	a	f	362
1747	client/img/fotos_pedidos/fotos_pedidos/8-6-117-vszz7l3pq-4.jpeg	a	a	f	381
1753	client/img/fotos_pedidos/fotos_pedidos/8-6-117-q3qa1g0vb-0.jpeg	a	a	f	381
1759	client/img/fotos_pedidos/fotos_pedidos/8-6-117-qmhj1byli-6.jpeg	a	a	f	381
1765	client/img/fotos_pedidos/fotos_pedidos/8-6-117-nzfcrkx1b-3.jpeg	a	a	f	381
1873	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ewou64o0h-0.jpeg	a	a	f	398
1874	client/img/fotos_pedidos/fotos_pedidos/9-6-117-l95tidb53-0.png	a	a	f	398
1960	client/img/fotos_pedidos/fotos_pedidos/9-2-117-xaxmucshw-0.jpeg	a	a	f	410
1961	client/img/fotos_pedidos/fotos_pedidos/9-2-117-likln1myk-0.jpeg	a	a	f	410
1962	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ghhpiq492-0.jpeg	a	a	f	410
1964	client/img/fotos_pedidos/fotos_pedidos/9-2-117-9d191mr4g-0.jpeg	a	a	f	410
2069	client/img/fotos_pedidos/fotos_pedidos/9-0-117-6w9hi9x4m-0.jpeg	a	a	f	426
2071	client/img/fotos_pedidos/fotos_pedidos/9-0-117-fh5d5hfxv-0.jpeg	a	a	f	426
2127	client/img/fotos_pedidos/fotos_pedidos/9-3-117-out79qogk-0.jpeg	a	a	f	433
2133	client/img/fotos_pedidos/fotos_pedidos/9-3-117-0c812uwzo-0.jpeg	a	a	f	433
2139	client/img/fotos_pedidos/fotos_pedidos/9-3-117-2aqtccgfg-0.jpeg	a	a	f	433
2240	client/img/fotos_pedidos/fotos_pedidos/9-2-117-h76p5nqqm-1.jpeg	a	a	f	450
2244	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ouemrsx27-7.jpeg	a	a	f	450
2343	client/img/fotos_pedidos/fotos_pedidos/10-5-117-ntpghwggn-0.jpeg	a	a	f	460
2344	client/img/fotos_pedidos/fotos_pedidos/10-5-117-8o8aht454-1.jpeg	a	a	f	460
2345	client/img/fotos_pedidos/fotos_pedidos/10-5-117-jssslj3ji-2.jpeg	a	a	f	460
2346	client/img/fotos_pedidos/fotos_pedidos/10-5-117-uz7ogmq9v-3.jpeg	a	a	f	460
2347	client/img/fotos_pedidos/fotos_pedidos/10-5-117-oouwns8w9-4.jpeg	a	a	f	460
2348	client/img/fotos_pedidos/fotos_pedidos/10-5-117-320qyphek-5.jpeg	a	a	f	460
2349	client/img/fotos_pedidos/fotos_pedidos/10-5-117-931cr8zgz-6.jpeg	a	a	f	460
2350	client/img/fotos_pedidos/fotos_pedidos/10-5-117-96pmri3q2-7.jpeg	a	a	f	460
2351	client/img/fotos_pedidos/fotos_pedidos/10-5-117-062wys53y-8.jpeg	a	a	f	460
2352	client/img/fotos_pedidos/fotos_pedidos/10-5-117-qaf7a7bgv-9.jpeg	a	a	f	460
2353	client/img/fotos_pedidos/fotos_pedidos/10-5-117-sazqmaj3y-0.jpeg	a	a	f	460
2354	client/img/fotos_pedidos/fotos_pedidos/10-5-117-u9tmx5e10-1.jpeg	a	a	f	460
2434	client/img/fotos_pedidos/fotos_pedidos/10-4-117-fl7jq84mu-1.jpeg	a	a	f	471
2435	client/img/fotos_pedidos/fotos_pedidos/10-4-117-vfzvc7qle-0.jpeg	a	a	f	471
2436	client/img/fotos_pedidos/fotos_pedidos/10-4-117-9adyexga9-2.jpeg	a	a	f	471
2438	client/img/fotos_pedidos/fotos_pedidos/10-4-117-1cx5zdd8o-4.jpeg	a	a	f	471
2440	client/img/fotos_pedidos/fotos_pedidos/10-4-117-mwow87ois-1.jpeg	a	a	f	471
2446	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7fwxmm94y-2.jpeg	a	a	f	471
2541	client/img/fotos_pedidos/fotos_pedidos/10-1-117-7pg18cty8-5.jpeg	a	a	t	479
2633	client/img/fotos_pedidos/fotos_pedidos/10-1-117-70k7z5cvw-4.jpeg	a	a	f	490
2635	client/img/fotos_pedidos/fotos_pedidos/10-1-117-onxrzeg6v-6.png	a	a	f	490
2638	client/img/fotos_pedidos/fotos_pedidos/10-1-117-wp4v1bw62-9.jpeg	a	a	f	490
2641	client/img/fotos_pedidos/fotos_pedidos/10-1-117-yenn5jtx7-0.jpeg	a	a	f	490
2644	client/img/fotos_pedidos/fotos_pedidos/10-1-117-xlhnbn892-3.jpeg	a	a	f	490
2647	client/img/fotos_pedidos/fotos_pedidos/10-1-117-mdjfx0xx3-0.jpeg	a	a	f	490
2650	client/img/fotos_pedidos/fotos_pedidos/10-1-117-yijn4r53q-3.jpeg	a	a	f	490
2717	client/img/fotos_pedidos/fotos_pedidos/10-2-117-gparo9wqo-3.jpeg	a	a	f	505
2855	client/img/fotos_pedidos/fotos_pedidos/11-2-117-oj1sz6sdq-0.png	a	a	f	515
2959	client/img/fotos_pedidos/fotos_pedidos/11-2-117-1pmzgpyp3-0.jpeg	a	a	f	526
2960	client/img/fotos_pedidos/fotos_pedidos/11-2-117-7cbjralny-0.png	a	a	f	526
2963	client/img/fotos_pedidos/fotos_pedidos/11-2-117-vmivutow3-3.png	a	a	f	526
3042	client/img/fotos_pedidos/fotos_pedidos/11-3-117-zcbmeg9ut-3.jpeg	a	a	f	535
3046	client/img/fotos_pedidos/fotos_pedidos/11-3-117-naoyglzhg-7.jpeg	a	a	f	535
3052	client/img/fotos_pedidos/fotos_pedidos/11-3-117-15529gvzw-3.jpeg	a	a	f	535
3058	client/img/fotos_pedidos/fotos_pedidos/11-3-117-83vs8kx6i-9.jpeg	a	a	f	535
1069	client/img/fotos_pedidos/fotos_pedidos/7-3-117-hprxfoe8u-5.png	a	a	f	298
1073	client/img/fotos_pedidos/fotos_pedidos/7-3-117-emrb4y0om-8.png	a	a	f	298
1462	client/img/fotos_pedidos/fotos_pedidos/8-5-117-jxq1hkbb3-0.jpeg	a	a	f	348
1226	client/img/fotos_pedidos/fotos_pedidos/7-1-117-el74jvvjh-0.jpeg	a	a	f	320
1230	client/img/fotos_pedidos/fotos_pedidos/7-1-117-fml55212s-5.jpeg	a	a	f	320
1234	client/img/fotos_pedidos/fotos_pedidos/7-1-117-0f4nozdb6-8.jpeg	a	a	f	320
1326	client/img/fotos_pedidos/fotos_pedidos/7-5-117-gxjs94g1h-0.jpeg	a	a	f	329
1425	client/img/fotos_pedidos/fotos_pedidos/8-3-117-8vng2b1ec-2.jpeg	a	a	f	340
1429	client/img/fotos_pedidos/fotos_pedidos/8-3-117-c3z7tqmu1-5.jpeg	a	a	f	340
1463	client/img/fotos_pedidos/fotos_pedidos/8-5-117-fc93rism0-1.jpeg	a	a	f	348
1464	client/img/fotos_pedidos/fotos_pedidos/8-5-117-dins13zde-3.jpeg	a	a	f	348
1466	client/img/fotos_pedidos/fotos_pedidos/8-5-117-u7fp4e9kk-5.jpeg	a	a	f	348
1468	client/img/fotos_pedidos/fotos_pedidos/8-5-117-l771y5vtv-6.jpeg	a	a	f	348
1470	client/img/fotos_pedidos/fotos_pedidos/8-5-117-jofvnt3z0-8.jpeg	a	a	f	348
1472	client/img/fotos_pedidos/fotos_pedidos/8-5-117-ecwa7hbsv-11.jpeg	a	a	f	348
1474	client/img/fotos_pedidos/fotos_pedidos/8-5-117-wb98kmsdt-12.jpeg	a	a	f	348
1477	client/img/fotos_pedidos/fotos_pedidos/8-5-117-799jjnwuf-0.jpeg	a	a	f	348
1479	client/img/fotos_pedidos/fotos_pedidos/8-5-117-z2mzcxdbk-2.jpeg	a	a	f	348
1624	client/img/fotos_pedidos/fotos_pedidos/8-1-117-6whtzfpgd-0.jpeg	a	a	f	363
1625	client/img/fotos_pedidos/fotos_pedidos/8-1-117-je7nrjeqe-0.jpeg	a	a	f	363
1626	client/img/fotos_pedidos/fotos_pedidos/8-1-117-edco9onho-0.jpeg	a	a	f	363
1627	client/img/fotos_pedidos/fotos_pedidos/8-1-117-rjdwsilfi-2.jpeg	a	a	f	363
1629	client/img/fotos_pedidos/fotos_pedidos/8-1-117-u8ht55jol-3.jpeg	a	a	f	363
1631	client/img/fotos_pedidos/fotos_pedidos/8-1-117-spqegi40c-6.jpeg	a	a	f	363
1633	client/img/fotos_pedidos/fotos_pedidos/8-1-117-ts6m9fq3h-7.jpeg	a	a	f	363
1635	client/img/fotos_pedidos/fotos_pedidos/8-1-117-3fuuabqi9-8.jpeg	a	a	f	363
1637	client/img/fotos_pedidos/fotos_pedidos/8-1-117-8uw1vb8l6-2.jpeg	a	a	f	363
1639	client/img/fotos_pedidos/fotos_pedidos/8-1-117-63oealv23-3.jpeg	a	a	f	363
1641	client/img/fotos_pedidos/fotos_pedidos/8-1-117-rbl1vnxap-4.jpeg	a	a	f	363
1643	client/img/fotos_pedidos/fotos_pedidos/8-1-117-mltxeyr8a-0.jpeg	a	a	f	363
1645	client/img/fotos_pedidos/fotos_pedidos/8-1-117-8f8v7fm7o-2.png	a	a	f	363
1647	client/img/fotos_pedidos/fotos_pedidos/8-1-117-as39393q6-3.jpeg	a	a	f	363
1748	client/img/fotos_pedidos/fotos_pedidos/8-6-117-ru3a8tlb8-3.jpeg	a	a	f	381
1754	client/img/fotos_pedidos/fotos_pedidos/8-6-117-y0qgiy2ve-1.jpeg	a	a	f	381
1760	client/img/fotos_pedidos/fotos_pedidos/8-6-117-ogiuqvuxl-7.jpeg	a	a	f	381
1766	client/img/fotos_pedidos/fotos_pedidos/8-6-117-ff9xew2e5-4.jpeg	a	a	f	381
1875	client/img/fotos_pedidos/fotos_pedidos/9-6-117-i2bt6gz6o-0.jpeg	a	a	f	398
1963	client/img/fotos_pedidos/fotos_pedidos/9-2-117-206tq5iq0-0.jpeg	a	a	f	410
1965	client/img/fotos_pedidos/fotos_pedidos/9-2-117-mrqe6q2nb-0.jpeg	a	a	f	410
2072	client/img/fotos_pedidos/fotos_pedidos/9-0-117-byq9tfsgm-0.jpeg	a	a	f	427
2074	client/img/fotos_pedidos/fotos_pedidos/9-0-117-r0p2fe2mj-1.jpeg	a	a	f	427
2077	client/img/fotos_pedidos/fotos_pedidos/9-0-117-65o7yy4t5-4.jpeg	a	a	f	427
2140	client/img/fotos_pedidos/fotos_pedidos/9-4-117-vpj1bpjeq-1.jpeg	a	a	f	434
2141	client/img/fotos_pedidos/fotos_pedidos/9-4-117-rukfpegyq-2.jpeg	a	a	f	434
2142	client/img/fotos_pedidos/fotos_pedidos/9-4-117-p5fu5a9u6-3.jpeg	a	a	f	434
2143	client/img/fotos_pedidos/fotos_pedidos/9-4-117-kee8s70dz-0.jpeg	a	a	f	434
2241	client/img/fotos_pedidos/fotos_pedidos/9-2-117-zenl03hnk-4.jpeg	a	a	f	450
2245	client/img/fotos_pedidos/fotos_pedidos/9-2-117-wdp1t8vfy-9.jpeg	a	a	f	450
2355	client/img/fotos_pedidos/fotos_pedidos/10-5-117-jc5wh5cxt-0.jpeg	a	a	f	461
2356	client/img/fotos_pedidos/fotos_pedidos/10-5-117-nhhnyw3p1-1.jpeg	a	a	f	461
2357	client/img/fotos_pedidos/fotos_pedidos/10-5-117-6jv9o65z0-2.jpeg	a	a	f	461
2358	client/img/fotos_pedidos/fotos_pedidos/10-5-117-1bpexn8ey-3.jpeg	a	a	f	461
2359	client/img/fotos_pedidos/fotos_pedidos/10-5-117-ia1mlk93p-4.jpeg	a	a	f	461
2360	client/img/fotos_pedidos/fotos_pedidos/10-5-117-uzu05vnjp-5.jpeg	a	a	f	461
2361	client/img/fotos_pedidos/fotos_pedidos/10-5-117-7ca4ptnx1-6.jpeg	a	a	f	461
2362	client/img/fotos_pedidos/fotos_pedidos/10-5-117-9xtvlnxg7-7.jpeg	a	a	f	461
2363	client/img/fotos_pedidos/fotos_pedidos/10-5-117-ivtx3a2as-8.jpeg	a	a	f	461
2364	client/img/fotos_pedidos/fotos_pedidos/10-5-117-lqb5b5ktp-9.jpeg	a	a	f	461
2437	client/img/fotos_pedidos/fotos_pedidos/10-4-117-zyay809bs-3.jpeg	a	a	f	471
2439	client/img/fotos_pedidos/fotos_pedidos/10-4-117-uew19e9qk-0.jpeg	a	a	f	471
2441	client/img/fotos_pedidos/fotos_pedidos/10-4-117-arcc4hyfa-2.jpeg	a	a	f	471
2447	client/img/fotos_pedidos/fotos_pedidos/10-4-117-6amane3j9-3.jpeg	a	a	f	471
2542	client/img/fotos_pedidos/fotos_pedidos/10-2-117-h7inn2ykc-1.jpeg	a	a	f	480
2543	client/img/fotos_pedidos/fotos_pedidos/10-2-117-7nmehodzk-0.jpeg	a	a	f	480
2544	client/img/fotos_pedidos/fotos_pedidos/10-2-117-jjf1yo2qy-2.jpeg	a	a	f	480
2545	client/img/fotos_pedidos/fotos_pedidos/10-2-117-qv2qmehdb-3.jpeg	a	a	f	480
2550	client/img/fotos_pedidos/fotos_pedidos/10-2-117-qnuwgip6g-0.jpeg	a	a	f	480
2557	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0pxrlxups-2.jpeg	a	a	f	480
2637	client/img/fotos_pedidos/fotos_pedidos/10-1-117-41b6b0aa7-8.jpeg	a	a	f	490
2640	client/img/fotos_pedidos/fotos_pedidos/10-1-117-4qxf5gqen-11.jpeg	a	a	f	490
2643	client/img/fotos_pedidos/fotos_pedidos/10-1-117-clh8e01ez-2.jpeg	a	a	f	490
2646	client/img/fotos_pedidos/fotos_pedidos/10-1-117-v7zeg748c-5.jpeg	a	a	f	490
2649	client/img/fotos_pedidos/fotos_pedidos/10-1-117-1q9diblon-2.jpeg	a	a	f	490
2652	client/img/fotos_pedidos/fotos_pedidos/10-1-117-zsbyvk3ek-5.png	a	a	f	490
2718	client/img/fotos_pedidos/fotos_pedidos/10-2-117-z1wrlgfbx-4.jpeg	a	a	f	505
1070	client/img/fotos_pedidos/fotos_pedidos/7-3-117-yup1y1un5-4.png	a	a	f	298
1074	client/img/fotos_pedidos/fotos_pedidos/7-3-117-1v9vmrz4r-9.png	a	a	f	298
1465	client/img/fotos_pedidos/fotos_pedidos/8-5-117-ljfqd1yet-2.jpeg	a	a	f	348
1227	client/img/fotos_pedidos/fotos_pedidos/7-1-117-c0p4a1oie-2.jpeg	a	a	f	320
1231	client/img/fotos_pedidos/fotos_pedidos/7-1-117-7vj800hzi-9.jpeg	a	a	f	320
1328	client/img/fotos_pedidos/fotos_pedidos/7-6-117-5hrnms45l-2.jpeg	a	a	f	330
1330	client/img/fotos_pedidos/fotos_pedidos/7-6-117-jq80wc520-1.png	a	a	f	330
1332	client/img/fotos_pedidos/fotos_pedidos/7-6-117-17xdhn2he-6.jpeg	a	a	f	330
1334	client/img/fotos_pedidos/fotos_pedidos/7-6-117-bz3v8qs89-4.jpeg	a	a	f	330
1336	client/img/fotos_pedidos/fotos_pedidos/7-6-117-3b5ds1pth-7.png	a	a	f	330
1426	client/img/fotos_pedidos/fotos_pedidos/8-3-117-xzh5lshko-1.jpeg	a	a	f	340
1467	client/img/fotos_pedidos/fotos_pedidos/8-5-117-tr4wq6vec-4.jpeg	a	a	f	348
1469	client/img/fotos_pedidos/fotos_pedidos/8-5-117-1eixxkngr-7.jpeg	a	a	f	348
1471	client/img/fotos_pedidos/fotos_pedidos/8-5-117-j91pqx0ma-9.jpeg	a	a	f	348
1473	client/img/fotos_pedidos/fotos_pedidos/8-5-117-zc2pmyl48-10.jpeg	a	a	f	348
1475	client/img/fotos_pedidos/fotos_pedidos/8-5-117-7i8y27tgo-14.jpeg	a	a	f	348
1476	client/img/fotos_pedidos/fotos_pedidos/8-5-117-r4rlvtrmu-13.jpeg	a	a	f	348
1478	client/img/fotos_pedidos/fotos_pedidos/8-5-117-3lk2etgg2-1.jpeg	a	a	f	348
1628	client/img/fotos_pedidos/fotos_pedidos/8-1-117-18ij2q3rc-1.jpeg	a	a	f	363
1630	client/img/fotos_pedidos/fotos_pedidos/8-1-117-o4sa4h5ku-4.jpeg	a	a	f	363
1632	client/img/fotos_pedidos/fotos_pedidos/8-1-117-b115zn6iq-5.jpeg	a	a	f	363
1634	client/img/fotos_pedidos/fotos_pedidos/8-1-117-ysg16umi3-9.jpeg	a	a	f	363
1636	client/img/fotos_pedidos/fotos_pedidos/8-1-117-47l1evige-1.jpeg	a	a	f	363
1638	client/img/fotos_pedidos/fotos_pedidos/8-1-117-9hlgsa7se-0.jpeg	a	a	f	363
1640	client/img/fotos_pedidos/fotos_pedidos/8-1-117-1iozio7nr-5.jpeg	a	a	f	363
1642	client/img/fotos_pedidos/fotos_pedidos/8-1-117-297liknib-6.jpeg	a	a	f	363
1644	client/img/fotos_pedidos/fotos_pedidos/8-1-117-q576newrk-1.jpeg	a	a	f	363
1646	client/img/fotos_pedidos/fotos_pedidos/8-1-117-7m3bppte0-4.png	a	a	f	363
1749	client/img/fotos_pedidos/fotos_pedidos/8-6-117-9lg66dn93-5.jpeg	a	a	f	381
1755	client/img/fotos_pedidos/fotos_pedidos/8-6-117-9920roms0-2.jpeg	a	a	f	381
1761	client/img/fotos_pedidos/fotos_pedidos/8-6-117-elsfjifra-8.jpeg	a	a	f	381
1876	client/img/fotos_pedidos/fotos_pedidos/9-6-117-0twdkekfj-0.jpeg	a	a	f	398
1966	client/img/fotos_pedidos/fotos_pedidos/9-1-117-m8fhsenyk-0.jpeg	a	a	f	411
1967	client/img/fotos_pedidos/fotos_pedidos/9-1-117-9zbiv0ov0-3.jpeg	a	a	f	411
1968	client/img/fotos_pedidos/fotos_pedidos/9-1-117-b5pgsl8s1-1.jpeg	a	a	f	411
1969	client/img/fotos_pedidos/fotos_pedidos/9-1-117-rpy8901vi-2.jpeg	a	a	f	411
1970	client/img/fotos_pedidos/fotos_pedidos/9-1-117-cd9edbqj8-4.jpeg	a	a	f	411
1971	client/img/fotos_pedidos/fotos_pedidos/9-1-117-wx73j3huz-5.jpeg	a	a	f	411
1972	client/img/fotos_pedidos/fotos_pedidos/9-1-117-nomccb5iz-7.jpeg	a	a	f	411
1973	client/img/fotos_pedidos/fotos_pedidos/9-1-117-z7uf1jilq-6.jpeg	a	a	f	411
1974	client/img/fotos_pedidos/fotos_pedidos/9-1-117-m6cg18beg-8.jpeg	a	a	f	411
1976	client/img/fotos_pedidos/fotos_pedidos/9-1-117-i9jc0y93h-11.jpeg	a	a	f	411
1978	client/img/fotos_pedidos/fotos_pedidos/9-1-117-bntjvu61c-12.jpeg	a	a	f	411
1980	client/img/fotos_pedidos/fotos_pedidos/9-1-117-xfdelfjk3-14.jpeg	a	a	f	411
1982	client/img/fotos_pedidos/fotos_pedidos/9-1-117-mhbbsnxc8-16.jpeg	a	a	f	411
1984	client/img/fotos_pedidos/fotos_pedidos/9-1-117-xprvmsoyq-18.jpeg	a	a	f	411
2073	client/img/fotos_pedidos/fotos_pedidos/9-0-117-2qsimb58c-0.jpeg	a	a	f	427
2076	client/img/fotos_pedidos/fotos_pedidos/9-0-117-0kkbyx2ns-3.jpeg	a	a	f	427
2078	client/img/fotos_pedidos/fotos_pedidos/9-0-117-mop2q4mg0-5.jpeg	a	a	f	427
2144	client/img/fotos_pedidos/fotos_pedidos/9-4-117-68bhctkgj-1.jpeg	a	a	f	435
2145	client/img/fotos_pedidos/fotos_pedidos/9-4-117-16mp4n0b4-0.jpeg	a	a	f	435
2146	client/img/fotos_pedidos/fotos_pedidos/9-4-117-gimtl9toi-3.jpeg	a	a	f	435
2148	client/img/fotos_pedidos/fotos_pedidos/9-4-117-qmg53tmvq-4.jpeg	a	a	f	435
2150	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ohehzrm6t-6.jpeg	a	a	f	435
2152	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ezw7gksom-7.jpeg	a	a	f	435
2154	client/img/fotos_pedidos/fotos_pedidos/9-4-117-950zhxmgt-2.jpeg	a	a	f	435
2156	client/img/fotos_pedidos/fotos_pedidos/9-4-117-11dy3rwqx-4.jpeg	a	a	f	435
2158	client/img/fotos_pedidos/fotos_pedidos/9-4-117-7xxqywfup-6.jpeg	a	a	f	435
2160	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ianx8srx9-0.jpeg	a	a	f	435
2162	client/img/fotos_pedidos/fotos_pedidos/9-4-117-57uvjqeps-2.jpeg	a	a	f	435
2242	client/img/fotos_pedidos/fotos_pedidos/9-2-117-7gqdcfhr1-6.jpeg	a	a	f	450
2365	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mxlwytpsy-1.jpeg	a	a	f	462
2366	client/img/fotos_pedidos/fotos_pedidos/10-0-117-wcycgklas-2.jpeg	a	a	f	462
2367	client/img/fotos_pedidos/fotos_pedidos/10-0-117-clan12kgh-0.jpeg	a	a	f	462
2368	client/img/fotos_pedidos/fotos_pedidos/10-0-117-f0cblnbn5-3.jpeg	a	a	f	462
2371	client/img/fotos_pedidos/fotos_pedidos/10-0-117-7ioqqr2di-4.jpeg	a	a	f	462
2374	client/img/fotos_pedidos/fotos_pedidos/10-0-117-b7orsh6ku-8.jpeg	a	a	f	462
2442	client/img/fotos_pedidos/fotos_pedidos/10-4-117-xhtsmcehf-3.jpeg	a	a	f	471
2448	client/img/fotos_pedidos/fotos_pedidos/10-4-117-x7ohedhoh-4.jpeg	a	a	f	471
2546	client/img/fotos_pedidos/fotos_pedidos/10-2-117-rs3gdr9ut-5.jpeg	a	a	f	480
2552	client/img/fotos_pedidos/fotos_pedidos/10-2-117-5yt0yojgy-2.jpeg	a	a	f	480
2558	client/img/fotos_pedidos/fotos_pedidos/10-2-117-bvnh0sq8e-4.jpeg	a	a	f	480
2653	client/img/fotos_pedidos/fotos_pedidos/10-2-117-a8modwv0u-0.jpeg	a	a	t	491
2720	client/img/fotos_pedidos/fotos_pedidos/11-4-117-kv92lnrxl-0.jpeg	a	a	f	506
2856	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ve7rfw8dv-2.jpeg	a	a	f	517
1071	client/img/fotos_pedidos/fotos_pedidos/7-3-117-7f4yrlmu4-6.png	a	a	f	298
1480	client/img/fotos_pedidos/fotos_pedidos/8-5-117-yp6d2f333-3.jpeg	a	a	f	348
1648	client/img/fotos_pedidos/fotos_pedidos/8-2-117-qfj7tuu19-0.jpeg	a	a	f	364
1228	client/img/fotos_pedidos/fotos_pedidos/7-1-117-sf5wm4vk7-3.jpeg	a	a	f	320
1232	client/img/fotos_pedidos/fotos_pedidos/7-1-117-8q51ub84b-7.jpeg	a	a	f	320
1329	client/img/fotos_pedidos/fotos_pedidos/7-6-117-206sujk7i-0.png	a	a	f	330
1331	client/img/fotos_pedidos/fotos_pedidos/7-6-117-rn3379xmc-3.jpeg	a	a	f	330
1333	client/img/fotos_pedidos/fotos_pedidos/7-6-117-k3ey8o9rz-5.jpeg	a	a	f	330
1335	client/img/fotos_pedidos/fotos_pedidos/7-6-117-6jqkp8c3f-8.jpeg	a	a	f	330
1427	client/img/fotos_pedidos/fotos_pedidos/8-3-117-fgvvrgd7b-3.jpeg	a	a	f	340
1649	client/img/fotos_pedidos/fotos_pedidos/8-2-117-yk3tes7qg-1.jpeg	a	a	f	364
1650	client/img/fotos_pedidos/fotos_pedidos/8-2-117-652fp6ww4-2.png	a	a	f	364
1750	client/img/fotos_pedidos/fotos_pedidos/8-6-117-xsqcsfboa-6.jpeg	a	a	f	381
1756	client/img/fotos_pedidos/fotos_pedidos/8-6-117-q7ncpxfue-3.jpeg	a	a	f	381
1762	client/img/fotos_pedidos/fotos_pedidos/8-6-117-60wh4xcev-0.jpeg	a	a	f	381
1975	client/img/fotos_pedidos/fotos_pedidos/9-1-117-79i0n26g6-9.jpeg	a	a	f	411
1977	client/img/fotos_pedidos/fotos_pedidos/9-1-117-q9jesg6ax-10.jpeg	a	a	f	411
1979	client/img/fotos_pedidos/fotos_pedidos/9-1-117-7yh6g82oi-13.jpeg	a	a	f	411
1981	client/img/fotos_pedidos/fotos_pedidos/9-1-117-5uqup1e38-15.jpeg	a	a	f	411
1983	client/img/fotos_pedidos/fotos_pedidos/9-1-117-4czanwwdd-17.jpeg	a	a	f	411
1985	client/img/fotos_pedidos/fotos_pedidos/9-1-117-cfxxiemkp-19.jpeg	a	a	f	411
2075	client/img/fotos_pedidos/fotos_pedidos/9-0-117-bnf6tud02-2.jpeg	a	a	f	427
2147	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ixwp0dr5w-2.jpeg	a	a	f	435
2149	client/img/fotos_pedidos/fotos_pedidos/9-4-117-2krhd9riz-5.jpeg	a	a	f	435
2151	client/img/fotos_pedidos/fotos_pedidos/9-4-117-hzmvd7xyd-0.jpeg	a	a	f	435
2153	client/img/fotos_pedidos/fotos_pedidos/9-4-117-9gt37t4pn-1.jpeg	a	a	f	435
2155	client/img/fotos_pedidos/fotos_pedidos/9-4-117-y3m19vhhc-3.jpeg	a	a	f	435
2157	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ig4xj3bj7-5.jpeg	a	a	f	435
2159	client/img/fotos_pedidos/fotos_pedidos/9-4-117-xhv6l0lwm-7.jpeg	a	a	f	435
2161	client/img/fotos_pedidos/fotos_pedidos/9-4-117-sxetfw310-1.jpeg	a	a	f	435
2163	client/img/fotos_pedidos/fotos_pedidos/9-4-117-1nttjgioj-3.jpeg	a	a	f	435
2246	client/img/fotos_pedidos/fotos_pedidos/9-2-117-sr9p9ds4v-0.jpeg	a	a	f	451
2247	client/img/fotos_pedidos/fotos_pedidos/9-2-117-w74031xeq-3.jpeg	a	a	f	451
2248	client/img/fotos_pedidos/fotos_pedidos/9-2-117-8l0aaff8h-2.jpeg	a	a	f	451
2249	client/img/fotos_pedidos/fotos_pedidos/9-2-117-fuheaul2t-4.jpeg	a	a	f	451
2250	client/img/fotos_pedidos/fotos_pedidos/9-2-117-u1696drrf-1.jpeg	a	a	f	451
2255	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ponb4h3ss-9.jpeg	a	a	f	451
2369	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ooedpriqf-6.jpeg	a	a	f	462
2372	client/img/fotos_pedidos/fotos_pedidos/10-0-117-2ughfiz9h-7.jpeg	a	a	f	462
2443	client/img/fotos_pedidos/fotos_pedidos/10-4-117-ibqu1v5be-4.jpeg	a	a	f	471
2449	client/img/fotos_pedidos/fotos_pedidos/10-4-117-g20thwmgt-5.jpeg	a	a	f	471
2547	client/img/fotos_pedidos/fotos_pedidos/10-2-117-yoc6pz6hj-4.jpeg	a	a	f	480
2553	client/img/fotos_pedidos/fotos_pedidos/10-2-117-es0p4lzd6-3.jpeg	a	a	f	480
2559	client/img/fotos_pedidos/fotos_pedidos/10-2-117-slrpamp5z-3.jpeg	a	a	f	480
2654	client/img/fotos_pedidos/fotos_pedidos/10-2-117-egt08bza2-0.jpeg	a	a	f	492
2656	client/img/fotos_pedidos/fotos_pedidos/10-2-117-l38c543he-0.jpeg	a	a	f	492
2657	client/img/fotos_pedidos/fotos_pedidos/10-2-117-8r8fahbp0-0.jpeg	a	a	f	492
2659	client/img/fotos_pedidos/fotos_pedidos/10-2-117-nt9drd6go-0.jpeg	a	a	f	492
2662	client/img/fotos_pedidos/fotos_pedidos/10-2-117-olvwwx76a-0.jpeg	a	a	f	492
2721	client/img/fotos_pedidos/fotos_pedidos/11-4-117-nac3a1a0l-0.jpeg	a	a	f	507
2723	client/img/fotos_pedidos/fotos_pedidos/11-4-117-1qb2eym3e-1.jpeg	a	a	f	507
2725	client/img/fotos_pedidos/fotos_pedidos/11-4-117-zkdmuxj1x-4.png	a	a	f	507
2729	client/img/fotos_pedidos/fotos_pedidos/11-4-117-4m6gzl2ry-8.jpeg	a	a	f	507
2733	client/img/fotos_pedidos/fotos_pedidos/11-4-117-gpqeal7w8-0.jpeg	a	a	f	507
2737	client/img/fotos_pedidos/fotos_pedidos/11-4-117-g7ko8ci23-4.jpeg	a	a	f	507
2741	client/img/fotos_pedidos/fotos_pedidos/11-4-117-9oyloqzy1-0.jpeg	a	a	f	507
2857	client/img/fotos_pedidos/fotos_pedidos/11-4-117-faklppdlf-1.jpeg	a	a	f	517
2858	client/img/fotos_pedidos/fotos_pedidos/11-4-117-wz5w1m8mv-0.jpeg	a	a	f	517
2859	client/img/fotos_pedidos/fotos_pedidos/11-4-117-x5y7cxw6f-3.jpeg	a	a	f	517
2862	client/img/fotos_pedidos/fotos_pedidos/11-4-117-074yrrkan-6.jpeg	a	a	f	517
2866	client/img/fotos_pedidos/fotos_pedidos/11-4-117-1aievd1a9-0.jpeg	a	a	f	517
2870	client/img/fotos_pedidos/fotos_pedidos/11-4-117-8lwmv2a6q-3.jpeg	a	a	f	517
2874	client/img/fotos_pedidos/fotos_pedidos/11-4-117-h1m4c59nf-6.jpeg	a	a	f	517
2877	client/img/fotos_pedidos/fotos_pedidos/11-4-117-792ypqpwm-2.jpeg	a	a	f	517
2961	client/img/fotos_pedidos/fotos_pedidos/11-2-117-oupxgmtm9-1.png	a	a	f	526
2964	client/img/fotos_pedidos/fotos_pedidos/11-2-117-pu6ppo18o-4.png	a	a	f	526
3043	client/img/fotos_pedidos/fotos_pedidos/11-3-117-yl7gaxend-4.jpeg	a	a	f	535
3047	client/img/fotos_pedidos/fotos_pedidos/11-3-117-1ee940lqs-9.jpeg	a	a	f	535
3053	client/img/fotos_pedidos/fotos_pedidos/11-3-117-f8ibb4jhc-4.jpeg	a	a	f	535
3059	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ne8er7ll3-0.jpeg	a	a	f	535
3065	client/img/fotos_pedidos/fotos_pedidos/11-3-117-5ruunvmct-6.jpeg	a	a	f	535
3166	client/img/fotos_pedidos/fotos_pedidos/11-4-117-xf5alckkx-1.jpeg	a	a	f	543
3167	client/img/fotos_pedidos/fotos_pedidos/11-4-117-o2q65u53t-2.jpeg	a	a	f	543
3168	client/img/fotos_pedidos/fotos_pedidos/11-4-117-eh0o3ozm6-3.jpeg	a	a	f	543
3169	client/img/fotos_pedidos/fotos_pedidos/11-4-117-tdo535r28-4.jpeg	a	a	f	543
1481	client/img/fotos_pedidos/fotos_pedidos/8-5-117-bm4ujfi35-4.jpeg	a	a	f	348
1651	client/img/fotos_pedidos/fotos_pedidos/8-2-117-mfpmcz6y6-0.jpeg	You are young and life is long and there is time to kill today	a	t	365
1767	client/img/fotos_pedidos/fotos_pedidos/8-2-117-dsal3cn0k-0.jpeg	a	a	f	382
1768	client/img/fotos_pedidos/fotos_pedidos/8-2-117-wvl6tbhlz-0.jpeg	a	a	f	382
1155	client/img/fotos_pedidos/fotos_pedidos/7-1-117-e1hp8cgul-0.jpeg	a	a	f	308
1769	client/img/fotos_pedidos/fotos_pedidos/8-2-117-1kkfud33z-0.jpeg	a	a	f	382
1770	client/img/fotos_pedidos/fotos_pedidos/8-2-117-ra4uxi8nv-0.jpeg	a	a	f	382
1771	client/img/fotos_pedidos/fotos_pedidos/8-2-117-tgx820gvy-0.jpeg	a	a	f	382
1772	client/img/fotos_pedidos/fotos_pedidos/8-2-117-fj1ivca3i-0.jpeg	a	a	f	382
1773	client/img/fotos_pedidos/fotos_pedidos/8-2-117-iwrxk1vw7-0.jpeg	a	a	f	382
1337	client/img/fotos_pedidos/fotos_pedidos/7-6-117-u9j2s8jlr-2.jpeg	a	a	f	331
1338	client/img/fotos_pedidos/fotos_pedidos/7-6-117-lui3m9b1z-1.jpeg	a	a	f	331
1339	client/img/fotos_pedidos/fotos_pedidos/7-6-117-l22v3v92d-0.jpeg	a	a	f	331
1340	client/img/fotos_pedidos/fotos_pedidos/7-6-117-e89a6sgxh-3.jpeg	a	a	f	331
1344	client/img/fotos_pedidos/fotos_pedidos/7-6-117-nhihgnmhe-7.jpeg	a	a	f	331
1774	client/img/fotos_pedidos/fotos_pedidos/8-2-117-8ztr12gym-0.jpeg	a	a	f	382
1775	client/img/fotos_pedidos/fotos_pedidos/8-2-117-zryifza14-0.jpeg	a	a	f	382
1776	client/img/fotos_pedidos/fotos_pedidos/8-2-117-n6engxval-0.jpeg	a	a	f	382
1986	client/img/fotos_pedidos/fotos_pedidos/9-1-117-9zbiv0ov0-3.jpeg	a	a	f	412
1987	client/img/fotos_pedidos/fotos_pedidos/9-1-117-rpy8901vi-2.jpeg	a	a	f	412
1988	client/img/fotos_pedidos/fotos_pedidos/9-1-117-b5pgsl8s1-1.jpeg	a	a	f	412
1989	client/img/fotos_pedidos/fotos_pedidos/9-1-117-m8fhsenyk-0.jpeg	a	a	f	412
1990	client/img/fotos_pedidos/fotos_pedidos/9-1-117-z7uf1jilq-6.jpeg	a	a	f	412
1991	client/img/fotos_pedidos/fotos_pedidos/9-1-117-wx73j3huz-5.jpeg	a	a	f	412
1992	client/img/fotos_pedidos/fotos_pedidos/9-1-117-cd9edbqj8-4.jpeg	a	a	f	412
1993	client/img/fotos_pedidos/fotos_pedidos/9-1-117-nomccb5iz-7.jpeg	a	a	f	412
1995	client/img/fotos_pedidos/fotos_pedidos/9-1-117-bntjvu61c-12.jpeg	a	a	f	412
1997	client/img/fotos_pedidos/fotos_pedidos/9-1-117-79i0n26g6-9.jpeg	a	a	f	412
1999	client/img/fotos_pedidos/fotos_pedidos/9-1-117-q9jesg6ax-10.jpeg	a	a	f	412
2001	client/img/fotos_pedidos/fotos_pedidos/9-1-117-5uqup1e38-15.jpeg	a	a	f	412
2003	client/img/fotos_pedidos/fotos_pedidos/9-1-117-4czanwwdd-17.jpeg	a	a	f	412
2005	client/img/fotos_pedidos/fotos_pedidos/9-1-117-cfxxiemkp-19.jpeg	a	a	f	412
2079	client/img/fotos_pedidos/fotos_pedidos/9-0-117-qqyi1uvkg-6.jpeg	a	a	f	427
2164	client/img/fotos_pedidos/fotos_pedidos/9-5-117-do5f6x9n6-0.jpeg	a	a	f	436
2251	client/img/fotos_pedidos/fotos_pedidos/9-2-117-rrsczizb5-5.jpeg	a	a	f	451
2370	client/img/fotos_pedidos/fotos_pedidos/10-0-117-1i92t7mov-5.jpeg	a	a	f	462
2373	client/img/fotos_pedidos/fotos_pedidos/10-0-117-qsoopbrru-9.jpeg	a	a	f	462
2444	client/img/fotos_pedidos/fotos_pedidos/10-4-117-bm5g7t7fm-0.jpeg	a	a	f	471
2548	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0wg7hmyhw-6.jpeg	a	a	f	480
2554	client/img/fotos_pedidos/fotos_pedidos/10-2-117-kybglklzl-4.jpeg	a	a	f	480
2560	client/img/fotos_pedidos/fotos_pedidos/10-2-117-8lomvvb2g-5.jpeg	a	a	f	480
2655	client/img/fotos_pedidos/fotos_pedidos/10-2-117-ooek9t0lp-0.jpeg	a	a	f	492
2658	client/img/fotos_pedidos/fotos_pedidos/10-2-117-kx5rzcr0u-0.jpeg	a	a	f	492
2661	client/img/fotos_pedidos/fotos_pedidos/10-2-117-z1li4u1nu-0.jpeg	a	a	f	492
2722	client/img/fotos_pedidos/fotos_pedidos/11-4-117-paqk5h7j6-2.jpeg	a	a	f	507
2724	client/img/fotos_pedidos/fotos_pedidos/11-4-117-aetltgq6u-3.jpeg	a	a	f	507
2726	client/img/fotos_pedidos/fotos_pedidos/11-4-117-2ogvsc95n-7.jpeg	a	a	f	507
2730	client/img/fotos_pedidos/fotos_pedidos/11-4-117-o8dfpadbi-10.jpeg	a	a	f	507
2734	client/img/fotos_pedidos/fotos_pedidos/11-4-117-aof2jpd2i-1.jpeg	a	a	f	507
2738	client/img/fotos_pedidos/fotos_pedidos/11-4-117-x5c720n49-5.jpeg	a	a	f	507
2742	client/img/fotos_pedidos/fotos_pedidos/11-4-117-juyt8l7fj-1.jpeg	a	a	f	507
2860	client/img/fotos_pedidos/fotos_pedidos/11-4-117-fe4es83vx-4.jpeg	a	a	f	517
2864	client/img/fotos_pedidos/fotos_pedidos/11-4-117-u75y980w3-8.jpeg	a	a	f	517
2868	client/img/fotos_pedidos/fotos_pedidos/11-4-117-k06otg29v-2.jpeg	a	a	f	517
2872	client/img/fotos_pedidos/fotos_pedidos/11-4-117-72h9pw0md-7.jpeg	a	a	f	517
2876	client/img/fotos_pedidos/fotos_pedidos/11-4-117-qnodhigpx-1.jpeg	a	a	f	517
2962	client/img/fotos_pedidos/fotos_pedidos/11-2-117-t1um9vmq3-2.png	a	a	f	526
3044	client/img/fotos_pedidos/fotos_pedidos/11-3-117-v3gsxlv5m-5.jpeg	a	a	f	535
3049	client/img/fotos_pedidos/fotos_pedidos/11-3-117-wedgajvhw-8.jpeg	a	a	f	535
3055	client/img/fotos_pedidos/fotos_pedidos/11-3-117-bnqb2wclb-6.jpeg	a	a	f	535
3061	client/img/fotos_pedidos/fotos_pedidos/11-3-117-4mw0bvn5o-2.jpeg	a	a	f	535
3067	client/img/fotos_pedidos/fotos_pedidos/11-3-117-a8twur1rm-8.jpeg	a	a	f	535
3170	client/img/fotos_pedidos/fotos_pedidos/11-4-117-znv4o2bt5-5.jpeg	a	a	f	543
3172	client/img/fotos_pedidos/fotos_pedidos/11-4-117-8buyglke9-7.jpeg	a	a	f	543
3174	client/img/fotos_pedidos/fotos_pedidos/11-4-117-owt6ln285-9.jpeg	a	a	f	543
3176	client/img/fotos_pedidos/fotos_pedidos/11-4-117-z8d7nzzmb-1.jpeg	a	a	f	543
3256	client/img/fotos_pedidos/fotos_pedidos/11-4-117-l9t8a9jdx-0.jpeg	a	a	f	546
3277	client/img/fotos_pedidos/fotos_pedidos/11-2-117-y20zfn1zy-0.jpeg	a	a	f	551
3279	client/img/fotos_pedidos/fotos_pedidos/11-2-117-d7twtomk6-0.jpeg	a	a	f	551
3281	client/img/fotos_pedidos/fotos_pedidos/11-2-117-27xjim8fz-1.jpeg	a	a	f	551
3283	client/img/fotos_pedidos/fotos_pedidos/11-2-117-insk17cqc-3.jpeg	a	a	f	551
3285	client/img/fotos_pedidos/fotos_pedidos/11-2-117-6h0c8tybf-5.jpeg	a	a	f	551
3287	client/img/fotos_pedidos/fotos_pedidos/11-2-117-kg90zowss-7.jpeg	a	a	f	551
3289	client/img/fotos_pedidos/fotos_pedidos/11-2-117-ptp84h994-9.jpeg	a	a	f	551
1482	client/img/fotos_pedidos/fotos_pedidos/8-5-117-317op1141-0.jpeg	a	a	t	349
1652	client/img/fotos_pedidos/fotos_pedidos/8-2-117-zu7kedae8-0.jpeg	Is there anybody in there? Just nod if you can hear me	a	t	366
1156	client/img/fotos_pedidos/fotos_pedidos/7-1-117-q0uqds675-1.jpeg	a	a	f	309
1157	client/img/fotos_pedidos/fotos_pedidos/7-1-117-adhweohi8-0.jpeg	a	a	f	309
1158	client/img/fotos_pedidos/fotos_pedidos/7-1-117-12yi55x6g-2.jpeg	a	a	f	309
1160	client/img/fotos_pedidos/fotos_pedidos/7-1-117-qivkby9lx-4.png	a	a	f	309
1162	client/img/fotos_pedidos/fotos_pedidos/7-1-117-nqlo4122q-6.png	a	a	f	309
1164	client/img/fotos_pedidos/fotos_pedidos/7-1-117-voprhzknh-8.png	a	a	f	309
1777	client/img/fotos_pedidos/fotos_pedidos/8-2-117-pwaxzywui-0.jpeg	a	a	f	383
1778	client/img/fotos_pedidos/fotos_pedidos/8-2-117-dwbla12su-0.jpeg	a	a	f	383
1779	client/img/fotos_pedidos/fotos_pedidos/8-2-117-j0aiwnqhq-0.jpeg	a	a	f	383
1341	client/img/fotos_pedidos/fotos_pedidos/7-6-117-lcswy7m1u-4.jpeg	a	a	f	331
1345	client/img/fotos_pedidos/fotos_pedidos/7-6-117-0pvpjiex9-8.jpeg	a	a	f	331
1994	client/img/fotos_pedidos/fotos_pedidos/9-1-117-m6cg18beg-8.jpeg	a	a	f	412
1996	client/img/fotos_pedidos/fotos_pedidos/9-1-117-7yh6g82oi-13.jpeg	a	a	f	412
1998	client/img/fotos_pedidos/fotos_pedidos/9-1-117-i9jc0y93h-11.jpeg	a	a	f	412
2000	client/img/fotos_pedidos/fotos_pedidos/9-1-117-xfdelfjk3-14.jpeg	a	a	f	412
2002	client/img/fotos_pedidos/fotos_pedidos/9-1-117-mhbbsnxc8-16.jpeg	a	a	f	412
2004	client/img/fotos_pedidos/fotos_pedidos/9-1-117-xprvmsoyq-18.jpeg	a	a	f	412
2080	client/img/fotos_pedidos/fotos_pedidos/9-0-117-q06e2mz4w-7.jpeg	a	a	f	427
2165	client/img/fotos_pedidos/fotos_pedidos/9-6-117-uen9yo9pv-0.jpeg	a	a	f	437
2166	client/img/fotos_pedidos/fotos_pedidos/9-6-117-swypolw2j-1.jpeg	a	a	f	437
2167	client/img/fotos_pedidos/fotos_pedidos/9-6-117-cnqnnaonm-2.jpeg	a	a	f	437
2168	client/img/fotos_pedidos/fotos_pedidos/9-6-117-6wyikwvct-4.jpeg	a	a	f	437
2174	client/img/fotos_pedidos/fotos_pedidos/9-6-117-dnkyx0esq-0.jpeg	a	a	f	437
2252	client/img/fotos_pedidos/fotos_pedidos/9-2-117-x57z83r3c-6.jpeg	a	a	f	451
2375	client/img/fotos_pedidos/fotos_pedidos/10-0-117-vg3lmj0mu-2.jpeg	a	a	f	463
2377	client/img/fotos_pedidos/fotos_pedidos/10-0-117-gcgovi58z-0.jpeg	a	a	f	463
2381	client/img/fotos_pedidos/fotos_pedidos/10-0-117-2m38nako2-5.jpeg	a	a	f	463
2445	client/img/fotos_pedidos/fotos_pedidos/10-4-117-v2zz88p3w-1.jpeg	a	a	f	471
2549	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0c8hzbeki-7.jpeg	a	a	f	480
2555	client/img/fotos_pedidos/fotos_pedidos/10-2-117-gq652zuvo-0.jpeg	a	a	f	480
2561	client/img/fotos_pedidos/fotos_pedidos/10-2-117-e9iqy09a6-6.jpeg	a	a	f	480
2660	client/img/fotos_pedidos/fotos_pedidos/10-2-117-pg7h3lb4o-0.jpeg	a	a	f	492
2663	client/img/fotos_pedidos/fotos_pedidos/10-2-117-yu1n2540k-0.jpeg	a	a	f	492
2727	client/img/fotos_pedidos/fotos_pedidos/11-4-117-vwyxvvhwa-5.jpeg	a	a	f	507
2731	client/img/fotos_pedidos/fotos_pedidos/11-4-117-hjxqks8vb-11.jpeg	a	a	f	507
2735	client/img/fotos_pedidos/fotos_pedidos/11-4-117-8pthai2gu-2.jpeg	a	a	f	507
2739	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ufy248ivv-6.jpeg	a	a	f	507
2743	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ffqnjday3-2.jpeg	a	a	f	507
2861	client/img/fotos_pedidos/fotos_pedidos/11-4-117-1ynieicja-5.jpeg	a	a	f	517
2865	client/img/fotos_pedidos/fotos_pedidos/11-4-117-6k1c9xbpe-9.jpeg	a	a	f	517
2869	client/img/fotos_pedidos/fotos_pedidos/11-4-117-v01fapoqj-4.jpeg	a	a	f	517
2873	client/img/fotos_pedidos/fotos_pedidos/11-4-117-087lf2ecw-9.jpeg	a	a	f	517
2878	client/img/fotos_pedidos/fotos_pedidos/11-4-117-28q53gi29-0.jpeg	a	a	f	517
2965	client/img/fotos_pedidos/fotos_pedidos/11-2-117-10i0lf8qg-0.jpeg	a	a	f	527
2967	client/img/fotos_pedidos/fotos_pedidos/11-2-117-ww1suqepg-2.jpeg	a	a	f	527
2969	client/img/fotos_pedidos/fotos_pedidos/11-2-117-hprn2tctb-4.jpeg	a	a	f	527
2973	client/img/fotos_pedidos/fotos_pedidos/11-2-117-z0n5voznq-8.jpeg	a	a	f	527
2978	client/img/fotos_pedidos/fotos_pedidos/11-2-117-2ifxbnh96-14.jpeg	a	a	f	527
3048	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ka66vms8v-0.jpeg	a	a	f	535
3054	client/img/fotos_pedidos/fotos_pedidos/11-3-117-pe96gqpv9-5.jpeg	a	a	f	535
3060	client/img/fotos_pedidos/fotos_pedidos/11-3-117-j4dc84zc7-1.jpeg	a	a	f	535
3066	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6b6h3cdh1-7.jpeg	a	a	f	535
3171	client/img/fotos_pedidos/fotos_pedidos/11-4-117-xyiq40jz8-6.jpeg	a	a	f	543
3173	client/img/fotos_pedidos/fotos_pedidos/11-4-117-e8046v0uj-8.jpeg	a	a	f	543
3175	client/img/fotos_pedidos/fotos_pedidos/11-4-117-z28f58deo-0.jpeg	a	a	f	543
3177	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cjpwc4e74-2.jpeg	a	a	f	543
3257	client/img/fotos_pedidos/fotos_pedidos/11-4-117-1h9x5le2x-1.jpeg	a	a	f	546
3290	client/img/fotos_pedidos/fotos_pedidos/11-2-117-y8za8f0ws-0.jpeg	a	a	f	552
3318	client/img/fotos_pedidos/fotos_pedidos/11-3-117-mva960g8p-0.jpeg	a	a	f	556
3319	client/img/fotos_pedidos/fotos_pedidos/11-3-117-3pfakf81m-2.jpeg	a	a	f	556
3320	client/img/fotos_pedidos/fotos_pedidos/11-3-117-cg1ir9ps6-1.jpeg	a	a	f	556
3321	client/img/fotos_pedidos/fotos_pedidos/11-3-117-lbm9sq8ne-3.jpeg	a	a	f	556
3323	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7eocqlbfg-4.jpeg	a	a	f	556
3325	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6rvv5cnz2-7.jpeg	a	a	f	556
3327	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8ce2b03v6-8.jpeg	a	a	f	556
3337	client/img/fotos_pedidos/fotos_pedidos/11-3-117-j9t50abgr-0.jpeg	a	a	f	566
3340	client/img/fotos_pedidos/fotos_pedidos/11-3-117-5608gvm3i-0.png	a	a	f	565
3346	client/img/fotos_pedidos/fotos_pedidos/11-4-117-dspmb8e62-0.jpeg	a	a	f	571
3353	client/img/fotos_pedidos/fotos_pedidos/12-6-117-6d0mvgvyk-4.jpeg	a	a	t	574
3358	client/img/fotos_pedidos/fotos_pedidos/12-6-117-0vp7stlry-8.jpeg	a	a	t	574
3359	client/img/fotos_pedidos/fotos_pedidos/12-0-117-l8zgp2x9l-0.jpeg	a	a	f	575
3360	client/img/fotos_pedidos/fotos_pedidos/12-0-117-m4hqstokl-0.jpeg	a	a	f	575
1483	client/img/fotos_pedidos/fotos_pedidos/8-5-117-4f0cuisvm-0.jpeg	a	a	f	350
1653	client/img/fotos_pedidos/fotos_pedidos/8-3-117-iazuuot14-0.jpeg	a	a	f	367
1159	client/img/fotos_pedidos/fotos_pedidos/7-1-117-bx68j9y4c-3.jpeg	a	a	f	309
1161	client/img/fotos_pedidos/fotos_pedidos/7-1-117-gcpmbcjbp-5.png	a	a	f	309
1163	client/img/fotos_pedidos/fotos_pedidos/7-1-117-wmhyakbsg-7.jpeg	a	a	f	309
1654	client/img/fotos_pedidos/fotos_pedidos/8-3-117-6b4vqspx2-0.jpeg	a	a	f	368
1780	client/img/fotos_pedidos/fotos_pedidos/8-2-117-s6j5l4uxm-0.jpeg	Abuelita:\nTe envío esta foto que tomé en mi viaje porque mientras caminaba entre santos e iglesias sólo pude pensarte. Te amo. Denisse.	a	t	384
1342	client/img/fotos_pedidos/fotos_pedidos/7-6-117-91isqpn3z-5.jpeg	a	a	f	331
2006	client/img/fotos_pedidos/fotos_pedidos/9-2-117-94t0m27a2-0.jpeg	a	a	f	413
2081	client/img/fotos_pedidos/fotos_pedidos/9-2-117-6q7fm03q0-0.jpeg	a	a	f	428
2082	client/img/fotos_pedidos/fotos_pedidos/9-2-117-bziedo1to-0.jpeg	a	a	f	428
2169	client/img/fotos_pedidos/fotos_pedidos/9-6-117-5kxcnlxfg-3.jpeg	a	a	f	437
2253	client/img/fotos_pedidos/fotos_pedidos/9-2-117-y35zo65a3-7.jpeg	a	a	f	451
2376	client/img/fotos_pedidos/fotos_pedidos/10-0-117-7rr3gs8ix-3.jpeg	a	a	f	463
2379	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mvjxjkawp-6.jpeg	a	a	f	463
2383	client/img/fotos_pedidos/fotos_pedidos/10-0-117-8ujkm7ey4-0.jpeg	a	a	f	463
2450	client/img/fotos_pedidos/fotos_pedidos/10-4-117-0nrfl3id4-0.jpeg	a	a	f	472
2455	client/img/fotos_pedidos/fotos_pedidos/10-4-117-bnd8qsvmt-3.jpeg	a	a	f	472
2551	client/img/fotos_pedidos/fotos_pedidos/10-2-117-zy6hbzf7j-1.jpeg	a	a	f	480
2556	client/img/fotos_pedidos/fotos_pedidos/10-2-117-62nfbh4ym-1.jpeg	a	a	f	480
2664	client/img/fotos_pedidos/fotos_pedidos/10-2-117-t04grihr5-0.jpeg	a	a	t	493
2666	client/img/fotos_pedidos/fotos_pedidos/10-2-117-5tbnwu8kb-0.jpeg	a	a	t	493
2668	client/img/fotos_pedidos/fotos_pedidos/10-2-117-q1w7hxjjv-0.jpeg	a	a	t	493
2670	client/img/fotos_pedidos/fotos_pedidos/10-2-117-a8vr2gj83-0.jpeg	a	a	t	493
2672	client/img/fotos_pedidos/fotos_pedidos/10-2-117-p2md1651r-0.jpeg	a	a	t	493
2728	client/img/fotos_pedidos/fotos_pedidos/11-4-117-60af4jbod-6.jpeg	a	a	f	507
2732	client/img/fotos_pedidos/fotos_pedidos/11-4-117-4qvlohm8x-9.jpeg	a	a	f	507
2736	client/img/fotos_pedidos/fotos_pedidos/11-4-117-94nih7ltu-3.jpeg	a	a	f	507
2740	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ifwfzjb79-7.jpeg	a	a	f	507
2744	client/img/fotos_pedidos/fotos_pedidos/11-4-117-zerxbjffl-3.jpeg	a	a	f	507
2863	client/img/fotos_pedidos/fotos_pedidos/11-4-117-h3jlbhwur-7.jpeg	a	a	f	517
2867	client/img/fotos_pedidos/fotos_pedidos/11-4-117-csjngyh5q-1.jpeg	a	a	f	517
2871	client/img/fotos_pedidos/fotos_pedidos/11-4-117-5m20n1dnr-5.jpeg	a	a	f	517
2875	client/img/fotos_pedidos/fotos_pedidos/11-4-117-22gwwbr9i-8.jpeg	a	a	f	517
2879	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ac1juq465-3.jpeg	a	a	f	517
2966	client/img/fotos_pedidos/fotos_pedidos/11-2-117-2xof7b2nl-1.jpeg	a	a	f	527
2968	client/img/fotos_pedidos/fotos_pedidos/11-2-117-yztc3v3zc-3.jpeg	a	a	f	527
2970	client/img/fotos_pedidos/fotos_pedidos/11-2-117-kxy59ehen-5.jpeg	a	a	f	527
2974	client/img/fotos_pedidos/fotos_pedidos/11-2-117-a4th1s8pe-9.jpeg	a	a	f	527
2979	client/img/fotos_pedidos/fotos_pedidos/11-2-117-3vahdg82p-13.jpeg	a	a	f	527
3050	client/img/fotos_pedidos/fotos_pedidos/11-3-117-46m65yarm-1.jpeg	a	a	f	535
3056	client/img/fotos_pedidos/fotos_pedidos/11-3-117-eya4ey1et-7.jpeg	a	a	f	535
3062	client/img/fotos_pedidos/fotos_pedidos/11-3-117-oc0iml2n9-3.jpeg	a	a	f	535
3068	client/img/fotos_pedidos/fotos_pedidos/11-3-117-nafvpif91-9.jpeg	a	a	f	535
3178	client/img/fotos_pedidos/fotos_pedidos/11-4-117-iewr1wsap-0.jpeg	a	a	f	544
3179	client/img/fotos_pedidos/fotos_pedidos/11-4-117-7zj8k7zl1-1.jpeg	a	a	f	544
3180	client/img/fotos_pedidos/fotos_pedidos/11-4-117-5pi7fhvpq-2.jpeg	a	a	f	544
3184	client/img/fotos_pedidos/fotos_pedidos/11-4-117-v7drp9h8j-6.jpeg	a	a	f	544
3190	client/img/fotos_pedidos/fotos_pedidos/11-4-117-mroerjlw2-2.jpeg	a	a	f	544
3196	client/img/fotos_pedidos/fotos_pedidos/11-4-117-guulkgcyv-8.jpeg	a	a	f	544
3202	client/img/fotos_pedidos/fotos_pedidos/11-4-117-k6vl9rsmu-4.jpeg	a	a	f	544
3208	client/img/fotos_pedidos/fotos_pedidos/11-4-117-n7ndrdttf-0.jpeg	a	a	f	544
3214	client/img/fotos_pedidos/fotos_pedidos/11-4-117-et1o5hwvb-6.jpeg	a	a	f	544
3220	client/img/fotos_pedidos/fotos_pedidos/11-4-117-wdskejzzr-2.jpeg	a	a	f	544
3226	client/img/fotos_pedidos/fotos_pedidos/11-4-117-s6ru4zog0-8.jpeg	a	a	f	544
3232	client/img/fotos_pedidos/fotos_pedidos/11-4-117-9npv0qkmi-4.jpeg	a	a	f	544
3260	client/img/fotos_pedidos/fotos_pedidos/11-5-117-dw85vwxpd-0.jpeg	a	a	f	547
3261	client/img/fotos_pedidos/fotos_pedidos/11-5-117-p78xswayu-0.jpeg	a	a	f	547
3262	client/img/fotos_pedidos/fotos_pedidos/11-5-117-1t3kmdosf-0.jpeg	a	a	f	547
3291	client/img/fotos_pedidos/fotos_pedidos/11-2-117-ap6gj8o8v-1.jpeg	a	a	f	553
3292	client/img/fotos_pedidos/fotos_pedidos/11-2-117-fmkzgu8rl-3.jpeg	a	a	f	553
3293	client/img/fotos_pedidos/fotos_pedidos/11-2-117-6l87333nc-0.jpeg	a	a	f	553
3295	client/img/fotos_pedidos/fotos_pedidos/11-2-117-kw5ekmk6c-4.jpeg	a	a	f	553
3297	client/img/fotos_pedidos/fotos_pedidos/11-2-117-m2udkethh-7.jpeg	a	a	f	553
3299	client/img/fotos_pedidos/fotos_pedidos/11-2-117-ak0o5au1u-8.jpeg	a	a	f	553
3301	client/img/fotos_pedidos/fotos_pedidos/11-2-117-8bvu7ef9j-10.jpeg	a	a	f	553
3304	client/img/fotos_pedidos/fotos_pedidos/11-2-117-cr3pixlrx-13.jpeg	a	a	f	553
3307	client/img/fotos_pedidos/fotos_pedidos/11-2-117-2b66qr8jx-16.jpeg	a	a	f	553
3310	client/img/fotos_pedidos/fotos_pedidos/11-2-117-nsz2sya2x-19.jpeg	a	a	f	553
1484	client/img/fotos_pedidos/fotos_pedidos/8-5-117-a42yo49wd-0.jpeg	a	a	f	351
1165	client/img/fotos_pedidos/fotos_pedidos/7-1-117-ivgjczk9d-0.jpeg	a	a	t	310
1167	client/img/fotos_pedidos/fotos_pedidos/7-1-117-msl7k0c4m-0.jpeg	a	a	t	310
1171	client/img/fotos_pedidos/fotos_pedidos/7-1-117-rl66owr7n-0.jpeg	a	a	t	310
1175	client/img/fotos_pedidos/fotos_pedidos/7-1-117-yhqoasfg1-0.jpeg	a	a	t	310
1247	client/img/fotos_pedidos/fotos_pedidos/7-1-117-5qo9kv2f8-0.jpeg	a	a	f	322
1248	client/img/fotos_pedidos/fotos_pedidos/7-1-117-8p654sgz6-1.jpeg	a	a	f	322
1249	client/img/fotos_pedidos/fotos_pedidos/7-1-117-wsqp407jl-2.jpeg	a	a	f	322
1251	client/img/fotos_pedidos/fotos_pedidos/7-1-117-tfn0bq1rg-4.jpeg	a	a	f	322
1255	client/img/fotos_pedidos/fotos_pedidos/7-1-117-c87r7ho4p-8.jpeg	a	a	f	322
1343	client/img/fotos_pedidos/fotos_pedidos/7-6-117-svsdgqeib-6.jpeg	a	a	f	331
1346	client/img/fotos_pedidos/fotos_pedidos/7-6-117-zi9b0ddjh-0.jpeg	a	a	f	331
1781	client/img/fotos_pedidos/fotos_pedidos/8-4-117-db4rf2dh8-0.jpeg	a	a	t	385
1782	client/img/fotos_pedidos/fotos_pedidos/8-4-117-aimdft4kp-0.jpeg	a	a	f	386
1901	client/img/fotos_pedidos/fotos_pedidos/9-0-117-tk6b8fwpk-0.jpeg	a	a	f	400
1903	client/img/fotos_pedidos/fotos_pedidos/9-0-117-7wan1rouk-2.jpeg	a	a	f	400
2007	client/img/fotos_pedidos/fotos_pedidos/9-2-117-vlwun6qg4-0.jpeg	a	a	f	413
2083	client/img/fotos_pedidos/fotos_pedidos/9-2-117-5qpkyhop6-0.jpeg	a	a	f	429
2084	client/img/fotos_pedidos/fotos_pedidos/9-2-117-6johxd2iv-0.png	a	a	f	429
2085	client/img/fotos_pedidos/fotos_pedidos/9-2-117-g5w720t0a-0.jpeg	a	a	f	429
2170	client/img/fotos_pedidos/fotos_pedidos/9-6-117-vguowzb9p-0.jpeg	a	a	f	437
2254	client/img/fotos_pedidos/fotos_pedidos/9-2-117-9eob50xgl-8.jpeg	a	a	f	451
2378	client/img/fotos_pedidos/fotos_pedidos/10-0-117-z07s77rx9-7.jpeg	a	a	f	463
2382	client/img/fotos_pedidos/fotos_pedidos/10-0-117-4b2uhepn4-1.png	a	a	f	463
2451	client/img/fotos_pedidos/fotos_pedidos/10-4-117-h62fzfru0-2.jpeg	a	a	f	472
2453	client/img/fotos_pedidos/fotos_pedidos/10-4-117-h8huhil5w-4.jpeg	a	a	f	472
2456	client/img/fotos_pedidos/fotos_pedidos/10-4-117-yrivca330-6.jpeg	a	a	f	472
2562	client/img/fotos_pedidos/fotos_pedidos/10-2-117-o9s41rzub-0.jpeg	a	a	f	481
2563	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0oxfm9v9r-1.jpeg	a	a	f	481
2564	client/img/fotos_pedidos/fotos_pedidos/10-2-117-ng3iu5rcw-2.jpeg	a	a	f	481
2565	client/img/fotos_pedidos/fotos_pedidos/10-2-117-425urlg4z-3.jpeg	a	a	f	481
2566	client/img/fotos_pedidos/fotos_pedidos/10-2-117-w3ts48u87-4.jpeg	a	a	f	481
2567	client/img/fotos_pedidos/fotos_pedidos/10-2-117-pc0e72p93-5.jpeg	a	a	f	481
2568	client/img/fotos_pedidos/fotos_pedidos/10-2-117-htgraxw5c-6.jpeg	a	a	f	481
2569	client/img/fotos_pedidos/fotos_pedidos/10-2-117-g89lcutxx-7.jpeg	a	a	f	481
2570	client/img/fotos_pedidos/fotos_pedidos/10-2-117-vkh0xev2k-8.jpeg	a	a	f	481
2571	client/img/fotos_pedidos/fotos_pedidos/10-2-117-8mkuxrdhh-9.jpeg	a	a	f	481
2665	client/img/fotos_pedidos/fotos_pedidos/10-2-117-shjhgfpej-0.jpeg	a	a	t	493
2667	client/img/fotos_pedidos/fotos_pedidos/10-2-117-0opi7b8a0-0.jpeg	a	a	t	493
2669	client/img/fotos_pedidos/fotos_pedidos/10-2-117-oqfxcb6po-0.jpeg	a	a	t	493
2671	client/img/fotos_pedidos/fotos_pedidos/10-2-117-z3jxj6dcz-0.jpeg	a	a	t	493
2673	client/img/fotos_pedidos/fotos_pedidos/10-2-117-bfq3lpbgk-0.jpeg	a	a	t	493
2745	client/img/fotos_pedidos/fotos_pedidos/11-5-117-9x89unsa0-0.jpeg	a	a	f	508
2749	client/img/fotos_pedidos/fotos_pedidos/11-5-117-4fae21hay-2.jpeg	a	a	f	508
2753	client/img/fotos_pedidos/fotos_pedidos/11-5-117-vysh7bwon-6.jpeg	a	a	f	508
2757	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ij20p1ku4-0.jpeg	a	a	f	508
2761	client/img/fotos_pedidos/fotos_pedidos/11-5-117-2x4uwqnve-4.jpeg	a	a	f	508
2765	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ur2f79xje-8.jpeg	a	a	f	508
2769	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ip43es850-2.jpeg	a	a	f	508
2773	client/img/fotos_pedidos/fotos_pedidos/11-5-117-t0v6bxfmw-6.jpeg	a	a	f	508
2777	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g6qizqoly-0.jpeg	a	a	f	508
2781	client/img/fotos_pedidos/fotos_pedidos/11-5-117-6ddkg6blf-3.jpeg	a	a	f	510
2785	client/img/fotos_pedidos/fotos_pedidos/11-5-117-iob3lcgcw-7.jpeg	a	a	f	510
2789	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ud9s8subx-10.jpeg	a	a	f	510
2793	client/img/fotos_pedidos/fotos_pedidos/11-5-117-k5afvbpdj-13.jpeg	a	a	f	510
2797	client/img/fotos_pedidos/fotos_pedidos/11-5-117-xa8rzi27x-18.jpeg	a	a	f	510
2801	client/img/fotos_pedidos/fotos_pedidos/11-5-117-4ovk3c0eg-22.jpeg	a	a	f	510
2805	client/img/fotos_pedidos/fotos_pedidos/11-5-117-jb2xh8vxe-1.jpeg	a	a	f	510
2809	client/img/fotos_pedidos/fotos_pedidos/11-5-117-99pf93l8j-2.jpeg	a	a	f	510
2880	client/img/fotos_pedidos/fotos_pedidos/11-4-117-1238sgmv1-0.jpeg	a	a	f	518
2971	client/img/fotos_pedidos/fotos_pedidos/11-2-117-i27k5wyh6-6.jpeg	a	a	f	527
2976	client/img/fotos_pedidos/fotos_pedidos/11-2-117-nxap9p6si-11.jpeg	a	a	f	527
3064	client/img/fotos_pedidos/fotos_pedidos/11-3-117-xokzisbeu-5.jpeg	a	a	f	535
3070	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ubwle2zi8-1.jpeg	a	a	f	535
3181	client/img/fotos_pedidos/fotos_pedidos/11-4-117-jhiniqhkj-3.jpeg	a	a	f	544
3185	client/img/fotos_pedidos/fotos_pedidos/11-4-117-jdxw4un5h-7.jpeg	a	a	f	544
3191	client/img/fotos_pedidos/fotos_pedidos/11-4-117-zwzutkusr-3.jpeg	a	a	f	544
3197	client/img/fotos_pedidos/fotos_pedidos/11-4-117-8a8dn6a4i-9.jpeg	a	a	f	544
3203	client/img/fotos_pedidos/fotos_pedidos/11-4-117-n060e5pao-5.jpeg	a	a	f	544
3209	client/img/fotos_pedidos/fotos_pedidos/11-4-117-i7he28oms-1.jpeg	a	a	f	544
3215	client/img/fotos_pedidos/fotos_pedidos/11-4-117-gc1rccgxk-7.jpeg	a	a	f	544
3221	client/img/fotos_pedidos/fotos_pedidos/11-4-117-t7g67x50c-3.jpeg	a	a	f	544
1084	client/img/fotos_pedidos/fotos_pedidos/7-3-117-blro5f4zk-1.jpeg	a	a	f	300
1085	client/img/fotos_pedidos/fotos_pedidos/7-3-117-ud1173umt-0.jpeg	a	a	f	300
1086	client/img/fotos_pedidos/fotos_pedidos/7-3-117-rm26rxfdg-2.jpeg	a	a	f	300
1089	client/img/fotos_pedidos/fotos_pedidos/7-3-117-8i20efoj2-5.jpeg	a	a	f	300
1094	client/img/fotos_pedidos/fotos_pedidos/7-3-117-o6jh0uwfc-1.png	a	a	f	300
1166	client/img/fotos_pedidos/fotos_pedidos/7-1-117-b8d49ykeg-0.jpeg	a	a	t	310
1170	client/img/fotos_pedidos/fotos_pedidos/7-1-117-4txmo6i24-0.jpeg	a	a	t	310
1174	client/img/fotos_pedidos/fotos_pedidos/7-1-117-3r43c8oms-0.jpeg	a	a	t	310
1250	client/img/fotos_pedidos/fotos_pedidos/7-1-117-2rz09b96k-3.jpeg	a	a	f	322
1252	client/img/fotos_pedidos/fotos_pedidos/7-1-117-m5afswtiv-5.jpeg	a	a	f	322
1258	client/img/fotos_pedidos/fotos_pedidos/7-1-117-uabmuid7q-11.jpeg	a	a	f	322
1485	client/img/fotos_pedidos/fotos_pedidos/8-5-117-2rtrh1m92-0.jpeg	a	a	f	352
1658	client/img/fotos_pedidos/fotos_pedidos/8-3-117-q1c49i0ik-0.jpeg	a	a	f	370
1660	client/img/fotos_pedidos/fotos_pedidos/8-3-117-67j7v86c2-0.jpeg	a	a	f	370
1783	client/img/fotos_pedidos/fotos_pedidos/8-4-117-rqwbpnzc1-1.jpeg	a	a	f	386
1902	client/img/fotos_pedidos/fotos_pedidos/9-0-117-9kdcc8sib-1.jpeg	a	a	f	400
2008	client/img/fotos_pedidos/fotos_pedidos/9-3-117-czp6w70d5-3.jpeg	a	a	f	414
2009	client/img/fotos_pedidos/fotos_pedidos/9-3-117-pjaws7m08-2.jpeg	a	a	f	414
2010	client/img/fotos_pedidos/fotos_pedidos/9-3-117-1fao2cnpp-0.jpeg	a	a	f	414
2012	client/img/fotos_pedidos/fotos_pedidos/9-3-117-3wo19xmue-6.jpeg	a	a	f	414
2014	client/img/fotos_pedidos/fotos_pedidos/9-3-117-pihzx0ahn-8.jpeg	a	a	f	414
2016	client/img/fotos_pedidos/fotos_pedidos/9-3-117-5z81hfq6h-4.jpeg	a	a	f	414
2086	client/img/fotos_pedidos/fotos_pedidos/9-2-117-z77n6f1x3-0.jpeg	a	a	t	430
2087	client/img/fotos_pedidos/fotos_pedidos/9-2-117-b89wnz636-0.jpeg	a	a	t	430
2171	client/img/fotos_pedidos/fotos_pedidos/9-6-117-i0295esi8-1.jpeg	a	a	f	437
2256	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ku829r6j1-0.jpeg	a	a	f	452
2257	client/img/fotos_pedidos/fotos_pedidos/9-2-117-mqngd84h3-0.jpeg	a	a	f	452
2380	client/img/fotos_pedidos/fotos_pedidos/10-0-117-yz1sbvuak-4.jpeg	a	a	f	463
2452	client/img/fotos_pedidos/fotos_pedidos/10-4-117-t6gxidvaa-1.jpeg	a	a	f	472
2454	client/img/fotos_pedidos/fotos_pedidos/10-4-117-ryrsxz44e-5.jpeg	a	a	f	472
2457	client/img/fotos_pedidos/fotos_pedidos/10-4-117-32l51wqea-7.jpeg	a	a	f	472
2572	client/img/fotos_pedidos/fotos_pedidos/10-2-117-vofr8hajh-0.jpeg	a	a	f	482
2575	client/img/fotos_pedidos/fotos_pedidos/10-2-117-4znepn521-3.jpeg	a	a	f	482
2578	client/img/fotos_pedidos/fotos_pedidos/10-2-117-wf1vucr1i-6.jpeg	a	a	f	482
2581	client/img/fotos_pedidos/fotos_pedidos/10-2-117-00khpvvj1-9.jpeg	a	a	f	482
2584	client/img/fotos_pedidos/fotos_pedidos/10-2-117-knplvnp4u-13.jpeg	a	a	f	482
2587	client/img/fotos_pedidos/fotos_pedidos/10-2-117-3664xmda4-15.jpeg	a	a	f	482
2674	client/img/fotos_pedidos/fotos_pedidos/10-3-117-mx3ctc9g1-0.jpeg	a	a	t	494
2746	client/img/fotos_pedidos/fotos_pedidos/11-5-117-6e8xz0gmb-0.jpeg	a	a	f	509
2750	client/img/fotos_pedidos/fotos_pedidos/11-5-117-mg8i86dpq-5.jpeg	a	a	f	508
2754	client/img/fotos_pedidos/fotos_pedidos/11-5-117-5m46cniyc-7.jpeg	a	a	f	508
2758	client/img/fotos_pedidos/fotos_pedidos/11-5-117-90pkuwv1f-2.jpeg	a	a	f	508
2762	client/img/fotos_pedidos/fotos_pedidos/11-5-117-o37teyj7e-7.jpeg	a	a	f	508
2766	client/img/fotos_pedidos/fotos_pedidos/11-5-117-baut4d0j1-9.jpeg	a	a	f	508
2770	client/img/fotos_pedidos/fotos_pedidos/11-5-117-m70i7sm62-3.jpeg	a	a	f	508
2774	client/img/fotos_pedidos/fotos_pedidos/11-5-117-n8hoxoacu-7.jpeg	a	a	f	508
2778	client/img/fotos_pedidos/fotos_pedidos/11-5-117-1fbmdi7py-0.jpeg	a	a	f	508
2782	client/img/fotos_pedidos/fotos_pedidos/11-5-117-5hw0hfeo8-2.jpeg	a	a	f	510
2786	client/img/fotos_pedidos/fotos_pedidos/11-5-117-3z5m7j3ki-6.jpeg	a	a	f	510
2790	client/img/fotos_pedidos/fotos_pedidos/11-5-117-2v7pfgwv7-11.jpeg	a	a	f	510
2794	client/img/fotos_pedidos/fotos_pedidos/11-5-117-fb2nsygyw-15.jpeg	a	a	f	510
2798	client/img/fotos_pedidos/fotos_pedidos/11-5-117-m08w6990e-19.jpeg	a	a	f	510
2802	client/img/fotos_pedidos/fotos_pedidos/11-5-117-o754rmpj7-23.jpeg	a	a	f	510
2806	client/img/fotos_pedidos/fotos_pedidos/11-5-117-n9541uz7j-2.jpeg	a	a	f	510
2810	client/img/fotos_pedidos/fotos_pedidos/11-5-117-92y8l7esi-3.jpeg	a	a	f	510
2881	client/img/fotos_pedidos/fotos_pedidos/11-4-117-gnrhbi8n9-1.jpeg	a	a	f	518
2972	client/img/fotos_pedidos/fotos_pedidos/11-2-117-rhhl37xrt-7.jpeg	a	a	f	527
2977	client/img/fotos_pedidos/fotos_pedidos/11-2-117-hii00ifsx-12.jpeg	a	a	f	527
3071	client/img/fotos_pedidos/fotos_pedidos/11-3-117-muqknc8x8-1.jpeg	a	a	f	536
3072	client/img/fotos_pedidos/fotos_pedidos/11-3-117-gof1epwfk-0.jpeg	a	a	f	536
3073	client/img/fotos_pedidos/fotos_pedidos/11-3-117-c3cksmzh3-3.jpeg	a	a	f	536
3075	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ecpm48xv3-4.jpeg	a	a	f	536
3079	client/img/fotos_pedidos/fotos_pedidos/11-3-117-my4hxj59d-8.jpeg	a	a	f	536
3085	client/img/fotos_pedidos/fotos_pedidos/11-3-117-rnuoxxatw-5.jpeg	a	a	f	536
3091	client/img/fotos_pedidos/fotos_pedidos/11-3-117-nhwb3of8r-0.jpeg	a	a	f	536
3096	client/img/fotos_pedidos/fotos_pedidos/11-3-117-bnof5m9m1-6.jpeg	a	a	f	536
3101	client/img/fotos_pedidos/fotos_pedidos/11-3-117-eyul0gu61-0.jpeg	a	a	f	536
3182	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cwayyb24u-4.jpeg	a	a	f	544
3186	client/img/fotos_pedidos/fotos_pedidos/11-4-117-e7s2meok7-8.jpeg	a	a	f	544
3192	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ef7nfkiyk-4.jpeg	a	a	f	544
3198	client/img/fotos_pedidos/fotos_pedidos/11-4-117-f722qceie-0.jpeg	a	a	f	544
3204	client/img/fotos_pedidos/fotos_pedidos/11-4-117-mjx9wytaf-6.jpeg	a	a	f	544
3210	client/img/fotos_pedidos/fotos_pedidos/11-4-117-gum7qt3fi-2.jpeg	a	a	f	544
3216	client/img/fotos_pedidos/fotos_pedidos/11-4-117-64rjisb4x-8.jpeg	a	a	f	544
3222	client/img/fotos_pedidos/fotos_pedidos/11-4-117-vde594gf1-4.jpeg	a	a	f	544
1087	client/img/fotos_pedidos/fotos_pedidos/7-3-117-4yx2kwuho-4.jpeg	a	a	f	300
1092	client/img/fotos_pedidos/fotos_pedidos/7-3-117-81uti2qki-0.jpeg	a	a	f	300
1098	client/img/fotos_pedidos/fotos_pedidos/7-3-117-lba05s8eg-5.jpeg	a	a	f	300
1168	client/img/fotos_pedidos/fotos_pedidos/7-1-117-98cz779vn-0.jpeg	a	a	t	310
1172	client/img/fotos_pedidos/fotos_pedidos/7-1-117-17qdovi35-0.jpeg	a	a	t	310
1176	client/img/fotos_pedidos/fotos_pedidos/7-1-117-smfsr9qvg-0.jpeg	a	a	t	310
1253	client/img/fotos_pedidos/fotos_pedidos/7-1-117-4m4ip8yy0-6.jpeg	a	a	f	322
1486	client/img/fotos_pedidos/fotos_pedidos/8-5-117-jgla0ny1m-0.jpeg	a	a	f	353
1487	client/img/fotos_pedidos/fotos_pedidos/8-5-117-vy9f1tn0o-0.jpeg	a	a	f	353
1488	client/img/fotos_pedidos/fotos_pedidos/8-5-117-spn3ag9k4-0.jpeg	a	a	f	353
1490	client/img/fotos_pedidos/fotos_pedidos/8-5-117-yn7x400dc-2.jpeg	a	a	f	353
1493	client/img/fotos_pedidos/fotos_pedidos/8-5-117-d1zse9gv8-5.jpeg	a	a	f	353
1495	client/img/fotos_pedidos/fotos_pedidos/8-5-117-rqqxpvwnc-7.jpeg	a	a	f	353
1659	client/img/fotos_pedidos/fotos_pedidos/8-3-117-7tzwyagwv-0.jpeg	a	a	f	370
1784	client/img/fotos_pedidos/fotos_pedidos/9-1-117-tc76g862m-0.jpeg	a	a	f	387
1786	client/img/fotos_pedidos/fotos_pedidos/9-1-117-yzzmwtktu-0.jpeg	a	a	f	388
2011	client/img/fotos_pedidos/fotos_pedidos/9-3-117-7xmblcgfi-5.jpeg	a	a	f	414
2013	client/img/fotos_pedidos/fotos_pedidos/9-3-117-fswa7g6xw-7.jpeg	a	a	f	414
2015	client/img/fotos_pedidos/fotos_pedidos/9-3-117-b6t2mq1l0-9.jpeg	a	a	f	414
2017	client/img/fotos_pedidos/fotos_pedidos/9-3-117-z2pbbkas5-1.jpeg	a	a	f	414
2088	client/img/fotos_pedidos/fotos_pedidos/9-2-117-45npvmjib-0.jpeg	a	a	f	431
2094	client/img/fotos_pedidos/fotos_pedidos/9-2-117-y696xcb1f-6.jpeg	a	a	f	431
2172	client/img/fotos_pedidos/fotos_pedidos/9-6-117-va5lg69hp-2.jpeg	a	a	f	437
2258	client/img/fotos_pedidos/fotos_pedidos/9-2-117-8tlk6uxqf-0.jpeg	a	a	f	453
2259	client/img/fotos_pedidos/fotos_pedidos/9-2-117-6ix31797e-0.jpeg	a	a	f	453
2260	client/img/fotos_pedidos/fotos_pedidos/9-2-117-1jy7o33du-0.jpeg	a	a	f	453
2263	client/img/fotos_pedidos/fotos_pedidos/9-2-117-qusl9mj18-0.jpeg	a	a	f	453
2266	client/img/fotos_pedidos/fotos_pedidos/9-2-117-mhxwarnb3-0.jpeg	a	a	f	453
2269	client/img/fotos_pedidos/fotos_pedidos/10-0-117-jky27zpww-1.jpeg	a	a	f	454
2275	client/img/fotos_pedidos/fotos_pedidos/10-0-117-kal6jeuvt-0.jpeg	a	a	f	454
2384	client/img/fotos_pedidos/fotos_pedidos/10-0-117-f72cnicel-0.jpeg	a	a	f	464
2385	client/img/fotos_pedidos/fotos_pedidos/10-0-117-m0opay264-1.jpeg	a	a	f	464
2386	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mbpaly0b5-2.jpeg	a	a	f	464
2388	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ahqqhq201-3.jpeg	a	a	f	464
2390	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ijmcnztf9-4.jpeg	a	a	f	464
2392	client/img/fotos_pedidos/fotos_pedidos/10-0-117-vj2q135tm-8.png	a	a	f	464
2458	client/img/fotos_pedidos/fotos_pedidos/10-4-117-mi0a43i42-8.jpeg	a	a	f	472
2573	client/img/fotos_pedidos/fotos_pedidos/10-2-117-nfzj3lcop-2.jpeg	a	a	f	482
2576	client/img/fotos_pedidos/fotos_pedidos/10-2-117-241azh7bv-5.jpeg	a	a	f	482
2579	client/img/fotos_pedidos/fotos_pedidos/10-2-117-wvvn2p385-8.jpeg	a	a	f	482
2582	client/img/fotos_pedidos/fotos_pedidos/10-2-117-uvece0156-10.jpeg	a	a	f	482
2585	client/img/fotos_pedidos/fotos_pedidos/10-2-117-1nau80xa7-12.jpeg	a	a	f	482
2675	client/img/fotos_pedidos/fotos_pedidos/10-3-117-qypa6spci-0.jpeg	a	a	f	495
2747	client/img/fotos_pedidos/fotos_pedidos/11-5-117-m251mup2k-0.jpeg	a	a	f	509
2751	client/img/fotos_pedidos/fotos_pedidos/11-5-117-egw2yvxy9-3.jpeg	a	a	f	508
2755	client/img/fotos_pedidos/fotos_pedidos/11-5-117-3cs8706q2-8.jpeg	a	a	f	508
2759	client/img/fotos_pedidos/fotos_pedidos/11-5-117-q9sullcle-1.jpeg	a	a	f	508
2763	client/img/fotos_pedidos/fotos_pedidos/11-5-117-yinmyrxtc-5.jpeg	a	a	f	508
2767	client/img/fotos_pedidos/fotos_pedidos/11-5-117-mvicz3ww5-0.jpeg	a	a	f	508
2771	client/img/fotos_pedidos/fotos_pedidos/11-5-117-1q3oxr630-4.jpeg	a	a	f	508
2775	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g4ofei01z-0.jpeg	a	a	f	508
2779	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g8lg9nvvi-0.jpeg	a	a	f	510
2783	client/img/fotos_pedidos/fotos_pedidos/11-5-117-tu78jtieu-4.jpeg	a	a	f	510
2787	client/img/fotos_pedidos/fotos_pedidos/11-5-117-5nozv4lls-8.jpeg	a	a	f	510
2791	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ap8il9q69-12.jpeg	a	a	f	510
2795	client/img/fotos_pedidos/fotos_pedidos/11-5-117-97fymmzjj-16.jpeg	a	a	f	510
2799	client/img/fotos_pedidos/fotos_pedidos/11-5-117-63t26f0f8-21.jpeg	a	a	f	510
2803	client/img/fotos_pedidos/fotos_pedidos/11-5-117-88kggi4rl-24.jpeg	a	a	f	510
2807	client/img/fotos_pedidos/fotos_pedidos/11-5-117-u03w5mylo-0.jpeg	a	a	f	510
2882	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cwkz00go2-2.jpeg	a	a	f	519
2883	client/img/fotos_pedidos/fotos_pedidos/11-4-117-5jdcp0kx1-0.jpeg	a	a	f	519
2884	client/img/fotos_pedidos/fotos_pedidos/11-4-117-d2omombgz-1.jpeg	a	a	f	519
2887	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ida7zlo7x-6.jpeg	a	a	f	519
2891	client/img/fotos_pedidos/fotos_pedidos/11-4-117-vgz7q7hvm-9.jpeg	a	a	f	519
2975	client/img/fotos_pedidos/fotos_pedidos/11-2-117-8l6zk5hmz-10.jpeg	a	a	f	527
2980	client/img/fotos_pedidos/fotos_pedidos/11-2-117-2grarmivc-15.jpeg	a	a	f	527
3074	client/img/fotos_pedidos/fotos_pedidos/11-3-117-by6wcisqd-2.jpeg	a	a	f	536
3076	client/img/fotos_pedidos/fotos_pedidos/11-3-117-wkqukzh8i-5.jpeg	a	a	f	536
3080	client/img/fotos_pedidos/fotos_pedidos/11-3-117-3a6e1vysu-9.jpeg	a	a	f	536
3086	client/img/fotos_pedidos/fotos_pedidos/11-3-117-we1ot5cdl-3.jpeg	a	a	f	536
3092	client/img/fotos_pedidos/fotos_pedidos/11-3-117-wes9rqc5j-1.jpeg	a	a	f	536
3099	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7zbvxefmo-9.jpeg	a	a	f	536
3183	client/img/fotos_pedidos/fotos_pedidos/11-4-117-4oktbk622-5.jpeg	a	a	f	544
3187	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ztv1ali8t-9.jpeg	a	a	f	544
3193	client/img/fotos_pedidos/fotos_pedidos/11-4-117-dn7divp6l-5.jpeg	a	a	f	544
1088	client/img/fotos_pedidos/fotos_pedidos/7-3-117-yh1g3eoit-3.jpeg	a	a	f	300
1093	client/img/fotos_pedidos/fotos_pedidos/7-3-117-djnzx7t10-8.jpeg	a	a	f	300
1099	client/img/fotos_pedidos/fotos_pedidos/7-3-117-jf8u06wp5-6.jpeg	a	a	f	300
1169	client/img/fotos_pedidos/fotos_pedidos/7-1-117-i0ak77avz-0.jpeg	a	a	t	310
1173	client/img/fotos_pedidos/fotos_pedidos/7-1-117-m2oydatel-0.jpeg	a	a	t	310
1254	client/img/fotos_pedidos/fotos_pedidos/7-1-117-btod8scee-7.jpeg	a	a	f	322
1350	client/img/fotos_pedidos/fotos_pedidos/7-0-117-whwfs5vhf-0.jpeg	a	a	f	333
1351	client/img/fotos_pedidos/fotos_pedidos/7-0-117-8nbtltxx7-1.jpeg	a	a	f	333
1442	client/img/fotos_pedidos/fotos_pedidos/8-3-117-5u91b4g5i-1.jpeg	a	a	f	342
1443	client/img/fotos_pedidos/fotos_pedidos/8-3-117-2oxahlaa1-2.jpeg	a	a	f	342
1444	client/img/fotos_pedidos/fotos_pedidos/8-3-117-u4338cv8c-1.jpeg	a	a	f	342
1449	client/img/fotos_pedidos/fotos_pedidos/8-3-117-spra0k73a-0.jpeg	a	a	f	342
1489	client/img/fotos_pedidos/fotos_pedidos/8-5-117-xtt32pu3b-1.jpeg	a	a	f	353
1492	client/img/fotos_pedidos/fotos_pedidos/8-5-117-iqebq2bib-6.jpeg	a	a	f	353
1496	client/img/fotos_pedidos/fotos_pedidos/8-5-117-7wqczhl9w-0.jpeg	a	a	f	353
1661	client/img/fotos_pedidos/fotos_pedidos/8-4-117-fvvphwqh4-0.jpeg	a	a	f	371
1662	client/img/fotos_pedidos/fotos_pedidos/8-4-117-u8hcobc8y-0.jpeg	a	a	f	371
1785	client/img/fotos_pedidos/fotos_pedidos/9-1-117-non8q91zm-0.jpeg	a	a	f	387
1907	client/img/fotos_pedidos/fotos_pedidos/9-0-117-b5fgwq9g9-0.jpeg	a	a	f	402
1908	client/img/fotos_pedidos/fotos_pedidos/9-0-117-1d8nzpk4y-1.jpeg	a	a	f	402
1909	client/img/fotos_pedidos/fotos_pedidos/9-0-117-mqp1yiuwk-2.jpeg	a	a	f	402
1910	client/img/fotos_pedidos/fotos_pedidos/9-0-117-fvhn28wty-3.jpeg	a	a	f	402
1912	client/img/fotos_pedidos/fotos_pedidos/9-0-117-3i82ag7ko-7.jpeg	a	a	f	402
1914	client/img/fotos_pedidos/fotos_pedidos/9-0-117-gzt8lqx74-5.jpeg	a	a	f	402
1916	client/img/fotos_pedidos/fotos_pedidos/9-0-117-3rdlbzjnc-0.jpeg	a	a	f	402
1919	client/img/fotos_pedidos/fotos_pedidos/9-0-117-dcknwry6c-2.jpeg	a	a	f	402
1922	client/img/fotos_pedidos/fotos_pedidos/9-0-117-tdlnff8rl-3.jpeg	a	a	f	402
1925	client/img/fotos_pedidos/fotos_pedidos/9-0-117-8iu1ksovv-1.jpeg	a	a	f	402
1928	client/img/fotos_pedidos/fotos_pedidos/9-0-117-bfj6f1qor-0.jpeg	a	a	f	402
2018	client/img/fotos_pedidos/fotos_pedidos/9-3-117-mbkyjblnt-0.jpeg	a	a	f	415
2019	client/img/fotos_pedidos/fotos_pedidos/9-3-117-pvuuto135-1.jpeg	a	a	f	415
2089	client/img/fotos_pedidos/fotos_pedidos/9-2-117-2fqb8fwwn-1.jpeg	a	a	f	431
2095	client/img/fotos_pedidos/fotos_pedidos/9-2-117-yl5j6lq7f-7.jpeg	a	a	f	431
2173	client/img/fotos_pedidos/fotos_pedidos/9-6-117-r81pwbjpu-0.jpeg	a	a	f	437
2261	client/img/fotos_pedidos/fotos_pedidos/9-2-117-6vvc3clwh-0.jpeg	a	a	f	453
2264	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ugn8h1n31-0.jpeg	a	a	f	453
2267	client/img/fotos_pedidos/fotos_pedidos/10-0-117-0ll6h9xwy-0.jpeg	a	a	f	454
2270	client/img/fotos_pedidos/fotos_pedidos/10-0-117-emkzhnml3-2.jpeg	a	a	f	454
2276	client/img/fotos_pedidos/fotos_pedidos/10-0-117-u9o1klxlm-0.jpeg	a	a	f	454
2387	client/img/fotos_pedidos/fotos_pedidos/10-0-117-wfredqfxi-6.jpeg	a	a	f	464
2389	client/img/fotos_pedidos/fotos_pedidos/10-0-117-uambmli4b-5.jpeg	a	a	f	464
2391	client/img/fotos_pedidos/fotos_pedidos/10-0-117-6tzctzhve-7.jpeg	a	a	f	464
2459	client/img/fotos_pedidos/fotos_pedidos/10-4-117-nb9wbj7hl-9.png	a	a	f	472
2574	client/img/fotos_pedidos/fotos_pedidos/10-2-117-dvh2oplhy-1.jpeg	a	a	f	482
2577	client/img/fotos_pedidos/fotos_pedidos/10-2-117-mu5rmnjj3-4.jpeg	a	a	f	482
2580	client/img/fotos_pedidos/fotos_pedidos/10-2-117-kvi3a2t75-7.jpeg	a	a	f	482
2583	client/img/fotos_pedidos/fotos_pedidos/10-2-117-niygmobio-11.jpeg	a	a	f	482
2586	client/img/fotos_pedidos/fotos_pedidos/10-2-117-mssb8y74y-14.jpeg	a	a	f	482
2676	client/img/fotos_pedidos/fotos_pedidos/10-3-117-21i83gpgr-0.jpeg	a	a	f	496
2748	client/img/fotos_pedidos/fotos_pedidos/11-5-117-zvfty8nt4-1.jpeg	a	a	f	508
2752	client/img/fotos_pedidos/fotos_pedidos/11-5-117-l08kbrhja-4.jpeg	a	a	f	508
2756	client/img/fotos_pedidos/fotos_pedidos/11-5-117-q6sxvp7k5-9.jpeg	a	a	f	508
2760	client/img/fotos_pedidos/fotos_pedidos/11-5-117-djcazf274-3.jpeg	a	a	f	508
2764	client/img/fotos_pedidos/fotos_pedidos/11-5-117-hsb1oc9zx-6.jpeg	a	a	f	508
2768	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g74uwecqo-1.jpeg	a	a	f	508
2772	client/img/fotos_pedidos/fotos_pedidos/11-5-117-cqm6nwmvl-5.jpeg	a	a	f	508
2776	client/img/fotos_pedidos/fotos_pedidos/11-5-117-fnxfbdiz9-0.jpeg	a	a	f	508
2780	client/img/fotos_pedidos/fotos_pedidos/11-5-117-3k8hgmwqs-1.jpeg	a	a	f	510
2784	client/img/fotos_pedidos/fotos_pedidos/11-5-117-0jss5pd7v-5.jpeg	a	a	f	510
2788	client/img/fotos_pedidos/fotos_pedidos/11-5-117-55mj54wnx-9.jpeg	a	a	f	510
2792	client/img/fotos_pedidos/fotos_pedidos/11-5-117-vdcq73g43-14.jpeg	a	a	f	510
2796	client/img/fotos_pedidos/fotos_pedidos/11-5-117-vcryoxtqz-17.jpeg	a	a	f	510
2800	client/img/fotos_pedidos/fotos_pedidos/11-5-117-vx2uhprsv-20.jpeg	a	a	f	510
2804	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ger5o1s1a-0.jpeg	a	a	f	510
2808	client/img/fotos_pedidos/fotos_pedidos/11-5-117-7jr9sgn8x-1.jpeg	a	a	f	510
2885	client/img/fotos_pedidos/fotos_pedidos/11-4-117-k5o7lkmka-3.jpeg	a	a	f	519
2888	client/img/fotos_pedidos/fotos_pedidos/11-4-117-31k7wf1yb-5.jpeg	a	a	f	519
2981	client/img/fotos_pedidos/fotos_pedidos/11-3-117-cchvx92q0-0.jpeg	Brindo por que estemos juntos por muchísimo más tiempo y que siempre seamos muy felices!	a	t	528
3077	client/img/fotos_pedidos/fotos_pedidos/11-3-117-cc0jzy4kh-6.jpeg	a	a	f	536
3083	client/img/fotos_pedidos/fotos_pedidos/11-3-117-p5d0stvse-2.jpeg	a	a	f	536
3089	client/img/fotos_pedidos/fotos_pedidos/11-3-117-i9pmplumy-8.jpeg	a	a	f	536
3095	client/img/fotos_pedidos/fotos_pedidos/11-3-117-n8pvnpz8z-4.jpeg	a	a	f	536
3188	client/img/fotos_pedidos/fotos_pedidos/11-4-117-5jx01ygp0-0.jpeg	a	a	f	544
3194	client/img/fotos_pedidos/fotos_pedidos/11-4-117-e61rpv03y-6.jpeg	a	a	f	544
1090	client/img/fotos_pedidos/fotos_pedidos/7-3-117-e8ds2yehe-6.jpeg	a	a	f	300
1096	client/img/fotos_pedidos/fotos_pedidos/7-3-117-h5uqrwu40-3.jpeg	a	a	f	300
1177	client/img/fotos_pedidos/fotos_pedidos/7-1-117-xcq6fvse6-0.jpeg	a	a	t	311
1179	client/img/fotos_pedidos/fotos_pedidos/7-1-117-xhk34o44q-0.png	a	a	t	311
1187	client/img/fotos_pedidos/fotos_pedidos/7-1-117-v0q9d3hsn-0.jpeg	a	a	t	312
1256	client/img/fotos_pedidos/fotos_pedidos/7-1-117-nkrrenz6w-9.jpeg	a	a	f	322
1352	client/img/fotos_pedidos/fotos_pedidos/7-0-117-g0dbtekhm-2.jpeg	a	a	f	333
1445	client/img/fotos_pedidos/fotos_pedidos/8-3-117-512xicso9-3.jpeg	a	a	f	342
1450	client/img/fotos_pedidos/fotos_pedidos/8-3-117-11gd62hp6-1.jpeg	a	a	f	342
1491	client/img/fotos_pedidos/fotos_pedidos/8-5-117-hawcb42mv-3.jpeg	a	a	f	353
1494	client/img/fotos_pedidos/fotos_pedidos/8-5-117-qp35ugd5h-4.jpeg	a	a	f	353
1497	client/img/fotos_pedidos/fotos_pedidos/8-5-117-0uyphflmc-0.jpeg	a	a	f	353
1663	client/img/fotos_pedidos/fotos_pedidos/8-4-117-2ph71hjdh-0.jpeg	a	a	f	372
1664	client/img/fotos_pedidos/fotos_pedidos/8-4-117-iy9gyr9e4-0.jpeg	a	a	f	372
1665	client/img/fotos_pedidos/fotos_pedidos/8-4-117-gy5ukhfhe-0.jpeg	a	a	f	372
1666	client/img/fotos_pedidos/fotos_pedidos/8-4-117-5o791fwbs-0.jpeg	a	a	f	372
1668	client/img/fotos_pedidos/fotos_pedidos/8-4-117-t0fanmjzr-0.jpeg	a	a	f	372
1670	client/img/fotos_pedidos/fotos_pedidos/8-4-117-w2biu1w7t-0.jpeg	a	a	f	372
1672	client/img/fotos_pedidos/fotos_pedidos/8-4-117-92pd5668z-0.jpeg	a	a	f	372
1787	client/img/fotos_pedidos/fotos_pedidos/9-1-117-hyj96mgxo-0.jpeg	a	a	f	388
1911	client/img/fotos_pedidos/fotos_pedidos/9-0-117-tnns5nxl8-4.jpeg	a	a	f	402
1913	client/img/fotos_pedidos/fotos_pedidos/9-0-117-xrwcgx33k-6.jpeg	a	a	f	402
1915	client/img/fotos_pedidos/fotos_pedidos/9-0-117-6ksrgv5wo-8.jpeg	a	a	f	402
1918	client/img/fotos_pedidos/fotos_pedidos/9-0-117-wbueutb2a-1.jpeg	a	a	f	402
1921	client/img/fotos_pedidos/fotos_pedidos/9-0-117-cbd8i0mb2-0.jpeg	a	a	f	402
1924	client/img/fotos_pedidos/fotos_pedidos/9-0-117-dqacgl2mu-0.jpeg	a	a	f	402
1927	client/img/fotos_pedidos/fotos_pedidos/9-0-117-4xfb74h8h-0.jpeg	a	a	f	402
1930	client/img/fotos_pedidos/fotos_pedidos/9-0-117-d7kr4rvrq-0.jpeg	a	a	f	402
2020	client/img/fotos_pedidos/fotos_pedidos/9-4-117-jwobz7izf-2.jpeg	a	a	f	416
2021	client/img/fotos_pedidos/fotos_pedidos/9-4-117-n2fi56jc7-0.png	a	a	f	416
2022	client/img/fotos_pedidos/fotos_pedidos/9-4-117-234m7gkxc-0.jpeg	a	a	f	416
2090	client/img/fotos_pedidos/fotos_pedidos/9-2-117-sf41gf4j7-2.jpeg	a	a	f	431
2096	client/img/fotos_pedidos/fotos_pedidos/9-2-117-a40sje6uj-8.jpeg	a	a	f	431
2262	client/img/fotos_pedidos/fotos_pedidos/9-2-117-mfhu3php7-0.jpeg	a	a	f	453
2265	client/img/fotos_pedidos/fotos_pedidos/9-2-117-1wsrc0kxq-0.jpeg	a	a	f	453
2268	client/img/fotos_pedidos/fotos_pedidos/9-2-117-rzd20v3e6-0.jpeg	a	a	f	453
2274	client/img/fotos_pedidos/fotos_pedidos/10-0-117-65plmpexm-0.jpeg	a	a	f	454
2393	client/img/fotos_pedidos/fotos_pedidos/10-1-117-gsitanbt3-2.jpeg	a	a	f	465
2394	client/img/fotos_pedidos/fotos_pedidos/10-1-117-gq85n8a0z-0.jpeg	a	a	f	465
2396	client/img/fotos_pedidos/fotos_pedidos/10-1-117-mxkxhxvnf-0.png	a	a	f	465
2403	client/img/fotos_pedidos/fotos_pedidos/10-1-117-75lbab6xi-4.jpeg	a	a	f	466
2460	client/img/fotos_pedidos/fotos_pedidos/10-4-117-ycpb212p5-0.jpeg	a	a	f	473
2463	client/img/fotos_pedidos/fotos_pedidos/10-4-117-q2i9j8b9y-3.jpeg	a	a	f	473
2465	client/img/fotos_pedidos/fotos_pedidos/10-4-117-f8x4ioiwb-5.jpeg	a	a	f	473
2588	client/img/fotos_pedidos/fotos_pedidos/10-3-117-xa04v007n-0.jpeg	a	a	f	483
2677	client/img/fotos_pedidos/fotos_pedidos/10-3-117-d39h0kxcl-1.jpeg	a	a	f	497
2678	client/img/fotos_pedidos/fotos_pedidos/10-3-117-mqcqyu4m7-0.jpeg	a	a	f	497
2682	client/img/fotos_pedidos/fotos_pedidos/10-3-117-xxzjo3qbf-0.jpeg	a	a	f	497
2811	client/img/fotos_pedidos/fotos_pedidos/11-0-117-aln25c4e4-0.jpeg	a	a	f	511
2812	client/img/fotos_pedidos/fotos_pedidos/11-0-117-p4kp42k1s-1.jpeg	a	a	f	511
2813	client/img/fotos_pedidos/fotos_pedidos/11-0-117-lwfoutxy8-2.jpeg	a	a	f	511
2814	client/img/fotos_pedidos/fotos_pedidos/11-0-117-6drz5oqck-3.jpeg	a	a	f	511
2816	client/img/fotos_pedidos/fotos_pedidos/11-0-117-zk0kdwfq4-5.jpeg	a	a	f	511
2818	client/img/fotos_pedidos/fotos_pedidos/11-0-117-ufjzb9aet-10.jpeg	a	a	f	511
2820	client/img/fotos_pedidos/fotos_pedidos/11-0-117-5x6rskqd3-7.jpeg	a	a	f	511
2823	client/img/fotos_pedidos/fotos_pedidos/11-0-117-ejco4u7iq-11.jpeg	a	a	f	511
2826	client/img/fotos_pedidos/fotos_pedidos/11-0-117-5sg5w9jxf-15.jpeg	a	a	f	511
2829	client/img/fotos_pedidos/fotos_pedidos/11-0-117-mjozd6jzi-18.jpeg	a	a	f	511
2832	client/img/fotos_pedidos/fotos_pedidos/11-0-117-z6rvrdur7-20.jpeg	a	a	f	511
2886	client/img/fotos_pedidos/fotos_pedidos/11-4-117-mlddvnvmy-4.jpeg	a	a	f	519
2890	client/img/fotos_pedidos/fotos_pedidos/11-4-117-grj7b4aig-8.jpeg	a	a	f	519
2982	client/img/fotos_pedidos/fotos_pedidos/11-3-117-t8ax0c4wb-0.jpeg	Estas cordialmente invitado a pasar una noche conmigo en el Monte Teepee del 20 al 21 de enero de 2018.	a	t	529
3078	client/img/fotos_pedidos/fotos_pedidos/11-3-117-4xsovc89p-7.jpeg	a	a	f	536
3084	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6edw07vf5-6.jpeg	a	a	f	536
3090	client/img/fotos_pedidos/fotos_pedidos/11-3-117-laxvsixvo-9.jpeg	a	a	f	536
3097	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6tcsxbf2h-5.jpeg	a	a	f	536
3102	client/img/fotos_pedidos/fotos_pedidos/11-3-117-n06bdjb0j-1.jpeg	a	a	f	536
3189	client/img/fotos_pedidos/fotos_pedidos/11-4-117-0j63tfuen-1.jpeg	a	a	f	544
3195	client/img/fotos_pedidos/fotos_pedidos/11-4-117-v8l9tyrng-7.jpeg	a	a	f	544
3201	client/img/fotos_pedidos/fotos_pedidos/11-4-117-x1k9d2jjk-3.jpeg	a	a	f	544
3207	client/img/fotos_pedidos/fotos_pedidos/11-4-117-wm0bak3jy-8.jpeg	a	a	f	544
3213	client/img/fotos_pedidos/fotos_pedidos/11-4-117-2kpdcaabb-5.jpeg	a	a	f	544
3219	client/img/fotos_pedidos/fotos_pedidos/11-4-117-jjye95hzv-1.jpeg	a	a	f	544
3225	client/img/fotos_pedidos/fotos_pedidos/11-4-117-y008mnewk-7.jpeg	a	a	f	544
1091	client/img/fotos_pedidos/fotos_pedidos/7-3-117-n9dsttyvo-7.jpeg	a	a	f	300
1097	client/img/fotos_pedidos/fotos_pedidos/7-3-117-zylqhjkk1-4.jpeg	a	a	f	300
1178	client/img/fotos_pedidos/fotos_pedidos/7-1-117-4nm75rs7k-0.jpeg	a	a	t	311
1180	client/img/fotos_pedidos/fotos_pedidos/7-1-117-v5v4ef27z-0.jpeg	a	a	t	311
1188	client/img/fotos_pedidos/fotos_pedidos/7-1-117-067yzj42t-0.jpeg	a	a	t	312
1257	client/img/fotos_pedidos/fotos_pedidos/7-1-117-vz5s180kd-10.jpeg	a	a	f	322
1353	client/img/fotos_pedidos/fotos_pedidos/7-1-117-vzmv0fsd2-0.png	a	a	f	334
1354	client/img/fotos_pedidos/fotos_pedidos/7-1-117-jqry2gxcy-1.png	a	a	f	334
1355	client/img/fotos_pedidos/fotos_pedidos/7-1-117-5kl6f53es-2.png	a	a	f	334
1446	client/img/fotos_pedidos/fotos_pedidos/8-3-117-v59isomak-0.jpeg	a	a	f	342
1451	client/img/fotos_pedidos/fotos_pedidos/8-3-117-dx5m4moob-0.jpeg	a	a	f	342
1498	client/img/fotos_pedidos/fotos_pedidos/8-2-117-aglfhjwv8-0.jpeg	a	a	f	354
1501	client/img/fotos_pedidos/fotos_pedidos/8-2-117-bk30864p4-3.jpeg	a	a	f	354
1667	client/img/fotos_pedidos/fotos_pedidos/8-4-117-mhhlblc0v-0.jpeg	a	a	f	372
1669	client/img/fotos_pedidos/fotos_pedidos/8-4-117-b2x5m6vz0-0.jpeg	a	a	f	372
1671	client/img/fotos_pedidos/fotos_pedidos/8-4-117-47ajpq9fq-0.jpeg	a	a	f	372
1788	client/img/fotos_pedidos/fotos_pedidos/9-2-117-f8yjyh7ey-0.jpeg	a	a	f	389
1789	client/img/fotos_pedidos/fotos_pedidos/9-2-117-bsofd0hxn-3.jpeg	a	a	f	389
1790	client/img/fotos_pedidos/fotos_pedidos/9-2-117-g5fcfcv56-2.jpeg	a	a	f	389
1791	client/img/fotos_pedidos/fotos_pedidos/9-2-117-ul3nvfn4x-1.jpeg	a	a	f	389
1794	client/img/fotos_pedidos/fotos_pedidos/9-2-117-yiz02sanz-6.jpeg	a	a	f	389
1797	client/img/fotos_pedidos/fotos_pedidos/9-2-117-kedy1xdki-0.jpeg	a	a	f	389
1798	client/img/fotos_pedidos/fotos_pedidos/9-2-117-3urtpgk58-0.jpeg	a	a	f	390
1917	client/img/fotos_pedidos/fotos_pedidos/9-0-117-oypzrt5kq-9.jpeg	a	a	f	402
1920	client/img/fotos_pedidos/fotos_pedidos/9-0-117-nd5tfjtcd-1.jpeg	a	a	f	402
1923	client/img/fotos_pedidos/fotos_pedidos/9-0-117-f3srinz64-0.jpeg	a	a	f	402
1926	client/img/fotos_pedidos/fotos_pedidos/9-0-117-ycc47ektw-2.jpeg	a	a	f	402
1929	client/img/fotos_pedidos/fotos_pedidos/9-0-117-dq58pxr7a-1.jpeg	a	a	f	402
2023	client/img/fotos_pedidos/fotos_pedidos/9-4-117-8ntwhnguz-0.jpeg	a	a	f	416
2091	client/img/fotos_pedidos/fotos_pedidos/9-2-117-051wh1897-3.jpeg	a	a	f	431
2097	client/img/fotos_pedidos/fotos_pedidos/9-2-117-58ow82bp0-9.jpeg	a	a	f	431
2271	client/img/fotos_pedidos/fotos_pedidos/10-0-117-qzpm60fiq-3.jpeg	a	a	f	454
2277	client/img/fotos_pedidos/fotos_pedidos/10-0-117-s6diygbxu-1.jpeg	a	a	f	454
2395	client/img/fotos_pedidos/fotos_pedidos/10-1-117-4hfu6eoei-1.jpeg	a	a	f	465
2397	client/img/fotos_pedidos/fotos_pedidos/10-1-117-pzcz914ig-4.jpeg	a	a	f	465
2404	client/img/fotos_pedidos/fotos_pedidos/10-1-117-3ik5dsw4m-5.jpeg	a	a	f	466
2461	client/img/fotos_pedidos/fotos_pedidos/10-4-117-2sph2tv2o-1.jpeg	a	a	f	473
2462	client/img/fotos_pedidos/fotos_pedidos/10-4-117-q89c3kvog-2.jpeg	a	a	f	473
2464	client/img/fotos_pedidos/fotos_pedidos/10-4-117-96441tc7y-4.jpeg	a	a	f	473
2589	client/img/fotos_pedidos/fotos_pedidos/10-3-117-32b6hkdrj-0.jpeg	a	a	f	483
2679	client/img/fotos_pedidos/fotos_pedidos/10-3-117-sxpm5ef9x-2.jpeg	a	a	f	497
2815	client/img/fotos_pedidos/fotos_pedidos/11-0-117-ja3yhm8tr-4.jpeg	a	a	f	511
2817	client/img/fotos_pedidos/fotos_pedidos/11-0-117-8bjw66j7i-9.jpeg	a	a	f	511
2819	client/img/fotos_pedidos/fotos_pedidos/11-0-117-1t8t6sxm9-6.jpeg	a	a	f	511
2822	client/img/fotos_pedidos/fotos_pedidos/11-0-117-dfb6r0u1j-12.jpeg	a	a	f	511
2825	client/img/fotos_pedidos/fotos_pedidos/11-0-117-c6af4i4ri-14.jpeg	a	a	f	511
2828	client/img/fotos_pedidos/fotos_pedidos/11-0-117-w1rpclbzw-17.jpeg	a	a	f	511
2831	client/img/fotos_pedidos/fotos_pedidos/11-0-117-lyh4vdria-21.jpeg	a	a	f	511
2834	client/img/fotos_pedidos/fotos_pedidos/11-0-117-wqz0g8y2n-23.jpeg	a	a	f	511
2889	client/img/fotos_pedidos/fotos_pedidos/11-4-117-u4zbipfja-7.jpeg	a	a	f	519
2983	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ah1fx2nft-0.jpeg	Estas cordialmente invitado a pasar una velada romántica conmigo en el Monte Teepee del 20 al 21 de enero del 2018.	a	t	530
3081	client/img/fotos_pedidos/fotos_pedidos/11-3-117-rjqr2izwg-1.jpeg	a	a	f	536
3087	client/img/fotos_pedidos/fotos_pedidos/11-3-117-pnhtquuhn-4.jpeg	a	a	f	536
3093	client/img/fotos_pedidos/fotos_pedidos/11-3-117-nlbtfzqdk-2.jpeg	a	a	f	536
3098	client/img/fotos_pedidos/fotos_pedidos/11-3-117-18b70z5lz-8.jpeg	a	a	f	536
3199	client/img/fotos_pedidos/fotos_pedidos/11-4-117-3mac2v2si-1.jpeg	a	a	f	544
3205	client/img/fotos_pedidos/fotos_pedidos/11-4-117-g6jibzgch-7.jpeg	a	a	f	544
3211	client/img/fotos_pedidos/fotos_pedidos/11-4-117-lo6lnbp64-3.jpeg	a	a	f	544
3217	client/img/fotos_pedidos/fotos_pedidos/11-4-117-tz54ar4jo-9.jpeg	a	a	f	544
3223	client/img/fotos_pedidos/fotos_pedidos/11-4-117-hw3tqnutq-5.jpeg	a	a	f	544
3229	client/img/fotos_pedidos/fotos_pedidos/11-4-117-4e18fgq97-1.jpeg	a	a	f	544
3235	client/img/fotos_pedidos/fotos_pedidos/11-4-117-15mjopa1v-7.jpeg	a	a	f	544
3263	client/img/fotos_pedidos/fotos_pedidos/11-5-117-cdf5qa0lc-0.jpeg	a	a	f	547
3294	client/img/fotos_pedidos/fotos_pedidos/11-2-117-g3hggqq7j-2.jpeg	a	a	f	553
3296	client/img/fotos_pedidos/fotos_pedidos/11-2-117-4qghmey2w-6.jpeg	a	a	f	553
3298	client/img/fotos_pedidos/fotos_pedidos/11-2-117-1l7uyoq71-5.jpeg	a	a	f	553
3300	client/img/fotos_pedidos/fotos_pedidos/11-2-117-lzzuhpvo0-9.jpeg	a	a	f	553
3303	client/img/fotos_pedidos/fotos_pedidos/11-2-117-0ebg0tt39-12.jpeg	a	a	f	553
3306	client/img/fotos_pedidos/fotos_pedidos/11-2-117-5tggi6vsl-14.jpeg	a	a	f	553
3309	client/img/fotos_pedidos/fotos_pedidos/11-2-117-jkul8say0-17.jpeg	a	a	f	553
3312	client/img/fotos_pedidos/fotos_pedidos/11-2-117-efp8g6amm-21.jpeg	a	a	f	553
3322	client/img/fotos_pedidos/fotos_pedidos/11-3-117-12yre4m02-5.jpeg	a	a	f	556
3324	client/img/fotos_pedidos/fotos_pedidos/11-3-117-iyjvkmgtk-6.jpeg	a	a	f	556
3326	client/img/fotos_pedidos/fotos_pedidos/11-3-117-xx9xhv9vf-9.jpeg	a	a	f	556
1095	client/img/fotos_pedidos/fotos_pedidos/7-3-117-hdyglfdwp-2.jpeg	a	a	f	300
1181	client/img/fotos_pedidos/fotos_pedidos/7-1-117-tl3w0qda4-0.jpeg	a	a	t	311
1185	client/img/fotos_pedidos/fotos_pedidos/7-1-117-crxj4kywh-0.jpeg	a	a	t	312
1259	client/img/fotos_pedidos/fotos_pedidos/7-1-117-4tmb5maob-0.jpeg	a	a	f	323
1260	client/img/fotos_pedidos/fotos_pedidos/7-1-117-ofw8cyywe-2.jpeg	a	a	f	323
1261	client/img/fotos_pedidos/fotos_pedidos/7-1-117-vlpv3wtzz-1.jpeg	a	a	f	323
1262	client/img/fotos_pedidos/fotos_pedidos/7-1-117-xzx7vskna-3.jpeg	a	a	f	323
1263	client/img/fotos_pedidos/fotos_pedidos/7-1-117-g9wg55sr6-4.jpeg	a	a	f	323
1266	client/img/fotos_pedidos/fotos_pedidos/7-1-117-56y1redhr-7.jpeg	a	a	f	323
1499	client/img/fotos_pedidos/fotos_pedidos/8-2-117-wr7q448is-1.jpeg	a	a	f	354
1673	client/img/fotos_pedidos/fotos_pedidos/8-2-117-mu94nuls4-0.jpeg	a	a	f	373
1674	client/img/fotos_pedidos/fotos_pedidos/8-2-117-3a3b5oza9-0.jpeg	a	a	f	373
1792	client/img/fotos_pedidos/fotos_pedidos/9-2-117-qo53x7mz4-4.jpeg	a	a	f	389
1795	client/img/fotos_pedidos/fotos_pedidos/9-2-117-opsw4os0u-7.jpeg	a	a	f	389
1931	client/img/fotos_pedidos/fotos_pedidos/9-0-117-6oi9loosj-0.jpeg	a	a	f	403
1932	client/img/fotos_pedidos/fotos_pedidos/9-0-117-45b9tyan5-1.jpeg	a	a	f	403
1933	client/img/fotos_pedidos/fotos_pedidos/9-0-117-ui27pe6j9-2.jpeg	a	a	f	403
2024	client/img/fotos_pedidos/fotos_pedidos/9-4-117-fiz4nfklv-0.jpeg	Negrito: \n\nUno momento para recordar, de un encuentro inolvidable!. \n\nHotel Estelar, Asia, Lima-Perú. 1 Sept/2017	a	t	417
2092	client/img/fotos_pedidos/fotos_pedidos/9-2-117-zl27sf3yw-4.jpeg	a	a	f	431
1447	client/img/fotos_pedidos/fotos_pedidos/8-3-117-jd2tkehex-0.jpeg	a	a	f	342
1452	client/img/fotos_pedidos/fotos_pedidos/8-3-117-9h8nhkiv0-3.jpeg	a	a	f	342
2098	client/img/fotos_pedidos/fotos_pedidos/9-2-117-sll771k78-10.jpeg	a	a	f	431
2185	client/img/fotos_pedidos/fotos_pedidos/9-0-117-5g9gkwlzk-0.jpeg	a	a	f	439
2186	client/img/fotos_pedidos/fotos_pedidos/9-0-117-zp3gh191i-1.jpeg	a	a	f	439
2187	client/img/fotos_pedidos/fotos_pedidos/9-0-117-jhq62z8jc-2.jpeg	a	a	f	439
2188	client/img/fotos_pedidos/fotos_pedidos/9-0-117-ecj0yu352-3.jpeg	a	a	f	439
2190	client/img/fotos_pedidos/fotos_pedidos/9-0-117-egdjnovyi-5.jpeg	a	a	f	439
2193	client/img/fotos_pedidos/fotos_pedidos/9-0-117-000e0hvqn-8.jpeg	a	a	f	439
2196	client/img/fotos_pedidos/fotos_pedidos/9-0-117-x1h4aispa-11.jpeg	a	a	f	439
2272	client/img/fotos_pedidos/fotos_pedidos/10-0-117-xticdv619-4.jpeg	a	a	f	454
2398	client/img/fotos_pedidos/fotos_pedidos/10-1-117-fdvr9hzn4-3.jpeg	a	a	f	465
2401	client/img/fotos_pedidos/fotos_pedidos/10-1-117-0pjijv6v6-1.jpeg	a	a	f	466
2466	client/img/fotos_pedidos/fotos_pedidos/10-4-117-d1zns94f5-7.jpeg	a	a	f	473
2590	client/img/fotos_pedidos/fotos_pedidos/10-4-117-kltzr30u9-0.jpeg	a	a	f	484
2680	client/img/fotos_pedidos/fotos_pedidos/10-3-117-y3i6cqiq6-0.jpeg	a	a	f	497
2821	client/img/fotos_pedidos/fotos_pedidos/11-0-117-daptinlo2-8.jpeg	a	a	f	511
2824	client/img/fotos_pedidos/fotos_pedidos/11-0-117-czbuxt8na-13.jpeg	a	a	f	511
2827	client/img/fotos_pedidos/fotos_pedidos/11-0-117-kirzr8igq-16.jpeg	a	a	f	511
2830	client/img/fotos_pedidos/fotos_pedidos/11-0-117-sskdpfknj-19.jpeg	a	a	f	511
2833	client/img/fotos_pedidos/fotos_pedidos/11-0-117-adoily7k3-22.jpeg	a	a	f	511
2892	client/img/fotos_pedidos/fotos_pedidos/11-5-117-nds0pvxnl-0.png	a	a	f	520
2893	client/img/fotos_pedidos/fotos_pedidos/11-5-117-2cvu831ot-1.png	a	a	f	520
2894	client/img/fotos_pedidos/fotos_pedidos/11-5-117-la9s9aksu-2.png	a	a	f	520
2895	client/img/fotos_pedidos/fotos_pedidos/11-5-117-jsjg5c7l7-3.png	a	a	f	520
2896	client/img/fotos_pedidos/fotos_pedidos/11-5-117-te43if8az-4.png	a	a	f	520
2897	client/img/fotos_pedidos/fotos_pedidos/11-5-117-pannqm8pl-5.png	a	a	f	520
2898	client/img/fotos_pedidos/fotos_pedidos/11-5-117-7ghbrvmnz-6.png	a	a	f	520
2901	client/img/fotos_pedidos/fotos_pedidos/11-5-117-8f04nelc1-0.png	a	a	f	520
2904	client/img/fotos_pedidos/fotos_pedidos/11-5-117-seldaokzw-2.png	a	a	f	520
2907	client/img/fotos_pedidos/fotos_pedidos/11-5-117-k4w7yo1g7-4.png	a	a	f	520
2910	client/img/fotos_pedidos/fotos_pedidos/11-5-117-vrxkcju25-0.png	a	a	f	520
2913	client/img/fotos_pedidos/fotos_pedidos/11-5-117-100irvn72-2.png	a	a	f	520
2916	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ssjzbzfjj-5.png	a	a	f	520
2919	client/img/fotos_pedidos/fotos_pedidos/11-5-117-9qsfhjo8f-9.png	a	a	f	520
2922	client/img/fotos_pedidos/fotos_pedidos/11-5-117-8p0p3l1kq-0.png	a	a	f	520
2984	client/img/fotos_pedidos/fotos_pedidos/11-3-117-w72ic5j9i-0.jpeg	Brindo por nosotros, porque tenemos un gran relación que siempre me hace feliz y porque podamos salir adelante juntos. Te amo	a	t	531
3082	client/img/fotos_pedidos/fotos_pedidos/11-3-117-xq8yfkldw-0.jpeg	a	a	f	536
3088	client/img/fotos_pedidos/fotos_pedidos/11-3-117-lc01ld0ms-7.jpeg	a	a	f	536
3094	client/img/fotos_pedidos/fotos_pedidos/11-3-117-0r463021c-3.jpeg	a	a	f	536
3100	client/img/fotos_pedidos/fotos_pedidos/11-3-117-vh7wpvhqi-7.jpeg	a	a	f	536
3200	client/img/fotos_pedidos/fotos_pedidos/11-4-117-bslrpnybl-2.jpeg	a	a	f	544
3206	client/img/fotos_pedidos/fotos_pedidos/11-4-117-46fpqu95j-9.jpeg	a	a	f	544
3212	client/img/fotos_pedidos/fotos_pedidos/11-4-117-gtsecl8qr-4.jpeg	a	a	f	544
3218	client/img/fotos_pedidos/fotos_pedidos/11-4-117-kh2hzrww6-0.jpeg	a	a	f	544
3224	client/img/fotos_pedidos/fotos_pedidos/11-4-117-eepins1ek-6.jpeg	a	a	f	544
3230	client/img/fotos_pedidos/fotos_pedidos/11-4-117-2xw7xlbbu-2.jpeg	a	a	f	544
3236	client/img/fotos_pedidos/fotos_pedidos/11-4-117-27x36q0yp-8.jpeg	a	a	f	544
3264	client/img/fotos_pedidos/fotos_pedidos/11-0-117-i2zxjq5pj-0.jpeg	a	a	f	548
3266	client/img/fotos_pedidos/fotos_pedidos/11-0-117-orisxr4xd-0.jpeg	a	a	f	548
3302	client/img/fotos_pedidos/fotos_pedidos/11-2-117-si40zx6ur-11.jpeg	a	a	f	553
3305	client/img/fotos_pedidos/fotos_pedidos/11-2-117-oh0gkyrss-15.jpeg	a	a	f	553
3308	client/img/fotos_pedidos/fotos_pedidos/11-2-117-ceelr0ly8-18.jpeg	a	a	f	553
3311	client/img/fotos_pedidos/fotos_pedidos/11-2-117-qfljnvxof-20.jpeg	a	a	f	553
1500	client/img/fotos_pedidos/fotos_pedidos/8-2-117-era1049ji-2.jpeg	a	a	f	354
1675	client/img/fotos_pedidos/fotos_pedidos/8-2-117-t2nm3dz3q-0.jpeg	a	a	f	374
1676	client/img/fotos_pedidos/fotos_pedidos/8-2-117-ieno8dqwa-0.jpeg	a	a	f	374
1793	client/img/fotos_pedidos/fotos_pedidos/9-2-117-vzlr7jbii-5.jpeg	a	a	f	389
1796	client/img/fotos_pedidos/fotos_pedidos/9-2-117-5apuw8fj1-8.jpeg	a	a	f	389
1934	client/img/fotos_pedidos/fotos_pedidos/9-1-117-0vc150503-0.jpeg	a	a	f	404
2025	client/img/fotos_pedidos/fotos_pedidos/9-4-117-omfx4t1ep-0.jpeg	Negrito: \n\nUn momento muy especial para recordar, de un encuentro inolvidable!\n\nHotel Estelar, Asia, Lima-Perú\n1 sep/2017	a	t	418
1182	client/img/fotos_pedidos/fotos_pedidos/7-1-117-uysnzaroy-0.jpeg	a	a	t	311
1186	client/img/fotos_pedidos/fotos_pedidos/7-1-117-u0ylu0zhd-0.jpeg	a	a	t	312
1264	client/img/fotos_pedidos/fotos_pedidos/7-1-117-vob9vidhm-6.jpeg	a	a	f	323
1267	client/img/fotos_pedidos/fotos_pedidos/7-1-117-y2rzqyg2x-8.jpeg	a	a	f	323
2093	client/img/fotos_pedidos/fotos_pedidos/9-2-117-uro4q2wzl-5.jpeg	a	a	f	431
2099	client/img/fotos_pedidos/fotos_pedidos/9-2-117-kkbqymi1f-11.jpeg	a	a	f	431
2189	client/img/fotos_pedidos/fotos_pedidos/9-0-117-e4guseu2m-4.jpeg	a	a	f	439
2192	client/img/fotos_pedidos/fotos_pedidos/9-0-117-9a2gvj9wk-6.jpeg	a	a	f	439
2195	client/img/fotos_pedidos/fotos_pedidos/9-0-117-dxsej8j91-10.jpeg	a	a	f	439
2273	client/img/fotos_pedidos/fotos_pedidos/10-0-117-4t5hxg1ss-5.jpeg	a	a	f	454
2399	client/img/fotos_pedidos/fotos_pedidos/10-1-117-lje4f09in-0.jpeg	a	a	f	466
2402	client/img/fotos_pedidos/fotos_pedidos/10-1-117-swupvaprt-3.jpeg	a	a	f	466
2467	client/img/fotos_pedidos/fotos_pedidos/10-4-117-vvta33q74-6.jpeg	a	a	f	473
2591	client/img/fotos_pedidos/fotos_pedidos/10-4-117-sk9njwnt4-0.jpeg	a	a	f	485
1448	client/img/fotos_pedidos/fotos_pedidos/8-3-117-1tf89q0c3-2.jpeg	a	a	f	342
2592	client/img/fotos_pedidos/fotos_pedidos/10-4-117-oeai40gmj-2.jpeg	a	a	f	485
2593	client/img/fotos_pedidos/fotos_pedidos/10-4-117-9dzo6bmgj-1.jpeg	a	a	f	485
2681	client/img/fotos_pedidos/fotos_pedidos/10-3-117-5au6b1wxk-3.jpeg	a	a	f	497
2835	client/img/fotos_pedidos/fotos_pedidos/11-2-117-e9e5yd07o-0.jpeg	a	a	f	513
2838	client/img/fotos_pedidos/fotos_pedidos/11-2-117-j54xw6rjd-2.jpeg	a	a	f	512
2899	client/img/fotos_pedidos/fotos_pedidos/11-5-117-45ah3r6ls-7.png	a	a	f	520
2902	client/img/fotos_pedidos/fotos_pedidos/11-5-117-n5ax876c6-1.png	a	a	f	520
2905	client/img/fotos_pedidos/fotos_pedidos/11-5-117-x8hvvz58x-3.png	a	a	f	520
2908	client/img/fotos_pedidos/fotos_pedidos/11-5-117-35iyjy4jl-6.png	a	a	f	520
2911	client/img/fotos_pedidos/fotos_pedidos/11-5-117-2zz1fuhps-8.png	a	a	f	520
2914	client/img/fotos_pedidos/fotos_pedidos/11-5-117-iixch2gvz-3.png	a	a	f	520
2917	client/img/fotos_pedidos/fotos_pedidos/11-5-117-8anzw2kvy-6.png	a	a	f	520
2920	client/img/fotos_pedidos/fotos_pedidos/11-5-117-8xws01wnc-8.png	a	a	f	520
2923	client/img/fotos_pedidos/fotos_pedidos/11-5-117-bl2tbpieh-1.png	a	a	f	520
2985	client/img/fotos_pedidos/fotos_pedidos/11-3-117-qdsyty57s-0.jpeg	a	a	f	532
2986	client/img/fotos_pedidos/fotos_pedidos/11-3-117-fvshvqeqw-1.jpeg	a	a	f	532
2987	client/img/fotos_pedidos/fotos_pedidos/11-3-117-hk1o7iczp-2.jpeg	a	a	f	532
2988	client/img/fotos_pedidos/fotos_pedidos/11-3-117-94ptmnza6-4.jpeg	a	a	f	532
2989	client/img/fotos_pedidos/fotos_pedidos/11-3-117-rt7hqub20-5.jpeg	a	a	f	532
2990	client/img/fotos_pedidos/fotos_pedidos/11-3-117-9bv3dnzj8-3.jpeg	a	a	f	532
2991	client/img/fotos_pedidos/fotos_pedidos/11-3-117-kaci7xitj-6.jpeg	a	a	f	532
2992	client/img/fotos_pedidos/fotos_pedidos/11-3-117-pkbskyj4f-7.jpeg	a	a	f	532
2993	client/img/fotos_pedidos/fotos_pedidos/11-3-117-hmhbtfr2e-8.jpeg	a	a	f	532
2994	client/img/fotos_pedidos/fotos_pedidos/11-3-117-qegldfkmy-9.png	a	a	f	532
2995	client/img/fotos_pedidos/fotos_pedidos/11-3-117-9bj79oifc-10.jpeg	a	a	f	532
2996	client/img/fotos_pedidos/fotos_pedidos/11-3-117-m12ru0i6j-11.jpeg	a	a	f	532
3103	client/img/fotos_pedidos/fotos_pedidos/11-4-117-smuu91mtw-0.jpeg	a	a	f	537
3227	client/img/fotos_pedidos/fotos_pedidos/11-4-117-m980k278d-9.jpeg	a	a	f	544
3233	client/img/fotos_pedidos/fotos_pedidos/11-4-117-6j0qu63t2-5.jpeg	a	a	f	544
3265	client/img/fotos_pedidos/fotos_pedidos/11-0-117-jtgscmepg-0.jpeg	a	a	f	548
3313	client/img/fotos_pedidos/fotos_pedidos/11-2-117-hjixhnt8z-22.jpeg	a	a	f	553
3328	client/img/fotos_pedidos/fotos_pedidos/11-2-117-dv81ayz61-0.png	a	a	f	557
3332	client/img/fotos_pedidos/fotos_pedidos/11-2-117-5bhm509o0-0.png	a	a	f	559
3338	client/img/fotos_pedidos/fotos_pedidos/11-3-117-pwxun8ro0-0.jpeg	a	a	f	566
3343	client/img/fotos_pedidos/fotos_pedidos/11-3-117-3kw218sb2-0.png	a	a	f	564
3347	client/img/fotos_pedidos/fotos_pedidos/12-6-117-qh37fhl6z-0.jpeg	a	a	t	572
3348	client/img/fotos_pedidos/fotos_pedidos/12-6-117-rr9z6x52o-0.jpeg	a	a	t	573
3354	client/img/fotos_pedidos/fotos_pedidos/12-6-117-7knksiv4q-6.jpeg	a	a	t	574
3361	client/img/fotos_pedidos/fotos_pedidos/12-0-117-yqb97jxnl-0.jpeg	a	a	f	575
3363	client/img/fotos_pedidos/fotos_pedidos/12-0-117-p2f5xkv8o-0.jpeg	a	a	t	577
3364	client/img/fotos_pedidos/fotos_pedidos/12-0-117-euab34xb5-0.jpeg	a	a	f	578
3365	client/img/fotos_pedidos/fotos_pedidos/12-0-117-vxxxu8ipx-1.jpeg	a	a	f	578
3366	client/img/fotos_pedidos/fotos_pedidos/12-0-117-wbamn3x06-2.jpeg	a	a	f	578
3367	client/img/fotos_pedidos/fotos_pedidos/12-0-117-fbr52ff6v-3.jpeg	a	a	f	578
3368	client/img/fotos_pedidos/fotos_pedidos/12-0-117-99b0fqf2p-4.jpeg	a	a	f	578
3369	client/img/fotos_pedidos/fotos_pedidos/12-0-117-wkpspur90-5.jpeg	a	a	f	578
3370	client/img/fotos_pedidos/fotos_pedidos/12-0-117-c5yxiuzjb-6.jpeg	a	a	f	578
3371	client/img/fotos_pedidos/fotos_pedidos/12-0-117-vf9a0wj7b-7.jpeg	a	a	f	578
3372	client/img/fotos_pedidos/fotos_pedidos/12-0-117-3rmqdcay2-8.jpeg	a	a	f	578
3373	client/img/fotos_pedidos/fotos_pedidos/12-0-117-ah7p8djig-9.jpeg	a	a	f	578
3374	client/img/fotos_pedidos/fotos_pedidos/12-0-117-p94p957o4-10.jpeg	a	a	f	578
3375	client/img/fotos_pedidos/fotos_pedidos/12-0-117-233pvd50i-11.jpeg	a	a	f	578
1502	client/img/fotos_pedidos/fotos_pedidos/8-4-117-37fghn3fl-1.jpeg	a	a	f	355
1503	client/img/fotos_pedidos/fotos_pedidos/8-4-117-0nhj4lcg0-0.jpeg	a	a	f	355
1183	client/img/fotos_pedidos/fotos_pedidos/7-1-117-97vz6as4h-0.jpeg	a	a	t	312
1265	client/img/fotos_pedidos/fotos_pedidos/7-1-117-epq0zt9c3-5.jpeg	a	a	f	323
1268	client/img/fotos_pedidos/fotos_pedidos/7-1-117-9wbo1l77z-9.jpeg	a	a	f	323
1376	client/img/fotos_pedidos/fotos_pedidos/7-1-117-9t9assbiq-0.jpeg	a	a	f	336
1377	client/img/fotos_pedidos/fotos_pedidos/7-1-117-r0lvcb9m8-0.jpeg	a	a	f	336
1378	client/img/fotos_pedidos/fotos_pedidos/7-1-117-v0z1wuy86-0.jpeg	a	a	f	336
1379	client/img/fotos_pedidos/fotos_pedidos/7-1-117-lk0yqwum1-0.jpeg	a	a	f	336
1380	client/img/fotos_pedidos/fotos_pedidos/7-1-117-xw8ubkigh-0.jpeg	a	a	f	336
1381	client/img/fotos_pedidos/fotos_pedidos/7-1-117-pgwoo6ey5-0.jpeg	a	a	f	336
1382	client/img/fotos_pedidos/fotos_pedidos/7-1-117-pbsvw8k7z-0.jpeg	a	a	f	336
1383	client/img/fotos_pedidos/fotos_pedidos/7-1-117-2a7k68u13-0.jpeg	a	a	f	336
1384	client/img/fotos_pedidos/fotos_pedidos/7-1-117-gobqo6vy8-0.jpeg	a	a	f	336
1453	client/img/fotos_pedidos/fotos_pedidos/8-3-117-jinxdld4r-0.jpeg	a	a	f	342
1504	client/img/fotos_pedidos/fotos_pedidos/8-4-117-rund1lspd-2.jpeg	a	a	f	355
1506	client/img/fotos_pedidos/fotos_pedidos/8-4-117-8ed7xem1e-4.jpeg	a	a	f	355
1509	client/img/fotos_pedidos/fotos_pedidos/8-4-117-q012ilojv-7.jpeg	a	a	f	355
1512	client/img/fotos_pedidos/fotos_pedidos/8-4-117-67y76b9m6-0.jpeg	a	a	f	355
1515	client/img/fotos_pedidos/fotos_pedidos/8-4-117-hr92h8tmt-5.jpeg	a	a	f	355
1520	client/img/fotos_pedidos/fotos_pedidos/8-4-117-mh305a3vk-0.jpeg	a	a	f	355
1523	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ydq12einw-5.jpeg	a	a	f	355
1527	client/img/fotos_pedidos/fotos_pedidos/8-4-117-my02jcjk1-1.jpeg	a	a	f	355
1531	client/img/fotos_pedidos/fotos_pedidos/8-4-117-mqhd57v8i-5.jpeg	a	a	f	355
1535	client/img/fotos_pedidos/fotos_pedidos/8-4-117-len5hkfjm-1.jpeg	a	a	f	355
1539	client/img/fotos_pedidos/fotos_pedidos/8-4-117-tvgfaxvv6-5.jpeg	a	a	f	355
1799	client/img/fotos_pedidos/fotos_pedidos/9-3-117-h9eiqcjkg-0.jpeg	a	a	f	391
1800	client/img/fotos_pedidos/fotos_pedidos/9-3-117-22d2ukmeo-0.jpeg	a	a	f	392
1802	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ek8a40xiz-2.jpeg	a	a	f	392
1804	client/img/fotos_pedidos/fotos_pedidos/9-3-117-mgq02eao5-4.jpeg	a	a	f	392
1935	client/img/fotos_pedidos/fotos_pedidos/9-1-117-yi5u9zauy-0.jpeg	a	a	f	405
2026	client/img/fotos_pedidos/fotos_pedidos/9-4-117-zdw0t8ubk-0.jpeg	a	a	f	419
2027	client/img/fotos_pedidos/fotos_pedidos/9-4-117-9iayso77l-0.jpeg	a	a	f	419
2028	client/img/fotos_pedidos/fotos_pedidos/9-4-117-hpw6hcga7-0.jpeg	a	a	f	419
2029	client/img/fotos_pedidos/fotos_pedidos/9-4-117-81yt340py-0.jpeg	a	a	f	419
2030	client/img/fotos_pedidos/fotos_pedidos/9-4-117-po1d2ohlq-0.jpeg	a	a	f	419
2031	client/img/fotos_pedidos/fotos_pedidos/9-4-117-ug8zdynfm-0.jpeg	a	a	f	419
2032	client/img/fotos_pedidos/fotos_pedidos/9-4-117-x86505eu1-0.jpeg	a	a	f	419
2033	client/img/fotos_pedidos/fotos_pedidos/9-4-117-tazxewl17-0.jpeg	a	a	f	419
2034	client/img/fotos_pedidos/fotos_pedidos/9-4-117-dse0bzrrj-0.jpeg	a	a	f	419
2100	client/img/fotos_pedidos/fotos_pedidos/9-3-117-g61wkhq0t-2.jpeg	a	a	f	432
2105	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ottggz1kb-5.jpeg	a	a	f	432
2108	client/img/fotos_pedidos/fotos_pedidos/9-3-117-fcz7hvt3e-8.jpeg	a	a	f	432
2191	client/img/fotos_pedidos/fotos_pedidos/9-0-117-13ehetyjq-7.jpeg	a	a	f	439
2194	client/img/fotos_pedidos/fotos_pedidos/9-0-117-plvfxgl8a-9.jpeg	a	a	f	439
2278	client/img/fotos_pedidos/fotos_pedidos/10-1-117-mabik96fg-0.jpeg	a	a	f	455
2279	client/img/fotos_pedidos/fotos_pedidos/10-1-117-r39zatms3-1.jpeg	a	a	f	455
2280	client/img/fotos_pedidos/fotos_pedidos/10-1-117-lf87hf4nr-2.jpeg	a	a	f	455
2282	client/img/fotos_pedidos/fotos_pedidos/10-1-117-bh4dqzze3-4.jpeg	a	a	f	455
2284	client/img/fotos_pedidos/fotos_pedidos/10-1-117-xng2qv139-6.jpeg	a	a	f	455
2286	client/img/fotos_pedidos/fotos_pedidos/10-1-117-xb30ww01o-0.jpeg	a	a	f	455
2400	client/img/fotos_pedidos/fotos_pedidos/10-1-117-yg54fyv1x-2.jpeg	a	a	f	466
2468	client/img/fotos_pedidos/fotos_pedidos/10-4-117-jnt32m9tq-8.jpeg	a	a	f	473
2594	client/img/fotos_pedidos/fotos_pedidos/10-4-117-b6t7p9sd5-0.jpeg	a	a	f	486
2596	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7ucyabs53-0.jpeg	a	a	f	486
2598	client/img/fotos_pedidos/fotos_pedidos/10-4-117-4r4ir9jwd-0.jpeg	a	a	f	486
2600	client/img/fotos_pedidos/fotos_pedidos/10-4-117-sfjhnaw2a-0.jpeg	a	a	f	486
2683	client/img/fotos_pedidos/fotos_pedidos/10-3-117-yzce5dgxj-0.jpeg	a	a	f	498
2684	client/img/fotos_pedidos/fotos_pedidos/10-3-117-2esj3iuoj-0.jpeg	a	a	f	498
2685	client/img/fotos_pedidos/fotos_pedidos/10-3-117-c17uya0zs-0.jpeg	a	a	f	498
2836	client/img/fotos_pedidos/fotos_pedidos/11-2-117-lzy7fvg5w-0.jpeg	a	a	f	512
2900	client/img/fotos_pedidos/fotos_pedidos/11-5-117-0nj1nwdze-9.png	a	a	f	520
2903	client/img/fotos_pedidos/fotos_pedidos/11-5-117-ct7c927t5-8.png	a	a	f	520
2906	client/img/fotos_pedidos/fotos_pedidos/11-5-117-yeiid50sr-5.png	a	a	f	520
2909	client/img/fotos_pedidos/fotos_pedidos/11-5-117-1qf9sojo2-7.png	a	a	f	520
2912	client/img/fotos_pedidos/fotos_pedidos/11-5-117-goajmhhqq-1.png	a	a	f	520
2915	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g5eb7p5vu-4.png	a	a	f	520
2918	client/img/fotos_pedidos/fotos_pedidos/11-5-117-g6r7sgfso-7.png	a	a	f	520
2921	client/img/fotos_pedidos/fotos_pedidos/11-5-117-9xsw9mmvc-10.png	a	a	f	520
2997	client/img/fotos_pedidos/fotos_pedidos/11-3-117-0tpkm43zb-0.jpeg	a	a	f	533
2999	client/img/fotos_pedidos/fotos_pedidos/11-3-117-vcbfgy37r-2.jpeg	a	a	f	533
3001	client/img/fotos_pedidos/fotos_pedidos/11-3-117-zggjpucqk-4.jpeg	a	a	f	533
3004	client/img/fotos_pedidos/fotos_pedidos/11-3-117-hkq4gsoua-9.jpeg	a	a	f	533
3008	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7kk7yecsf-1.jpeg	a	a	f	533
3012	client/img/fotos_pedidos/fotos_pedidos/11-3-117-o70gvyv9j-5.jpeg	a	a	f	533
1505	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ytxswh5j1-3.jpeg	a	a	f	355
1507	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ft3f6gif1-5.jpeg	a	a	f	355
1510	client/img/fotos_pedidos/fotos_pedidos/8-4-117-p8399c28p-1.jpeg	a	a	f	355
1513	client/img/fotos_pedidos/fotos_pedidos/8-4-117-4nxxf4zfi-3.jpeg	a	a	f	355
1184	client/img/fotos_pedidos/fotos_pedidos/7-1-117-njcc4usi3-0.jpeg	a	a	t	312
1516	client/img/fotos_pedidos/fotos_pedidos/8-4-117-tsf6z9wl9-6.jpeg	a	a	f	355
1519	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ky9f7bo4l-1.jpeg	a	a	f	355
1524	client/img/fotos_pedidos/fotos_pedidos/8-4-117-uh3opbnsd-7.jpeg	a	a	f	355
1528	client/img/fotos_pedidos/fotos_pedidos/8-4-117-yj9wdbcia-3.jpeg	a	a	f	355
1532	client/img/fotos_pedidos/fotos_pedidos/8-4-117-pks6bnftv-6.jpeg	a	a	f	355
1536	client/img/fotos_pedidos/fotos_pedidos/8-4-117-qjn6xvf9l-2.jpeg	a	a	f	355
1540	client/img/fotos_pedidos/fotos_pedidos/8-4-117-1qs9hqykw-6.jpeg	a	a	f	355
1680	client/img/fotos_pedidos/fotos_pedidos/8-3-117-p6noua2ge-0.jpeg	a	a	f	376
1385	client/img/fotos_pedidos/fotos_pedidos/7-1-117-2yb2x89j1-0.jpeg	a	a	f	336
1454	client/img/fotos_pedidos/fotos_pedidos/8-4-117-52z3sdc4k-0.jpeg	a	a	f	343
1681	client/img/fotos_pedidos/fotos_pedidos/8-3-117-3l23zgabn-0.jpeg	a	a	f	376
1682	client/img/fotos_pedidos/fotos_pedidos/8-3-117-1fbnf7qp8-0.jpeg	a	a	f	376
1801	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ftd5fwrke-1.jpeg	a	a	f	392
1803	client/img/fotos_pedidos/fotos_pedidos/9-3-117-kjdfpyk48-3.jpeg	a	a	f	392
1805	client/img/fotos_pedidos/fotos_pedidos/9-3-117-35n9ln5p1-5.jpeg	a	a	f	392
1936	client/img/fotos_pedidos/fotos_pedidos/9-1-117-kelm0blh3-0.jpeg	a	a	f	406
2035	client/img/fotos_pedidos/fotos_pedidos/9-4-117-66ehc2q08-0.png	a	a	f	420
2036	client/img/fotos_pedidos/fotos_pedidos/9-4-117-1q5j0w81b-1.jpeg	a	a	f	420
2037	client/img/fotos_pedidos/fotos_pedidos/9-4-117-dhecvsxhb-2.jpeg	a	a	f	420
2101	client/img/fotos_pedidos/fotos_pedidos/9-3-117-2iowf76qd-0.jpeg	a	a	f	432
2103	client/img/fotos_pedidos/fotos_pedidos/9-3-117-k64w0jnyi-3.jpeg	a	a	f	432
2106	client/img/fotos_pedidos/fotos_pedidos/9-3-117-17uo0acdo-7.jpeg	a	a	f	432
2197	client/img/fotos_pedidos/fotos_pedidos/9-0-117-x93z2wta9-0.jpeg	a	a	f	440
2199	client/img/fotos_pedidos/fotos_pedidos/9-0-117-diw2zje0l-0.jpeg	a	a	f	440
2201	client/img/fotos_pedidos/fotos_pedidos/9-0-117-6p3saqydd-0.jpeg	a	a	f	440
2205	client/img/fotos_pedidos/fotos_pedidos/9-0-117-be32derj4-0.jpeg	a	a	f	440
2209	client/img/fotos_pedidos/fotos_pedidos/9-0-117-kx1yj7ux5-0.jpeg	a	a	f	440
2214	client/img/fotos_pedidos/fotos_pedidos/9-0-117-25k20fayx-0.jpeg	a	a	f	440
2281	client/img/fotos_pedidos/fotos_pedidos/10-1-117-qlu4hx0vs-3.jpeg	a	a	f	455
2283	client/img/fotos_pedidos/fotos_pedidos/10-1-117-6047vzbcw-5.jpeg	a	a	f	455
2285	client/img/fotos_pedidos/fotos_pedidos/10-1-117-u439lmhej-7.jpeg	a	a	f	455
2287	client/img/fotos_pedidos/fotos_pedidos/10-1-117-12mcr9f4y-1.jpeg	a	a	f	455
2405	client/img/fotos_pedidos/fotos_pedidos/10-1-117-90p6oy1lr-0.jpeg	a	a	f	467
2469	client/img/fotos_pedidos/fotos_pedidos/10-4-117-gx8naylot-9.png	a	a	f	473
2595	client/img/fotos_pedidos/fotos_pedidos/10-4-117-3swfeamwt-0.jpeg	a	a	f	486
2597	client/img/fotos_pedidos/fotos_pedidos/10-4-117-q70nna5wb-0.jpeg	a	a	f	486
2599	client/img/fotos_pedidos/fotos_pedidos/10-4-117-wclmhrxpq-0.jpeg	a	a	f	486
2603	client/img/fotos_pedidos/fotos_pedidos/10-4-117-80yl6238i-0.jpeg	a	a	f	486
2686	client/img/fotos_pedidos/fotos_pedidos/10-4-117-57yodla5s-0.jpeg	a	a	t	499
2687	client/img/fotos_pedidos/fotos_pedidos/10-4-117-qz5e2afto-0.jpeg	a	a	f	500
2837	client/img/fotos_pedidos/fotos_pedidos/11-2-117-x079l2e1y-1.jpeg	a	a	f	512
2924	client/img/fotos_pedidos/fotos_pedidos/11-1-117-ajkc3759a-2.jpeg	a	a	f	521
2998	client/img/fotos_pedidos/fotos_pedidos/11-3-117-96dhhpdvj-1.jpeg	a	a	f	533
3000	client/img/fotos_pedidos/fotos_pedidos/11-3-117-nf23rh8xm-3.jpeg	a	a	f	533
3002	client/img/fotos_pedidos/fotos_pedidos/11-3-117-orqmn93ce-5.jpeg	a	a	f	533
3006	client/img/fotos_pedidos/fotos_pedidos/11-3-117-o1f6eef53-0.jpeg	a	a	f	533
3010	client/img/fotos_pedidos/fotos_pedidos/11-3-117-dys6dnsbe-3.png	a	a	f	533
3014	client/img/fotos_pedidos/fotos_pedidos/11-3-117-beovch4e2-7.jpeg	a	a	f	533
3018	client/img/fotos_pedidos/fotos_pedidos/11-3-117-drw2xgldu-2.jpeg	a	a	f	533
3022	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8pxftuxtr-5.jpeg	a	a	f	533
3026	client/img/fotos_pedidos/fotos_pedidos/11-3-117-lupwqn020-9.jpeg	a	a	f	533
3104	client/img/fotos_pedidos/fotos_pedidos/11-3-117-e7v5xgg73-0.jpeg	a	a	f	538
3107	client/img/fotos_pedidos/fotos_pedidos/11-3-117-cm9nv4v5u-3.jpeg	a	a	f	538
3110	client/img/fotos_pedidos/fotos_pedidos/11-3-117-a9205hiai-6.jpeg	a	a	f	538
3113	client/img/fotos_pedidos/fotos_pedidos/11-3-117-xosgen4cd-9.jpeg	a	a	f	538
3116	client/img/fotos_pedidos/fotos_pedidos/11-3-117-0oj5lzrwc-2.jpeg	a	a	f	538
3119	client/img/fotos_pedidos/fotos_pedidos/11-3-117-a1ea70a72-5.jpeg	a	a	f	538
3123	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6oth8c0r6-9.jpeg	a	a	f	538
3126	client/img/fotos_pedidos/fotos_pedidos/11-3-117-jlezx9kao-2.jpeg	a	a	f	538
3128	client/img/fotos_pedidos/fotos_pedidos/11-3-117-swx1hov8q-4.jpeg	a	a	f	538
3131	client/img/fotos_pedidos/fotos_pedidos/11-3-117-a33qdciqx-7.jpeg	a	a	f	538
3135	client/img/fotos_pedidos/fotos_pedidos/11-3-117-0pkhmffgq-1.jpeg	a	a	f	538
3228	client/img/fotos_pedidos/fotos_pedidos/11-4-117-lxf49yv4o-0.jpeg	a	a	f	544
3234	client/img/fotos_pedidos/fotos_pedidos/11-4-117-g4nt87p5d-6.jpeg	a	a	f	544
3267	client/img/fotos_pedidos/fotos_pedidos/11-1-117-l5rtn6a0c-0.jpeg	a	a	f	549
3314	client/img/fotos_pedidos/fotos_pedidos/11-2-117-dmy9ntejb-23.jpeg	a	a	f	553
3329	client/img/fotos_pedidos/fotos_pedidos/11-2-117-r42g4ngv2-0.jpeg	a	a	f	557
3333	client/img/fotos_pedidos/fotos_pedidos/11-2-117-i9k6esrqa-0.png	a	a	f	560
3341	client/img/fotos_pedidos/fotos_pedidos/11-3-117-66mctglku-0.png	a	a	f	568
3349	client/img/fotos_pedidos/fotos_pedidos/12-6-117-v5qsaqdik-2.jpeg	a	a	t	574
688	client/img/fotos_pedidos/fotos_pedidos/6-4-117-mbju8limt-0.jpeg	a	a	f	186
1508	client/img/fotos_pedidos/fotos_pedidos/8-4-117-n0vqy22vr-6.jpeg	a	a	f	355
1511	client/img/fotos_pedidos/fotos_pedidos/8-4-117-pcalgo742-2.jpeg	a	a	f	355
1514	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ylogusqim-4.jpeg	a	a	f	355
1518	client/img/fotos_pedidos/fotos_pedidos/8-4-117-dx0lj2tw9-2.jpeg	a	a	f	355
1522	client/img/fotos_pedidos/fotos_pedidos/8-4-117-xoegcs5yb-4.jpeg	a	a	f	355
1526	client/img/fotos_pedidos/fotos_pedidos/8-4-117-w6ugmj1rj-0.jpeg	a	a	f	355
1530	client/img/fotos_pedidos/fotos_pedidos/8-4-117-5gvltm7hv-4.jpeg	a	a	f	355
1534	client/img/fotos_pedidos/fotos_pedidos/8-4-117-83lnz26vh-0.jpeg	a	a	f	355
1538	client/img/fotos_pedidos/fotos_pedidos/8-4-117-12sb9tr3w-4.jpeg	a	a	f	355
1386	client/img/fotos_pedidos/fotos_pedidos/7-1-117-r8rgfbwuc-0.jpeg	a	a	f	337
1387	client/img/fotos_pedidos/fotos_pedidos/7-1-117-k4d3d9m06-1.jpeg	a	a	f	337
1388	client/img/fotos_pedidos/fotos_pedidos/7-1-117-qfyt66sce-0.jpeg	a	a	f	337
1389	client/img/fotos_pedidos/fotos_pedidos/7-1-117-sxap4kgq0-2.jpeg	a	a	f	337
1393	client/img/fotos_pedidos/fotos_pedidos/7-1-117-z9r6iyie5-0.jpeg	a	a	f	337
1397	client/img/fotos_pedidos/fotos_pedidos/7-1-117-5hpfhiajf-0.jpeg	a	a	f	337
1401	client/img/fotos_pedidos/fotos_pedidos/7-1-117-z3my89h5m-0.jpeg	a	a	f	337
1455	client/img/fotos_pedidos/fotos_pedidos/8-4-117-g8hnctghn-0.jpeg	a	a	f	344
1806	client/img/fotos_pedidos/fotos_pedidos/9-4-117-9z5h1k8y0-0.jpeg	a	a	f	393
1807	client/img/fotos_pedidos/fotos_pedidos/9-4-117-aaqbvdb57-0.jpeg	a	a	f	393
1809	client/img/fotos_pedidos/fotos_pedidos/9-4-117-moqh904vg-0.jpeg	a	a	f	393
1811	client/img/fotos_pedidos/fotos_pedidos/9-4-117-bmbhgipnf-0.jpeg	a	a	f	393
1814	client/img/fotos_pedidos/fotos_pedidos/9-4-117-22amxy8pm-0.jpeg	a	a	f	393
1937	client/img/fotos_pedidos/fotos_pedidos/9-1-117-ad5rvpkyh-1.jpeg	a	a	f	406
2038	client/img/fotos_pedidos/fotos_pedidos/9-4-117-th0vte516-3.jpeg	a	a	f	420
2102	client/img/fotos_pedidos/fotos_pedidos/9-3-117-xsboo0nf3-1.jpeg	a	a	f	432
2104	client/img/fotos_pedidos/fotos_pedidos/9-3-117-oisoo3wg9-4.jpeg	a	a	f	432
2107	client/img/fotos_pedidos/fotos_pedidos/9-3-117-7fmylkhfn-6.jpeg	a	a	f	432
2198	client/img/fotos_pedidos/fotos_pedidos/9-0-117-7ilorys26-0.jpeg	a	a	f	440
2200	client/img/fotos_pedidos/fotos_pedidos/9-0-117-mc68b5701-0.jpeg	a	a	f	440
2202	client/img/fotos_pedidos/fotos_pedidos/9-0-117-vseal5h1g-0.jpeg	a	a	f	440
2206	client/img/fotos_pedidos/fotos_pedidos/9-0-117-61oswq1qt-0.jpeg	a	a	f	440
2210	client/img/fotos_pedidos/fotos_pedidos/9-0-117-qn1kybd5k-0.jpeg	a	a	f	440
2213	client/img/fotos_pedidos/fotos_pedidos/9-0-117-pu6m4giw0-0.jpeg	a	a	f	440
2288	client/img/fotos_pedidos/fotos_pedidos/10-1-117-vyjy0p3uf-2.jpeg	a	a	t	456
2290	client/img/fotos_pedidos/fotos_pedidos/10-1-117-cm59ukrf5-1.jpeg	a	a	t	456
2292	client/img/fotos_pedidos/fotos_pedidos/10-1-117-14pz1roql-4.jpeg	a	a	t	456
2295	client/img/fotos_pedidos/fotos_pedidos/10-1-117-004pz6qdq-6.jpeg	a	a	t	456
2298	client/img/fotos_pedidos/fotos_pedidos/10-1-117-c67a15ubd-1.jpeg	a	a	t	456
2301	client/img/fotos_pedidos/fotos_pedidos/10-1-117-m920qxraj-5.jpeg	a	a	t	456
2406	client/img/fotos_pedidos/fotos_pedidos/10-1-117-2b3zfg0jm-0.png	a	a	f	467
2407	client/img/fotos_pedidos/fotos_pedidos/10-1-117-dr2re2gmp-1.png	a	a	f	467
2408	client/img/fotos_pedidos/fotos_pedidos/10-1-117-2grpw2bz4-2.png	a	a	f	467
2470	client/img/fotos_pedidos/fotos_pedidos/10-4-117-q9cnoafv3-0.jpeg	a	a	f	474
2471	client/img/fotos_pedidos/fotos_pedidos/10-4-117-c6lkeofwn-0.jpeg	a	a	f	474
2472	client/img/fotos_pedidos/fotos_pedidos/10-4-117-dhbbrf4yd-0.png	a	a	f	474
2473	client/img/fotos_pedidos/fotos_pedidos/10-4-117-8a3cyeq8l-0.jpeg	a	a	f	474
2601	client/img/fotos_pedidos/fotos_pedidos/10-4-117-rhwg1la7t-0.jpeg	a	a	f	486
2688	client/img/fotos_pedidos/fotos_pedidos/10-4-117-reubjr411-0.jpeg	a	a	f	501
2689	client/img/fotos_pedidos/fotos_pedidos/10-4-117-k3jyafwrv-2.jpeg	a	a	f	501
2690	client/img/fotos_pedidos/fotos_pedidos/10-4-117-h0h8qg0ka-1.jpeg	a	a	f	501
2691	client/img/fotos_pedidos/fotos_pedidos/10-4-117-udjzq0c4b-3.jpeg	a	a	f	501
2692	client/img/fotos_pedidos/fotos_pedidos/10-4-117-rlz9vejxp-4.jpeg	a	a	f	501
2693	client/img/fotos_pedidos/fotos_pedidos/10-4-117-z8vaeu077-5.jpeg	a	a	f	501
2694	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7tkbjj0xs-7.jpeg	a	a	f	501
2695	client/img/fotos_pedidos/fotos_pedidos/10-4-117-3yn9miek3-6.jpeg	a	a	f	501
2696	client/img/fotos_pedidos/fotos_pedidos/10-4-117-4tko2anzi-8.jpeg	a	a	f	501
2698	client/img/fotos_pedidos/fotos_pedidos/10-4-117-p57777ujy-0.jpeg	a	a	f	501
2839	client/img/fotos_pedidos/fotos_pedidos/11-2-117-rpa2tu946-3.jpeg	a	a	f	512
2925	client/img/fotos_pedidos/fotos_pedidos/11-1-117-4u46s7ey4-0.jpeg	a	a	f	521
3003	client/img/fotos_pedidos/fotos_pedidos/11-3-117-63q6ndcle-6.jpeg	a	a	f	533
3007	client/img/fotos_pedidos/fotos_pedidos/11-3-117-2zck2s3n7-8.jpeg	a	a	f	533
3011	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8gougox0t-4.jpeg	a	a	f	533
3015	client/img/fotos_pedidos/fotos_pedidos/11-3-117-2qf4on7wc-8.jpeg	a	a	f	533
3019	client/img/fotos_pedidos/fotos_pedidos/11-3-117-3fmc5aoo9-1.jpeg	a	a	f	533
3023	client/img/fotos_pedidos/fotos_pedidos/11-3-117-9nf38tytk-6.jpeg	a	a	f	533
3028	client/img/fotos_pedidos/fotos_pedidos/11-3-117-bbkos1r0g-0.jpeg	a	a	f	533
3105	client/img/fotos_pedidos/fotos_pedidos/11-3-117-hy6jutjm4-1.jpeg	a	a	f	538
3108	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7p2jduoxw-4.jpeg	a	a	f	538
3111	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8gyr3ro0e-7.jpeg	a	a	f	538
3114	client/img/fotos_pedidos/fotos_pedidos/11-3-117-5ddyklcyq-0.jpeg	a	a	f	538
3117	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8swh5f8ca-3.jpeg	a	a	f	538
3121	client/img/fotos_pedidos/fotos_pedidos/11-3-117-6olpyqvkr-7.jpeg	a	a	f	538
3124	client/img/fotos_pedidos/fotos_pedidos/11-3-117-d8ncu7eh9-0.jpeg	a	a	f	538
3127	client/img/fotos_pedidos/fotos_pedidos/11-3-117-fptprdbgq-3.jpeg	a	a	f	538
1517	client/img/fotos_pedidos/fotos_pedidos/8-4-117-nlg6xrwvk-7.jpeg	a	a	f	355
1521	client/img/fotos_pedidos/fotos_pedidos/8-4-117-xnjeltyef-3.jpeg	a	a	f	355
1525	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ss4vc3vh0-6.jpeg	a	a	f	355
1529	client/img/fotos_pedidos/fotos_pedidos/8-4-117-2y77s8tg9-2.jpeg	a	a	f	355
1533	client/img/fotos_pedidos/fotos_pedidos/8-4-117-e84t5jv0y-7.jpeg	a	a	f	355
1537	client/img/fotos_pedidos/fotos_pedidos/8-4-117-8emnnaool-3.jpeg	a	a	f	355
1541	client/img/fotos_pedidos/fotos_pedidos/8-4-117-baeh5hotp-7.jpeg	a	a	f	355
1281	client/img/fotos_pedidos/fotos_pedidos/7-2-117-utrudk0qf-0.jpeg	a	a	f	325
1282	client/img/fotos_pedidos/fotos_pedidos/7-2-117-7o24xy3a5-1.jpeg	a	a	f	325
1283	client/img/fotos_pedidos/fotos_pedidos/7-2-117-mo1rimzig-3.jpeg	a	a	f	325
1284	client/img/fotos_pedidos/fotos_pedidos/7-2-117-tyq7tvwhm-2.jpeg	a	a	f	325
1285	client/img/fotos_pedidos/fotos_pedidos/7-2-117-iiuez8b0p-5.jpeg	a	a	f	325
1286	client/img/fotos_pedidos/fotos_pedidos/7-2-117-zk8hzlaxp-4.jpeg	a	a	f	325
1287	client/img/fotos_pedidos/fotos_pedidos/7-2-117-2d5zbnkdw-7.jpeg	a	a	f	325
1289	client/img/fotos_pedidos/fotos_pedidos/7-2-117-g8yszw4z3-6.jpeg	a	a	f	325
1292	client/img/fotos_pedidos/fotos_pedidos/7-2-117-rnarbwp0c-11.jpeg	a	a	f	325
1390	client/img/fotos_pedidos/fotos_pedidos/7-1-117-273gxsqdv-0.jpeg	a	a	f	337
1394	client/img/fotos_pedidos/fotos_pedidos/7-1-117-yjt0jnggs-0.jpeg	a	a	f	337
1398	client/img/fotos_pedidos/fotos_pedidos/7-1-117-x0ign27re-0.jpeg	a	a	f	337
1808	client/img/fotos_pedidos/fotos_pedidos/9-4-117-yuulr9wyh-0.jpeg	a	a	f	393
1810	client/img/fotos_pedidos/fotos_pedidos/9-4-117-9ak6qss6i-0.jpeg	a	a	f	393
1813	client/img/fotos_pedidos/fotos_pedidos/9-4-117-hmkn5o9y6-0.jpeg	a	a	f	393
1938	client/img/fotos_pedidos/fotos_pedidos/9-1-117-34gsy4gh7-1.png	a	a	f	407
1939	client/img/fotos_pedidos/fotos_pedidos/9-1-117-yxacbigws-0.jpeg	a	a	f	407
1940	client/img/fotos_pedidos/fotos_pedidos/9-1-117-hifu1w5bk-3.jpeg	a	a	f	407
1942	client/img/fotos_pedidos/fotos_pedidos/9-1-117-lqfgu4e5k-5.jpeg	a	a	f	407
1944	client/img/fotos_pedidos/fotos_pedidos/9-1-117-2mifn0bkv-6.jpeg	a	a	f	407
2039	client/img/fotos_pedidos/fotos_pedidos/9-5-117-82m9fnigh-0.jpeg	a	a	f	421
2040	client/img/fotos_pedidos/fotos_pedidos/9-5-117-zwjdrek3u-0.jpeg	a	a	f	421
2041	client/img/fotos_pedidos/fotos_pedidos/9-5-117-v0y2fs7jq-0.jpeg	a	a	f	421
2109	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ez04lg3ns-2.jpeg	a	a	f	432
2203	client/img/fotos_pedidos/fotos_pedidos/9-0-117-khz8ku18q-0.jpeg	a	a	f	440
2207	client/img/fotos_pedidos/fotos_pedidos/9-0-117-krtc9fbbz-0.jpeg	a	a	f	440
2211	client/img/fotos_pedidos/fotos_pedidos/9-0-117-5lmspqnm0-0.jpeg	a	a	f	440
2289	client/img/fotos_pedidos/fotos_pedidos/10-1-117-90c12ebjr-0.jpeg	a	a	t	456
2291	client/img/fotos_pedidos/fotos_pedidos/10-1-117-d2kbi4apb-3.jpeg	a	a	t	456
2294	client/img/fotos_pedidos/fotos_pedidos/10-1-117-063gaw4cq-7.jpeg	a	a	t	456
2297	client/img/fotos_pedidos/fotos_pedidos/10-1-117-moggbdtv5-0.jpeg	a	a	t	456
2300	client/img/fotos_pedidos/fotos_pedidos/10-1-117-s26h6ggbr-2.jpeg	a	a	t	456
2303	client/img/fotos_pedidos/fotos_pedidos/10-1-117-gymz2fk2w-3.jpeg	a	a	t	456
2409	client/img/fotos_pedidos/fotos_pedidos/10-3-117-5v0h2dv57-0.jpeg	a	a	f	468
2412	client/img/fotos_pedidos/fotos_pedidos/10-3-117-qm7ueeorx-0.jpeg	a	a	f	468
2474	client/img/fotos_pedidos/fotos_pedidos/10-5-117-9qo0pjv0f-0.jpeg	a	a	f	475
2475	client/img/fotos_pedidos/fotos_pedidos/10-5-117-i3p2rcgnc-1.jpeg	a	a	f	475
2476	client/img/fotos_pedidos/fotos_pedidos/10-5-117-jkk2svd3h-2.jpeg	a	a	f	475
2478	client/img/fotos_pedidos/fotos_pedidos/10-5-117-6gfev3tyn-4.jpeg	a	a	f	475
2480	client/img/fotos_pedidos/fotos_pedidos/10-5-117-94arqnvfa-6.jpeg	a	a	f	475
2482	client/img/fotos_pedidos/fotos_pedidos/10-5-117-ei0vs0lx5-8.jpeg	a	a	f	475
2602	client/img/fotos_pedidos/fotos_pedidos/10-4-117-pwm1w3uwh-0.jpeg	a	a	f	486
2697	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7tx8ryyfo-0.jpeg	a	a	f	501
2699	client/img/fotos_pedidos/fotos_pedidos/10-4-117-foxl95bef-1.jpeg	a	a	f	501
2840	client/img/fotos_pedidos/fotos_pedidos/11-2-117-tfgqs0yqe-1.jpeg	a	a	f	514
2842	client/img/fotos_pedidos/fotos_pedidos/11-2-117-l0to8ziq7-3.jpeg	a	a	f	514
2844	client/img/fotos_pedidos/fotos_pedidos/11-2-117-evwx7olv9-4.jpeg	a	a	f	514
2849	client/img/fotos_pedidos/fotos_pedidos/11-2-117-lk8rdm4or-9.png	a	a	f	514
2926	client/img/fotos_pedidos/fotos_pedidos/11-1-117-aixf1si2r-1.jpeg	a	a	f	521
3005	client/img/fotos_pedidos/fotos_pedidos/11-3-117-mxkko5z08-7.jpeg	a	a	f	533
3009	client/img/fotos_pedidos/fotos_pedidos/11-3-117-7zqp5e45f-2.jpeg	a	a	f	533
3013	client/img/fotos_pedidos/fotos_pedidos/11-3-117-eglj7i8xr-6.jpeg	a	a	f	533
3017	client/img/fotos_pedidos/fotos_pedidos/11-3-117-f6ax15fai-9.jpeg	a	a	f	533
3021	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ia0mvdti5-4.jpeg	a	a	f	533
3025	client/img/fotos_pedidos/fotos_pedidos/11-3-117-uslj1fala-7.jpeg	a	a	f	533
3106	client/img/fotos_pedidos/fotos_pedidos/11-3-117-c50fcp3dp-2.jpeg	a	a	f	538
3109	client/img/fotos_pedidos/fotos_pedidos/11-3-117-oiy3ybiau-5.jpeg	a	a	f	538
3112	client/img/fotos_pedidos/fotos_pedidos/11-3-117-sd0sykgce-8.jpeg	a	a	f	538
3115	client/img/fotos_pedidos/fotos_pedidos/11-3-117-8hkcv2cs0-1.jpeg	a	a	f	538
3118	client/img/fotos_pedidos/fotos_pedidos/11-3-117-zaesojtu4-4.jpeg	a	a	f	538
3120	client/img/fotos_pedidos/fotos_pedidos/11-3-117-uljfzkzq1-6.jpeg	a	a	f	538
3122	client/img/fotos_pedidos/fotos_pedidos/11-3-117-wzgss0puu-8.jpeg	a	a	f	538
3125	client/img/fotos_pedidos/fotos_pedidos/11-3-117-r0npv12a9-1.jpeg	a	a	f	538
3129	client/img/fotos_pedidos/fotos_pedidos/11-3-117-i57zfupuv-6.jpeg	a	a	f	538
3132	client/img/fotos_pedidos/fotos_pedidos/11-3-117-89oj082rd-8.jpeg	a	a	f	538
3134	client/img/fotos_pedidos/fotos_pedidos/11-3-117-io0sd4xe4-0.jpeg	a	a	f	538
3231	client/img/fotos_pedidos/fotos_pedidos/11-4-117-8y37mld9m-3.jpeg	a	a	f	544
3237	client/img/fotos_pedidos/fotos_pedidos/11-4-117-k71wjsmby-9.jpeg	a	a	f	544
1542	client/img/fotos_pedidos/fotos_pedidos/8-4-117-mvlvlykwf-0.jpeg	a	a	f	356
1199	client/img/fotos_pedidos/fotos_pedidos/7-5-117-cqg9m36ur-0.jpeg	a	a	f	315
1202	client/img/fotos_pedidos/fotos_pedidos/7-5-117-av9j6yqt2-3.jpeg	a	a	f	315
1288	client/img/fotos_pedidos/fotos_pedidos/7-2-117-425dlodz3-8.jpeg	a	a	f	325
1290	client/img/fotos_pedidos/fotos_pedidos/7-2-117-obj5d6i2m-10.jpeg	a	a	f	325
1391	client/img/fotos_pedidos/fotos_pedidos/7-1-117-1n9tyejyb-0.jpeg	a	a	f	337
1395	client/img/fotos_pedidos/fotos_pedidos/7-1-117-5vllj0ifg-0.jpeg	a	a	f	337
1399	client/img/fotos_pedidos/fotos_pedidos/7-1-117-8lme4wtzb-0.jpeg	a	a	f	337
1543	client/img/fotos_pedidos/fotos_pedidos/8-4-117-fa0f26zad-0.jpeg	a	a	f	356
1544	client/img/fotos_pedidos/fotos_pedidos/8-4-117-vfknazgap-0.jpeg	a	a	f	356
1545	client/img/fotos_pedidos/fotos_pedidos/8-4-117-e8gf1xyip-0.jpeg	a	a	f	356
1546	client/img/fotos_pedidos/fotos_pedidos/8-4-117-q4w95av3w-0.jpeg	a	a	f	356
1547	client/img/fotos_pedidos/fotos_pedidos/8-4-117-x97ojqct7-0.jpeg	a	a	f	356
1548	client/img/fotos_pedidos/fotos_pedidos/8-4-117-hevcvtv7s-0.jpeg	a	a	f	356
1549	client/img/fotos_pedidos/fotos_pedidos/8-4-117-f1owm03ja-0.jpeg	a	a	f	356
1550	client/img/fotos_pedidos/fotos_pedidos/8-4-117-gody7y7q3-0.jpeg	a	a	f	356
1551	client/img/fotos_pedidos/fotos_pedidos/8-4-117-ckwqq56k8-0.jpeg	a	a	f	356
1552	client/img/fotos_pedidos/fotos_pedidos/8-4-117-wsi8htdc1-0.jpeg	a	a	f	356
1553	client/img/fotos_pedidos/fotos_pedidos/8-4-117-3xrkanm4k-0.jpeg	a	a	f	356
1554	client/img/fotos_pedidos/fotos_pedidos/8-4-117-w88ux7we4-0.jpeg	a	a	f	356
1555	client/img/fotos_pedidos/fotos_pedidos/8-4-117-h43e9e0gb-0.jpeg	a	a	f	356
1556	client/img/fotos_pedidos/fotos_pedidos/8-4-117-e8w9b66lo-0.jpeg	a	a	f	356
1557	client/img/fotos_pedidos/fotos_pedidos/8-4-117-hvma95xdj-0.jpeg	a	a	f	356
1812	client/img/fotos_pedidos/fotos_pedidos/9-4-117-n3hfdtytb-0.jpeg	a	a	f	393
1815	client/img/fotos_pedidos/fotos_pedidos/9-4-117-mbc4uitc1-0.jpeg	a	a	f	393
1941	client/img/fotos_pedidos/fotos_pedidos/9-1-117-w8lxbq003-4.jpeg	a	a	f	407
1943	client/img/fotos_pedidos/fotos_pedidos/9-1-117-16ishfqpv-2.jpeg	a	a	f	407
2042	client/img/fotos_pedidos/fotos_pedidos/9-5-117-30list186-0.jpeg	a	a	f	422
2043	client/img/fotos_pedidos/fotos_pedidos/9-5-117-6lqop124c-0.jpeg	a	a	f	422
2110	client/img/fotos_pedidos/fotos_pedidos/9-3-117-krbwvb1ue-4.jpeg	a	a	f	432
876	client/img/fotos_pedidos/fotos_pedidos/6-3-117-46jl1o9im-0.jpeg	a	a	f	228
2204	client/img/fotos_pedidos/fotos_pedidos/9-0-117-vlv5ll3k0-0.jpeg	a	a	f	440
2208	client/img/fotos_pedidos/fotos_pedidos/9-0-117-913e0x78x-0.jpeg	a	a	f	440
2212	client/img/fotos_pedidos/fotos_pedidos/9-0-117-uews1ulve-0.jpeg	a	a	f	440
2293	client/img/fotos_pedidos/fotos_pedidos/10-1-117-8fpic1rba-5.jpeg	a	a	t	456
2296	client/img/fotos_pedidos/fotos_pedidos/10-1-117-2fevi7saz-8.jpeg	a	a	t	456
2299	client/img/fotos_pedidos/fotos_pedidos/10-1-117-7fp41t65i-9.jpeg	a	a	t	456
2302	client/img/fotos_pedidos/fotos_pedidos/10-1-117-6whqg1yv3-4.jpeg	a	a	t	456
2410	client/img/fotos_pedidos/fotos_pedidos/10-3-117-zha3hf94y-0.jpeg	a	a	f	468
2477	client/img/fotos_pedidos/fotos_pedidos/10-5-117-f8cgt4kvi-3.jpeg	a	a	f	475
2479	client/img/fotos_pedidos/fotos_pedidos/10-5-117-i4dj4fd66-5.jpeg	a	a	f	475
2481	client/img/fotos_pedidos/fotos_pedidos/10-5-117-lxrdquagw-7.jpeg	a	a	f	475
2483	client/img/fotos_pedidos/fotos_pedidos/10-5-117-paa94pn21-9.jpeg	a	a	f	475
2604	client/img/fotos_pedidos/fotos_pedidos/10-4-117-sk9njwnt4-0.jpeg	a	a	f	487
2605	client/img/fotos_pedidos/fotos_pedidos/10-4-117-9dzo6bmgj-1.jpeg	a	a	f	487
2700	client/img/fotos_pedidos/fotos_pedidos/10-4-117-reubjr411-0.jpeg	a	a	f	502
2701	client/img/fotos_pedidos/fotos_pedidos/10-4-117-h0h8qg0ka-1.jpeg	a	a	f	502
2702	client/img/fotos_pedidos/fotos_pedidos/10-4-117-udjzq0c4b-3.jpeg	a	a	f	502
2703	client/img/fotos_pedidos/fotos_pedidos/10-4-117-k3jyafwrv-2.jpeg	a	a	f	502
2706	client/img/fotos_pedidos/fotos_pedidos/10-4-117-z8vaeu077-5.jpeg	a	a	f	502
2709	client/img/fotos_pedidos/fotos_pedidos/10-4-117-foxl95bef-1.jpeg	a	a	f	502
2841	client/img/fotos_pedidos/fotos_pedidos/11-2-117-tqdmr1g0o-0.jpeg	a	a	f	514
2843	client/img/fotos_pedidos/fotos_pedidos/11-2-117-jft89auvs-2.jpeg	a	a	f	514
2848	client/img/fotos_pedidos/fotos_pedidos/11-2-117-gb984wse6-8.jpeg	a	a	f	514
2927	client/img/fotos_pedidos/fotos_pedidos/11-1-117-auqtdvb9h-0.png	a	a	f	522
2928	client/img/fotos_pedidos/fotos_pedidos/11-1-117-16176widd-0.png	a	a	f	522
2929	client/img/fotos_pedidos/fotos_pedidos/11-1-117-bo6a55fel-0.png	a	a	f	522
2930	client/img/fotos_pedidos/fotos_pedidos/11-1-117-jtzaiio1j-0.png	a	a	f	522
2933	client/img/fotos_pedidos/fotos_pedidos/11-1-117-4cjx66gd2-0.jpeg	a	a	f	522
3016	client/img/fotos_pedidos/fotos_pedidos/11-3-117-iszr8v4lf-0.jpeg	a	a	f	533
3020	client/img/fotos_pedidos/fotos_pedidos/11-3-117-wozm0rz4n-3.png	a	a	f	533
3024	client/img/fotos_pedidos/fotos_pedidos/11-3-117-rfhchnf3i-8.jpeg	a	a	f	533
3027	client/img/fotos_pedidos/fotos_pedidos/11-3-117-dd8s5n0gg-0.jpeg	a	a	f	533
3130	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ic7audrlu-5.jpeg	a	a	f	538
3133	client/img/fotos_pedidos/fotos_pedidos/11-3-117-ej5kftbv3-9.jpeg	a	a	f	538
3238	client/img/fotos_pedidos/fotos_pedidos/11-4-117-tx6dxsrna-0.jpeg	a	a	f	545
3239	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ol40gev97-0.jpeg	a	a	f	545
3240	client/img/fotos_pedidos/fotos_pedidos/11-4-117-h45yk2n33-1.jpeg	a	a	f	545
3241	client/img/fotos_pedidos/fotos_pedidos/11-4-117-6q2u1bat0-2.jpeg	a	a	f	545
3242	client/img/fotos_pedidos/fotos_pedidos/11-4-117-w0i6nvf25-3.jpeg	a	a	f	545
3243	client/img/fotos_pedidos/fotos_pedidos/11-4-117-9p92yy80e-4.jpeg	a	a	f	545
3244	client/img/fotos_pedidos/fotos_pedidos/11-4-117-dyi3za3i3-5.jpeg	a	a	f	545
3245	client/img/fotos_pedidos/fotos_pedidos/11-4-117-6i5u03uee-6.jpeg	a	a	f	545
3246	client/img/fotos_pedidos/fotos_pedidos/11-4-117-u2gb0qyhj-7.jpeg	a	a	f	545
3247	client/img/fotos_pedidos/fotos_pedidos/11-4-117-kh44gi07x-0.jpeg	a	a	f	545
1118	client/img/fotos_pedidos/fotos_pedidos/7-4-117-91b6ompt5-0.jpeg	a	a	f	303
1119	client/img/fotos_pedidos/fotos_pedidos/7-4-117-gtacq4nuj-0.jpeg	a	a	f	303
1121	client/img/fotos_pedidos/fotos_pedidos/7-4-117-y9o5t07ja-0.jpeg	a	a	f	303
1123	client/img/fotos_pedidos/fotos_pedidos/7-4-117-y0qiyezv5-0.png	a	a	f	303
1125	client/img/fotos_pedidos/fotos_pedidos/7-4-117-5tiz5tw68-0.jpeg	a	a	f	303
1200	client/img/fotos_pedidos/fotos_pedidos/7-5-117-x319z09c0-2.jpeg	a	a	f	315
1204	client/img/fotos_pedidos/fotos_pedidos/7-5-117-kub401nyo-5.jpeg	a	a	f	315
1291	client/img/fotos_pedidos/fotos_pedidos/7-2-117-7pzbamdhx-9.jpeg	a	a	f	325
1392	client/img/fotos_pedidos/fotos_pedidos/7-1-117-tilt2oygx-0.jpeg	a	a	f	337
1396	client/img/fotos_pedidos/fotos_pedidos/7-1-117-8vo7iy9jm-0.jpeg	a	a	f	337
1400	client/img/fotos_pedidos/fotos_pedidos/7-1-117-30opi39jw-0.jpeg	a	a	f	337
1558	client/img/fotos_pedidos/fotos_pedidos/8-4-117-pn8s6jyre-0.jpeg	a	a	f	357
1707	client/img/fotos_pedidos/fotos_pedidos/8-3-117-sjkz6n9wm-0.jpeg	a	a	f	378
1708	client/img/fotos_pedidos/fotos_pedidos/8-3-117-rf9m5fcjx-0.jpeg	a	a	f	378
1709	client/img/fotos_pedidos/fotos_pedidos/8-3-117-uwa7aoz0o-1.jpeg	a	a	f	378
1713	client/img/fotos_pedidos/fotos_pedidos/8-3-117-qvk606sjv-0.png	a	a	f	378
1719	client/img/fotos_pedidos/fotos_pedidos/8-3-117-stl2t1rd7-1.png	a	a	f	378
1725	client/img/fotos_pedidos/fotos_pedidos/8-3-117-11nyp90xf-0.jpeg	a	a	f	378
1816	client/img/fotos_pedidos/fotos_pedidos/9-6-117-8ppbobsel-4.jpeg	a	a	f	394
1817	client/img/fotos_pedidos/fotos_pedidos/9-6-117-jrkoilor2-0.jpeg	a	a	f	394
1818	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ok47xlkwd-1.jpeg	a	a	f	394
1823	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ky1wicis4-6.jpeg	a	a	f	394
1829	client/img/fotos_pedidos/fotos_pedidos/9-6-117-x9kauxzhg-5.jpeg	a	a	f	394
1835	client/img/fotos_pedidos/fotos_pedidos/9-6-117-2wci9y7gq-3.jpeg	a	a	f	394
1841	client/img/fotos_pedidos/fotos_pedidos/9-6-117-yr9i7cx6p-1.jpeg	a	a	f	394
1847	client/img/fotos_pedidos/fotos_pedidos/9-6-117-hpoihi9ok-7.jpeg	a	a	f	394
1945	client/img/fotos_pedidos/fotos_pedidos/9-1-117-qnyi8unv3-7.jpeg	a	a	f	407
2044	client/img/fotos_pedidos/fotos_pedidos/9-5-117-s1gkqzy0r-0.jpeg	a	a	f	422
2111	client/img/fotos_pedidos/fotos_pedidos/9-3-117-b7vh6ul3c-0.jpeg	a	a	f	432
2215	client/img/fotos_pedidos/fotos_pedidos/9-1-117-sfjem4933-0.jpeg	a	a	f	442
2304	client/img/fotos_pedidos/fotos_pedidos/10-2-117-68hg5s3b3-0.jpeg	a	a	f	457
2307	client/img/fotos_pedidos/fotos_pedidos/10-2-117-aqf3lmals-3.jpeg	a	a	f	457
2411	client/img/fotos_pedidos/fotos_pedidos/10-3-117-3k4wip7uu-0.jpeg	a	a	f	468
2484	client/img/fotos_pedidos/fotos_pedidos/10-5-117-vmz54bhbj-0.jpeg	a	a	f	476
2485	client/img/fotos_pedidos/fotos_pedidos/10-5-117-q4j6a26ew-1.jpeg	a	a	f	476
2486	client/img/fotos_pedidos/fotos_pedidos/10-5-117-6457k0xwp-2.jpeg	a	a	f	476
2488	client/img/fotos_pedidos/fotos_pedidos/10-5-117-t1ogfa9ft-4.jpeg	a	a	f	476
2490	client/img/fotos_pedidos/fotos_pedidos/10-5-117-c8lhxzq99-6.jpeg	a	a	f	476
2492	client/img/fotos_pedidos/fotos_pedidos/10-5-117-8mtio8458-8.jpeg	a	a	f	476
2606	client/img/fotos_pedidos/fotos_pedidos/10-4-117-oeai40gmj-2.jpeg	a	a	f	487
2704	client/img/fotos_pedidos/fotos_pedidos/10-4-117-rlz9vejxp-4.jpeg	a	a	f	502
2707	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7tkbjj0xs-7.jpeg	a	a	f	502
956	client/img/fotos_pedidos/fotos_pedidos/7-4-117-4q2uc2vpx-1.jpeg	a	a	f	292
957	client/img/fotos_pedidos/fotos_pedidos/7-4-117-t40a94chu-0.jpeg	a	a	f	292
958	client/img/fotos_pedidos/fotos_pedidos/7-4-117-9rt753dom-3.jpeg	a	a	f	292
959	client/img/fotos_pedidos/fotos_pedidos/7-4-117-u7knp9axq-2.jpeg	a	a	f	292
960	client/img/fotos_pedidos/fotos_pedidos/7-4-117-jecu6mgul-0.jpeg	a	a	f	293
961	client/img/fotos_pedidos/fotos_pedidos/7-4-117-g21102v2l-1.jpeg	a	a	f	293
962	client/img/fotos_pedidos/fotos_pedidos/7-4-117-r5pxb98xf-2.jpeg	a	a	f	293
963	client/img/fotos_pedidos/fotos_pedidos/7-4-117-mew7f0o5u-3.jpeg	a	a	f	293
964	client/img/fotos_pedidos/fotos_pedidos/7-4-117-jg6afdxok-4.jpeg	a	a	f	293
965	client/img/fotos_pedidos/fotos_pedidos/7-4-117-g9ixikeja-5.jpeg	a	a	f	293
966	client/img/fotos_pedidos/fotos_pedidos/7-4-117-wwsydk11k-6.jpeg	a	a	f	293
967	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ci48uiuc4-7.jpeg	a	a	f	293
968	client/img/fotos_pedidos/fotos_pedidos/7-4-117-od5zrux5w-8.jpeg	a	a	f	293
969	client/img/fotos_pedidos/fotos_pedidos/7-4-117-qd500b8a3-9.jpeg	a	a	f	293
970	client/img/fotos_pedidos/fotos_pedidos/7-4-117-hzj9agmbz-10.jpeg	a	a	f	293
971	client/img/fotos_pedidos/fotos_pedidos/7-4-117-qunavubce-11.jpeg	a	a	f	293
972	client/img/fotos_pedidos/fotos_pedidos/7-4-117-d1qjpbcnk-12.jpeg	a	a	f	293
973	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ykl3su3fh-0.jpeg	a	a	f	293
974	client/img/fotos_pedidos/fotos_pedidos/7-4-117-7epagcj0k-1.jpeg	a	a	f	293
975	client/img/fotos_pedidos/fotos_pedidos/7-4-117-x9hl986f9-2.jpeg	a	a	f	293
976	client/img/fotos_pedidos/fotos_pedidos/7-4-117-q882chca9-3.jpeg	a	a	f	293
977	client/img/fotos_pedidos/fotos_pedidos/7-4-117-8f29c1i6y-4.jpeg	a	a	f	293
978	client/img/fotos_pedidos/fotos_pedidos/7-4-117-wlqato2sv-6.jpeg	a	a	f	293
979	client/img/fotos_pedidos/fotos_pedidos/7-4-117-blnl1j1wq-5.jpeg	a	a	f	293
980	client/img/fotos_pedidos/fotos_pedidos/7-4-117-pkui3h5dz-7.jpeg	a	a	f	293
981	client/img/fotos_pedidos/fotos_pedidos/7-4-117-vln8cjsrh-8.jpeg	a	a	f	293
982	client/img/fotos_pedidos/fotos_pedidos/7-4-117-65fu9gg5l-9.jpeg	a	a	f	293
983	client/img/fotos_pedidos/fotos_pedidos/7-4-117-62pi9wr3j-10.jpeg	a	a	f	293
984	client/img/fotos_pedidos/fotos_pedidos/7-4-117-cqopflorw-11.jpeg	a	a	f	293
985	client/img/fotos_pedidos/fotos_pedidos/7-4-117-mcj52npu9-12.jpeg	a	a	f	293
986	client/img/fotos_pedidos/fotos_pedidos/7-4-117-tmhk33g4u-13.jpeg	a	a	f	293
987	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ajitrfitd-0.jpeg	a	a	f	293
988	client/img/fotos_pedidos/fotos_pedidos/7-4-117-3eatz51yc-1.jpeg	a	a	f	293
989	client/img/fotos_pedidos/fotos_pedidos/7-4-117-cm837ki8t-2.jpeg	a	a	f	293
995	client/img/fotos_pedidos/fotos_pedidos/7-4-117-sdpjl1rvi-8.jpeg	a	a	f	293
1001	client/img/fotos_pedidos/fotos_pedidos/7-4-117-vxulc5unu-0.jpeg	a	a	f	293
1007	client/img/fotos_pedidos/fotos_pedidos/7-4-117-h7mped90x-6.jpeg	a	a	f	293
1013	client/img/fotos_pedidos/fotos_pedidos/7-4-117-71fh5z9vh-12.jpeg	a	a	f	293
1019	client/img/fotos_pedidos/fotos_pedidos/7-4-117-vlpwi1np4-4.jpeg	a	a	f	293
1025	client/img/fotos_pedidos/fotos_pedidos/7-4-117-wncrpr0gl-10.jpeg	a	a	f	293
1120	client/img/fotos_pedidos/fotos_pedidos/7-4-117-kg6vggpw7-0.jpeg	a	a	f	303
1122	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ilc0lqht3-0.png	a	a	f	303
1124	client/img/fotos_pedidos/fotos_pedidos/7-4-117-dlm70lmc0-0.png	a	a	f	303
1126	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fhhnidjmc-0.jpeg	a	a	f	303
1201	client/img/fotos_pedidos/fotos_pedidos/7-5-117-p55upl8ct-1.jpeg	a	a	f	315
1293	client/img/fotos_pedidos/fotos_pedidos/7-2-117-1zxx1kvd2-2.jpeg	a	a	t	326
1294	client/img/fotos_pedidos/fotos_pedidos/7-2-117-j1s8nj6ei-0.jpeg	a	a	t	326
1295	client/img/fotos_pedidos/fotos_pedidos/7-2-117-7uf2yzuk4-1.jpeg	a	a	t	326
1297	client/img/fotos_pedidos/fotos_pedidos/7-2-117-5xruflsj3-4.jpeg	a	a	t	326
1299	client/img/fotos_pedidos/fotos_pedidos/7-2-117-qv8e1ltk3-6.png	a	a	t	326
1301	client/img/fotos_pedidos/fotos_pedidos/7-2-117-dcme5bu78-9.jpeg	a	a	t	326
1303	client/img/fotos_pedidos/fotos_pedidos/7-2-117-c1jhgokjx-10.jpeg	a	a	t	326
1402	client/img/fotos_pedidos/fotos_pedidos/7-1-117-w3wq63fw6-1.jpeg	a	a	f	338
1404	client/img/fotos_pedidos/fotos_pedidos/7-1-117-g7e1hwlw9-3.jpeg	a	a	f	338
1405	client/img/fotos_pedidos/fotos_pedidos/7-1-117-aagegppdi-4.jpeg	a	a	f	338
1407	client/img/fotos_pedidos/fotos_pedidos/7-1-117-obwfykapd-5.jpeg	a	a	f	338
1412	client/img/fotos_pedidos/fotos_pedidos/7-1-117-ruemo0av0-7.jpeg	a	a	f	338
1416	client/img/fotos_pedidos/fotos_pedidos/7-1-117-kx0wammyd-9.jpeg	a	a	f	338
1559	client/img/fotos_pedidos/fotos_pedidos/8-4-117-q15amhmub-1.jpeg	a	a	f	357
1710	client/img/fotos_pedidos/fotos_pedidos/8-3-117-h8nqpln7h-2.jpeg	a	a	f	378
1714	client/img/fotos_pedidos/fotos_pedidos/8-3-117-v2crfv48l-1.png	a	a	f	378
1720	client/img/fotos_pedidos/fotos_pedidos/8-3-117-s7mm5rnce-2.png	a	a	f	378
1726	client/img/fotos_pedidos/fotos_pedidos/8-3-117-swn0c4mc7-1.png	a	a	f	378
1819	client/img/fotos_pedidos/fotos_pedidos/9-6-117-n1wqz8zqn-2.jpeg	a	a	f	394
1824	client/img/fotos_pedidos/fotos_pedidos/9-6-117-frbsc611v-0.jpeg	a	a	f	394
1830	client/img/fotos_pedidos/fotos_pedidos/9-6-117-hgri1z0f5-6.jpeg	a	a	f	394
1836	client/img/fotos_pedidos/fotos_pedidos/9-6-117-5r9us9zmf-4.jpeg	a	a	f	394
1842	client/img/fotos_pedidos/fotos_pedidos/9-6-117-qrre8340h-2.jpeg	a	a	f	394
1946	client/img/fotos_pedidos/fotos_pedidos/9-1-117-lbpgtw087-8.jpeg	a	a	f	407
2045	client/img/fotos_pedidos/fotos_pedidos/9-5-117-h7g19bflx-0.jpeg	a	a	f	423
2046	client/img/fotos_pedidos/fotos_pedidos/9-5-117-f0cxc7u8x-0.jpeg	a	a	f	423
2112	client/img/fotos_pedidos/fotos_pedidos/9-3-117-kszxw1o1i-9.jpeg	a	a	f	432
2216	client/img/fotos_pedidos/fotos_pedidos/9-1-117-00ieeuu5z-0.jpeg	a	a	t	443
2305	client/img/fotos_pedidos/fotos_pedidos/10-2-117-f5wa4yddg-1.jpeg	a	a	f	457
2306	client/img/fotos_pedidos/fotos_pedidos/10-2-117-bav1u5c0f-2.jpeg	a	a	f	457
2413	client/img/fotos_pedidos/fotos_pedidos/10-3-117-riwm87f0v-0.jpeg	a	a	f	469
2415	client/img/fotos_pedidos/fotos_pedidos/10-3-117-spnb60inl-0.jpeg	a	a	f	469
2417	client/img/fotos_pedidos/fotos_pedidos/10-3-117-96euwea68-0.jpeg	a	a	f	469
2421	client/img/fotos_pedidos/fotos_pedidos/10-3-117-nngjdo4ul-0.jpeg	a	a	f	469
2487	client/img/fotos_pedidos/fotos_pedidos/10-5-117-owsollzp3-3.jpeg	a	a	f	476
2489	client/img/fotos_pedidos/fotos_pedidos/10-5-117-vvmsnoruc-5.jpeg	a	a	f	476
2491	client/img/fotos_pedidos/fotos_pedidos/10-5-117-ddud7edsw-7.jpeg	a	a	f	476
2493	client/img/fotos_pedidos/fotos_pedidos/10-5-117-t8qv8vz1r-9.jpeg	a	a	f	476
2607	client/img/fotos_pedidos/fotos_pedidos/10-4-117-cjdt6mkg3-0.jpeg	a	a	f	488
2608	client/img/fotos_pedidos/fotos_pedidos/10-4-117-rqgm2jz52-0.jpeg	a	a	f	488
2609	client/img/fotos_pedidos/fotos_pedidos/10-4-117-fvqt58uq3-0.jpeg	a	a	f	488
2610	client/img/fotos_pedidos/fotos_pedidos/10-4-117-884way9rz-0.jpeg	a	a	f	488
2611	client/img/fotos_pedidos/fotos_pedidos/10-4-117-ko6vguvts-0.jpeg	a	a	f	488
2612	client/img/fotos_pedidos/fotos_pedidos/10-4-117-0j23pjpyf-0.jpeg	a	a	f	488
2615	client/img/fotos_pedidos/fotos_pedidos/10-4-117-1tlbvx06w-0.png	a	a	f	488
2705	client/img/fotos_pedidos/fotos_pedidos/10-4-117-3yn9miek3-6.jpeg	a	a	f	502
2708	client/img/fotos_pedidos/fotos_pedidos/10-4-117-7tx8ryyfo-0.jpeg	a	a	f	502
2711	client/img/fotos_pedidos/fotos_pedidos/10-4-117-p57777ujy-0.jpeg	a	a	f	502
2845	client/img/fotos_pedidos/fotos_pedidos/11-2-117-k65gw5pzp-5.jpeg	a	a	f	514
2851	client/img/fotos_pedidos/fotos_pedidos/11-2-117-2ela2lcoh-11.jpeg	a	a	f	514
2931	client/img/fotos_pedidos/fotos_pedidos/11-1-117-wi81aw0xr-0.png	a	a	f	522
2934	client/img/fotos_pedidos/fotos_pedidos/11-1-117-bv8ydym6s-0.jpeg	a	a	f	522
3029	client/img/fotos_pedidos/fotos_pedidos/11-1-117-tfgtxrc0q-0.jpeg	a	a	f	534
3031	client/img/fotos_pedidos/fotos_pedidos/11-1-117-jp73j9hj8-2.jpeg	a	a	f	534
3033	client/img/fotos_pedidos/fotos_pedidos/11-1-117-mjx69xxga-4.jpeg	a	a	f	534
3035	client/img/fotos_pedidos/fotos_pedidos/11-1-117-csd352k62-7.jpeg	a	a	f	534
3136	client/img/fotos_pedidos/fotos_pedidos/11-4-117-b7mmealvg-0.jpeg	a	a	f	539
3141	client/img/fotos_pedidos/fotos_pedidos/11-4-117-9ra16u1z6-0.jpeg	a	a	f	539
3144	client/img/fotos_pedidos/fotos_pedidos/11-4-117-sbdo2tk38-0.jpeg	a	a	f	540
3152	client/img/fotos_pedidos/fotos_pedidos/11-4-117-6fdwvdv5s-0.jpeg	a	a	f	541
3156	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ikd1hcut3-0.jpeg	a	a	f	542
3162	client/img/fotos_pedidos/fotos_pedidos/11-4-117-m5qsc4yb7-0.jpeg	a	a	f	542
3248	client/img/fotos_pedidos/fotos_pedidos/11-4-117-sq0v60teq-1.jpeg	a	a	f	545
990	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fo9hcre7e-3.jpeg	a	a	f	293
996	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ng70nu3w1-9.jpeg	a	a	f	293
1002	client/img/fotos_pedidos/fotos_pedidos/7-4-117-zrt4nlz67-1.jpeg	a	a	f	293
1008	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fs67elo4k-7.jpeg	a	a	f	293
1014	client/img/fotos_pedidos/fotos_pedidos/7-4-117-zp927ma8u-13.jpeg	a	a	f	293
1020	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fuke1t23h-5.jpeg	a	a	f	293
1026	client/img/fotos_pedidos/fotos_pedidos/7-4-117-p1s1h63of-11.jpeg	a	a	f	293
1560	client/img/fotos_pedidos/fotos_pedidos/8-5-117-b17ficwii-0.jpeg	a	a	f	358
1561	client/img/fotos_pedidos/fotos_pedidos/8-5-117-vzh0kwmeu-0.jpeg	a	a	f	358
1562	client/img/fotos_pedidos/fotos_pedidos/8-5-117-t1r83anvx-0.jpeg	a	a	f	358
1563	client/img/fotos_pedidos/fotos_pedidos/8-5-117-uyq1uh010-0.jpeg	a	a	f	358
1203	client/img/fotos_pedidos/fotos_pedidos/7-5-117-nc9bgly8w-4.jpeg	a	a	f	315
1296	client/img/fotos_pedidos/fotos_pedidos/7-2-117-vtx0uan7s-3.jpeg	a	a	t	326
1298	client/img/fotos_pedidos/fotos_pedidos/7-2-117-sjcxdaklk-5.jpeg	a	a	t	326
1300	client/img/fotos_pedidos/fotos_pedidos/7-2-117-3b3dm4zv8-7.jpeg	a	a	t	326
1302	client/img/fotos_pedidos/fotos_pedidos/7-2-117-imp0f6ty2-8.jpeg	a	a	t	326
1304	client/img/fotos_pedidos/fotos_pedidos/7-2-117-6gjmc852x-11.jpeg	a	a	t	326
1403	client/img/fotos_pedidos/fotos_pedidos/7-1-117-oj7ccdk43-0.jpeg	a	a	f	338
1406	client/img/fotos_pedidos/fotos_pedidos/7-1-117-e1xosu1ny-2.jpeg	a	a	f	338
1408	client/img/fotos_pedidos/fotos_pedidos/7-1-117-3b0pht9ro-6.jpeg	a	a	f	338
1413	client/img/fotos_pedidos/fotos_pedidos/7-1-117-nxeacmjyo-0.jpeg	a	a	f	338
1564	client/img/fotos_pedidos/fotos_pedidos/8-5-117-xk4utczma-0.jpeg	a	a	f	358
1566	client/img/fotos_pedidos/fotos_pedidos/8-5-117-xn78osmeb-0.jpeg	a	a	f	358
1568	client/img/fotos_pedidos/fotos_pedidos/8-5-117-0prfe7aqc-0.jpeg	a	a	f	358
1570	client/img/fotos_pedidos/fotos_pedidos/8-5-117-2zqj2x8gx-0.jpeg	a	a	f	359
1572	client/img/fotos_pedidos/fotos_pedidos/8-5-117-ir32ph7kt-0.jpeg	a	a	f	359
1574	client/img/fotos_pedidos/fotos_pedidos/8-5-117-h5cqf8a9l-0.jpeg	a	a	f	359
1578	client/img/fotos_pedidos/fotos_pedidos/8-5-117-x8b6znul2-0.jpeg	a	a	f	359
1711	client/img/fotos_pedidos/fotos_pedidos/8-3-117-ktuwu03rm-3.jpeg	a	a	f	378
1715	client/img/fotos_pedidos/fotos_pedidos/8-3-117-mz0w7gsex-2.png	a	a	f	378
1721	client/img/fotos_pedidos/fotos_pedidos/8-3-117-2d6ukrqsn-3.png	a	a	f	378
1727	client/img/fotos_pedidos/fotos_pedidos/8-3-117-6xxtakcz2-2.png	a	a	f	378
1820	client/img/fotos_pedidos/fotos_pedidos/9-6-117-dek1pi3sd-3.jpeg	a	a	f	394
1825	client/img/fotos_pedidos/fotos_pedidos/9-6-117-f2mst1eit-1.jpeg	a	a	f	394
1831	client/img/fotos_pedidos/fotos_pedidos/9-6-117-a9ne5szam-0.jpeg	a	a	f	394
1837	client/img/fotos_pedidos/fotos_pedidos/9-6-117-xihexylwz-6.jpeg	a	a	f	394
1843	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ravlezi9a-3.jpeg	a	a	f	394
1947	client/img/fotos_pedidos/fotos_pedidos/9-1-117-0l8fqtvfx-0.jpeg	a	a	f	407
2047	client/img/fotos_pedidos/fotos_pedidos/9-5-117-lpkpbgi3k-0.jpeg	a	a	f	423
2113	client/img/fotos_pedidos/fotos_pedidos/9-3-117-b54pupwjp-1.jpeg	a	a	f	432
2217	client/img/fotos_pedidos/fotos_pedidos/9-1-117-3v100w19o-0.jpeg	a	a	t	444
2308	client/img/fotos_pedidos/fotos_pedidos/10-2-117-ctvibwfsg-0.jpeg	a	a	f	458
2309	client/img/fotos_pedidos/fotos_pedidos/10-2-117-8t53z148k-1.jpeg	a	a	f	458
2310	client/img/fotos_pedidos/fotos_pedidos/10-2-117-x7loim6qi-2.jpeg	a	a	f	458
2312	client/img/fotos_pedidos/fotos_pedidos/10-2-117-nuushocef-4.jpeg	a	a	f	458
2316	client/img/fotos_pedidos/fotos_pedidos/10-2-117-q8wia7esf-8.jpeg	a	a	f	458
2322	client/img/fotos_pedidos/fotos_pedidos/10-2-117-73an6m55d-14.jpeg	a	a	f	458
2414	client/img/fotos_pedidos/fotos_pedidos/10-3-117-l5w8ye3zb-0.jpeg	a	a	f	469
2416	client/img/fotos_pedidos/fotos_pedidos/10-3-117-uj55q3t01-0.jpeg	a	a	f	469
2420	client/img/fotos_pedidos/fotos_pedidos/10-3-117-wnys9n970-0.jpeg	a	a	f	469
2494	client/img/fotos_pedidos/fotos_pedidos/10-6-117-ta9qlm1rg-0.jpeg	a	a	f	477
2495	client/img/fotos_pedidos/fotos_pedidos/10-6-117-paj1mbifu-0.jpeg	a	a	f	477
2613	client/img/fotos_pedidos/fotos_pedidos/10-4-117-sbl3muzva-0.jpeg	a	a	f	488
2616	client/img/fotos_pedidos/fotos_pedidos/10-4-117-gee60jxed-0.jpeg	a	a	f	488
2710	client/img/fotos_pedidos/fotos_pedidos/10-4-117-4tko2anzi-8.jpeg	a	a	f	502
2846	client/img/fotos_pedidos/fotos_pedidos/11-2-117-il6q48w90-6.jpeg	a	a	f	514
2932	client/img/fotos_pedidos/fotos_pedidos/11-1-117-1azjv2l97-0.png	a	a	f	522
2935	client/img/fotos_pedidos/fotos_pedidos/11-1-117-j79usu9eo-0.jpeg	a	a	f	522
3030	client/img/fotos_pedidos/fotos_pedidos/11-1-117-5odgg82cg-1.jpeg	a	a	f	534
3032	client/img/fotos_pedidos/fotos_pedidos/11-1-117-qlll9yhpc-3.jpeg	a	a	f	534
3034	client/img/fotos_pedidos/fotos_pedidos/11-1-117-2tw8d0wg1-5.jpeg	a	a	f	534
3137	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ae7rpocam-0.jpeg	a	a	f	539
3145	client/img/fotos_pedidos/fotos_pedidos/11-4-117-v5rmpx2cr-0.jpeg	a	a	f	540
3153	client/img/fotos_pedidos/fotos_pedidos/11-4-117-sqqk8s21h-0.jpeg	a	a	f	541
3157	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ws2h90mfk-0.jpeg	a	a	f	542
3163	client/img/fotos_pedidos/fotos_pedidos/11-4-117-2zfaqgnob-0.jpeg	a	a	f	542
3249	client/img/fotos_pedidos/fotos_pedidos/11-4-117-45fqcusgf-2.jpeg	a	a	f	545
3268	client/img/fotos_pedidos/fotos_pedidos/11-1-117-dvpipck17-0.jpeg	a	a	f	549
3315	client/img/fotos_pedidos/fotos_pedidos/11-2-117-q7653x4rb-0.jpeg	a	a	f	554
3330	client/img/fotos_pedidos/fotos_pedidos/11-2-117-djtbfo8p8-0.png	a	a	f	558
3334	client/img/fotos_pedidos/fotos_pedidos/11-2-117-uukyuxh2t-0.png	a	a	f	561
3342	client/img/fotos_pedidos/fotos_pedidos/11-3-117-nm7wmjhld-0.png	a	a	f	567
3350	client/img/fotos_pedidos/fotos_pedidos/12-6-117-h3ki51o0g-0.jpeg	a	a	t	574
3351	client/img/fotos_pedidos/fotos_pedidos/12-6-117-5gjd8piw5-1.jpeg	a	a	t	574
3355	client/img/fotos_pedidos/fotos_pedidos/12-6-117-42n10s5j8-7.jpeg	a	a	t	574
991	client/img/fotos_pedidos/fotos_pedidos/7-4-117-heonpn834-4.jpeg	a	a	f	293
997	client/img/fotos_pedidos/fotos_pedidos/7-4-117-48pqt2sgl-10.jpeg	a	a	f	293
1003	client/img/fotos_pedidos/fotos_pedidos/7-4-117-njhvx8oko-2.jpeg	a	a	f	293
1009	client/img/fotos_pedidos/fotos_pedidos/7-4-117-x7v2o22od-8.jpeg	a	a	f	293
1015	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ctd2t4cwb-0.jpeg	a	a	f	293
1022	client/img/fotos_pedidos/fotos_pedidos/7-4-117-qnd12z5ka-7.jpeg	a	a	f	293
1027	client/img/fotos_pedidos/fotos_pedidos/7-4-117-et7f47irk-13.jpeg	a	a	f	293
1565	client/img/fotos_pedidos/fotos_pedidos/8-5-117-op83qsyux-0.jpeg	a	a	f	358
1567	client/img/fotos_pedidos/fotos_pedidos/8-5-117-kerblnr5f-0.jpeg	a	a	f	358
1569	client/img/fotos_pedidos/fotos_pedidos/8-5-117-9xf9542id-0.jpeg	a	a	f	358
1205	client/img/fotos_pedidos/fotos_pedidos/7-5-117-sj1hs9hkr-0.jpeg	a	a	f	316
1206	client/img/fotos_pedidos/fotos_pedidos/7-5-117-bngr5imdy-0.jpeg	a	a	f	316
1207	client/img/fotos_pedidos/fotos_pedidos/7-5-117-9w88e2le3-0.jpeg	a	a	f	316
1208	client/img/fotos_pedidos/fotos_pedidos/7-5-117-2vv2gqily-0.jpeg	a	a	f	316
1209	client/img/fotos_pedidos/fotos_pedidos/7-5-117-a08biynu1-0.jpeg	a	a	f	316
1210	client/img/fotos_pedidos/fotos_pedidos/7-5-117-0vnpusejz-0.jpeg	a	a	f	316
1211	client/img/fotos_pedidos/fotos_pedidos/7-5-117-jpq3qbaqf-0.jpeg	a	a	f	316
1213	client/img/fotos_pedidos/fotos_pedidos/7-5-117-fpzdu0bx0-0.jpeg	a	a	f	316
1215	client/img/fotos_pedidos/fotos_pedidos/7-5-117-s5w4ko3gr-0.jpeg	a	a	f	316
1305	client/img/fotos_pedidos/fotos_pedidos/7-3-117-rhtizkjwn-0.jpeg	a	a	f	327
1306	client/img/fotos_pedidos/fotos_pedidos/7-3-117-f7ynzxbkw-1.jpeg	a	a	f	327
1307	client/img/fotos_pedidos/fotos_pedidos/7-3-117-nk47ec1u7-3.jpeg	a	a	f	327
1309	client/img/fotos_pedidos/fotos_pedidos/7-3-117-x323o2mhl-4.jpeg	a	a	f	327
1311	client/img/fotos_pedidos/fotos_pedidos/7-3-117-c5wl73e9k-7.png	a	a	f	327
1313	client/img/fotos_pedidos/fotos_pedidos/7-3-117-pb54k7v21-8.jpeg	a	a	f	327
1409	client/img/fotos_pedidos/fotos_pedidos/7-1-117-6qqcizg24-8.jpeg	a	a	f	338
1417	client/img/fotos_pedidos/fotos_pedidos/7-1-117-glmhzy9u1-2.jpeg	a	a	f	338
1571	client/img/fotos_pedidos/fotos_pedidos/8-5-117-m0mma1q3n-0.jpeg	a	a	f	359
1573	client/img/fotos_pedidos/fotos_pedidos/8-5-117-5kch6h0m4-0.jpeg	a	a	f	359
1575	client/img/fotos_pedidos/fotos_pedidos/8-5-117-d7b64odcr-0.jpeg	a	a	f	359
1579	client/img/fotos_pedidos/fotos_pedidos/8-5-117-p7gbg7uts-0.jpeg	a	a	f	359
1712	client/img/fotos_pedidos/fotos_pedidos/8-3-117-sfcnp3g9t-4.jpeg	a	a	f	378
1718	client/img/fotos_pedidos/fotos_pedidos/8-3-117-bgcdr5yii-0.png	a	a	f	378
1724	client/img/fotos_pedidos/fotos_pedidos/8-3-117-2vjakfnfi-2.jpeg	a	a	f	378
1730	client/img/fotos_pedidos/fotos_pedidos/8-3-117-fquuxrfmi-0.png	a	a	f	378
1821	client/img/fotos_pedidos/fotos_pedidos/9-6-117-mg1jacge9-5.jpeg	a	a	f	394
1827	client/img/fotos_pedidos/fotos_pedidos/9-6-117-268iwx4t6-3.jpeg	a	a	f	394
1833	client/img/fotos_pedidos/fotos_pedidos/9-6-117-s1ukxvhnk-1.jpeg	a	a	f	394
1839	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ye1z7h5ca-7.jpeg	a	a	f	394
1845	client/img/fotos_pedidos/fotos_pedidos/9-6-117-dbzt859t7-4.jpeg	a	a	f	394
2048	client/img/fotos_pedidos/fotos_pedidos/9-4-117-136yy79vj-0.jpeg	a	a	f	424
2049	client/img/fotos_pedidos/fotos_pedidos/9-4-117-172x5uuyw-0.jpeg	a	a	f	424
2050	client/img/fotos_pedidos/fotos_pedidos/9-4-117-8cc4tk5y9-0.jpeg	a	a	f	424
2051	client/img/fotos_pedidos/fotos_pedidos/9-4-117-4zedbrv5t-0.jpeg	a	a	f	424
2053	client/img/fotos_pedidos/fotos_pedidos/9-4-117-sako84s1y-0.jpeg	a	a	f	424
2055	client/img/fotos_pedidos/fotos_pedidos/9-4-117-kbnhw1vox-0.jpeg	a	a	f	424
2057	client/img/fotos_pedidos/fotos_pedidos/9-4-117-d7ci2myjn-0.png	a	a	f	424
2114	client/img/fotos_pedidos/fotos_pedidos/9-3-117-956bvkr9h-3.jpeg	a	a	f	432
2218	client/img/fotos_pedidos/fotos_pedidos/9-1-117-fxfl14l06-1.jpeg	a	a	f	445
2219	client/img/fotos_pedidos/fotos_pedidos/9-1-117-t706gpp0m-0.jpeg	a	a	f	445
2311	client/img/fotos_pedidos/fotos_pedidos/10-2-117-53f3biqh1-3.jpeg	a	a	f	458
2313	client/img/fotos_pedidos/fotos_pedidos/10-2-117-qjk6k58ox-5.jpeg	a	a	f	458
2317	client/img/fotos_pedidos/fotos_pedidos/10-2-117-js5koo689-9.jpeg	a	a	f	458
2323	client/img/fotos_pedidos/fotos_pedidos/10-2-117-c9vrr93cs-15.jpeg	a	a	f	458
2418	client/img/fotos_pedidos/fotos_pedidos/10-3-117-kqvaff27u-0.jpeg	a	a	f	469
2496	client/img/fotos_pedidos/fotos_pedidos/10-0-117-hu2vuod5w-0.jpeg	a	a	t	478
2497	client/img/fotos_pedidos/fotos_pedidos/10-0-117-soh7jjpgw-2.jpeg	a	a	t	478
2498	client/img/fotos_pedidos/fotos_pedidos/10-0-117-xj86poglf-1.jpeg	a	a	t	478
2500	client/img/fotos_pedidos/fotos_pedidos/10-0-117-fxttqdjqu-4.jpeg	a	a	t	478
2502	client/img/fotos_pedidos/fotos_pedidos/10-0-117-c2atycts8-1.jpeg	a	a	t	478
2504	client/img/fotos_pedidos/fotos_pedidos/10-0-117-i3lx5ksmj-4.jpeg	a	a	t	478
2506	client/img/fotos_pedidos/fotos_pedidos/10-0-117-jz20946u1-5.jpeg	a	a	t	478
2508	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ark758b6j-1.jpeg	a	a	t	478
2510	client/img/fotos_pedidos/fotos_pedidos/10-0-117-yfnbf66b5-3.jpeg	a	a	t	478
2512	client/img/fotos_pedidos/fotos_pedidos/10-0-117-tf0n4ibfv-1.jpeg	a	a	t	478
2514	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mnzqo4mwi-2.jpeg	a	a	t	478
2516	client/img/fotos_pedidos/fotos_pedidos/10-0-117-htvqwholq-0.jpeg	a	a	t	478
2518	client/img/fotos_pedidos/fotos_pedidos/10-0-117-h8qel2qyp-4.jpeg	a	a	t	478
2520	client/img/fotos_pedidos/fotos_pedidos/10-0-117-b48edk8xz-3.jpeg	a	a	t	478
2522	client/img/fotos_pedidos/fotos_pedidos/10-0-117-lwl4mr8oe-0.jpeg	a	a	t	478
2524	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mub4w4oma-1.jpeg	a	a	t	478
2526	client/img/fotos_pedidos/fotos_pedidos/10-0-117-gz0fuy557-2.jpeg	a	a	t	478
2528	client/img/fotos_pedidos/fotos_pedidos/10-0-117-znmf9zj49-6.jpeg	a	a	t	478
2530	client/img/fotos_pedidos/fotos_pedidos/10-0-117-pmfd2ynrd-8.jpeg	a	a	t	478
2532	client/img/fotos_pedidos/fotos_pedidos/10-0-117-0amfpz7uz-1.jpeg	a	a	t	478
992	client/img/fotos_pedidos/fotos_pedidos/7-4-117-vvskzplqv-5.jpeg	a	a	f	293
998	client/img/fotos_pedidos/fotos_pedidos/7-4-117-ek56mqzq9-11.jpeg	a	a	f	293
1004	client/img/fotos_pedidos/fotos_pedidos/7-4-117-z0kd9ixg2-3.jpeg	a	a	f	293
1010	client/img/fotos_pedidos/fotos_pedidos/7-4-117-b6lqpqlyy-9.jpeg	a	a	f	293
1016	client/img/fotos_pedidos/fotos_pedidos/7-4-117-qm7fpj2nk-1.jpeg	a	a	f	293
1021	client/img/fotos_pedidos/fotos_pedidos/7-4-117-v79nqn3s1-6.jpeg	a	a	f	293
1028	client/img/fotos_pedidos/fotos_pedidos/7-4-117-sdhh8rywr-12.jpeg	a	a	f	293
1576	client/img/fotos_pedidos/fotos_pedidos/8-5-117-wzhj9k7uz-0.jpeg	a	a	f	359
1212	client/img/fotos_pedidos/fotos_pedidos/7-5-117-ph1tne2ah-0.jpeg	a	a	f	316
1214	client/img/fotos_pedidos/fotos_pedidos/7-5-117-cj5awo2it-0.jpeg	a	a	f	316
1216	client/img/fotos_pedidos/fotos_pedidos/7-5-117-loj5kclaa-0.jpeg	a	a	f	316
1308	client/img/fotos_pedidos/fotos_pedidos/7-3-117-utydoorb5-2.jpeg	a	a	f	327
1310	client/img/fotos_pedidos/fotos_pedidos/7-3-117-25k0iirbo-5.jpeg	a	a	f	327
1312	client/img/fotos_pedidos/fotos_pedidos/7-3-117-xnrg211a8-6.jpeg	a	a	f	327
1314	client/img/fotos_pedidos/fotos_pedidos/7-3-117-bmdln6qav-9.jpeg	a	a	f	327
1410	client/img/fotos_pedidos/fotos_pedidos/7-1-117-pthmdumxb-1.jpeg	a	a	f	338
1414	client/img/fotos_pedidos/fotos_pedidos/7-1-117-q41mpr7p2-0.jpeg	a	a	f	338
1716	client/img/fotos_pedidos/fotos_pedidos/8-3-117-k9w8farbu-3.png	a	a	f	378
1722	client/img/fotos_pedidos/fotos_pedidos/8-3-117-o83i8p34i-0.png	a	a	f	378
1728	client/img/fotos_pedidos/fotos_pedidos/8-3-117-9btvcp5nz-3.jpeg	a	a	f	378
1822	client/img/fotos_pedidos/fotos_pedidos/9-6-117-20rsrphmp-7.jpeg	a	a	f	394
1828	client/img/fotos_pedidos/fotos_pedidos/9-6-117-jxgp010xc-4.jpeg	a	a	f	394
1834	client/img/fotos_pedidos/fotos_pedidos/9-6-117-6szozdigb-2.jpeg	a	a	f	394
1840	client/img/fotos_pedidos/fotos_pedidos/9-6-117-iuqtw1alx-0.jpeg	a	a	f	394
1846	client/img/fotos_pedidos/fotos_pedidos/9-6-117-di9r9u00x-6.jpeg	a	a	f	394
2052	client/img/fotos_pedidos/fotos_pedidos/9-4-117-gjnzkryiw-0.jpeg	a	a	f	424
2054	client/img/fotos_pedidos/fotos_pedidos/9-4-117-13xwlsqyy-0.jpeg	a	a	f	424
2056	client/img/fotos_pedidos/fotos_pedidos/9-4-117-11fll8tqb-0.jpeg	a	a	f	424
2115	client/img/fotos_pedidos/fotos_pedidos/9-3-117-atkd948fz-5.jpeg	a	a	f	432
2220	client/img/fotos_pedidos/fotos_pedidos/9-1-117-rxjz4mw2s-0.jpeg	a	a	f	446
2221	client/img/fotos_pedidos/fotos_pedidos/9-1-117-16twa8fvv-0.jpeg	a	a	f	446
2222	client/img/fotos_pedidos/fotos_pedidos/9-1-117-28k82o0rn-0.jpeg	a	a	f	446
2223	client/img/fotos_pedidos/fotos_pedidos/9-1-117-1ove6sj0v-0.jpeg	a	a	f	446
2224	client/img/fotos_pedidos/fotos_pedidos/9-1-117-wwozghorl-0.jpeg	a	a	f	446
2225	client/img/fotos_pedidos/fotos_pedidos/9-1-117-lo4z2po63-0.jpeg	a	a	f	446
2226	client/img/fotos_pedidos/fotos_pedidos/9-1-117-jeoeggemc-0.jpeg	a	a	f	446
2227	client/img/fotos_pedidos/fotos_pedidos/9-1-117-z6tgepd76-0.jpeg	a	a	f	446
2228	client/img/fotos_pedidos/fotos_pedidos/9-1-117-3ij1vutco-0.jpeg	a	a	f	446
2229	client/img/fotos_pedidos/fotos_pedidos/9-1-117-xldotxh4f-0.jpeg	a	a	f	446
2314	client/img/fotos_pedidos/fotos_pedidos/10-2-117-rkuoxh49q-6.jpeg	a	a	f	458
2320	client/img/fotos_pedidos/fotos_pedidos/10-2-117-o5961xqf4-12.jpeg	a	a	f	458
2419	client/img/fotos_pedidos/fotos_pedidos/10-3-117-w2bkxckli-0.jpeg	a	a	f	469
2499	client/img/fotos_pedidos/fotos_pedidos/10-0-117-oyly9tb0n-3.jpeg	a	a	t	478
2501	client/img/fotos_pedidos/fotos_pedidos/10-0-117-jw11wc1in-0.jpeg	a	a	t	478
2503	client/img/fotos_pedidos/fotos_pedidos/10-0-117-q2gbzhymv-2.jpeg	a	a	t	478
2505	client/img/fotos_pedidos/fotos_pedidos/10-0-117-5u375ippj-3.jpeg	a	a	t	478
2507	client/img/fotos_pedidos/fotos_pedidos/10-0-117-v7z2e1msp-0.jpeg	a	a	t	478
2509	client/img/fotos_pedidos/fotos_pedidos/10-0-117-q3xvwl3rt-2.jpeg	a	a	t	478
2511	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ortqd8ybx-4.jpeg	a	a	t	478
2513	client/img/fotos_pedidos/fotos_pedidos/10-0-117-1b5u5iovv-0.jpeg	a	a	t	478
2515	client/img/fotos_pedidos/fotos_pedidos/10-0-117-sr49iqwub-3.jpeg	a	a	t	478
2517	client/img/fotos_pedidos/fotos_pedidos/10-0-117-5uatjj46l-1.jpeg	a	a	t	478
2519	client/img/fotos_pedidos/fotos_pedidos/10-0-117-mnp9g0c7p-2.jpeg	a	a	t	478
2521	client/img/fotos_pedidos/fotos_pedidos/10-0-117-o7c2hv6qu-4.jpeg	a	a	t	478
2523	client/img/fotos_pedidos/fotos_pedidos/10-0-117-z2gu5v79r-3.jpeg	a	a	t	478
2525	client/img/fotos_pedidos/fotos_pedidos/10-0-117-4a7ndy1l1-4.jpeg	a	a	t	478
2527	client/img/fotos_pedidos/fotos_pedidos/10-0-117-drejdvrza-5.jpeg	a	a	t	478
2529	client/img/fotos_pedidos/fotos_pedidos/10-0-117-55ilio54k-7.jpeg	a	a	t	478
2531	client/img/fotos_pedidos/fotos_pedidos/10-0-117-it9wvcfq5-0.jpeg	a	a	t	478
2533	client/img/fotos_pedidos/fotos_pedidos/10-0-117-2mw00cftl-2.jpeg	a	a	t	478
2535	client/img/fotos_pedidos/fotos_pedidos/10-0-117-cp4g4rkbn-3.jpeg	a	a	t	478
2614	client/img/fotos_pedidos/fotos_pedidos/10-4-117-yz9w8f5ua-0.jpeg	a	a	f	488
2712	client/img/fotos_pedidos/fotos_pedidos/10-2-117-ub8ow16yd-0.jpeg	a	a	f	503
2847	client/img/fotos_pedidos/fotos_pedidos/11-2-117-pljzb931t-7.jpeg	a	a	f	514
2936	client/img/fotos_pedidos/fotos_pedidos/11-1-117-g1mzwghcx-0.jpeg	a	a	f	522
3036	client/img/fotos_pedidos/fotos_pedidos/11-1-117-cxqa4kbzl-6.jpeg	a	a	f	534
3138	client/img/fotos_pedidos/fotos_pedidos/11-4-117-eau2fgeu7-0.jpeg	a	a	f	539
3146	client/img/fotos_pedidos/fotos_pedidos/11-4-117-xw1ixfuqc-0.jpeg	a	a	f	540
3148	client/img/fotos_pedidos/fotos_pedidos/11-4-117-vs05oidhn-0.jpeg	a	a	f	541
3158	client/img/fotos_pedidos/fotos_pedidos/11-4-117-777xptfxo-0.jpeg	a	a	f	542
3164	client/img/fotos_pedidos/fotos_pedidos/11-4-117-5fge7w2fk-0.jpeg	a	a	f	542
3250	client/img/fotos_pedidos/fotos_pedidos/11-4-117-i9ybkmw9b-0.jpeg	a	a	f	546
3251	client/img/fotos_pedidos/fotos_pedidos/11-4-117-wvd67ssaf-3.jpeg	a	a	f	546
3252	client/img/fotos_pedidos/fotos_pedidos/11-4-117-k4fnpbywu-2.jpeg	a	a	f	546
3258	client/img/fotos_pedidos/fotos_pedidos/11-4-117-9hesvhtti-0.jpeg	a	a	f	546
993	client/img/fotos_pedidos/fotos_pedidos/7-4-117-43850kw1t-7.jpeg	a	a	f	293
999	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fhowwzl20-12.jpeg	a	a	f	293
1005	client/img/fotos_pedidos/fotos_pedidos/7-4-117-fh0boqixi-4.jpeg	a	a	f	293
1011	client/img/fotos_pedidos/fotos_pedidos/7-4-117-3lihyhovc-10.jpeg	a	a	f	293
1017	client/img/fotos_pedidos/fotos_pedidos/7-4-117-n4q5nai20-2.jpeg	a	a	f	293
1023	client/img/fotos_pedidos/fotos_pedidos/7-4-117-68976rgwg-8.jpeg	a	a	f	293
1029	client/img/fotos_pedidos/fotos_pedidos/7-4-117-6qhd7qtie-0.jpeg	a	a	f	293
1577	client/img/fotos_pedidos/fotos_pedidos/8-5-117-lahiwqy83-0.jpeg	a	a	f	359
1217	client/img/fotos_pedidos/fotos_pedidos/7-5-117-qls0e1lwx-1.jpeg	a	a	t	317
1218	client/img/fotos_pedidos/fotos_pedidos/7-5-117-9dq18mkff-0.jpeg	a	a	t	317
1219	client/img/fotos_pedidos/fotos_pedidos/7-5-117-rhlj0u17k-2.jpeg	a	a	t	317
1315	client/img/fotos_pedidos/fotos_pedidos/7-5-117-n4hyan7h1-0.jpeg	a	a	f	328
1317	client/img/fotos_pedidos/fotos_pedidos/7-5-117-w4c1zkgx3-0.jpeg	a	a	f	328
1411	client/img/fotos_pedidos/fotos_pedidos/7-1-117-fwf4d5973-10.jpeg	a	a	f	338
1415	client/img/fotos_pedidos/fotos_pedidos/7-1-117-h91dq33d0-1.jpeg	a	a	f	338
1717	client/img/fotos_pedidos/fotos_pedidos/8-3-117-nh7r2v3eq-4.png	a	a	f	378
1723	client/img/fotos_pedidos/fotos_pedidos/8-3-117-7nj85m75b-1.png	a	a	f	378
1729	client/img/fotos_pedidos/fotos_pedidos/8-3-117-wnr5ofpbb-0.png	a	a	f	378
1826	client/img/fotos_pedidos/fotos_pedidos/9-6-117-b8jvdeut8-2.jpeg	a	a	f	394
1832	client/img/fotos_pedidos/fotos_pedidos/9-6-117-jopoi6k6a-7.jpeg	a	a	f	394
1838	client/img/fotos_pedidos/fotos_pedidos/9-6-117-darlv02lt-5.jpeg	a	a	f	394
1844	client/img/fotos_pedidos/fotos_pedidos/9-6-117-9y7c1y5oo-5.jpeg	a	a	f	394
2058	client/img/fotos_pedidos/fotos_pedidos/9-6-117-i1rk8dle5-0.jpeg	a	a	f	425
2059	client/img/fotos_pedidos/fotos_pedidos/9-6-117-pf5e10orq-1.jpeg	a	a	f	425
2060	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ydhvt5vjk-3.jpeg	a	a	f	425
2061	client/img/fotos_pedidos/fotos_pedidos/9-6-117-s2fp58iw3-2.jpeg	a	a	f	425
2065	client/img/fotos_pedidos/fotos_pedidos/9-6-117-l56n1unyf-7.jpeg	a	a	f	425
2116	client/img/fotos_pedidos/fotos_pedidos/9-3-117-k3so4a6yv-0.jpeg	a	a	f	433
2117	client/img/fotos_pedidos/fotos_pedidos/9-3-117-mo86z9jcy-0.jpeg	a	a	f	433
2118	client/img/fotos_pedidos/fotos_pedidos/9-3-117-edpa6hz2s-0.jpeg	a	a	f	433
2123	client/img/fotos_pedidos/fotos_pedidos/9-3-117-bpchnpdvy-0.jpeg	a	a	f	433
2129	client/img/fotos_pedidos/fotos_pedidos/9-3-117-me08nw6pn-0.jpeg	a	a	f	433
2135	client/img/fotos_pedidos/fotos_pedidos/9-3-117-vrjgmkti1-0.jpeg	a	a	f	433
2230	client/img/fotos_pedidos/fotos_pedidos/9-1-117-0bnp2z7zs-0.jpeg	a	a	f	447
2315	client/img/fotos_pedidos/fotos_pedidos/10-2-117-uaud4sghi-7.jpeg	a	a	f	458
2321	client/img/fotos_pedidos/fotos_pedidos/10-2-117-untk2pqru-13.jpeg	a	a	f	458
2422	client/img/fotos_pedidos/fotos_pedidos/10-3-117-y75w6weof-0.jpeg	a	a	f	470
2423	client/img/fotos_pedidos/fotos_pedidos/10-3-117-ro5eesurm-0.jpeg	a	a	f	470
2424	client/img/fotos_pedidos/fotos_pedidos/10-3-117-hfkrr00h8-0.jpeg	a	a	f	470
2427	client/img/fotos_pedidos/fotos_pedidos/10-3-117-g1c1xt4z8-0.jpeg	a	a	f	470
2431	client/img/fotos_pedidos/fotos_pedidos/10-3-117-og06k84ne-0.jpeg	a	a	f	470
2534	client/img/fotos_pedidos/fotos_pedidos/10-0-117-sue34agd9-4.jpeg	a	a	t	478
2617	client/img/fotos_pedidos/fotos_pedidos/10-0-117-xy5ig7spj-0.jpeg	a	a	f	489
2618	client/img/fotos_pedidos/fotos_pedidos/10-0-117-siv9lkmy5-1.jpeg	a	a	f	489
2621	client/img/fotos_pedidos/fotos_pedidos/10-0-117-eur1kblru-5.jpeg	a	a	f	489
2624	client/img/fotos_pedidos/fotos_pedidos/10-0-117-rsfig2mav-8.jpeg	a	a	f	489
2627	client/img/fotos_pedidos/fotos_pedidos/10-0-117-txa3qy6np-1.jpeg	a	a	f	489
2713	client/img/fotos_pedidos/fotos_pedidos/10-2-117-vi0oddyf7-0.jpeg	a	a	f	504
2850	client/img/fotos_pedidos/fotos_pedidos/11-2-117-e56hu850n-10.png	a	a	f	514
2937	client/img/fotos_pedidos/fotos_pedidos/11-1-117-m0whf8x5s-0.jpeg	a	a	f	523
2938	client/img/fotos_pedidos/fotos_pedidos/11-1-117-mr8ojlzeh-0.jpeg	a	a	f	523
2939	client/img/fotos_pedidos/fotos_pedidos/11-1-117-zduomz871-0.jpeg	a	a	f	523
2940	client/img/fotos_pedidos/fotos_pedidos/11-1-117-t10odrj31-0.jpeg	a	a	f	523
2942	client/img/fotos_pedidos/fotos_pedidos/11-1-117-r6w7t3r4j-0.jpeg	a	a	f	523
2945	client/img/fotos_pedidos/fotos_pedidos/11-1-117-b4olmfbd5-0.jpeg	a	a	f	523
2948	client/img/fotos_pedidos/fotos_pedidos/11-1-117-3a8uuvqo0-0.jpeg	a	a	f	524
2951	client/img/fotos_pedidos/fotos_pedidos/11-1-117-z6zxmtipx-0.jpeg	a	a	f	524
2954	client/img/fotos_pedidos/fotos_pedidos/11-1-117-ubr858za2-0.jpeg	a	a	f	524
3037	client/img/fotos_pedidos/fotos_pedidos/11-1-117-iznuo85rt-0.jpeg	a	a	f	534
3139	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cqwyfpkn7-0.jpeg	a	a	f	539
3147	client/img/fotos_pedidos/fotos_pedidos/11-4-117-rx8vkbaug-0.jpeg	a	a	f	540
3149	client/img/fotos_pedidos/fotos_pedidos/11-4-117-y3847s8i4-0.jpeg	a	a	f	541
3159	client/img/fotos_pedidos/fotos_pedidos/11-4-117-yacci8it9-0.jpeg	a	a	f	542
3165	client/img/fotos_pedidos/fotos_pedidos/11-4-117-yoo0o9v6p-0.jpeg	a	a	f	542
3253	client/img/fotos_pedidos/fotos_pedidos/11-4-117-dl4c8sy7z-0.jpeg	a	a	f	546
3259	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ew43a65q4-0.jpeg	a	a	f	546
3269	client/img/fotos_pedidos/fotos_pedidos/11-1-117-b8zfiqbkh-0.jpeg	a	a	f	550
3316	client/img/fotos_pedidos/fotos_pedidos/11-2-117-v12fdnzi0-0.jpeg	a	a	f	555
3331	client/img/fotos_pedidos/fotos_pedidos/11-2-117-0yudkdzga-1.jpeg	a	a	f	559
3335	client/img/fotos_pedidos/fotos_pedidos/11-2-117-nhw87fuap-0.png	a	a	f	562
3344	client/img/fotos_pedidos/fotos_pedidos/11-3-117-5mgai8qns-0.jpeg	a	a	f	569
3352	client/img/fotos_pedidos/fotos_pedidos/12-6-117-fxt4u3u1i-3.png	a	a	t	574
3356	client/img/fotos_pedidos/fotos_pedidos/12-6-117-w89o8spn7-5.jpeg	a	a	t	574
3357	client/img/fotos_pedidos/fotos_pedidos/12-6-117-qqiq0iefc-9.jpeg	a	a	t	574
3362	client/img/fotos_pedidos/fotos_pedidos/12-0-117-mai1qsjgq-0.jpeg	a	a	t	576
994	client/img/fotos_pedidos/fotos_pedidos/7-4-117-jx34e11oo-6.jpeg	a	a	f	293
1000	client/img/fotos_pedidos/fotos_pedidos/7-4-117-u890zcq14-13.jpeg	a	a	f	293
1006	client/img/fotos_pedidos/fotos_pedidos/7-4-117-1qh5bxdmt-5.jpeg	a	a	f	293
1012	client/img/fotos_pedidos/fotos_pedidos/7-4-117-dejm8u2k7-11.jpeg	a	a	f	293
1018	client/img/fotos_pedidos/fotos_pedidos/7-4-117-qrt28subl-3.jpeg	a	a	f	293
1024	client/img/fotos_pedidos/fotos_pedidos/7-4-117-cmj7ensoz-9.jpeg	a	a	f	293
1580	client/img/fotos_pedidos/fotos_pedidos/8-0-117-yuaq407ji-0.jpeg	a	a	f	360
1220	client/img/fotos_pedidos/fotos_pedidos/7-5-117-r2esz8n2e-3.jpeg	a	a	t	317
1221	client/img/fotos_pedidos/fotos_pedidos/7-5-117-mif9tifmz-4.jpeg	a	a	t	317
1316	client/img/fotos_pedidos/fotos_pedidos/7-5-117-4vu7kxk89-0.jpeg	a	a	f	328
1584	client/img/fotos_pedidos/fotos_pedidos/8-0-117-5764qbvpb-4.jpeg	a	a	f	360
1588	client/img/fotos_pedidos/fotos_pedidos/8-0-117-7qyfoil6n-8.jpeg	a	a	f	360
1592	client/img/fotos_pedidos/fotos_pedidos/8-0-117-iq0r37lxy-2.jpeg	a	a	f	360
1596	client/img/fotos_pedidos/fotos_pedidos/8-0-117-90srzg2lg-6.jpeg	a	a	f	360
1600	client/img/fotos_pedidos/fotos_pedidos/8-0-117-dtl1jaoax-0.jpeg	a	a	f	360
1604	client/img/fotos_pedidos/fotos_pedidos/8-5-117-rjvsyj5jd-0.jpeg	a	a	f	361
1608	client/img/fotos_pedidos/fotos_pedidos/8-5-117-t1pevm8ud-4.jpeg	a	a	f	361
1612	client/img/fotos_pedidos/fotos_pedidos/8-5-117-7isxq4vmt-8.jpeg	a	a	f	361
1731	client/img/fotos_pedidos/fotos_pedidos/8-3-117-08quq82fn-0.jpeg	Have no fear! Said the cat.\nI will not let you fall.\nI will hold you up high\nAs I stand on a ball.	a	t	379
1848	client/img/fotos_pedidos/fotos_pedidos/9-6-117-ilw0hwhlx-0.jpeg	a	a	f	395
1849	client/img/fotos_pedidos/fotos_pedidos/9-6-117-arrqjjdk9-0.jpeg	a	a	f	395
1850	client/img/fotos_pedidos/fotos_pedidos/9-6-117-dyoalhqed-0.jpeg	a	a	f	395
1852	client/img/fotos_pedidos/fotos_pedidos/9-6-117-8bg6r4uuk-0.jpeg	a	a	f	395
1854	client/img/fotos_pedidos/fotos_pedidos/9-6-117-25s0zdzra-0.jpeg	a	a	f	395
1856	client/img/fotos_pedidos/fotos_pedidos/9-6-117-siq29ueze-0.jpeg	a	a	f	395
1858	client/img/fotos_pedidos/fotos_pedidos/9-6-117-oujmwfpf6-0.jpeg	a	a	f	395
2062	client/img/fotos_pedidos/fotos_pedidos/9-6-117-jjlu0herj-4.jpeg	a	a	f	425
2066	client/img/fotos_pedidos/fotos_pedidos/9-6-117-wky6lq6jc-9.jpeg	a	a	f	425
2119	client/img/fotos_pedidos/fotos_pedidos/9-3-117-66dicjg4b-0.jpeg	a	a	f	433
2124	client/img/fotos_pedidos/fotos_pedidos/9-3-117-gllg94paf-0.jpeg	a	a	f	433
2130	client/img/fotos_pedidos/fotos_pedidos/9-3-117-ts4u1y0i1-0.jpeg	a	a	f	433
2136	client/img/fotos_pedidos/fotos_pedidos/9-3-117-d4kxw124h-0.jpeg	a	a	f	433
2231	client/img/fotos_pedidos/fotos_pedidos/9-1-117-6fwokct91-0.png	a	a	f	448
2232	client/img/fotos_pedidos/fotos_pedidos/9-1-117-u73k730xm-0.jpeg	a	a	f	448
2233	client/img/fotos_pedidos/fotos_pedidos/9-1-117-pp9mlp5ak-0.jpeg	a	a	f	448
2318	client/img/fotos_pedidos/fotos_pedidos/10-2-117-utfui20qj-10.jpeg	a	a	f	458
2425	client/img/fotos_pedidos/fotos_pedidos/10-3-117-51mfiht2x-0.png	a	a	f	470
2428	client/img/fotos_pedidos/fotos_pedidos/10-3-117-jae7nlg2j-0.png	a	a	f	470
2432	client/img/fotos_pedidos/fotos_pedidos/10-3-117-oil9pu9zy-0.jpeg	a	a	f	470
2536	client/img/fotos_pedidos/fotos_pedidos/10-1-117-n9bx85ib7-0.jpeg	a	a	t	479
2537	client/img/fotos_pedidos/fotos_pedidos/10-1-117-2dj6ngiik-1.jpeg	a	a	t	479
2619	client/img/fotos_pedidos/fotos_pedidos/10-0-117-cdag18fwl-2.jpeg	a	a	f	489
2622	client/img/fotos_pedidos/fotos_pedidos/10-0-117-ka9b3pzit-7.jpeg	a	a	f	489
2626	client/img/fotos_pedidos/fotos_pedidos/10-0-117-gmimbw4d7-0.jpeg	a	a	f	489
2714	client/img/fotos_pedidos/fotos_pedidos/10-2-117-lpigzyen8-0.jpeg	a	a	f	505
2719	client/img/fotos_pedidos/fotos_pedidos/10-2-117-xiit8zk89-5.jpeg	a	a	f	505
2852	client/img/fotos_pedidos/fotos_pedidos/11-2-117-jvt7ort9l-0.jpeg	a	a	f	515
2941	client/img/fotos_pedidos/fotos_pedidos/11-1-117-j9fjhnn6n-0.jpeg	a	a	f	523
2944	client/img/fotos_pedidos/fotos_pedidos/11-1-117-nd2rz8b10-0.jpeg	a	a	f	523
2947	client/img/fotos_pedidos/fotos_pedidos/11-1-117-ani87pe39-0.jpeg	a	a	f	524
2950	client/img/fotos_pedidos/fotos_pedidos/11-1-117-merh7yw62-0.jpeg	a	a	f	524
2953	client/img/fotos_pedidos/fotos_pedidos/11-1-117-l6tr4aa8r-0.jpeg	a	a	f	524
2956	client/img/fotos_pedidos/fotos_pedidos/11-1-117-qjwrldg7o-0.jpeg	a	a	f	524
3038	client/img/fotos_pedidos/fotos_pedidos/11-1-117-q20zi0rg9-8.jpeg	a	a	f	534
3140	client/img/fotos_pedidos/fotos_pedidos/11-4-117-bnqsd7fcy-0.jpeg	a	a	f	539
3142	client/img/fotos_pedidos/fotos_pedidos/11-4-117-cv15g3dak-0.jpeg	a	a	f	540
3150	client/img/fotos_pedidos/fotos_pedidos/11-4-117-tblochxo5-0.jpeg	a	a	f	541
3154	client/img/fotos_pedidos/fotos_pedidos/11-4-117-u1s2f7q9n-0.jpeg	a	a	f	542
3160	client/img/fotos_pedidos/fotos_pedidos/11-4-117-xwvfavsfs-0.jpeg	a	a	f	542
3254	client/img/fotos_pedidos/fotos_pedidos/11-4-117-4ki53bl9f-1.jpeg	a	a	f	546
3270	client/img/fotos_pedidos/fotos_pedidos/11-2-117-d4cce9too-0.jpeg	a	a	f	551
3271	client/img/fotos_pedidos/fotos_pedidos/11-2-117-7g3hf1db3-0.jpeg	a	a	f	551
3274	client/img/fotos_pedidos/fotos_pedidos/11-2-117-g3srehx5n-1.jpeg	a	a	f	551
3276	client/img/fotos_pedidos/fotos_pedidos/11-2-117-u6d6hc7xd-0.jpeg	a	a	f	551
3278	client/img/fotos_pedidos/fotos_pedidos/11-2-117-kf7gejwxg-0.jpeg	a	a	f	551
3280	client/img/fotos_pedidos/fotos_pedidos/11-2-117-66xj5257d-1.jpeg	a	a	f	551
3282	client/img/fotos_pedidos/fotos_pedidos/11-2-117-4rqc807gz-2.jpeg	a	a	f	551
3284	client/img/fotos_pedidos/fotos_pedidos/11-2-117-5llo82r8z-4.jpeg	a	a	f	551
3286	client/img/fotos_pedidos/fotos_pedidos/11-2-117-h37icjrbq-6.jpeg	a	a	f	551
3288	client/img/fotos_pedidos/fotos_pedidos/11-2-117-fjlo9cdnr-8.jpeg	a	a	f	551
3317	client/img/fotos_pedidos/fotos_pedidos/11-2-117-dhye7xqt2-0.jpeg	a	a	f	555
3336	client/img/fotos_pedidos/fotos_pedidos/11-3-117-5lzzmttow-0.png	a	a	f	563
3339	client/img/fotos_pedidos/fotos_pedidos/11-3-117-3af6z4f3c-0.jpeg	a	a	f	568
3345	client/img/fotos_pedidos/fotos_pedidos/11-4-117-ch5c5blzt-0.jpeg	a	a	f	570
3376	client/img/fotos_pedidos/fotos_pedidos/12-0-117-r44i97xwt-12.jpeg	a	a	f	578
3378	client/img/fotos_pedidos/fotos_pedidos/12-0-117-8cwtqpo34-14.jpeg	a	a	f	578
3380	client/img/fotos_pedidos/fotos_pedidos/12-0-117-g4hugv844-16.jpeg	a	a	f	578
3382	client/img/fotos_pedidos/fotos_pedidos/12-0-117-htmbf9fmq-18.jpeg	a	a	f	578
3377	client/img/fotos_pedidos/fotos_pedidos/12-0-117-qd3t18w7o-13.jpeg	a	a	f	578
3379	client/img/fotos_pedidos/fotos_pedidos/12-0-117-gxa4xheg0-15.jpeg	a	a	f	578
3381	client/img/fotos_pedidos/fotos_pedidos/12-0-117-ksehva06j-17.jpeg	a	a	f	578
3383	client/img/fotos_pedidos/fotos_pedidos/12-0-117-rps8i3su2-19.jpeg	a	a	f	578
3384	client/img/fotos_pedidos/fotos_pedidos/12-1-117-64fdcd7gm-0.jpeg	a	a	f	579
3385	client/img/fotos_pedidos/fotos_pedidos/12-1-117-3ewzdrv6x-1.jpeg	a	a	f	579
3386	client/img/fotos_pedidos/fotos_pedidos/12-1-117-iivkjxdsu-5.jpeg	a	a	f	579
3387	client/img/fotos_pedidos/fotos_pedidos/12-1-117-6ftkbnqoy-6.jpeg	a	a	f	579
3388	client/img/fotos_pedidos/fotos_pedidos/12-1-117-bk7h86gvt-7.jpeg	a	a	f	579
3389	client/img/fotos_pedidos/fotos_pedidos/12-1-117-38h02nh7m-3.jpeg	a	a	f	579
3390	client/img/fotos_pedidos/fotos_pedidos/12-1-117-u85w5xsz8-4.jpeg	a	a	f	579
3391	client/img/fotos_pedidos/fotos_pedidos/12-1-117-hh7x2ocz8-2.jpeg	a	a	f	579
3392	client/img/fotos_pedidos/fotos_pedidos/12-1-117-4l5lhdjj2-8.jpeg	a	a	f	579
3393	client/img/fotos_pedidos/fotos_pedidos/12-1-117-1nh481yjl-10.jpeg	a	a	f	579
3394	client/img/fotos_pedidos/fotos_pedidos/12-1-117-hdrhcbory-9.jpeg	a	a	f	579
3395	client/img/fotos_pedidos/fotos_pedidos/12-1-117-kqe55ozwb-11.jpeg	a	a	f	579
3396	client/img/fotos_pedidos/fotos_pedidos/12-1-117-9h6gwy5ap-13.jpeg	a	a	f	579
3397	client/img/fotos_pedidos/fotos_pedidos/12-1-117-k6n4d8h0q-12.jpeg	a	a	f	579
3398	client/img/fotos_pedidos/fotos_pedidos/12-1-117-2tsb2ccc4-14.jpeg	a	a	f	579
3399	client/img/fotos_pedidos/fotos_pedidos/12-1-117-wh4pgah6v-16.jpeg	a	a	f	579
3400	client/img/fotos_pedidos/fotos_pedidos/12-1-117-5s0or0921-15.jpeg	a	a	f	579
3401	client/img/fotos_pedidos/fotos_pedidos/12-1-117-rt17wnzg6-17.jpeg	a	a	f	579
3402	client/img/fotos_pedidos/fotos_pedidos/12-1-117-f42k14q66-18.jpeg	a	a	f	579
3403	client/img/fotos_pedidos/fotos_pedidos/12-1-117-0mcxv2jpi-19.jpeg	a	a	f	579
3404	client/img/fotos_pedidos/fotos_pedidos/12-1-117-rmoe9d7oh-0.jpeg	a	a	t	580
3405	client/img/fotos_pedidos/fotos_pedidos/12-1-117-yjmq6frap-0.jpeg	a	a	t	580
3406	client/img/fotos_pedidos/fotos_pedidos/12-1-117-5yytu4a2e-0.jpeg	a	a	t	580
3407	client/img/fotos_pedidos/fotos_pedidos/12-1-117-ga0r6iebi-1.jpeg	a	a	t	580
3408	client/img/fotos_pedidos/fotos_pedidos/12-1-117-qtt92w4cc-3.jpeg	a	a	t	580
3409	client/img/fotos_pedidos/fotos_pedidos/12-1-117-kkwmlo93s-2.jpeg	a	a	t	580
3410	client/img/fotos_pedidos/fotos_pedidos/12-1-117-tehd9vitt-4.jpeg	a	a	t	580
3411	client/img/fotos_pedidos/fotos_pedidos/12-1-117-ztk8xvabi-5.jpeg	a	a	t	580
3412	client/img/fotos_pedidos/fotos_pedidos/12-1-117-9kd4usco0-6.jpeg	a	a	t	580
3413	client/img/fotos_pedidos/fotos_pedidos/12-1-117-m9tj1rem0-7.jpeg	a	a	t	580
3414	client/img/fotos_pedidos/fotos_pedidos/12-1-117-4mbhp74d9-0.jpeg	a	a	f	581
3415	client/img/fotos_pedidos/fotos_pedidos/12-2-117-vl692ntaj-3.jpeg	a	a	f	582
3416	client/img/fotos_pedidos/fotos_pedidos/12-2-117-7n3yeyjr3-2.jpeg	a	a	f	582
3417	client/img/fotos_pedidos/fotos_pedidos/12-2-117-s9lxi78fw-1.jpeg	a	a	f	582
3418	client/img/fotos_pedidos/fotos_pedidos/12-2-117-p1lqycvcy-0.jpeg	a	a	f	582
3419	client/img/fotos_pedidos/fotos_pedidos/12-2-117-fmp1uwzvo-4.jpeg	a	a	f	582
3420	client/img/fotos_pedidos/fotos_pedidos/12-2-117-5x5u4bbd3-6.jpeg	a	a	f	582
3421	client/img/fotos_pedidos/fotos_pedidos/12-2-117-44y2v0gbj-5.jpeg	a	a	f	582
3422	client/img/fotos_pedidos/fotos_pedidos/12-2-117-nvzdmom2r-7.jpeg	a	a	f	582
3423	client/img/fotos_pedidos/fotos_pedidos/12-2-117-rgz20j2yv-9.jpeg	a	a	f	582
3424	client/img/fotos_pedidos/fotos_pedidos/12-2-117-d5yuid603-8.jpeg	a	a	f	582
3425	client/img/fotos_pedidos/fotos_pedidos/12-2-117-3rh89km25-1.jpeg	a	a	f	582
3426	client/img/fotos_pedidos/fotos_pedidos/12-2-117-6z6v5mo06-0.jpeg	a	a	f	582
3427	client/img/fotos_pedidos/fotos_pedidos/12-2-117-9wk91ujzc-2.jpeg	a	a	f	582
3428	client/img/fotos_pedidos/fotos_pedidos/12-2-117-dvk2o31t6-3.jpeg	a	a	f	582
3429	client/img/fotos_pedidos/fotos_pedidos/12-2-117-o55ocax21-4.jpeg	a	a	f	582
3430	client/img/fotos_pedidos/fotos_pedidos/12-2-117-2ltsf2t5q-5.jpeg	a	a	f	582
3431	client/img/fotos_pedidos/fotos_pedidos/12-2-117-6tsq2zrts-9.jpeg	a	a	f	582
3432	client/img/fotos_pedidos/fotos_pedidos/12-2-117-wnmrc9xci-8.jpeg	a	a	f	582
3433	client/img/fotos_pedidos/fotos_pedidos/12-2-117-ziqas6nqu-6.jpeg	a	a	f	582
3434	client/img/fotos_pedidos/fotos_pedidos/12-2-117-e6jd7b77v-7.jpeg	a	a	f	582
3435	client/img/fotos_pedidos/fotos_pedidos/12-2-117-ohrsd7erq-0.jpeg	a	a	f	583
3436	client/img/fotos_pedidos/fotos_pedidos/12-2-117-9vq3rvdqj-1.jpeg	a	a	f	583
3437	client/img/fotos_pedidos/fotos_pedidos/12-2-117-yuw2q9h8y-2.jpeg	a	a	f	583
3438	client/img/fotos_pedidos/fotos_pedidos/12-2-117-6avlbbn9a-0.jpeg	a	a	f	583
3439	client/img/fotos_pedidos/fotos_pedidos/12-2-117-wmkyzk72a-0.jpeg	a	a	f	584
3440	client/img/fotos_pedidos/fotos_pedidos/12-2-117-dk7qnqcik-1.jpeg	a	a	f	584
3441	client/img/fotos_pedidos/fotos_pedidos/12-2-117-ui2g5id81-2.jpeg	a	a	f	584
3442	client/img/fotos_pedidos/fotos_pedidos/12-2-117-zhygoqych-3.jpeg	a	a	f	584
3443	client/img/fotos_pedidos/fotos_pedidos/12-2-117-cn0vye0qj-4.jpeg	a	a	f	584
3444	client/img/fotos_pedidos/fotos_pedidos/12-2-117-gi8ylo7ms-5.jpeg	a	a	f	584
3445	client/img/fotos_pedidos/fotos_pedidos/12-2-117-x046noxdy-6.jpeg	a	a	f	584
3446	client/img/fotos_pedidos/fotos_pedidos/12-2-117-vnrcfodgd-7.jpeg	a	a	f	584
3447	client/img/fotos_pedidos/fotos_pedidos/12-2-117-38e4ptdut-8.jpeg	a	a	f	584
3448	client/img/fotos_pedidos/fotos_pedidos/12-2-117-7c6rxus6e-9.jpeg	a	a	f	584
3449	client/img/fotos_pedidos/fotos_pedidos/12-2-117-jv0buow3u-10.jpeg	a	a	f	584
3450	client/img/fotos_pedidos/fotos_pedidos/12-2-117-czptyidru-11.jpeg	a	a	f	584
3451	client/img/fotos_pedidos/fotos_pedidos/12-2-117-vklhujsa7-12.jpeg	a	a	f	584
3452	client/img/fotos_pedidos/fotos_pedidos/12-2-117-zb7uawzkl-13.jpeg	a	a	f	584
3453	client/img/fotos_pedidos/fotos_pedidos/12-2-117-i159tujl8-14.jpeg	a	a	f	584
3454	client/img/fotos_pedidos/fotos_pedidos/12-2-117-ny3hybf9k-15.jpeg	a	a	f	584
3455	client/img/fotos_pedidos/fotos_pedidos/12-2-117-0kxaq8wy2-0.jpeg	a	a	f	586
3456	client/img/fotos_pedidos/fotos_pedidos/12-2-117-uv97y16z7-1.jpeg	a	a	f	586
3457	client/img/fotos_pedidos/fotos_pedidos/12-2-117-13xqb9xhq-2.jpeg	a	a	f	586
3458	client/img/fotos_pedidos/fotos_pedidos/12-2-117-bu7fov94v-0.jpeg	a	a	f	585
3459	client/img/fotos_pedidos/fotos_pedidos/12-2-117-71jqhw0bh-1.jpeg	a	a	f	585
3460	client/img/fotos_pedidos/fotos_pedidos/12-2-117-t5x6pavtv-0.jpeg	a	a	f	587
3461	client/img/fotos_pedidos/fotos_pedidos/12-2-117-69o19p5le-1.jpeg	a	a	f	587
3462	client/img/fotos_pedidos/fotos_pedidos/12-2-117-psql0x7w9-3.jpeg	a	a	f	587
3463	client/img/fotos_pedidos/fotos_pedidos/12-2-117-cnpk0iyu1-2.jpeg	a	a	f	587
3464	client/img/fotos_pedidos/fotos_pedidos/12-2-117-zrcz4eijg-4.jpeg	a	a	f	587
3465	client/img/fotos_pedidos/fotos_pedidos/12-2-117-mjehkoldk-5.jpeg	a	a	f	587
3466	client/img/fotos_pedidos/fotos_pedidos/12-2-117-rytvaqmt4-6.jpeg	a	a	f	587
3467	client/img/fotos_pedidos/fotos_pedidos/12-2-117-gth7ol0fw-8.jpeg	a	a	f	587
3468	client/img/fotos_pedidos/fotos_pedidos/12-2-117-27l7ptd2c-7.jpeg	a	a	f	587
3469	client/img/fotos_pedidos/fotos_pedidos/12-2-117-3r3q7cc3e-9.jpeg	a	a	f	587
3470	client/img/fotos_pedidos/fotos_pedidos/12-2-117-69opfgspe-10.jpeg	a	a	f	587
3471	client/img/fotos_pedidos/fotos_pedidos/12-2-117-p4sp5i6md-11.jpeg	a	a	f	587
3472	client/img/fotos_pedidos/fotos_pedidos/12-2-117-kq7jj5twr-12.jpeg	a	a	f	587
3473	client/img/fotos_pedidos/fotos_pedidos/12-2-117-1b12nvunx-13.jpeg	a	a	f	587
3474	client/img/fotos_pedidos/fotos_pedidos/12-2-117-83zlpe0d7-14.jpeg	a	a	f	587
3475	client/img/fotos_pedidos/fotos_pedidos/12-2-117-rkio3h431-15.jpeg	a	a	f	587
3476	client/img/fotos_pedidos/fotos_pedidos/12-3-117-kkp7u5v9q-1.jpeg	a	a	f	588
3477	client/img/fotos_pedidos/fotos_pedidos/12-3-117-ttowj8xej-2.jpeg	a	a	f	588
3478	client/img/fotos_pedidos/fotos_pedidos/12-3-117-zrd7tn94g-3.jpeg	a	a	f	588
3479	client/img/fotos_pedidos/fotos_pedidos/12-3-117-us7ehdp7y-5.jpeg	a	a	f	588
3480	client/img/fotos_pedidos/fotos_pedidos/12-3-117-1eldp4olp-7.jpeg	a	a	f	588
3481	client/img/fotos_pedidos/fotos_pedidos/12-3-117-flps3jvoo-6.jpeg	a	a	f	588
3482	client/img/fotos_pedidos/fotos_pedidos/12-3-117-7d2yrynny-8.jpeg	a	a	f	588
3483	client/img/fotos_pedidos/fotos_pedidos/12-3-117-n0yzclxs4-0.jpeg	a	a	f	588
3484	client/img/fotos_pedidos/fotos_pedidos/12-3-117-e5ok01k4l-2.jpeg	a	a	f	588
3485	client/img/fotos_pedidos/fotos_pedidos/12-3-117-vm1n9rekt-1.jpeg	a	a	f	588
3486	client/img/fotos_pedidos/fotos_pedidos/12-3-117-57xwcumo1-0.jpeg	a	a	f	589
3487	client/img/fotos_pedidos/fotos_pedidos/12-3-117-a1mynmswl-2.jpeg	a	a	f	589
3488	client/img/fotos_pedidos/fotos_pedidos/12-3-117-lbd4mcp01-3.jpeg	a	a	f	589
3489	client/img/fotos_pedidos/fotos_pedidos/12-3-117-yp7rmhojo-1.jpeg	a	a	f	589
3490	client/img/fotos_pedidos/fotos_pedidos/12-3-117-j3eojo1zb-5.jpeg	a	a	f	589
3491	client/img/fotos_pedidos/fotos_pedidos/12-3-117-eh66e7ase-4.jpeg	a	a	f	589
3492	client/img/fotos_pedidos/fotos_pedidos/12-3-117-jo9ma197a-6.jpeg	a	a	f	589
3493	client/img/fotos_pedidos/fotos_pedidos/12-3-117-w45b0o4bw-7.jpeg	a	a	f	589
3494	client/img/fotos_pedidos/fotos_pedidos/12-3-117-y7wlpc7h5-8.jpeg	a	a	f	589
3495	client/img/fotos_pedidos/fotos_pedidos/12-3-117-w5hk3ge5i-9.jpeg	a	a	f	589
\.


--
-- Name: foto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('foto_id_seq', 3495, true);


--
-- Data for Name: pedido; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pedido (id, nombre, cedula, direccion, direccionenvio, telefono, politicas, pago, estado, fecharecepcion, precio, iva, total, id_user, usuarioinstagram, costoenvio, referencecode, envioexpress, bday, ciudad) FROM stdin;
262	María Fernanda Rodriguez	52868925	Calle 116 # 12-20	Calle 116 # 12-20 apto 302 Edificio Aquimin	3212400692	t	a	Entregado	2017-07-11 13:17:23.131	81000	15390	96390	mafe.rodriguez.arango@gmail.com	maferodri82	6000	f39c33cf9441b9cef49844a592602cf7	f	Oct 03 1982	Bogotá D.C
359	Cristina Bermeo	36302591	Calle 47#7-35 Edif.PremierII Apto 101	Calle 47#7-35 Edif.PremierII Apto 101	3208833640	f	a	Entregado	2017-09-10 22:34:14.011	65000	12350	77350	scristinabp@hotmail.com	\N	6000	eb3c11dc1bc4ded8eb3da8783dfe74e6	f	Marzo 03	Bogotá D.C
276	CAROLINA  ARRIETA	1047393422	transversal 52 no. 21c-59	transversal 52 no. 21c-59 ap 403	3012426235	t	a	Entregado	2017-07-17 21:02:56.666	43000	8170	51170	carotoya87@gmail.com	carotoya87	9000	da86547a74d6c3ee3e97f7bca32ae430	f	Dec 15 1987	Cartagena de Indias
316	Daniela Casses	1072710802	carrera 19a # 103a-21	carrera 19a # 103a-21	3107694385	f	a	Entregado	2017-08-04 19:44:34.252	34000	6460	40460	casses.daniela@gmail.com	\N	8000	3c703d4d28bcbdba9bcea4027381e151	f	_	Bogotá D.C
277	Laura Céspedes	1016086481	Calle 97 # 70c-89 interior 6 apto 604 Portal de pontevedra 2	Calle 97 # 70c-89 interior 6 apto 604 Portal de pontevedra 2	3118447617	f	a	Entregado	2017-07-17 23:23:11.548	56000	10640	66640	lauracamilasc@hotmail.com	\N	6000	1d0621d5fd3da0e33fb97a5a5915b428	f	May 28 1996	Bogotá D.C
312	Laura Maria Bautista Narvaez	1015479190	Cra 61 # 95-02 bloque 2 apto 203	Cra 61 # 95-02 bloque 2 apto 203	3185540964	t	a	Rechazado	2017-08-04 18:07:37.075	61500	11685	73185	bautilaura@hotmail.com	@lbautistan	6000	1c3a8133b12e370e8bbf29c8ecfcfd7c	f	26/01/1999	Bogotá D.C
291	Andrea montiel	399493	Avenida carrera 7 #132-45	Avenida carrera 7 #132-45	3102188968	t	a	Entregado	2017-07-27 02:29:21.347	42500	8075	50575	maandreamontiel22@gmail.com	@maandreamontiel22	11000	5ff70e3d2a741065359edaa90723831f	t	22 noviembre	Bogotá D.C
284	Ana Maria Velez	1017237937	Carrera 46 # 76 Sur - 69. Apto 816. Edificio Cyprus	Carrera 46 # 76 Sur - 69. Apto 816. Edificio Cyprus	3218172997	f	a	Entregado	2017-07-24 08:20:10.238	39000	7410	46410	anavelezgo@gmail.com	\N	9000	a808edaed426fa6031ffd46c0b011333	f	Sep 10 1996	Sabaneta
372	Juan Camilo Arboleda Ramírez	1001228934	Calle 75 sur #52g-40	Calle 75sur #52g-40	3003750275	t	a	Entregado	2017-09-13 15:57:33.037	45000	8550	53550	arboledajuank@gmail.com	@jca_10r	10000	7c5b9a067dbab4ede78c9c635ce62a4d	f	28 mayo 2000	Medellín
269	Jehimy Jimenez Gonzalez	55301334	carrera 26B 2 No 74C26 Bloque 1 apto 103	carrera 26B 2 No 74C26 Bloque 1 apto 103	3046351392	f	a	Entregado	2017-07-13 15:05:32.743	79000	15010	94010	jehimy13@hotmail.com	\N	15000	914fce37239a231c3d8563795373df94	f	May 13 1984	Barranquilla
323	Manuel Yepes	1036605998	Cra 23 # 103 - 61, Apto 405	Cra 23 # 103 - 61, Apto 405	3137897944	f	a	Entregado	2017-08-13 20:03:19.227	102000	19380	121380	manuel.yepesgaviria@gmail.com	\N	6000	f98b7dcecb2f6cbc82138e5f26aec07b	f	Diciembre 16	Bogotá D.C
298	Luisa Fernanda Carabalí Sepulveda	1053796983	Calle 67 # 26 a 15 Palermo	Calle 67 # 26 a 15 palermo punto servientrega Efecty	3178953635	f	a	Entregado	2017-07-31 14:52:02.698	60000	11400	71400	luisacarabalisepulveda@gmail.com	\N	20000	a51a6784bea25cb64e93275aa4c62c5c	t	02 septiembre	Manizales
379	Carolina Calderón	1020821909	Cra 56 # 148-35	Cra 56a # 148-35. Torre 2 apt 601	3134674176	t	a	Pendiente	2017-09-15 16:07:46.646	53000	10070	63070	calderoncarolina77@hotmail.com	cr.carolina	13000	f341cab3132df1c80b5fdc5146fa8563	t	7 de enero	Bogotá D.C
330	María Camila Carvajal	1020786959	Diagonal 70b #2-14 este	Diagonal 70b #2-14 este. Apto 301. Edificio Mirador de la cañada. (sobre avenida circunvalar costado oriental)	3173630895	f	a	Entregado	2017-08-16 20:58:54.779	48000	9120	57120	camicarvajal@hotmail.com	\N	8000	484f62430ffcdfbeaeb6dd2f6bed3c8f	f	Noviembre 17	Bogotá D.C
344	DENISSE AMAYA	1121873770	27 OAKLEY LANE	calle 35a # 13a-05 urb. Castila	2037070279	f	a	Pendiente	2017-08-30 01:54:11.921	15000	2850	17850	denisse.amaya@hotmail.com	\N	9000	9db1ea780272e3d0675da1d2c34aeb5d	f	21/11	Villavicencio
351	Paula Andrea Gómez	1140899673	Calle 45 no 26A 03 urbanización muvdy	Calle 45 no 26A 03 muvdi	3008204544	t	a	Pendiente	2017-09-09 22:30:19.077	40000	7600	47600	paulagomez_98@hotmail.com	@_pagc	9000	45c95a70c2c248df646cf2be63c97299	f	24/09	Soledad
428	Marcela Gómez	1018493164	Carrera 24 # 63c-69	Carrera 24 # 63c-69	3108565116	f	a	Entregado	2017-10-12 19:13:56.517	42500	8075	50575	marcela.1997.mg@gmail.com	\N	11000	96431d007cce8aeea3c3be096aaf7fd4	t	16/03/97	Bogotá D.C
365	Estefania Tamayo Santa	1152195359	calle 49 # 87-12 Apto 301	calle 49 # 87-12 Apto 301	3016775378	t	a	Pendiente	2017-09-12 20:36:11.606	45000	8550	53550	estefatamayo19@hotmail.com	@tefitamayo	10000	1ccd01786841d82b111738058d0f618c	f	19/03/92	Medellín
421	Maria alejandra moriones	1112300607	Carrera 63a # 3c 76	Carrera 63a # 3c 76 apto 410 torre c	3163859599	t	a	Entregado	2017-10-09 18:57:14.994	97000	18430	115430	maalmosu0815@gmail.com	\N	18000	abb7cf7dfbe53f2583e575878f653eb7	f	19 04 1990	Cali
393	Laura Rodríguez	1221720636	Calle 147 No. 13-53	Carrera 19 No. 86A-48	3168757248	t	a	Pendiente	2017-09-22 13:15:34.368	39000	7410	46410	laura.rodriguez1797@gmail.com	@relatividad17	13000	bb31b69a0cece48dd9e0ba05252d0e47	t	09/01/1997	Bogotá D.C
386	Ingrid triana	1110517040	Calle 43 #4-26	Calle 43 #4-26 centro médico Javerianos 5 piso unicat	3108185094	f	a	Entregado	2017-09-19 19:57:31.446	50000	9500	59500	sukylu.it@gmail.com	\N	10000	338bf13b948791c748efd2222fd6ef99	f	12 noviembre	Ibagué
400	Juliana torres	1005342078	Carrera 68 d # 99- 57	Carrera 68 d # 99- 57 interior 10 201 - rincón de los sauces barrio la floresta	3057069828	f	a	Pendiente	2017-09-25 16:34:27.06	20000	3800	23800	jutoma03@gmail.com	\N	8000	1bd0941b051be3c2bb58667a975601fd	f	03 de febrero	Bogotá D.C
407	Salomé Botero Nieto	1019150159	Cra 54 #135-35	Cra 54 #135-35	3124396063	f	a	Pendiente	2017-09-27 01:41:34.112	37000	7030	44030	salo.botero_1999@hotmail.com	\N	11000	0f75b5c049cd7a09123b02d1311caa6c	t	Julio 21	Bogotá D.C
414	Alejandra Bohorquez Obando	42692177	Calle 20 Sur # 26 C 66 Apt 302 Edificio Altos de San Lucas	Calle 20 Sur # 26 C 66 Apt 302 Edificio Altos de San Lucas, Poblado	3013682350	f	a	Pendiente	2017-10-03 17:20:16.407	55500	10545	66045	abohorquezobando@yahoo.com	\N	9000	8b5add7bcf361f6c70fcb8deb210bd4f	f	18 de octubre	Medellín
435	Claudia Pinzón	51966567	Calle 97 # 22-45	Calle 97 # 22-45	3123971462	f	a	Entregado	2017-10-17 18:56:56.84	56000	10640	66640	mfbp92@hotmail.com	\N	6000	fd542d649867c8ce4496219af221f79d	f	4 de noviembre	Bogotá D.C
442	María Camila Behar	1020768177	Calle 137A 73-37 casa 16	Calle 137A 73-27 casa 16	3016271057	t	a	Entregado	2017-10-20 01:25:23.528	48000	9120	57120	camila.behar.m@gmail.com	Camila.behar	8000	73531dd630414120295484f15e247845	f	08/01/1992	Bogotá D.C
457	Diego Rodriguez	1014178289	Calle 79 b 7 - 59	Calle 79 b 7 - 59	3112900081	f	a	Pendiente	2017-10-31 23:06:48.026	34000	6460	40460	maolinano@gmail.com	\N	8000	8dd2d41587fb8d9a99316ed71cab948a	f	19 jun 1986	Bogotá D.C
449	Catalina mora	1020775360	Cra 6 no 125-35	Cra 6 no 125-35	3115768423	f	a	Entregado	2017-10-25 12:52:33.775	34000	6460	40460	catamora3@gmail.com	\N	8000	40655f8c63cb3bf60232ea26e555c838	f	Oct 8	Bogotá D.C
306	Amparo Guzman	41758096	Calle 114 A # 45-65 Int 9/Apto 203	Calle 114 A # 45-65 Int 9/Apto 203	3138706231	t	a	Entregado	2017-08-03 01:39:42.751	42500	8075	50575	amparoguzmans@gmail.co	juanitapaezg	6000	2e504d38bc715a3b40a759493611860d	f	28 de abril	Bogotá D.C
422	Sofia Barrera	1020832195	Carrera 7b bis #130b-41 vivis 1 apt 609	Carrera 7b bis #130b-41 vivis 1 apt 609	3165266382	f	a	Entregado	2017-10-09 23:04:51.119	53000	10070	63070	sbarreramurcia@gmail.com	\N	8000	3a613c0d83137496a351a4eba5374970	f	_	Bogotá D.C
317	yessenia ortegon	1014244770	cra 56a # 128 -61	cra 56a #128 -61	3014462899	f	a	Entregado	2017-08-05 01:27:17.389	47500	9025	56525	yeye5518_5@hotmail.com	\N	11000	94e2d566db5e07c4d55e240442f56742	t	17/05/93	Bogotá D.C
324	Tatiana Sanchez Arias	1018489473	TV 52C 1 47 ponderosa	KRA 95A 136 42 BLQ18 APT 401BOSQUES DE SUBA AGRUPACION 1	3155202387	t	a	Entregado	2017-08-14 03:34:52.497	36000	6840	42840	tatianasancheza@hotmail.com	@tatianasancheza	6000	373c115fb4f955b0ab4dbf269f380715	f	02/10/1996	Bogotá D.C
292	Natalia Serrano	1020795686	cra 22 # 122 - 65	cra 22 # 122 - 65	3016744137	f	a	Entregado	2017-07-28 19:09:30.331	48000	9120	57120	n.serrano11@uniandes.edu.co	\N	8000	f37908237681361f876e6e38ecaf1c1f	f	10 de agosto	Bogotá D.C
401	Juliana torres marin	1005342078	Carrera 68 D # 99 - 57	Carrera 68 D # 99- 57 interior 10- 201 Barrio la floresta conjunto rincón de los sauces	3057069828	f	a	Entregado	2017-09-25 16:41:39.411	20000	3800	23800	jutoma03@gmail.con	\N	8000	bd60d4dcee40e38aac7848eedde9c5b4	f	03 de febrero	Bogotá D.C
313	Daniela Casses	1072710802	carrera 19a # 103a-21	carrera 19a #103a - 21	3107694385	f	a	Pendiente	2017-08-04 18:54:43.842	34000	6460	40460	casses.daniela@gmail.com	\N	8000	363bf6873ae7f05153cfad3a4a9fb40b	f	_	Bogotá D.C
331	Katherine escobar	67030130	Avenida 3ra f norte # 59 - 111	Avenida 3ra f norte # 59 - 111	3104932802	t	a	Pendiente	2017-08-17 15:07:31.019	45000	8550	53550	kathy_escobar@hotmail.com	\N	10000	c0c63be97f8a5384adc0b8b0472df154	f	25 de marzo de 1984	Cali
338	monica linares	1136881238	cra 8 no 83-24	cra 8 no 83-24 apto 405	3103069064	f	a	Entregado	2017-08-23 20:03:26.243	70500	13395	83895	monicalinaresarciniegas@gmail.com	\N	6000	f2882b09a4eebc949fd8be38997ae441	f		Bogotá D.C
345	ANA MARIA CORREA GRANDAS	1098681160	calle 58 # 30 - 31	calle 58 # 30 - 31	3144196600	f	a	Pendiente	2017-08-31 21:56:24.637	57000	10830	67830	acorreagrandas@gmail.com	\N	10000	f6e9733103469172003093d4c5d2b382	f	24/01/1990	Bucaramanga
352	Paula Gómez	1140899673	Calle 45 no 26A 03 urb muvdi	Calle 45 no 26A 03 urb muvdi	3008204544	t	a	Pendiente	2017-09-09 22:41:07.358	40000	7600	47600	paulagomez_98@hotmail.com	@_pagc	9000	6ee82dd234ad71b87243dae03207243b	f	24/09	Soledad
443	Stephanie Alzate	1125804712	Cra 24a #13-53 barrio corbobes	Cra 24a #13-53 barrio corbones	3192237437	t	a	Entregado	2017-10-20 03:20:05.123	35000	6650	41650	stelephant@hotmail.com	@stelephant_	9000	c2fba576926360d306ff7b160fdcb2d6	f	06 de mayo	Armenia
366	Dayana lozano	1022423510	Cra 41#36-04sur	Cra 41#36-04	3103144049	t	a	Pendiente	2017-09-12 22:11:25.304	37500	7125	44625	dayana22lozano@hotmail.com	@dayana22lozano	6000	90812cb4a406c427ccaaa39cd3cd6fd4	f	22-01	Bogotá D.C
360	Manuela Londono Ríos	1088333549	Condominio San Jose de las villas etapa 2 casa 58	Condominio San Jose de las villas etapa 2 casa 58	3148894766	t	a	Entregado	2017-09-11 02:01:08.72	50000	9500	59500	manuelalr12@gmail.com	Lapetitelifestyle	10000	2f5920fc9c6299b1d09c43cb566fc7b0	f	19 de septiembre	Pereira
373	Valentina rodriguez	99120501492	Carrera 44 numero 17sur-59	Carrera 44 numero 17sur-59 apto 301	3113723752	t	a	Pendiente	2017-09-14 19:35:58.998	55000	10450	65450	valentinarodriguezyepes@hotmail.com	@valentinarodriguezy	10000	9f74add78b7b5331360e324e517b2a89	f	05 de diciembre	Medellín
394	Laura Daniela Tejada	1007284838	calle 164 #16c-15	calle 164 #16c-15	3152598185	t	a	Rechazado	2017-09-23 18:30:04.79	37500	7125	44625	laura.danielatejada@gmail.com	@lauradaniela31	6000	66a4a1c5c4d4b906f195f20500115155	f	28 de junio del 2000	Bogotá D.C
380	Carolina Calderón	1020821909	Carrera 56a # 148-35	Carrera 56a # 148-35. Torre 2 apt 601	3134674176	t	a	Entregado	2017-09-15 18:06:29.817	53000	10070	63070	calderonmaria@javeriana.edu.co	cr.carolina	13000	e68d369df2a06ca96685cd1702501756	t	7 de enero de 1997	Bogotá D.C
387	Paz Navarro Bernal	1020785178	carrera 12 #124-30 apto 216	Carrera 12 #124-30 apto 216	3138480541	t	a	Pendiente	2017-09-19 20:35:16.598	43000	8170	51170	paznavarrobernal93@outlook.com	@paznabe	8000	1f2ec2a0d5ec3bdf0897b0be3fd05a4e	f	_	Bogotá D.C
408	Salomé Botero Nieto	1019150159	Cra 54 #135-35	Cra 54 #135-35	3124396063	f	a	Entregado	2017-09-27 02:13:07.052	32000	6080	38080	salo.botero_1999@hotmail.com	\N	6000	a33beedb32975a3376259b2a9f06d123	f	Julio 21	Bogotá D.C
450	Paola Afanador Méndez	1026572000	Carrera 26 # 45A-66	Carrera 26 # 45A-66	3114460654	t	a	Pendiente	2017-10-26 02:31:17.249	51000	9690	60690	pafanadorm@gmail.com	pafanador	13000	89fefea79c05023eba440fe9b57ba320	t	26 de mayo de 1992	Bogotá D.C
458	Diego Rodriguez	1014178289	Calle 79 b 7-59	Calle 79 b 7-59	3112900081	f	a	Pendiente	2017-10-31 23:12:46.215	34000	6460	40460	maolinano@gmail.com	\N	8000	d856d497757893e46ccdb3c7e6bc9602	f	19 junio	Bogotá D.C
415	Adriana Escobar	36312494	Cra 43a #22-13	Cra 43a #22-13 conjunto los girasoles casa 36	3142380137	t	a	Entregado	2017-10-06 02:25:52.64	72000	13680	85680	adriana.escobar.gomez@gmail.com	Sofiagutierreze	10000	3725e7117d3d434a0feef1d80aea3724	f	29/04/83	Neiva
429	Maria alejandra olivares	1102851048	Cra 8a # 99-22 apto 803	Cra 8a # 99-22 apto 803	3134328384	t	a	Entregado	2017-10-13 00:45:53.342	58000	11020	69020	marialeolivares3@gmail.com	\N	13000	8bc6ff20ec2d704745a721d33317cdf6	t	03 noviembre	Bogotá D.C
467	Natalia Roa Moriones	1020773015	cra10A num 119-49 apto 701 ed: era 2000	cra10A num 119-49 apto 701 ed: era 2000	3164743251	f	a	Entregado	2017-11-09 10:12:16.704	70500	13395	83895	nicolasroamoriones@hotmail.com	\N	6000	55bce9c8fb41bedbbbb47ea2a7c95973	f	_	Bogotá D.C
436	Karol silva	1020840815	Calle150#50-47	Calle150#50-47	3142804852	t	a	Rechazado	2017-10-17 19:23:14.294	32000	6080	38080	karolsilva199903@gmail.com	@k.silva. 1	6000	6863558513a6c8412bcff1dc6d49fc15	f	03-05-1999	Bogotá D.C
462	Luisa Fernanda Montaño Miranda	1125292464	Calle 152 #54-39	Calle 152 # 54- 39 Parque Aragón 101	3118354274	f	a	Entregado	2017-11-03 18:34:58.591	184500	35055	219555	lumontano_82@hotmail.com	\N	8000	51a83f95779bd8934c948aa9ae773994	f	14/06/1992	Bogotá D.C
477	Pacheco	1023304272	Calle 121 #3a-20 apt 318c	Calle 121 #3a-20 apt 318c	3107993504	f	a	Pendiente	2017-11-15 12:41:20.031	18000	3420	21420	Ana20nov@gmail.com	\N	6000	382ca1b79d0b15d5efd3d6c1704a5820	f	20/11/94	Bogotá D.C
473	Stephanie mayor	1070925096	Transversal 94f # 83b 15	Transversal 94f # 83b 15	3226140380	f	a	Entregado	2017-11-14 02:09:07.762	57000	10830	67830	stephaniemayor100@gmail.com	\N	6000	41fece5a020ec7c7ec4129d2180bf263	f	13 de enero	Bogotá D.C
480	Paula López	1022393074	Calle 24A # 57 - 69 IN:6 AP:503	Calle 24A # 57 - 69 IN:6 AP:503	3115967026	f	a	Entregado	2017-11-16 03:19:46.937	86500	16435	102935	paulalopezotalora@gmail.com	\N	6000	3fe8e38b9c07469e58aba01b29642cbd	f	27/05/1994	Bogotá D.C
271	Catalina Renjifo Restrepo	1143840276	Calle 116#14-06 Apto 204	Calle 116#14-06 Apto 204 Edificio Tikal	3163565731	t	a	Entregado	2017-07-15 14:59:29.908	32000	6080	38080	catalinarr_p5@hotmail.com	catarr91	6000	e3c91940d14def176097b23181cd32ab	f	Nov 08 1991	Bogotá D.C
264	Camila Gaitan Mosquera	1030561421	Calle 160# 58-75. Barrio Gilmar. Conjunto Residencial Dali. Torre 33 Apto 404	Calle 160 # 58-75. Barrio Gilmar. Conjunto Residencial Dali. Torre 33 Apto 404	3174322368	f	a	Entregado	2017-07-12 14:55:54.203	32000	6080	38080	camila.gaitanm@gmail.com	\N	6000	eba6fe8ea5216ddb3efe8a3b7ed6d116	f	Sep 07 1989	Bogotá D.C
279	Natalia Navia	1144071804	calle 14A #68-60 casa 14, la Hacienda Manzana 10	cll 13e #68-39 casa 33 La hacienda manzana 5	3122460026	t	a	Pendiente	2017-07-21 15:03:07.189	34000	6460	40460	natalia.navia@hotmail.com	@natalianaviam @juanjpatino	9000	d07b33282b0a98d8b5436edb3c3e8db9	f	_	Cali
353	Madison Chacón	1007027695	Calle 142 #16a-25	Calle 93 #19-58	3008670245	t	a	Entregado	2017-09-09 22:49:09.691	39000	7410	46410	madisoncsv@gmail.com	@madisoncsv	13000	74650e7a62b87c86f3f4c95680b0847e	t	02/09/1995	Bogotá D.C
286	Sebastian Herrera Hernandez	1233693724	Cra 115 # 81 81 Int 3 Apt 206	Cra 115 # 81 81 Int 3 Apt 206	3202418179	t	a	Entregado	2017-07-24 19:37:19.22	37000	7030	44030	sebastianhh1999@hotmail.com	@laura_v09	6000	daf59cec5506b31c9511943f4c2357ed	f	Mar 04 1999	Bogotá D.C
318	Maria Camila Usubillaga	1018495626	calle 6 oeste # 4-200	Carrera 4 # 67-48 apto 203 enviar a nombre de: Juan Camilo Casas, con el mensaje: "No importa el lugar del mundo en el que estemos Te Amo"	3174382538	f	a	Entregado	2017-08-08 17:26:58.158	53000	10070	63070	mariak-usubillaga@hotmail.com	\N	8000	09df601b557ce2cd1ce0824df66969a1	f	23/07/1997	Bogotá D.C
300	Luisa Tabares	1144082117	Cra 82a#48a-66	carrera 82 a #48a66	3008339498	f	a	Pendiente	2017-07-31 17:08:43.7	35000	6650	41650	luisa.mar20@hotmail.com	\N	9000	1028f064faded73e2cf8d5585c06dd2b	f	18 agosto	Cali
293	Carolina Quintana	1130604519	Calle 114 56 89 Torre 2 apto 303	Calle 114 56 89 Torre 2 apto 303	3184754103	f	a	Entregado	2017-07-28 20:46:31.761	32000	6080	38080	cquintanag@outlook.com	\N	6000	b2cfb4ef9b884e5b807dd2b2e8037681	f	14 marzo 1986	Bogotá D.C
307	Katherine escobar	67030130	Avenida 3ra f norte # 59 - 111	Avenida 3ra f norte # 59 - 111	3104932802	t	a	Pendiente	2017-08-03 14:19:31.975	36000	6840	42840	kathy_escobar@hotmail.com	\N	10000	2d48ae9bcb271effe7e449d7e33fb2d7	f	25 de marzo de 1984	Cali
314	Daniela Casses	1072710802	carrera 19a # 103a-21	carrera 19a # 103a-21	3107694385	f	a	Rechazado	2017-08-04 18:58:17.775	34000	6460	40460	casses.daniela@gmail.com	\N	8000	c26444b2215559014a6c5ba741cba6d3	f	_	Bogotá D.C
361	Christian Knudsen	1020772446	diagonal 91 # 4-69	Km 2.5 vía Chia-cajica, Hacienda Fontanar, arce 20	3144482364	t	a	Pendiente	2017-09-11 13:24:54.993	36000	6840	42840	christian.knudsen92@gmail.com	christianknda	10000	ab6cdfde21947ff4b436229913f14680	f	9 de junio 1992	Chía
325	dayana peña	1015439097	cra 27 # 68-78	cra 27 # 68-78	3156704623	f	a	Entregado	2017-08-14 19:39:51.24	70000	13300	83300	dayana.anp@hotmail.com	\N	11000	40695dcc6e2ff317f398c1190b96e45a	t	06 de junio de 1993	Bogotá D.C
332	Katherine escobar	67030130	Avenida 3ra f norte # 59 - 111	Avenida 3ra f norte # 59 - 111	3104932802	t	a	Entregado	2017-08-17 16:48:52.627	35000	6650	41650	kathy_escobar@hotmail.com	\N	9000	341e8cb79a86e3ad9134d57c80e7a17f	f	25 de marzo de 1984	Cali
339	Manuela Barrera	52996727	Calle 80 No. 8-84 Interior 1	Calle 80 No. 8-84 Interior 1	3175652379	f	a	Entregado	2017-08-23 20:31:28.827	12000	2280	14280	manuela.barrera@gmail.com	\N	6000	486ffdd96e57970d6f4e33ac4bcc1714	f	_	Bogotá D.C
367	Eduardo Danies	1045743495	Calle 99 # 7a-76	Calle 99 # 7a-76	3106308203	f	a	Entregado	2017-09-12 22:27:42.923	69000	13110	82110	eduardodanies@gmail.com	\N	15000	5d261aee7210c7281cb50596f9587c4c	t	28 octubre 1996	Bogotá D.C
346	Felipe Orduz	1020746749	Calle 127D # 19-83 Edificio Calleja Plaza torre 3 apartamento 702	Calle 127D # 19-83. Edificio Calleja Plaza torre 3 apartamento 702	3153879555	f	a	Entregado	2017-09-04 18:12:38.414	78000	14820	92820	felipeorduzb@gmail.com	\N	8000	cde5e3d8872640006c03a2b5f0017629	f	Noviembre 7	Bogotá D.C
354	Madison Chacón	1007027695	Calle 142 #16a-25	Calle 93 #19-58	3008670245	t	a	Pendiente	2017-09-09 22:49:17.081	39000	7410	46410	madisoncsv@gmail.com	@madisoncsv	13000	74650e7a62b87c86f3f4c95680b0847e	t	02/09/1995	Bogotá D.C
374	LORENA ZAPATA	25285885	DIAGONAL 80 No 79-65 apto 302 edif nuevo horizonte robledo diamante	DIAGONAL 80 No 79-65 apto 302 edif nuevo horizonte robledo diamante	3216412581	f	a	Pendiente	2017-09-14 20:02:59.284	15000	2850	17850	loza28299@hotmail.com	\N	9000	f82e10198050c28525f66dbe39e94a05	f	28 enero 1980	Medellín
402	Sofía Mayoral	1020778985	carrera 17 #90-50	carrera 17 #90-50 apt.905	3107996938	f	a	Entregado	2017-09-25 20:07:03.843	48000	9120	57120	sofiamayoral693@gmail.com	\N	13000	16666552122d8a2bafa58df8cf7a2814	t	4 de feberero	Bogotá D.C
388	Dave Bermúdez	1026557471	Calle 72B # 85 - 02	Calle 72B # 85 - 02 Casa	3132492753	f	a	Entregado	2017-09-20 03:34:24.651	42500	8075	50575	davebaterista@gmail.com	\N	6000	ef6da5f43ff7e55f9d51884030384560	f	13 Marzo	Bogotá D.C
381	Daniela Tapias Cruz	99092003279	Calle 174 #8-31 casa 125	Calle 174 #8-31 Casa 125	3123024094	t	a	Entregado	2017-09-16 03:02:25.938	32000	6080	38080	danitapias1999@gmail.com	@taps.09	6000	35e0a49a7d9d07ada81a2d49c18df49c	f	20/09	Bogotá D.C
409	María José Alcalá Barceló	1140898225	Carrera 38D #74-190	CARRERA 38D #74 190	3183877059	t	a	Pendiente	2017-09-27 02:15:33.233	45000	8550	53550	mjalcalab@gmail.com	@whilemarasleeps	10000	f3cad1e84ead2b57d4673ac00fbc6e6c	f	Seis de Julio	Barranquilla
416	Carolina prueba 1	1033786932	Cll 00 #123	Cll 00 #123	3104090873	f	a	Rechazado	2017-10-06 21:49:36.537	56000	10640	66640	desarrollo@bgt404.com	\N	8000	9fcfa8fe59e8582c87f2fa241526c07a	f	08 abril	Bogotá D.C
423	Gabriel Barreto	1047385291	Calle 146 # 12-72 apto 1106	Calle 146 # 12-72 apto 1106	3168320545	f	a	Entregado	2017-10-11 16:02:23.679	58000	11020	69020	mariajulianaortiz@hotmail.com	\N	13000	30c29b891736ebba91d8e7db22b302f8	t	12 octubre de 2017	Bogotá D.C
430	Karol silva	1020840815	Calle150#50-47	Calle150#50-47	3142804852	t	a	Pendiente	2017-10-14 01:48:43.162	32000	6080	38080	karolsilva199903@gmail.com	@k.silva. 1	6000	098c3bd6269c6a132450436ee89f20c8	f	03-05-1999	Bogotá D.C
437	Daniela Maya Ochoa	1035419327	Cr 46B # 47C11	Cr 46B # 47C11	3104208832	t	a	Entregado	2017-10-17 22:34:47.463	50000	9500	59500	danimayaoc@gmail.com	@danimayaoc	9000	3a88760347eb44e18ac473c45c5c6bd0	f	06/07/1988	Copacabana
444	Andrea Estefanía victoria gallego	1116252518	Carrera 27 A #34-85	Carrera 27 A #34-85	3218526966	f	a	Entregado	2017-10-22 19:43:50.431	108000	20520	128520	annievictoria31@hotmail.com	\N	23000	4e45e8f98ea2b06f79cbb767ff3b3b7f	f	31-10/ 1991	Tuluá
451	jennifer laverde	1020742005	cra 18 #113 - 27	cra 18#113-27 apto 502	3102435691	f	a	Entregado	2017-10-26 03:21:38.037	69000	13110	82110	jennifer.laverde@gmail.com	\N	15000	f0b79bdfeaa6833bf313c9854da2d5a6	t	1 de junio	Bogotá D.C
468	Carolina Calderón	1020821909	Cra 56A # 148-35 Torre 2 apt 601	Cra 56A # 148-35 Torre 2 apt 601	3134674176	f	a	Pendiente	2017-11-09 22:41:44.022	48000	9120	57120	carolinacalderonramirez0107@gmail.com	\N	13000	f2cd9c799c9d99c87757b264b33608f9	t	07/01/97	Bogotá D.C
308	María Isabel Romero	52698570	calle 109 no 19 80, Apto 206	calle 109 no 19 80, Apto 206	3208653118	t	a	Entregado	2017-08-03 19:24:02.168	34000	6460	40460	romeromariaisabel@gmail.com	\N	8000	b292edbd1a7e1798811e1cdb216e9fea	f	07/07/1980	Bogotá D.C
287	Sophie Llano	1020812748	Carrera 10 # 91 - 10	Carrera 10 # 91 - 10. Apto 204	3183298971	f	a	Entregado	2017-07-24 22:57:15.2	32000	6080	38080	sophie.llano@gmail.com	\N	6000	05c07917bc0e46ef2762405da5a2482c	f	Feb 10 1996	Bogotá D.C
155	Bogota 404 S.A.S	901066990	Cll 135 c # 13 - 22 apt 402	Cll 135 c # 13 - 22 apt 402	3102176589	t	a	Entregado	2017-06-22 19:51:17.513	20000	3800	23800	desarrollo@bgt404.com	ricram2	8000	92078154d85712501a29d5ef71b12b91	f	\N	\N
280	Johanna lopez	1032404352	Carrera 116 b  # 70 a 54 apto 101 torre 27	Carrera 116 b  # 70 a 54 apto 101 torre 27	3195636701	t	a	Entregado	2017-07-21 16:39:16.4	40000	7600	47600	jo.halop@hotmail.com	@johannitalop	6000	d51a85b88b737bffa2fbf141a0885967	f	Jan 13 1988	Bogotá D.C
347	Leidy Daniela Campos	1144101573	Cra 41 A #14B - 47 B/ Guabal. 2do piso	Cra 41 A #14B - 47 B/ Guabal. 2do piso	3214892915	t	a	Entregado	2017-09-05 16:10:39.277	67500	12825	80325	Danielacaa06@hotmail.com	@danielac_8	10000	fcbea2287ce02d7653d7212948001551	f	6 de febrero	Cali
319	Maria Jose Benavides	1020758447	Calle 109 #1a-65 este	Calle 109 #1a-65 este	3103207485	t	a	Entregado	2017-08-10 17:44:19.486	98000	18620	116620	majobf91@gmail.com	mariajosebenavide	8000	a000f17dd9252cbd973770494a828f86	f	Enero 4	Bogotá D.C
294	Camila Prada	1018502577	Calle 94 # 21-59	Calle 94 # 21-59 apto 1102	3105702024	f	a	Entregado	2017-07-29 15:36:12.498	74000	14060	88060	camilaprada1@hotmail.com	\N	10000	a0719518829e5fea0241451738f60660	f		Bogotá D.C
301	Tatiana Cajamarca	1014185943	Calle 144 # 12 - 09 apto 206	Calle 144 # 12 - 09 apto 206	3015446030	f	a	Entregado	2017-07-31 19:19:40.045	47000	8930	55930	vivian.cajamarca@gmail.com	\N	6000	3e5db6085a690c7d7c14af7e8c6ee5f3	f	04/07/1987	Bogotá D.C
368	Sofia ramos	1018511337	Carrera 13a #102-76 apto 301	Carrera 13a #102-76 apto 301	3132950187	f	a	Pendiente	2017-09-13 00:01:39.434	61500	11685	73185	sofiaramosalgarra@gmail.com	\N	6000	9dd1995860e38afb9234b84d22d1eab6	f	8 de mayo 1999	Bogotá D.C
315	Daniela Casses	1072710802	carrera 19a # 103a-21	carrera 19a # 103a-21	3107694385	f	a	Pendiente	2017-08-04 19:04:44.961	34000	6460	40460	casses.daniela@gmail.com	\N	8000	8bb106670139451cab123e34ef177b42	f	_	Bogotá D.C
326	Natalia franco	1037643780	Calle 29c #35-130	Calle 29 c # 35-130 terrazas de San Diego	3147016743	f	a	Entregado	2017-08-15 15:02:54.925	50000	9500	59500	natalia.francosj@gmail.com	\N	10000	73185eeed7fb4f742938763f93e31777	f	30 Julio	Medellín
333	Juan Sebastián Sánchez	1136885247	CRA 68g # 9c-97 torre 3 apto 1001	CRA 68g # 9c-97 torre 3 apto 1001	3014106556	f	a	Pendiente	2017-08-22 17:28:52.969	43000	8170	51170	yaseloescribo@gmail.com	\N	8000	d7329cbc7ea2906cd1c498e8beef56a1	f	16 de abril	Bogotá D.C
389	Juan Diego Ocampo	1020773093	calle 124#20-36	calle 124#20-36	3148646872	f	a	Entregado	2017-09-20 19:59:28.396	64000	12160	76160	jd.ocampo880@gmail.com	\N	8000	fabb9c9db294cc9b339b5a28eaaafd3c	f	24/04/1992	Bogotá D.C
340	MARIA ALEJANDRA RINCON	1077873777	CARRERA 69B # 24-10 ALCAPARROS DE SAUZALITO-SALITRE INTERIOR 29 APTO 302	CARRERA 69B # 24-10 ALCAPARROS DE SAUZALITO-SALITRE INTERIOR 29 APTO 302	3138911665	f	a	Entregado	2017-08-26 17:33:37.215	48000	9120	57120	ALEJARF3@HOTMAIL.COM	\N	8000	5d29c7c0fb733e4538dc8293b818f6cc	f	3 DE FEBRERO	Bogotá D.C
424	Juan Martin Castro Díez	1130587172	Carrera 9bis # 97-19	carrera 9bis # 97-19 Edificio ID 97 Apto 201	3108043856	f	a	Entregado	2017-10-12 00:20:11.436	79000	15010	94010	juanmarcastro@hotmail.com	\N	15000	9978e4003600de81d130714eec95e804	t	2-08-1986	Bogotá D.C
355	Jimena velez	1090428245	Calle 60 #45-53 apt 201 Nicolás de federman	Calle 60 #45-53 apt 201 Nicolás de federman edificio corpus 60	3103205060	t	a	Entregado	2017-09-10 02:36:02.793	53000	10070	63070	velezgonzalezj@gmail.com	@javg0711	8000	aec5f2abb0cc84b585130b86d9a0beb1	f	7 de noviembre	Bogotá D.C
362	Christian nudsen	1020772446	diagonal 91 # 4 - 69	Km 2.5 vía Chia-cajica, Hacienda Fontanar, arce 20	314448364	t	a	Rechazado	2017-09-11 14:25:34.082	36000	6840	42840	christian.knudsen92@gmail.com	christianknda	10000	0ecc1cd800d0674de862a27fd66d30ee	f	9 de junio 1992	Chía
410	Javier Andres Peña Márquez	1018501757	calle 64#5-84 apt 401	calle 64#5-84 apt 401	3202329418	f	a	Entregado	2017-10-02 02:21:52.809	74000	14060	88060	Mjavier.andres757@gmail.com	\N	11000	2602e73ed149b5af7b9a84c0ad8454f3	t	12 de Marzo	Bogotá D.C
382	Juliana Ramírez León	1013610330	Cra 81B # 19B 80 Portal de Modelia 2 T23 Apto 203	Cra 81B # 19B 80 Portal de Modelia 2 T23 Apto 203	3158776803	f	a	Pendiente	2017-09-16 19:24:18.744	37500	7125	44625	juliana.ramirezl@hotmail.com	\N	6000	6c2dd2779aeb194eaa998fafa5b1723b	f	10 Febrero	Bogotá D.C
375	LORENA ZAPATA	25285885	DIAGONAL 80 No 79-65 apto 302 edif nuevo horizonte robledo diamante	DIAGONAL 80 No 79-65 apto 302 edif nuevo horizonte robledo diamante	3216412581	f	a	Entregado	2017-09-14 20:06:33.575	15000	2850	17850	loza28299@hotmail.com	\N	9000	adc5cdee10e2f3fa8532fcd56ec1c7e9	f	28 enero 1980	Medellín
396	Nathali Forero Almeciga	1030634039	calle 39 b bis# 72-67 sur	calle 100 #19-54 Edificio Primer Tower- Of 601	3016220560	t	a	Entregado	2017-09-25 01:24:26.842	42000	7980	49980	cnathalif@gmail.com	nataalmeciga	11000	9e0035766b576b2dae53112b73a9e0ac	t	07/11/93	Bogotá D.C
403	Diana Guarin	1098742360	carrera 7 bis # 94-11 edf gallery sierras del chico apto 702	carrera 7 bis # 94-11 edf gallery sierras del chico apto 702	3158524692	f	a	Entregado	2017-09-25 20:45:45.242	42500	8075	50575	diana_guarin@hotmail.com	\N	11000	ffb979d2aaf5f3f8a111dc0a9c677dd0	t	25/06/93	Bogotá D.C
417	Carolina prueba2	1033786932	Cll 00 #134	Cll 00 #123	3014090873	f	a	Rechazado	2017-10-06 22:01:08.948	37500	7125	44625	desarrollo@bgt404.com	\N	6000	6c41b1a0088950294aad6a118545a32b	f	08 abril	Bogotá D.C
431	Karol silva	1020840815	Calle150#50-47	Calle150#50-47	3142804852	t	a	Rechazado	2017-10-14 02:03:51.477	32000	6080	38080	karolsilva199903@gmail.com	@k.silva. 1	6000	39d63243bdeddced7c791d42b238e2c4	f	03-05-1999	Bogotá D.C
438	Paula Echeverry	29178796	cr 37 # 1 oeste - 46 Apto 102 C	Cr 37 # 1 oeste - 46 Apto 102 C Torres de Camlelot	3002664790	t	a	Entregado	2017-10-18 21:15:28.195	45000	8550	53550	paulaecheverryf@gmail.com	paulaeche	10000	28a294ddcd4c152b95a85a82a6826ed0	f	Feb 2	Cali
445	Luisa Fernanda Montaño Miranda	1125292464	Calle 152 #54-39	Calle 152 #54-39	3118354274	f	a	Entregado	2017-10-24 02:47:57.095	75500	14345	89845	lumontano_82@hotmail.com	\N	11000	a64fcf32768b7d9ef24588fa48f1f855	t	14/06/1992	Bogotá D.C
452	Alejandro Montaño	1019118796	Calle 152 # 54 - 39 Apto 101	Calle 152 # 54 - 39 Apto 101	3144727118	f	a	Entregado	2017-10-26 04:15:32.451	53000	10070	63070	luismontano_96@hotmail.com	\N	13000	d8d273cc40a7d12d60f19a2fd80a654d	t	Abril 2	Bogotá D.C
463	Angie Ruiz	1013637807	Calle 66 # 72a-92	Calle 66 # 72a-92	3192863712	f	a	Entregado	2017-11-06 02:16:33.341	70500	13395	83895	aliruizbermudez@hotmail.com	\N	6000	3b1ba683eef325560124dd69ccbb8372	f	22-diciembre-1992	Bogotá D.C
195	bgt404	1032452918	CALLE 135C 13-22 APTO 402	CALLE 135C 13-22 APTO 402	3184254654	t	a	Entregado	2017-06-28 21:18:33.946	20000	3800	23800	diseno@bgt404.com	RICRAM2	8000	f6ba1d73df11b4b9f730070e44470def	f	\N	\N
266	Camila Marin	1032502909	Calle 39 #15-13 apto 101	Calle 39 #15-13 apto 101	3143803596	f	a	Entregado	2017-07-12 20:53:25.934	62500	11875	74375	camimarin99@hotmail.com	\N	16000	909bbed80e9414cbbb1fff4c3fe251e4	t	-01-29T05:0	Bogotá D.C
411	andres sosa	1101757761	calle 25 g # 85c-05 of 203	calle 25 g # 85c-05 of 203	3204149683	f	a	Rechazado	2017-10-02 18:45:43.066	32000	6080	38080	andressosa9305@hotmail.com	\N	6000	5b4002438008f82a3ad95211a0c3fdc2	f	13-05-1993	Bogotá D.C
320	Juliana Neira	1072714778	Carrea 17 # 122 - 20	Carrera 17 # 122 - 20	3208239857	f	a	Entregado	2017-08-10 22:30:07.376	57500	10925	68425	juliananeira97@gmail.com	\N	11000	23c6108eba17eebc0b5a29729b683346	t	27 de Julio	Bogotá D.C
376	Diana Carolina Rengifo	1061719557	Calle 152 #53A-20 Apartamento 1-101. Milenio A, Mazurén	Calle 152 #53A-20 Apartamento 1-101. Milenio A, Mazurén	3043456404	f	a	Entregado	2017-09-15 03:34:47.182	79000	15010	94010	dianacarolinarengifo013@gmail.com	\N	15000	dc75677e47feb05fe54e31d1a340566f	t	_	Bogotá D.C
281	Mariana Lobo Guerrero	1020799004	Carrera 4 # 79-25	Carrera 4 # 79-25. apt. 502	3208596643	f	a	Entregado	2017-07-21 19:36:19.666	64000	12160	76160	mariana.lobo.guerrero@gmail.com	\N	10000	5e51a343747f22c90499d438598ac36a	f	_	Bogotá D.C
327	Manuela Barrera	52996727	Calle 80 No 8-84 Interior 1	Calle 80 No 8-84 Interior 1	3175752379	f	a	Entregado	2017-08-15 22:11:37.15	23000	4370	27370	manuela.barrera@gmail.com	\N	11000	084ce55cf787a1b9511af2b5457d7cc7	t	17/06/84	Bogotá D.C
302	Amparo Guzman	41758096	calle 114A # 45-65 Int 9/Apto 203	calle 114A # 45-65 Int 9/Apto 203	3138706231	t	a	Pendiente	2017-07-31 21:36:48.295	52500	9975	62475	jp1910@gimnasiovermont.edu.co	juanitapaezg	6000	cf08bf2efb352c3e45b164a54a6a6288	f	28 de abril	Bogotá D.C
295	Johanna aguirre	1112477358	Calle 14 no 11-25 B/Simón Bolívar	Calle 14 no 11-25 B/Simón Bolívar	3205639254	t	a	Entregado	2017-07-29 16:35:33.755	40500	7695	48195	aguirre174@hotmail.com	Johannaaguirre	9000	b34459c0ee9b4fac15cf1aa09925cfdc	f	14 de noviembre	Jamundí
309	Ana Reyes	1010245120	Crr77n#65i20sur	Carrera 22 #4-21	3194920788	f	a	Pendiente	2017-08-04 06:11:53.766	20000	3800	23800	mfe37@hotmail.com	\N	8000	934579297aafcdc7c4fd810112dce141	f	01/abril/1999	Bogotá D.C
383	Vanessa Sánchez	1022405942	Cra 77 t #55-40 sur apto 102	Cra 77 t #55-40 sur apto 102	3012772713	f	a	Entregado	2017-09-18 02:03:19.713	53000	10070	63070	dcomercializadora900@gmail.com	\N	8000	25b15fa7825c7ece219c09f4ac8c6e90	f	27 de julio 2017	Bogotá D.C
334	juan sebastian sánchez	1136885247	Cra 68g # 9c-97	Cra 68g # 9c-97 torre 2 apto 1001	3014106556	f	a	Entregado	2017-08-22 20:00:39.697	43000	8170	51170	yaseloescribo@gmail.com	\N	8000	982ed484004688f5485e6d0a3b6a7e7b	f		Bogotá D.C
341	Sara Bermudez	1047492126	carrera 11 #93a-24	carrera 11 #93a-24	3205660010	t	a	Entregado	2017-08-26 19:54:33.487	86000	16340	102340	sarabermudez01@gmail.com	@daniela.barragandt	11000	a5dd36979d57d8fd6fe53d97da6a08af	t	30 Octubre 1996	Bogotá D.C
390	diana catalina cortes cala	1018409174	carrera 14a #9sur -03 casa 85 - El Roble - Hacienda Alcala	carrera 14a #9sur -03 casa 85 - El Roble - Hacienda Alcala	3204609565	f	a	Entregado	2017-09-21 01:35:57.347	73500	13965	87465	catalina_568@hotmail.com	\N	9000	e958b54e49ea91a391e0d4bc24fd3491	f	6 de marzo	Mosquera
363	prueba hans Ignora	523452432	cll 4252345	cll245234	2545235	f	a	Pendiente	2017-09-11 15:24:33.855	45000	8550	53550	hsajhd@gmail.com	\N	10000	acae58116c6e82962924909632798cce	f	23/07/1932	Leticia
369	Sofia Ramos	1018511337	Carrera 13a #102-76 Apto 301	Carrera 13a #102-76 Apto 301	3132950187	f	a	Pendiente	2017-09-13 00:10:53.636	61500	11685	73185	sofiaramosalgarra@gmail.com	\N	6000	9af61e4dea670dc5a12b617dbb71c0dd	f	8 de Mayo 1999	Bogotá D.C
348	Leidy Campos	1144101573	Cra 41 A #14B - 47 B/ Guabal. 2do piso	Cra 41 A #14B - 47 B/ Guabal. 2do piso	3214892915	t	a	Rechazado	2017-09-06 19:14:35.295	67500	12825	80325	Danielacaa06@hotmail.com	danielac_8	10000	642034da90cd891769eda14246acc8e4	f	6 de febrero	Cali
404	Olga Marín	24040480	Carrera 68 d n 99 57	Carrera 68 d n 99 57	3013202071	f	a	Pendiente	2017-09-26 00:25:15.029	20000	3800	23800	olgaceci25@hotmail.com	\N	8000	1d41a46c2662e783bde2eebc52cb3747	f	24 de febrero	Bogotá D.C
397	susana martinez	1125640918	CALLE 80 NUMERO 14-45 PISO 2	CALLE 80 NUMERO 14-45 PISO 2	3213439772	t	a	Entregado	2017-09-25 01:38:49.773	56000	10640	66640	susym2@hotmail.com	susanamartinezm	6000	c652139a4a7bd09bf109dea0051a0a23	f	2 de marzo de 1992	Bogotá D.C
418	Lina velez	1020791908	Calle 139#72-28	Calle 139 #72-28 casa 53	3165357711	f	a	Pendiente	2017-10-08 22:24:56.121	37500	7125	44625	lina.velez125@gmail.com	\N	6000	375c42ffafa0b989c031a2abfe7945b3	f	20 de abril	Bogotá D.C
432	Lina Maria Arbelaez	1115079996	Calle 28 # 86-80 apto 744 torre 11 Nogales del Lili	Carrera 24 # 68-37 B/ San Felipe (Barranquilla)	3137814086	t	a	Entregado	2017-10-14 13:29:06.911	45000	8550	53550	lina.arbelaez18@hotmail.com	@linis18	10000	8e54361f9c26fd82038a7cd7e5b64e69	f	Marzo/18/1993	Cali
425	Maria Alejandra Olivares	1102851048	Carrera 8a # 99-22 apto 803.	Carrera 8a # 99-22 apto 803.	3134328384	f	a	Rechazado	2017-10-12 01:23:03.85	97000	18430	115430	marialeolivares3@gmail.com	\N	12000	63873fba449934e9d2ff258b302cdd6c	f	3 de noviembre	Bogotá D.C
439	Nadine Wong	1045707209	Cra 12 No 90 - 42, Apt 304	Cra 12 No 90 - 42, Apt 304	3145066199	t	a	Entregado	2017-10-19 18:42:32.506	34000	6460	40460	naniwong1@gmail.com	\N	8000	e9180c8cc930e21c2e13815c350975de	f	Julio 12 de 1992	Bogotá D.C
453	Paola Afanador	1026572000	Carrera 26 # 45A -66	Carrera 26 # 45A -66	3114460654	t	a	Entregado	2017-10-26 13:11:27.892	51000	9690	60690	pafanadorm@gmail.com	pafanador	13000	64ffe5cc7986d039aee4a4e17cb19824	t	26 de mayo de 1992	Bogotá D.C
446	Melva Juliana López	1020777124	Calle.106 #19a-42 Apto.505	Calle.106 #19a-42 Apto.505	3003239631	t	a	Entregado	2017-10-24 12:33:07.2	20000	3800	23800	melvajlopez@gmail.com	@melvalopez	8000	76fa99a3253d7db5aec61ebe9c2918af	f	_	Bogotá D.C
469	Ana María Toro	1015439447	Cll 66# 59-31 apto 706 torre 2	Cll 66#59-31 apto 706 torre 2	3212235234	t	a	Rechazado	2017-11-10 04:11:33.449	32000	6080	38080	ani93to04@gmail.com		6000	095450489b60cae8d6fddc586ff8762e	f	4 julio	Bogotá D.C
460	Paula Narvaez	1144188988	Calle 9c # 53-40	Calle 9c # 53-40 casa 13	3176649419	t	a	Rechazado	2017-11-02 14:11:58.225	22000	4180	26180	paulanarvaez_5@hotmail.com	@paulanarvaez_	10000	01cfd04ad17ab4452a07893fe2f81846	f	5 septiembre	Cali
464	Fernanda Acero	1057594211	Calle 63 # 74 a -  81 - Torre 6 Apto 703	Calle 63 # 74 a - 81 - Torre 6 Apto 703	3105505372	t	a	Entregado	2017-11-07 14:16:13.139	65500	12445	77945	laurafernandaacerovacca@gmail.com	lafertango	8000	964a2e979eabd4b2b0312bc7956b461a	f	20 de Enero	Bogotá D.C
474	Ana María Murillo Corredor	1020823799	cra 19#131a-60 apto 604	cra 19#131a-60 apto 604	3142937421	t	a	Entregado	2017-11-14 02:54:49.635	43000	8170	51170	animurillo.c@gmail.com	ammurilloc	8000	cfc73eae04a263e61fd9bb1eded87957	f	24/01/97	Bogotá D.C
321	Maria Alejandra Matallana Torres	1020798846	Calle 174A #47-11	Calle 174A #45-91	3103060178	t	a	Pendiente	2017-08-11 02:04:30.37	43000	8170	51170	ale.matallan.torres@gmail.com	@maleja.matallana	8000	80dfc6a64aeac517071841cd94e7a453	f	03/11/1994	Bogotá D.C
274	CAROLINA  ARRIETA	1047393422	transversal 52 no. 21c-59	calle 144 no. 13-71 la entrega es para natalia espitia y solicito poner el siguiente mensaje; Recordandote siempre que soy muy feliz a tu lado...Miss you!!!	3012426235	t	a	Entregado	2017-07-17 20:40:01.37	20000	3800	23800	carotoya@hotmail.com	carotoya87	8000	17842fb8a5fe95d382add3eb5463de16	f	Dec 15 1987	Bogotá D.C
377	Valentina Rodríguez	99120501492	Carrera 44 numero 17 sur 59	Carrera 44 numero 17 sur 59	3113723752	t	a	Entregado	2017-09-15 03:45:20.83	55000	10450	65450	valentinarodriguezyepes@hotmail.com	@valentinarodriguezy	10000	a99b61eb6dd3ab95ba3ec63feb323c08	f	05 de diciembre	Medellín
384	Stephania Montes Peñaranda	1140831927	Calle 64#1-15	Calle 70A#4-41. Barrio Rosales.  Edificio de oficinas Brigard & Urrutia.	3004930087	f	a	Entregado	2017-09-18 02:20:43.479	74000	14060	88060	stephaniamontes@gmail.com	\N	10000	087cc96cdba9a7a9cb96ccfe4dfdbcf7	f	15 de junio	Bogotá D.C
282	Natalia Payan	1151945331	Carrera 32 # 7-32 arroyohondo	Avenida 2b norte # 34 a 25 / edificio: Maria aclara / Apto: 304	3234639108	f	a	Entregado	2017-07-24 02:17:15.696	22000	4180	26180	naty1992_36@hotmail.com	\N	10000	f9010880fbdcb39888c0a41cf454266c	f	Mar 05 1992	Cali
289	Kelly Margarita Colmenares Vargas	63561205	Real de Minas Calle 61 # 3-91 Samanes4 Agrup2 T3 Apt202	Real de Minas Calle 61 # 3-91 Samanes4 Agrup2 T3 Apt202	3003441521	f	a	Entregado	2017-07-25 15:53:59.94	40000	7600	47600	kellymac701@yahoo.com	\N	9000	6d7a5b1770c381b1a83b302950ddf06f	f	_	Bucaramanga
328	Valeria gallo	1000697374	Carrera 17A#119A-68	Carrera 17A#119A-68	3108065953	t	a	Entregado	2017-08-16 18:29:34.59	32000	6080	38080	valelalinda123@hotmail.com	@valegash	8000	c3ad7500d748a5225fe64b54170ca09b	f	9 octubre	Bogotá D.C
310	Laura Maria Bautista Narvaez	1015479190	Cra 61 #95-02 bloque 2 apto 203	Cra 61 #95-02 bloque 2, apto 203	3185540964	t	a	Pendiente	2017-08-04 17:15:12.751	48000	9120	57120	bautilaura@hotmail.com	@lbautistan	8000	7a7d0b34e1547327061c1161f03586cc	f	26/01/1999	Bogotá D.C
342	Camila Fernanda Saavedra Salinas	1019076136	Calle 97#70c 89	Calle 97#70c 89 portal de pontrvedra II torre 6 apto 1001	3138690705	f	a	Pendiente	2017-08-29 14:04:53.144	32000	6080	38080	camilaf.15@hotmail.es	\N	6000	4867228240742c44af37ee57f6c6d345	f	15 de noviembre	Bogotá D.C
391	Alba Gaitan	1032363251	Cra 6 # 9-25 Cajicá	Cra. 6 # 9-25 Casa El Misterio	3138001368	f	a	Entregado	2017-09-21 13:32:16.42	55000	10450	65450	albagaitan@hotmail.com	\N	10000	667e39e7c58c9d0d7865a5fb0bc14ed0	f	27/05	Cajicá
349	Maria Fernanda Erasso	1020718337	Cr 9 bis # 96 -61 edificio avignon apto 501	Cr 9 bis # 96 -61 edificio avignon apto 501	3108678636	f	a	Entregado	2017-09-07 14:42:51.57	36000	6840	42840	mf_joke@hotmail.com	\N	6000	fb6ce990322f28df2cafa16d9af59a08	f	0108	Bogotá D.C
370	Estefania Tamayo Santa	1152195359	calle 49 # 87-12 apto 301	calle 49 # 87-12 apto 301	3016775378	t	a	Pendiente	2017-09-13 04:17:21.699	45000	8550	53550	estefatamayo19@hotmail.com	@tefitamayo	10000	0e222ad4c1489f6c11d5b249d5a54872	f	19/03/92	Medellín
357	Manuela Betancur	1035833323	Calle 17 # 27A-109 edificio Yerbabuena apartamento 2312	Calle 17 # 27A-109 edificio Yerbabuena apartamento 2312	3104988051	f	a	Entregado	2017-09-10 18:18:34.654	50000	9500	59500	manubeta09@gmail.com	\N	10000	9c939663348381f7f4479f813dfc9277	f	21 de septiembre	Medellín
364	jazmin Castaño	1017231953	calle 9a #73 95 apartamento 1014	calle 9a #73 95 apto 1014	3104672137	t	a	Entregado	2017-09-11 18:11:40.946	35000	6650	41650	jazmin-3195@hotmail.com	jazmincast1	9000	f2748ac9f0dda898fc55ba4cea22986e	f	30 de mayo	Medellín
426	Daniela Carmona Arango	1088028974	San Gregorio vía kennedy-pimpollo Mz a Cs 10	San Gregorio vía kennedy-pimpollo Mz a Cs 10	3145275325	t	a	Entregado	2017-10-12 16:37:07.913	55500	10545	66045	danicarmona1197@gmail.com	@danielaca11	9000	78fac23a5a63549f07db91df0d0d046d	f	01/01/1997	Pereira
398	Johana García	1015449658	Cra 23 N 26-69	Cra 23 N 26-69	3166204455	f	a	Pendiente	2017-09-25 13:58:42.266	39000	7410	46410	Ladyjohana-1694@hotmail.com	\N	13000	e19c2cdab353b10b5ea4cd75344fe369	t	16/08	Bogotá D.C
405	Tatiana Acosta Morales	1016042379	Carrera 19 #127b-31	Carrera 7 #24-89	3163200737	f	a	Entregado	2017-09-26 00:31:18.713	53000	10070	63070	tacostamorales@gmail.com	\N	8000	59f7d2d101f82968bdaa03abb01d6b8e	f	9 febrero	Bogotá D.C
454	Maria Camila Arbeláez	1151942338	Entregar en the Cakery co	Con galletas	3114525845	f	a	Pendiente	2017-10-26 18:40:59.352	56000	10640	66640	maria.cami26@hotmail.con	\N	8000	ffcb7766d8663f8db72194a925514eb4	f	Septiembre 30	Bogotá D.C
412	Amalia Arango	1020744675	Calle 99 # 11a-32	Calle 99 # 11a-32	3153601676	t	a	Entregado	2017-10-03 02:12:11.546	52500	9975	62475	amaliaarangoc@gmail.com	amaliaarangoc	6000	ec727dc200944a44a543d6ab1008c636	f	12 de julio	Bogotá D.C
419	Alexandra Aristizábal	1113652129	Carrera 11 #140 - 87 Santa cruz de sotavento Apto 604 torre 1	Carrera 11 #140 - 87 Santa cruz de sotavento Apto 604 torre 1	3003915080	f	a	Pendiente	2017-10-09 03:09:00.681	74000	14060	88060	a.aristizabal1@hotmail.com	\N	10000	50b7af1f83a62b442e91c976e907cf81	f	20 junio 1991	Bogotá D.C
433	Alexandra Bernal	52862308	Calle 126 #7-64 Apto 707	Calle 126 #7-64 Apto 707	3156433725	f	a	Entregado	2017-10-16 03:02:07.046	112000	21280	133280	alexandra.bernal@gmail.com	\N	11000	e4496b699a7081c2591ebf3ce5bd6c07	t	30 de Mayo	Bogotá D.C
440	Mariá Camila Behar	1020768177	Calle 137a # 73-27 casa 16	Calle 137A # 73-27 casa 16	3016271057	t	a	Pendiente	2017-10-19 19:54:28.93	48000	9120	57120	camila.behar.m@gmail.com	Camila.behar	8000	d29297c345020982dbfaf4b71c921fdb	f	08/01/1992	Bogotá D.C
455	Maria Camila Arbeláez	1151942338	Entregar en the Cakery co	Con galletas	3114525845	f	a	Pendiente	2017-10-26 18:41:20.842	61000	11590	72590	maria.cami26@hotmail.con	\N	13000	79bfa1b0a89126afc8f5c6698b0116e3	t	Septiembre 30	Bogotá D.C
447	Natalia Jerez	1020750408	Carrera 47a # 113-30	Carrera 47a # 113-30  Torre 2, apto 503	3123791766	f	a	Entregado	2017-10-24 19:52:07.837	32000	6080	38080	natijq90@gmail.com	\N	6000	abaea75c972561fbd8707dd990b220c5	f	9 de marzo	Bogotá D.C
461	Ana María Toro	1015439447	Cll 66 # 59 - 31 apto 706 torre 2	Cll 66 # 59 - 31 apto 706 torre 2	3212235234	t	a	Entregado	2017-11-02 17:05:42.653	65000	12350	77350	ani93to04@gmail.com	Eurialey93	6000	d24a28c2e9c3d985390fdba4475a099b	f	04 jul 1993	Bogotá D.C
465	NATALY PATACON	1020763313	CRA 16A # 85-37	CRA 16A # 85-37 APTO 607	3015505367	f	a	Entregado	2017-11-07 20:52:23.182	97000	18430	115430	natalypatacon@gmail.com	\N	12000	a1287df9f2b863bb693c1b079a0c090e	f	27/06/1991	Bogotá D.C
470	maria jimena herrera	1020767141	av cra 20 # 86-56	av cra 20 # 86-56	3115504420	f	a	Entregado	2017-11-10 18:34:02.286	92000	17480	109480	hmariajimena@yahoo.com	\N	13000	7d2116d2189774ff6903e718c83e4e3e	t	diciembre 1	Bogotá D.C
475	Karenth Bojaca	1072701032	Cra 8 no 16 a 58	Cra 8 No 16 A 58	3214306623	t	a	Entregado	2017-11-14 13:55:35.654	34000	6460	40460	karenthb27@gmail.com	@karenth.natalia	9000	6b2c05efd7fd3acbff0e03afeff2879d	f	27-09-1993	Chía
260	Juliana Rivera	1020781295	Diagonal 76 # 1-29 apt 801	Diagonal 76 # 1-29 apt 801	3102818893	f	a	Entregado	2017-07-07 04:03:42.241	161000	30590	191590	julirivera29@gmail.com	\N	6000	b1a099e21e5bab7556702032791b0825	f	Dec 18 1991	\N
259	Raul Lopez	1125641132	Calle 70 no 5 45	Calle 70 no 5 45	3186916772	f	a	Entregado	2017-07-07 02:54:06.579	63000	11970	74970	rlopezf@msn.com	\N	18000	a85f12baa5c1e12cade022e6352fc471	t	_	\N
275	Maura Braidy	1121894024	Carrera 7d # 127-93 apto 301	Carrera 7d # 127-93 apto 301	3102409665	t	a	Entregado	2017-07-17 20:59:27.512	74000	14060	88060	mauraa_braidyr@hotmail.com	\N	10000	35506b628c9b01178a274ecca8fce50b	f	Apr 30 1992	Bogotá D.C
304	julian garcia	1144106415	calle 15 #53-56 apto 301D	calle 15 #53-56 apto 301D	3175792354	t	a	Entregado	2017-08-02 18:23:08.289	34000	6460	40460	jags19-99@hotmail.com	@saenzjulian_	9000	52154fc3bc6ffb46e3bfcda027af3fb6	f	24/02	Cali
290	Camila Villamizar	1098810936	Calle 147. #22-64 casa 3	Calle 147 #22-64 casa 3	3177958846	t	a	Pendiente	2017-07-25 19:10:44.261	68000	12920	80920	camila980909@gmail.com	Camilavillamizar9	20000	44ace26d8985efc631304fc14907efe1	t	Sep 09 1998	Floridablanca
283	luisa gomez	1020834563	calle 142 #13-69	calle 142 #13-69 apto 204	3006398217	t	a	Rechazado	2017-07-24 02:28:38.185	34000	6460	40460	luisaf2998@hotmail.com	@luigomez15	8000	21afd3d09f291a4addd8a1c467a111cd	f	_	Bogotá D.C
297	stephanie gordillo ballesteros	1018460165	calle 25 # 68 a 70 condominio ponto verdi apto 307 (barrio salitre)	calle 25 # 68 a 70 condominio ponto verdi apto 307 (barrio salitre)	3143308860	f	a	Entregado	2017-07-30 21:54:20.757	48000	9120	57120	stephaniegordilloarq@gmail.com	\N	8000	1f50fbd1bb85855012839bbd88472e3f	f	27/02/1993	Bogotá D.C
378	Carolina Calderón	1020821909	Cra 56a # 148-35	Cra 56a # 148-35. Torre 2 apt 601	3134674176	t	a	Pendiente	2017-09-15 15:47:57.06	48000	9120	57120	calderonmaria@javeriana.edu.co	cr.carolina	8000	9492870a7d8c726416e9417cd6dc9cac	f	7 de enero de 1997	Bogotá D.C
311	Laura Maria Bautista Narvaez	1015479190	Cra 61 # 95-02 bloque 2 apto 203	Cra 61 # 95-02 bloque 2 apto 203	3185540964	t	a	Rechazado	2017-08-04 17:25:49.987	48000	9120	57120	bautilaura@hotmail.com	@lbautistan	13000	8d2b081b4f758ab59060320f94669d88	t	26/01/1999	Bogotá D.C
322	Eliana trujillo	1077864859	Calle 24a # 32 - 42	Calle 24a # 32- 42	3118655052	f	a	Entregado	2017-08-12 01:40:37.391	61000	11590	72590	eliana92trujillo@gmail.com	\N	9000	0355be7b64d19c283ff05f318943dc09	f	29 noviembre	Neiva
399	Johana García	1015449658	Cra 23 N 26-69	Cra 23 N 26-69	3166204455	f	a	Rechazado	2017-09-25 14:26:00.066	39000	7410	46410	ladyjohana-1694@hotmail.com	\N	13000	fa61f55a8f8a44574c1d8f69706e5154	t	16/08	Bogotá D.C
336	Jeisson Bastidas	1047360644	Calle 74 #38 46 Edif Verdi 74 apto 301 A	Calle 74 #38 46 Edif Verdi 74 apto 301 A	3008195058	t	a	Entregado	2017-08-23 15:02:31.243	50000	9500	59500	eritorolopez@gmail.com	@eritorolo	10000	98d1b4c9963b76eb3960c2d71c3b2db8	f	_	Barranquilla
385	Katherin ramirez sierra	1072706998	Carrrera 15 # 18-48 conjunto manzanillo casa-06	Carrera 15 # 18-48 conjunto manzanillo casa-06	3203192199	f	a	Entregado	2017-09-19 16:01:37.49	55000	10450	65450	katyr9516@gmail.com	\N	20000	481d78ffa01cf168a06d9ba2ff35f879	t	16 agosto	Chía
343	Angelica Vivas Sanchez	1012418340	Calle 6a #83 a-51	Calle 6a #83 a-51 Int 6 apto 203	3132503426	t	a	Entregado	2017-08-29 22:35:35.817	48000	9120	57120	angelicavivassanchez@gmail.com		8000	ef324dea778558cee70e84df56d40bf9	f	Enero 18	Bogotá D.C
420	Alexandra Aristizábal	1113652129	Carrera 11 #140 - 87 Santa cruz de sotavento Apto 604 torre 1	Carrera 11 #140 - 87 Santa cruz de sotavento Apto 604 torre 1	3003915080	f	a	Entregado	2017-10-09 03:13:01.846	74000	14060	88060	a.aristizabal1@hotmail.com	\N	10000	d7db59bd661cc7820269dd22a962f8a3	f	Junio 20 1991	Bogotá D.C
350	Juana Rueda	1015473902	Cr 58 N 127-20 apto 517	Cr 58 N 127-20 apto 517	3166907529	f	a	Rechazado	2017-09-09 21:06:35.17	81000	15390	96390	juanis_rueda@hotmail.com	\N	6000	d9a258d40d6d805c68d4fcd7094b7b39	f	24 febrero 1998	Bogotá D.C
371	Brenda medina	1018449218	Cra 118 no 89b 35 conjunto eucaliptos bloque 10 apto 101	Cra 118 no 89b 35 conjunto eucaliptos bloque 10 apto 101	3115818953	t	a	Entregado	2017-09-13 14:48:35.595	37500	7125	44625	bmedina24@hotmail.com	Brendaj2	6000	16ba863e1f80492b5fb7777781443a5f	f	24 de diciembre	Bogotá D.C
406	Juliana torres	1005342078	Carrera 68 d # 99 57	Carrera 68 d # 99 57	3057069828	f	a	Pendiente	2017-09-26 01:17:37.91	20000	3800	23800	jutoma03@gmail.com	\N	8000	8cc8a2110d4aaa99485ca7efa36a6f4b	f	03 febrero	Bogotá D.C
392	Andrea González	1032503652	Av. Calle 161 #19A-39	Av. Calle 161 #19A-39 Interior 1 Apto 604	3132415952	t	a	Entregado	2017-09-22 00:04:33.68	59000	11210	70210	paulaandreagonzalezroa@gmail.com	@pagr0327	6000	6647c45e3c1147819f95d967ea816c41	f	27 de Marzo	Bogotá D.C
427	Marcela Gómez	1018493164	Carrera 24 # 63c-69	Carrera 24 # 63c-69	3108565116	f	a	Pendiente	2017-10-12 19:11:48.718	37500	7125	44625	marcela.1997.mg@gmail.com	\N	6000	b5f3edaa3c6bbd013e6ef92c3d2bdee6	f	16/03/97	Bogotá D.C
413	bernardo mancini	1136880743	Calle 69 # 4-32, Apto 305	Calle 93 # 19-58	3135741162	f	a	Entregado	2017-10-03 17:07:15.888	53000	10070	63070	bernardomancini@gmail.com	\N	8000	db14a07af823904c34e914c05e194ef2	f	Junio 20 , 1988	Bogotá D.C
434	Monica Almonacid	52690003	cra. 23 no. 137 - 16 apto 506	cra. 23 no. 137 - 16 apto 506	3168758392	f	a	Entregado	2017-10-16 18:52:38.598	31000	5890	36890	monica_almonacid@hotmail.com	\N	6000	65383d71a258d3b6a7721388294c9b97	f	14 Agosto	Bogotá D.C
448	SERGIO FERNANDEZ	14795296	CARRERA 24 # 22 - 24	CARRERA 24 # 22 - 24	3159287925	f	a	Entregado	2017-10-24 22:31:48.68	35000	6650	41650	TALON520@HOTMAIL.COM	\N	9000	dadcc3130a299d840fe24886dcf57df1	f	27 NOV	Tuluá
441	nathalia bermudez	1015477908	carrera 68b #76 a 41	carrera 68 b# 76 a 41	3144285018	t	a	Entregado	2017-10-19 22:40:30.5	32000	6080	38080	bermudeznathalia@hotmail.com	@nathalia.bermudez	6000	47de160e674cf00cd65d8bc2838bcc3f	f	10/11/1998	Bogotá D.C
456	Maria camila arbelaez	1151942338	Entregar en the cakery co	Con galletas	3114525845	f	a	Entregado	2017-10-26 19:05:27.261	56000	10640	66640	maria.cami26@hotmail.com	\N	8000	9027b4d839f292c7b39a67eb69b7660b	f	Septiembre 30	Bogotá D.C
459	Carolina Acero	52786322	Calle 81 Bis #80-04 Apto 201 Bloque i Conjunto Residencial Afidro La Palestina	Calle 81 Bis #80-04 Apto 201 Bloque i Conjunto Residencial Afidro La Palestina	3143514697	t	a	Entregado	2017-11-01 03:47:58.665	36000	6840	42840	valenacero448@hotmail.com	@_acvv_01	11000	6730004f02eff05928c0d935ce6e3a13	t	29/11/1979	Bogotá D.C
466	Orlando Zuniga	1136879877	Carrera 1 # 22 - 79 apto 1501	Carrera 1 # 22 - 79 apto 1501	3205650263	f	a	Entregado	2017-11-07 21:46:13.852	55000	10450	65450	orlando@prozgroup.co	\N	10000	c41f18a3d0a41314631d931f09c09909	f	13 de febrero 1988	Santa Marta
471	Fredy beltran	1023898159	Cl 43c sur # 3d 16 este	Cl 43c sur # 3d 16 este	3154792671	f	a	Entregado	2017-11-13 15:22:29.839	48000	9120	57120	fredyabeltrans@gmail.com	\N	8000	acb7e142493fac496ac263d9d571babe	f	17 julio	Bogotá D.C
476	Laura Villegas	1014201180	Calle 146a # 58B - 22	Calle 146a # 58b-22	3103434482	t	a	Entregado	2017-11-14 18:22:11.433	52500	9975	62475	lauratville@gmail.com	lauratville	6000	d8e24808858476f4f6446cf5f3871a69	f	14 junio	Bogotá D.C
472	Mariana Aristizabal	1216713612	Carrera 16 # 134A-12 edificio Bell apartamento 404	Carrera 16 # 134A-12 edificio Bell apartamento 404	3212482393	f	a	Rechazado	2017-11-14 00:54:03.764	37500	7125	44625	marianaristi-23@hotmail.com	\N	6000	368f2543ba2f1c39d138754e86d77ae2	f	_	Bogotá D.C
478	Pacheco	1023304272	Calle 121 #3a- 20 apt 318c	Calle 121 #3a- 20 apt 318c	3107993504	f	a	Entregado	2017-11-15 13:51:33.483	23000	4370	27370	Ana20nov@gmail.com	@acpacheco94	11000	1ce743641a5c46057173d90dd57895d9	t	20/11/94	Bogotá D.C
479	stephanie gordillo	1018460165	calle 25 # 68 a 70 condominio ponto verdi apto 307	calle 25 # 68 a 70 condominio ponto verdi apto 307	3143308860	f	a	Entregado	2017-11-15 22:05:19.633	42500	8075	50575	stephaniegordilloarq@gmail.com	\N	6000	2412b4ef8166d3416886306153c7cf3a	f	27/02/1993	Bogotá D.C
481	Luisa Castellanos	1020798110	Carrera 13 # 74-71	Carrera 13 # 74-71	3115223406	f	a	Entregado	2017-11-21 01:46:52.59	37000	7030	44030	luisacastellanos94@gmail.com	\N	11000	3a7d32471c718e71c77a42f68a5ba706	t	21 Octubre 1994	Bogotá D.C
493	David	12598989	car dididhjhm hdssd	ssdsdsd	3217506374	f	a	Pendiente	2017-11-27 19:43:11.753	41000	7790	48790	dijijd@gmail.com	\N	13000	d7b6501668bdca450798c5aaf2eae50e	t	28-01-1980	Bogotá D.C
497	Luisa López	1030553517	Carrera 4 No. 16 - 15, apto 1102	Carrera 4 No. 16 - 15, apartamento 1102	3105541829	t	a	Entregado	2017-11-28 17:58:09.695	55600	10564	66164	luisalopezmunoz@gmail.com	@luisalopezmunoz	6000	0394d738c36b865185e5b64afab2f69d	f	16 de enero	Bogotá D.C
482	Juliana Rivera	1020781295	diagonal 76 # 1-29	diagonal 76 # 1-29	3102818893	t	a	Rechazado	2017-11-22 23:25:54.79	70400	13376	83776	julirivera29@gmail.com	juliriverav	6000	9050f23e0a56e3b4362a9967e5ca339f	f	18/12/1991	Bogotá D.C
483	juliana rivera	1020781295	diagonal 76 # 1-29	diagonal 76 # 1-29	3102818893	t	a	Pendiente	2017-11-22 23:46:00.363	70400	13376	83776	julirivera29@gmail.com	\N	6000	ea6a30aa377ec128f0946ee9a83ec27c	f	18/12/1991	Bogotá D.C
498	Laura Romero	1015431970	Calle 127b bis # 52a - 68	Carrera 19 # 109A - 30	3134731830	f	a	Entregado	2017-11-29 00:11:51.409	33800	6422	40222	lauramaria_92@hotmail.com	\N	13000	0277b9ecfae73a38645e76bb780fe5c9	t	23 de junio del 1992	Bogotá D.C
494	Sylvana Flórez Aguas	1098810135	Carrera 28 # 60 26	Carrera 28 # 60 26 torre 4 803	3166214613	f	a	Rechazado	2017-11-28 04:27:09.566	19600	3724	23324	sylvanaflorez.21@outlook.com	\N	10000	b3b8643916d77f1e0d1d6ce0694979f5	f	21 de agosto	Bucaramanga
484	Maria Luisa Ruea	1018499070	calle 76#8-56 apt 1101	calle 76 #8-56 apt 1101	3138179054	f	a	Entregado	2017-11-23 12:44:44.003	17600	3344	20944	malurueda14@hotmail.com	\N	8000	684e53af1c41521a6141d252ae7b81ef	f	20 de noviembre	Bogotá D.C
485	juliana rivera	1020781295	diagonal 76 # 1-29	diagonal 76 # 1-29	3102818893	t	a	Entregado	2017-11-23 12:47:18.476	70400	13376	83776	julirivera29@gmail.com	juliriverav	6000	69e38bc119b127c5e101abb296e07b2c	f	18/12/1991	Bogotá D.C
490	CATALINA LAVERDE	1136879371	Calle 83a # 21-08	Calle 83a # 21-08	3115069611	f	a	Pendiente	2017-11-24 03:59:20.369	31200	5928	37128	m.catalina.laverde@gmail.com	\N	6000	c90308361376454ccc7186cb3c3fcf2c	f	22/03	Bogotá D.C
489	Manuela Mesa Ortiz	1036688064	Calle 59 # 59 A 03	Calle 59 # 59 a 03	3174018698	t	a	Entregado	2017-11-24 02:36:29.828	38200	7258	45458	manuela991020@gmail.com	@manuelamesao	9000	757108d9f727c2bd6759afc549e77198	f	20 octubre	Itaguí
486	Lina María González agudelo	1116256484	Cra 4ta 9-54 barrio centro	Cra 4ta 9-54 barrio centro	3105510928	f	a	Entregado	2017-11-23 15:22:10.576	151000	28690	179690	linamaria0511@hotmail.com	\N	23000	72e9a1d3ce2fd5a765385aae0f54caf9	f	05 noviembre	Santa Rosa de Viterbo
487	CAROLINA  ARRIETA	1047393422	transversal 52 no. 21c-59	transversal 52 no. 21c-59 ap 403	3012426235	t	a	Entregado	2017-11-23 19:46:53.578	36200	6878	43078	carotoya@hotmail.com	carotoya87	9000	6450101226a4c2f125b39a4fe501f2af	f	15/12/1987	Cartagena de Indias
492	Lezlie Gómez	1026252558	Calle 19 b n 81 b 30	Calle 19 b n 81 b 30	3138853849	t	a	Pendiente	2017-11-27 04:26:42.507	40000	7600	47600	lez63@hotmail.com	\N	8000	d32ca01a5db57e4debf5a64a268e864a	f	8 de agosto	Bogotá D.C
488	Mariana Bayona	1020798735	Calle 70 # 6-22	Calle 70 # 6-22	3013407653	t	a	Entregado	2017-11-23 23:52:30.929	111200	21128	132328	bayonamariana94@gmail.com	@mariana_bayona1	8000	050ecd2d23e2eb02fbf4b67cdfaa0547	f	Noviembre, 3, 1994	Bogotá D.C
491	Maria Luisa Rueda	1018499070	calle 76#8-56	Calle 76 #8-56	3138179054	f	a	Entregado	2017-11-24 19:27:29.979	49000	9310	58310	malurueda14@hotmail.com	\N	13000	46ddea6c0592f2e2259735e5393549ee	t	20 de noviembre	Bogotá D.C
500	LORAINE RIVERA CORREA	1067866976	CALLE 25 # 10 -13 centro	CALLE 25 # 10 -13 centro	3012421077	f	a	Entregado	2017-11-29 13:14:22.109	34200	6498	40698	nanyrivera213@hotmail.com	\N	9000	89b307cca7559c39efa43d4303ab4dd1	f	13 dic	Montería
503	Ximena Garay Quevedo	1073238350	carrera 69b n 98a 31	carrera 69b 98a 31	3502127910	f	a	Entregado	2017-11-29 21:01:42.122	17600	3344	20944	ximenagaray6@gmail.com	\N	8000	a93b76a5f1c441f9190a2d375755dfdf	f	06/12/1991	Bogotá D.C
505	Monica Almonacid	52690003	cra 23 no. 137 - 16 apto 506	cra 23 no. 137 - 16 apto 506	3168758392	f	a	Aprobado	2017-12-02 17:48:22.884	49600	9424	59024	monica_almonacid@hotmail.com	\N	8000	ed577330f62f7679ebed2e4416b6308a	f	14 agosto	Bogotá D.C
501	Niccolle Fernandez	1020730586	Calle 141 # 7b-38	Calle 141 # 7b-38 apto 302	3208039380	t	a	Rechazado	2017-11-29 19:16:20.01	147200	27968	175168	nicky087@gmai.com	@nicky087	8000	82f72dd39eaa104c15f994550f20d35b	f	11-11-1987	Bogotá D.C
499	laura garcia	1053805777	transversal 5 No 75D 124 bloque 16 apto 301 unidad la mota	transversal 5 No 75D 124 bloque 16 apto 301 unidad la mota	3136715641	t	a	Entregado	2017-11-29 00:55:56.104	26800	5092	31892	lauragarciagib@gmail.com		10000	b8dc2b69defe51063a974ce667d72b11	f	12/10/1990	Medellín
506	Erika Torres	43618431	Calle 49D sur #40A-78 Apto.1501	Calle 49D sur #40A-78 Apto.1501	3217000039	f	a	Pendiente	2017-12-03 01:48:01.698	33000	6270	39270	erikatorreshoyos@yahoo.com	\N	9000	470add0b6f04139eef54a470aa9f85cc	f	24 enero	Envigado
495	Nicolas Izquierdo	1020757302	Calle 175 # 67 89	Calle 175 # 67 89	3138355011	t	a	Entregado	2017-11-28 16:08:17.096	110400	20976	131376	nizquierdo1115@hotmail.con	n_izquierdo	12000	0f53b88b45dfc5c67e2029b78c99b991	f	15 de Noviembre 1990	Bogotá D.C
496	Camila Mercado	1015462615	calle 25 # 68b-30 apt 703-2	calle 25 # 68b-30 apt 703-2	3204930906	f	a	Entregado	2017-11-28 17:19:42.705	17600	3344	20944	mer_cami@hotmail.com	\N	8000	7226f1d66b0c9df4ead0d2a39f9c270b	f	16/06/96	Bogotá D.C
502	Niccolle Fernandez	1020730586	Calle 141 # 7b-38	Calle 141 # 7b-38 apto 305	3208039380	t	a	Entregado	2017-11-29 19:28:26.069	152200	28918	181118	nicky087@gmai.com	@nicky087	13000	dcc593457ee7ca2dc3cc23188d473e62	t	11-11-1987	Bogotá D.C
507	Sergio Alonso	79811281	Cra 69b # 24-10 INt 20 Apto 201	Cra 69b # 24-10 INt 20 Apto 201	3115459539	f	a	Aprobado	2017-12-03 18:20:26.314	40000	7600	47600	salonsog@hotmail.com	\N	8000	887dacb62f484a808d98e9f3327db71d	f	20/12/1977	Bogotá D.C
504	María Paula Ocampo Paez	1020819112	Carrera 19 A # 103-80	Carrera 19 A # 103-80 Apto 403	3203460987	f	a	Rechazado	2017-11-30 15:57:40.592	49600	9424	59024	mariapaulaocampo19@hotmail.com	\N	8000	21b8a6dc4ecbc535788f04b67bf4e50d	f	09/19/96	Bogotá D.C
508	Rafael Sánchez	91161545	Calle 112 #18-15, Edificio SEPPIA, Barrio San Patricio	Calle 112 #18-15, Edificio SEPPIA, Barrio San Patricio	3156731049	f	a	Aprobado	2017-12-03 18:21:55.453	49600	9424	59024	rafaelsanchez1985@hotmail.com	\N	8000	6f69834dcaa05e23267075aea3e295dd	f	Enero 7	Bogotá D.C
509	Claudia Albarán	1110453771	Calle 125 #18-28 Edificio Life125 Torre 2 apartamento 202	Calle 125 #18-28 Edificio Life125 Torre 2 apartamento 202	3012087574	f	a	Aprobado	2017-12-04 02:33:17.724	53400	10146	63546	claudialbaran@hotmail.com	\N	11000	758fc6267371476c1f381b34daf9977b	t	21 de agosto	Bogotá D.C
510	Maria Margarita Viteri Toro	1018434498	Kra 21 No 105 - 47, Edificio Ryalto Apartamento	Kra 21 No 105 - 47, Edificio Ryalto Apartamento	3013638102	f	a	Pendiente	2017-12-04 22:16:32.458	51000	9690	60690	marga_031@hotmail.com	\N	11000	23ec977dfe6684ade37ee03b624cd90a	t	25/04/1990	Bogotá D.C
511	fernando	3005262602	cra 14 # 119 -83	fernando@iridian.co	3005262602	t	a	Pendiente	2017-12-05 01:45:37.841	44200	8398	52598	fernando@iridian.co	\N	19000	2dc4e528d27a069516c44206e5f596ba	t	25 de marzo	Abriaquí
513	Anamaria Torres Bernal	1018436160	Calle 50#13-76	Calle 50#13-76 Apto 403	3158886690	f	a	Aprobado	2017-12-05 14:27:00.901	51000	9690	60690	anmatobe@hotmail.com	\N	11000	80a2e62b3643730fa92c5909a550e2a1	t	Enero 6	Bogotá D.C
512	angelica perez	35195766	calle 126 #52a-96 apto 538	calle 126 #52a-96 apto 538	3004751660	f	a	Aprobado	2017-12-05 03:14:43.622	28800	5472	34272	angelicamariaps@gmail.com	\N	8000	55135ac04900279f7fc699c953302d8a	f	21 de junio	Bogotá D.C
515	Angela Hernandez	1019061354	Calle 138, Calle 138 74 51	Calle 138, Calle 138 74 51	3186005940	t	a	Pendiente	2017-12-05 22:21:44.539	38800	7372	46172	angelahm@gmail.com	AngelaHernandezMendieta	6000	d74684166914624dac54cbd56013576b	f	29 de junio	Bogotá D.C
516	Camila Hernandez	1136884123	Calle 114a # 21-79 apto 201	Calle 114a # 21-79 apto 201	3153599293	f	a	Aprobado	2017-12-05 22:31:51.795	72000	13680	85680	camiher_11@hotmail.com	\N	8000	3f9e1023303a96dece232ab1b8b8082f	f	20 de abril 1992	Bogotá D.C
517	Angela Hernandez	1019061354	Calle 138, Calle 138 74 51	Calle 138, Calle 138 74 51	3186005940	t	a	Aprobado	2017-12-05 22:38:33.338	43800	8322	52122	angelahm@gmail.com	AngelaHernandezMendieta	11000	c2aaf6883b4430cdfa0ee68319d05368	t	29 de junio	Bogotá D.C
514	Carolina Polanía Beltrán	1075318193	Cra 43a #19-66 Villa Rosa	Cra 43a  #19-66 Villa Rosa	3204788402	f	a	Aprobado	2017-12-05 19:30:48.677	46000	8740	54740	carolinapolaniabeltran10@gmail.com	\N	10000	dcfc0212b2c3783ac1b0c0323c898f3d	f	10 de mayo de 1999	Neiva
518	Sara Strochia	519250	Calle 147 #14-69 Apartamento 503 Torre 4	Calle 147 #14-69 Apartamento 503 Torre 4	3222196415	f	a	Pendiente	2017-12-06 14:01:41.335	31200	5928	37128	saralexs@gmail.com	\N	6000	2bb5b5b873282a1710b139178275c413	f	12/02	Bogotá D.C
519	Sara Strochia	519250	calle 147 #14-69 Apartamento 503 Torre 4	calle 147 #14-69 Apartamento 503 Torre 4	3222196415	f	a	Rechazado	2017-12-06 14:08:00.415	26800	5092	31892	saralexs@gmail.com	\N	6000	1b9a4a950cf7981b6f11dc682c8404bb	f	_	Bogotá D.C
\.


--
-- Name: pedido_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pedido_id_seq', 519, true);


--
-- Data for Name: politica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY politica (id, nombre, descripcion, obligatorio) FROM stdin;
1	Manifiesto que conozco y autorizo de forma libre y espontánea la <span>política de tratamiento de datos y fotos personales de INSTAMPA</span>	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel tincidunt urna. Morbi auctor ipsum dui, sit amet mattis urna tempor pretium. Nunc maximus eros et ante finibus volutpat. Mauris faucibus et elit eget volutpat. Nulla sem tortor, viverra eget nibh nec, gravida vehicula augue. Donec malesuada arcu at ligula viverra luctus. Donec sed aliquet nibh. Morbi vitae mi feugiat nibh luctus interdum. In tempus euismod varius. Suspendisse eget tincidunt augue, vel convallis nisl. Curabitur feugiat consectetur dictum. Nullam vel diam a mauris mattis auctor nec vitae metus. Nullam sed felis id diam malesuada mattis. Suspendisse imperdiet sem at tempor elementum. Nulla ac lobortis lacus, vitae vestibulum lectus. Integer mollis venenatis mi, quis vulputate ex sagittis et. Proin ullamcorper leo a lacus rhoncus finibus. Suspendisse et dapibus dolor. Donec aliquet fringilla leo, vel elementum massa congue quis. Morbi pulvinar, purus id venenatis pretium, nisi est euismod leo, ac lobortis mauris felis a mauris. Sed in sollicitudin urna, laoreet eleifend nulla. Suspendisse imperdiet velit in ligula fermentum, ac commodo ligula ornare. Maecenas tincidunt purus at enim tincidunt dignissim. Sed lobortis nisi aliquam nisl tincidunt, a finibus tellus placerat. Nunc accumsan mi sed gravida ultrices.	t
2	Autorizo que Instampa <span>publique mis fotos en Instagram</span>	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel tincidunt urna. Morbi auctor ipsum dui, sit amet mattis urna tempor pretium. Nunc maximus eros et ante finibus volutpat. Mauris faucibus et elit eget volutpat. Nulla sem tortor, viverra eget nibh nec, gravida vehicula augue. Donec malesuada arcu at ligula viverra luctus. Donec sed aliquet nibh. Morbi vitae mi feugiat nibh luctus interdum. In tempus euismod varius. Suspendisse eget tincidunt augue, vel convallis nisl. Curabitur feugiat consectetur dictum. Nullam vel diam a mauris mattis auctor nec vitae metus. Nullam sed felis id diam malesuada mattis. Suspendisse imperdiet sem at tempor elementum. Nulla ac lobortis lacus, vitae vestibulum lectus. Integer mollis venenatis mi, quis vulputate ex sagittis et. Proin ullamcorper leo a lacus rhoncus finibus. Suspendisse et dapibus dolor. Donec aliquet fringilla leo, vel elementum massa congue quis. Morbi pulvinar, purus id venenatis pretium, nisi est euismod leo, ac lobortis mauris felis a mauris. Sed in sollicitudin urna, laoreet eleifend nulla. Suspendisse imperdiet velit in ligula fermentum, ac commodo ligula ornare. Maecenas tincidunt purus at enim tincidunt dignissim. Sed lobortis nisi aliquam nisl tincidunt, a finibus tellus placerat. Nunc accumsan mi sed gravida ultrices.	f
\.


--
-- Name: politica_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('politica_id_seq', 2, true);


--
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY producto (id, nombre, descripcion, imagen, numfotos, precio, id_tipoproducto, habilitado) FROM stdin;
3	Paquete de 16 fotos	16 fotos	x	16	32000	1	t
4	Paquete de 20 fotos	20 fotos	x	20	39200	1	t
5	Paquete de 24 fotos	24 fotos	x	24	46400	1	t
6	Paquete de 32 fotos	32 fotos	x	32	59200	1	t
7	Paquete de 40 fotos	40 fotos	x	40	80000	1	t
8	Paquete de 50 fotos	50 fotos	x	50	96000	1	t
9	Paquete de 60 fotos	60 fotos	x	60	110400	1	t
10	Paquete de 70 fotos	70 fotos	x	70	123200	1	t
21	Marco de madera de 20 fotos	Marco de madera de 20 fotos 53 x 66 cm	x	20	98400	2	t
22	Marco de madera de 24 fotos	Marco de madera de 24 fotos 53 x 79 cm	x	24	108000	2	t
11	Paquete de 80 fotos	80 fotos	x	80	137600	1	t
12	Paquete de 90 fotos	90 fotos	x	90	151200	1	t
13	Paquete de 100 fotos	100 fotos	x	100	160000	1	t
33	Album + 24 Fotos	Album de fotografias + un set de 24 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	24	54400	5	t
34	Album + 32 Fotos	Album de fotografias + un set de 32 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	32	63200	5	t
35	Album + 40 Fotos	Album de fotografias + un set de 40 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	40	72000	5	t
37	Album + 60 Fotos	Album de fotografias + un set de 60 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	60	103200	5	t
36	Album + 50 Fotos	Album de fotografias + un set de 50 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	50	87200	5	t
23	Marco de madera de 32 fotos	Marco de madera de 32 fotos 55 x 105 cm	x	32	139200	2	t
38	Album + 70 Fotos	Album de fotografias + un set de 70 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	70	113600	5	t
39	Album + 80 Fotos	Album de fotografias + un set de 80 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	80	123200	5	t
45	Postal 1 Foto	Postal con 1 foto	img/fotos_pedidos/admin_fotos/postales/postal.svg	1	4800	6	t
24	Cuerda de lentajuelas de 1 m	Cuerda de lentejuelas de 1 m con 6 fotos	x	6	20000	4	t
25	Cuerda de lentejuelas 1.50 m	Cuerda de lentejuelas 1.50 m con 10 fotos	x	10	24000	4	t
26	Cuerda de lentejuelas 1.70m	Cuerda de lentejuelas 1.70m con 12 fotos	x	12	27200	4	t
27	Cuerda de lentejuelas 3m	Cuerda de lentejuelas 3m con 20 fotos	x	20	42400	4	t
28	Cuerda de lentejuelas 3.40m	Cuerda de lentejuelas 3.40m con 24 fotos	x	24	49600	4	t
40	1 Foto + 1 Minicaballete	1 Foto + 1 Minicaballete	x	1	9600	3	t
41	2 Fotos + 2 Minicaballetes	2 Fotos + 2 Minicaballetes	x	2	16800	3	t
42	3 Fotos + 3 Minicaballetes	3 Fotos + 3 Minicaballetes	x	3	24000	3	t
14	Marco de madera de 1 foto	Marco de madera 1 foto 20 x 20 cm	x	1	20800	2	t
15	Marco de madera de 2 fotos	Marco de madera de 2 Fotos 24 x 24 cm	x	2	28000	2	t
1	Paquete 10 fotos	10 fotos	x	10	20000	1	t
43	4 Fotos + 4 Minicaballetes	4 Fotos + 4 Minicaballetes	x	4	36000	3	t
16	Marco de madera de 3 fotos	Marco de madera de 3 fotos 36 x 24 cm	x	3	32000	2	t
2	Paquete 12 fotos	12 fotos	x	12	24000	1	t
17	Marco de madera de 4 fotos	Marco de madera de 4 fotos 30 x 30 cm	x	4	36000	2	t
18	Marco de madera de 6 fotos	Marco de madera de 6 fotos 38 x 38 cm	x	6	43200	2	t
19	Marco de madera de 9 fotos	Marco de madera de 9 fotos 38 x 45 cm	x	9	51200	2	t
20	Marco de madera de 12 fotos	Marco de madera de 12 fotos 42 x 53 cm	x	12	68000	2	t
30	Album + 12 Fotos	Album de fotografias + un set de 12 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	12	38400	5	t
31	Album + 16 Fotos	Album de fotografias + un set de 16 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	16	44800	5	t
32	Album + 20 Fotos	Album de fotografias + un set de 20 fotos	/img/fotos_pedidos/admin_fotos/albums/album.png	20	49600	5	t
\.


--
-- Name: producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('producto_id_seq', 71, true);


--
-- Data for Name: productopedido; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productopedido (id, id_producto, id_empaque, id_pedido) FROM stdin;
295	6	3	262
298	1	3	264
300	3	9	266
308	40	\N	274
310	26	\N	276
316	26	\N	280
318	40	\N	282
320	25	\N	284
322	2	1	286
326	30	\N	290
328	16	\N	292
330	19	\N	294
334	16	\N	298
336	1	5	300
338	3	11	302
340	24	\N	304
342	2	11	306
344	14	\N	308
346	16	\N	310
348	4	17	312
350	14	\N	314
352	14	\N	316
354	17	\N	318
356	3	7	320
358	1	3	322
359	1	3	322
361	1	13	323
363	5	2	325
365	45	22	327
366	45	20	327
371	15	\N	331
373	15	\N	333
379	45	22	339
381	6	5	341
383	16	\N	343
385	14	\N	345
386	41	\N	345
388	15	\N	346
391	14	\N	348
393	25	\N	349
395	2	26	351
397	14	\N	353
403	16	\N	360
405	14	\N	362
407	1	4	364
408	15	\N	365
410	18	\N	367
412	4	6	369
414	1	6	371
416	17	\N	373
418	45	20	375
420	17	\N	377
422	16	\N	379
424	1	1	381
426	17	\N	383
428	15	\N	385
430	15	\N	387
432	31	\N	389
434	17	\N	391
436	14	\N	393
440	4	5	397
442	14	\N	399
444	40	\N	401
446	1	6	403
448	17	\N	405
450	1	5	407
452	15	\N	409
455	1	2	411
457	17	\N	413
459	32	\N	415
461	1	7	417
463	19	\N	419
465	18	\N	421
466	24	\N	421
468	17	\N	423
470	20	\N	425
472	1	28	427
474	17	\N	429
476	1	3	431
478	7	26	433
480	4	1	435
482	3	1	437
484	14	\N	439
486	1	3	441
488	1	2	443
490	5	9	445
492	1	3	447
494	14	\N	449
496	40	\N	450
498	16	\N	452
500	40	\N	453
502	30	\N	456
504	14	\N	458
506	40	\N	460
508	6	18	462
509	41	\N	462
511	5	9	463
514	20	\N	465
516	17	\N	466
518	15	\N	468
520	34	\N	470
522	1	11	472
525	15	\N	474
527	3	14	476
529	45	20	477
532	2	13	479
534	1	5	481
536	6	9	483
538	6	9	485
541	24	\N	486
543	26	\N	487
545	2	9	489
547	17	\N	491
549	15	\N	493
551	21	\N	495
553	28	\N	497
555	41	\N	499
557	15	\N	501
559	15	\N	501
560	14	\N	501
562	14	\N	501
564	14	\N	502
566	15	\N	502
568	15	\N	502
570	14	\N	504
571	14	\N	504
574	25	\N	506
576	14	\N	508
578	27	\N	509
580	1	28	511
582	4	3	513
584	3	3	515
586	16	\N	516
588	1	13	518
186	40	\N	155
303	19	\N	269
305	1	5	271
309	19	\N	275
311	24	\N	277
312	24	\N	277
315	24	\N	279
317	18	\N	281
319	14	\N	283
323	1	1	287
325	2	2	289
327	1	14	291
329	1	2	293
331	1	9	295
333	16	\N	297
337	3	3	301
343	14	\N	307
345	40	\N	309
347	15	\N	311
349	14	\N	313
351	14	\N	315
353	2	7	317
355	35	\N	319
357	15	\N	321
360	5	13	323
362	25	\N	324
364	16	\N	326
367	40	\N	328
368	40	\N	328
228	40	\N	195
370	16	\N	330
372	1	1	332
374	15	\N	334
376	16	\N	336
378	5	6	338
380	16	\N	340
382	1	2	342
384	45	23	344
387	15	\N	346
389	1	17	347
390	14	\N	347
392	1	17	348
394	6	4	350
396	2	26	352
398	17	\N	355
400	16	\N	357
402	5	4	359
404	14	\N	361
406	15	\N	363
409	1	6	366
411	4	6	368
413	15	\N	370
415	15	\N	372
417	45	20	374
419	19	\N	376
421	16	\N	378
423	16	\N	380
425	1	12	382
427	19	\N	384
429	16	\N	386
431	2	8	388
433	5	6	390
435	27	\N	392
437	1	11	394
439	2	4	396
441	14	\N	398
443	40	\N	400
445	15	\N	402
447	40	\N	404
449	40	\N	406
451	1	5	408
453	1	8	410
454	1	16	410
456	3	9	412
458	3	8	414
460	30	\N	416
462	1	6	418
464	19	\N	420
467	17	\N	422
469	19	\N	424
471	3	15	426
473	1	28	428
475	1	3	430
477	15	\N	432
479	24	\N	434
481	1	3	436
483	15	\N	438
485	16	\N	440
487	16	\N	442
292	17	\N	259
293	10	5	260
489	20	\N	444
491	40	\N	446
493	1	5	448
495	14	\N	450
497	18	\N	451
499	14	\N	453
501	30	\N	454
503	14	\N	457
505	24	\N	459
507	5	3	461
510	6	4	462
512	1	9	464
513	14	\N	464
515	17	\N	466
517	5	8	467
519	1	2	469
521	16	\N	471
523	1	\N	473
524	1	5	473
526	24	\N	475
528	45	24	477
530	45	20	478
531	45	24	478
533	6	13	480
535	6	11	482
537	40	\N	484
539	24	\N	486
540	24	\N	486
542	20	\N	486
544	37	\N	488
546	1	7	490
548	16	\N	492
550	40	\N	494
552	40	\N	496
554	14	\N	498
556	1	6	500
558	14	\N	501
561	14	\N	501
563	14	\N	502
565	14	\N	502
567	14	\N	502
569	40	\N	503
572	14	\N	505
573	14	\N	505
575	16	\N	507
577	14	\N	508
579	4	5	510
581	14	\N	512
583	43	\N	514
585	16	\N	516
587	3	3	517
589	1	5	519
\.


--
-- Name: productopedido_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productopedido_id_seq', 589, true);


--
-- Data for Name: tipoproducto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipoproducto (id, nombre, slug, imagen, descripcion, disclaimer) FROM stdin;
1	Cajas y Sobres	paqueteFotos	img/fotos_pedidos/admin_fotos/Sobres.png	Arma tu sobre o cajita con fotos impresas en formato instantáneo o cuadradito.	*Las fotos tipo instantánea miden 8,5 de ancho x 10,5 de largo<br>*Las fotos cuadradas miden 9 x 9 cm<br>*Todas se imprimen en papel fotográfico grueso<br>*Las fotos tipo instantáneo son imitación polaroid<br>*No exigimos ninguna resolución, aceptamos fotos tomadas con celular o cámara<br>*No nos hacemos responsables por fotos enviadas en muy baja resolución (sin embargo nos comunicaremos contigo si encontramos alguna que no se pueda imprimir).<br>*No editamos las fotos, únicamente las ajustamos al tamaño para impresión.
2	Marcos	marcos	img/fotos_pedidos/admin_fotos/Marcos.png	Escoge tu marco de madera para colgar la cantidad de fotos que quieras imprimir	*Incluyen las fotos impresas<br>*Incluyen cuerdas de nylon y ganchitos de madera para las fotos.<br>*Todos los marcos tienen una base de 3 cm luego puede colgarse o pararse en una mesa.<br>*A tu casa llegan desarmados (marco con cuerdas+fotos impresas+ganchitos de madera aparte para que tú las cuelgues a tu gusto).
3	Caballetes	caballetes	img/fotos_pedidos/admin_fotos/Caballete.png	Escoge tu minicaballete de madera para poner esa foto especial o arma tu kit	*Incluyen las fotos impresas<br>*Si escoges un kit puedes escoger un color diferente para cada minicaballete<br>*A tu casa llega el minicaballete y la foto aparte para tú ponerla<br>*Los minicaballetes miden 14 cm ancho x 17 cm alto
4	Cuerdas y Clips	cuerdasPinzas	img/fotos_pedidos/admin_fotos/Cuerdas.png	Arma tu cuerda con sus ganchitos de madera para colgar tus fotos impresas	*Incluyen las fotos impresas<br>*Incluyen los ganchitos de madera<br>*A tu casa llegan desarmadas (cuerda de lentejuelas+fotos impresas+ganchitos de madera para que tú las cuelgues a tu gusto)
5	Álbumes	albumes	img/fotos_pedidos/admin_fotos/Album (1).png	Recupera la tradición y llévate tu álbum para pegar la cantidad de fotos que imprimas	*La portada es de pasta dura y las hojas son Earthpack de 290 gramos<br>*Todos los álbumes tienen 20 hojas (40 páginas) y por página caben 2 fotos. Caben hasta 80 fotos por álbum.<br>*El álbum solo tiene un diseño, lo que cambia es la cantidad de fotos que quieras incluirle<br>*El álbum mide 21,5 de ancho x 15,5 de alto (es horizontal)<br>*Puedes escoger si quieres recibir tu álbum con las fotos pegadas o aparte para tú pegarlas a tu gusto
6	Postales	postales	img/fotos_pedidos/admin_fotos/postalesHome.png	Manda un mensaje lleno de amor a quiénes están lejos junto con una foto de recuerdo	*Podrá escoger el mensaje que querrá incluir y este no podrá superar los 140 caracteres<br>*El precio incluye el sobre en el que viene la postal
\.


--
-- Name: tipoproducto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipoproducto_id_seq', 6, true);


--
-- Name: accesorioproducto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accesorioproducto
    ADD CONSTRAINT accesorioproducto_pkey PRIMARY KEY (id);


--
-- Name: accesorioproductopedido_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accesorioproductopedido
    ADD CONSTRAINT accesorioproductopedido_pkey PRIMARY KEY (id);


--
-- Name: costoenvio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY costoenvio
    ADD CONSTRAINT costoenvio_pkey PRIMARY KEY (id);


--
-- Name: empaque_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empaque
    ADD CONSTRAINT empaque_pkey PRIMARY KEY (id);


--
-- Name: foto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT foto_pkey PRIMARY KEY (id);


--
-- Name: pedido_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pedido
    ADD CONSTRAINT pedido_pkey PRIMARY KEY (id);


--
-- Name: politica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY politica
    ADD CONSTRAINT politica_pkey PRIMARY KEY (id);


--
-- Name: producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- Name: productopedido_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productopedido
    ADD CONSTRAINT productopedido_pkey PRIMARY KEY (id);


--
-- Name: tipoproducto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoproducto
    ADD CONSTRAINT tipoproducto_pkey PRIMARY KEY (id);


--
-- Name: accesorioproducto_id_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorioproducto
    ADD CONSTRAINT accesorioproducto_id_producto_fkey FOREIGN KEY (id_producto) REFERENCES producto(id);


--
-- Name: accesorioproductopedido_id_accesorioproducto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorioproductopedido
    ADD CONSTRAINT accesorioproductopedido_id_accesorioproducto_fkey FOREIGN KEY (id_accesorioproducto) REFERENCES accesorioproducto(id);


--
-- Name: accesorioproductopedido_id_productopedido_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY accesorioproductopedido
    ADD CONSTRAINT accesorioproductopedido_id_productopedido_fkey FOREIGN KEY (id_productopedido) REFERENCES productopedido(id);


--
-- Name: costoenvio_id_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY costoenvio
    ADD CONSTRAINT costoenvio_id_producto_fkey FOREIGN KEY (id_producto) REFERENCES producto(id);


--
-- Name: empaque_id_tipoproducto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empaque
    ADD CONSTRAINT empaque_id_tipoproducto_fkey FOREIGN KEY (id_tipoproducto) REFERENCES tipoproducto(id);


--
-- Name: fk_pedido; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productopedido
    ADD CONSTRAINT fk_pedido FOREIGN KEY (id_pedido) REFERENCES pedido(id);


--
-- Name: foto_id_productopedido_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT foto_id_productopedido_fkey FOREIGN KEY (id_productopedido) REFERENCES productopedido(id);


--
-- Name: producto_id_tipoproducto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT producto_id_tipoproducto_fkey FOREIGN KEY (id_tipoproducto) REFERENCES tipoproducto(id);


--
-- Name: productopedido_id_empaque_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productopedido
    ADD CONSTRAINT productopedido_id_empaque_fkey FOREIGN KEY (id_empaque) REFERENCES empaque(id);


--
-- Name: productopedido_id_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productopedido
    ADD CONSTRAINT productopedido_id_producto_fkey FOREIGN KEY (id_producto) REFERENCES producto(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

