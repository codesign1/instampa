'use strict';

var request = require('request');
var Md5 = require('md5');
var urlPedidos = 'http://instampa.co/api/pedidos';

function confirmation(req, res) {
  var getPedidoByReference = urlPedidos + '/?filter[where][referencecode]=' + req.query.reference_sale;
  request.get(getPedidoByReference, function(err, body) {
    if (!err) {
      var pedido = JSON.parse(body.body);
      var idPedido = pedido[0].id;
      var table = '';
      var apiKey = '4Vj8eK4rloUd272L48hsrarnUA';
      var merchantId = req.query.merchant_id;
      var referenceSale = req.query.reference_sale;
      var value = req.query.value;

      var txValue = value.toString();
      var length = txValue.length;
      var newValue = txValue.slice(0, length - 1);

      var currency = req.query.currency;
      var statePol = req.query.state_pol;

      var firmaCadena = apiKey + '~' + merchantId + '~' + referenceSale + '~' + newValue + '~' + currency + '~' + statePol;
      var firmaCreada = Md5.createHash(firmaCadena || '');

      if (firmaCreada == req.query.sign) {
        switch (statePol) {
          case 4:
            var estado = {estado: 'Aprobado'};
            var cambiarEstado = urlPedidos + '/' + idPedido;
            request.patch(cambiarEstado, estado);
            break;
          case 6:
            var estado = {estado: 'Rechazado'};
            var cambiarEstado = urlPedidos + '/' + idPedido;
            request.patch(cambiarEstado, estado);
            break;
          case 5:
            var estado = {estado: 'Rechazado'};
            var cambiarEstado = urlPedidos + '/' + idPedido;
            request.patch(cambiarEstado, estado);
            break;
        }
      }
    } else {
      res.status(500)
        .json({
          status: 'error en getPedidoByReference',
          data: err,
        });  
    }
  });
};
module.exports = {
  confirmation: confirmation,
};
