/**
 * Created by pipe on 23/04/17.
 */

var apiKey = 'key-35f54002012f90dbee0e59421dd26e93';
var domain = 'mg.printadelivery.com';
// var apiKey = 'key-c0939780c8bec095308cda0ab25e84e1';
// var domain = 'mg.instampa.co';
var mailgun = require('mailgun-js')({
  apiKey: apiKey,
  domain: domain
});


function mail1(req, res) {
  var data = {
    from: 'Excited User <me@samples.mailgun.org>',
    to: 'serobnic@mail.ru',
    subject: 'Hello',
    text: 'Testing some Mailgun awesomness!'
  };

  mailgun.messages().send(data, function (error, body) {
    console.log(body);
  });
}

//to admin
function nuevoPedido(req, res) {
  var data = {
    from: 'Instampa <instampa@instampa.co>',
    to: 'pedidos@instampa.co',
    subject: "Nuevo pedido",
    html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
      '<html xmlns="http://www.w3.org/1999/xhtml">' +
      '	<head>' +
      '		<title>Internal_email-29</title>' +
      '		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
      '		<meta name="viewport" content="width=device-width, initial-scale=1.0" />' +
      '	</head>' +
      '	<body style="margin:50px; padding:0;" bgcolor="#ffffff">' +
      '		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">' +
      '			<!-- fix for gmail -->' +
      '			<tr>' +
      '				<td class="hide">' +
      '					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">' +
      '						<tr>' +
      '							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>' +
      '						</tr>' +
      '					</table>' +
      '				</td>' +
      '			</tr>' +
      '			<tr>' +
      '				<td class="wrapper" style="padding:0 10px;">' +
      '					<!-- module 3 -->' +
      '					<table data-module="module-3" data-thumb="http://instampa.co/img/logo.svg" width="100%" cellpadding="0" cellspacing="0">' +
      '						<tr>' +
      '							<td data-bgcolor="bg-module" bgcolor="#ffffff">' +
      '								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">' +
      '									<tr>' +
      '										<td class="img-flex" style="background-color: #8582F9; padding: 50px 150px;">' +
      '											<img src="http://instampa.co/img/mailingLogo.png">' +
      '										</td>' +
      '									</tr>' +
      '									<tr>' +
      '										<td data-bgcolor="bg-block" class="holder" style="padding:65px 60px 50px;" bgcolor="#f9f9f9">' +
      '											<table width="100%" cellpadding="0" cellspacing="0">' +
      '												<tr>' +
      '													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:30px/33px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: bold;">' +
      '														¡Tienes un nuevo pedido!' +
      '													</td>' +
      '												</tr>' +
      '												<tr>' +
      '													<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:0 0 21px;">' +
      '														<img src="http://instampa.co/img/mailing1.png" style="    padding-bottom: 2rem;padding-left: 2rem;" width="30%">' +
      '														<br>' +
      '														Dirígete al Dashboard para administar éste y otros pedidos.' +
      '													</td>' +
      '												</tr>' +
      '												<tr>' +
      '													<td style="padding:0 0 20px;">' +
      '														<table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">' +
      '															<tr>' +
      '																<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#FFFFFF; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius: 25px;" bgcolor="#25BDE7">' +
      '																	<a target="_blank" style="text-decoration:none; color:#FFFFFF; display:block; padding:12px 10px 10px;" href="http://instampa.co/#/dashboard">IR AL DASHBOARD</a>' +
      '																</td>' +
      '															</tr>' +
      '														</table>' +
      '													</td>' +
      '												</tr>' +
      '											</table>' +
      '										</td>' +
      '									</tr>' +
      '									<tr><td height="28"></td></tr>' +
      '								</table>' +
      '							</td>' +
      '						</tr>' +
      '					</table>' +
      '					<!-- module 4 -->' +
      '				</td>' +
      '			</tr>' +
      '			<!-- fix for gmail -->' +
      '			<tr>' +
      '				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>' +
      '			</tr>' +
      '		</table>' +
      '	</body>' +
      '</html>'
  };
  mailgun.messages().send(data, function (error, body) {
    res.status(200)
      .json({
        status: 200,
        data: body,
        message: "succesfull"
      });
  });
}

function mensajeContacto(req, res) {
  var data = {
    from: 'Instampa <instampa@instampa.co>',
    to: 'escribenos@instampa.co',
    subject: "Nuevo mensaje de contacto",
    html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
      '<html xmlns="http://www.w3.org/1999/xhtml">' +
      '<head>' +
      '<title>Internal_email-29</title>' +
      '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
      '<meta name="viewport" content="width=device-width, initial-scale=1.0" />' +
      '</head>' +
      '<body style="margin:50px; padding:0;" bgcolor="#ffffff">' +
      '<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">' +
      '<!-- fix for gmail -->' +
      '<tr>' +
      '<td class="hide">' +
      '<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">' +
      '<tr>' +
      '<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>' +
      '</tr>' +
      '</table>' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td class="wrapper" style="padding:0 10px;">' +
      '<!-- module 3 -->' +
      '<table data-module="module-3" data-thumb="http://instampa.co/img/logo.svg" width="100%" cellpadding="0" cellspacing="0">' +
      '<tr>' +
      '<td data-bgcolor="bg-module" bgcolor="#ffffff">' +
      '<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">' +
      '<tr>' +
      '<td class="img-flex" style="background-color: #8582F9; padding: 50px 150px;">' +
      '<img src="http://instampa.co/img/mailingLogo.png">' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td data-bgcolor="bg-block" class="holder" style="padding:65px 60px 50px;" bgcolor="#f9f9f9">' +
      '<table width="100%" cellpadding="0" cellspacing="0">' +
      '<tr>' +
      '<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:30px/33px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: bold;">' +
      '¡Hola! ' + req.query.nombre + ' te ha enviado un mensaje de contacto.' +
      '</td>' +
      '</tr>' +
      '<tr>' +
      '<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:0 0 21px;">' +
      '<img src="http://instampa.co/img/new-message.png" style="padding-bottom: 2rem;padding-left: 2rem;" width="30%">' +
      '<br>' +
      '<p align="justify">Correo: ' + req.query.correo + '<br><br>"' + req.query.mensaje + '"</p>' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '</td>' +
      '</tr>' +
      '<tr><td height="28"></td></tr>' +
      '</table>' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '<!-- module 4 -->' +
      '</td>' +
      '</tr>' +
      '<!-- fix for gmail -->' +
      '<tr>' +
      '<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>' +
      '</tr>' +
      '</table>' +
      '</body>' +
      '</html>'
  };
  mailgun.messages().send(data, function (error, body) {
    if (!error) {
      res.status(200)
        .json({
          status: "Succesfull",
          data: body,
          message: "Gracias por contactarte con nosotros."
        });
    }
  });
}

//to customer
function pedidoRecibido(req, res) {
  var query1 = Object.keys(req.query);
  var query2 = query1.toString();

  var query = query2.split('&');

  var one = query[0].toString();
  var nombre = one.split('=');

  var two = query[1].toString();
  var correo = two.split('=');

  var three = query[2].toString();
  var tabla = three.replace(/\"/g, '\'');
  var productos = tabla.substring(10);

  var data = {
    from: 'Instampa <instampa@instampa.co>',
    to: correo[1],
    subject: "Hemos recibido tu pedido",
    html: "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
      "<html xmlns='http://www.w3.org/1999/xhtml'>" +
      "<head>" +
      "<title>Pedido Recibido mail</title>" +
      "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />" +
      "<meta name='viewport' content='width=device-width, initial-scale=1.0' />" +
      "</head>" +
      "<body style='margin:50px; padding:0;' bgcolor='#ffffff'>" +
      "<table style='min-width:320px;' width='100%' cellspacing='0' cellpadding='0' bgcolor='#ffffff'>" +
      "<tr>" +
      "<td class='hide'>" +
      "<table width='600' cellpadding='0' cellspacing='0' style='width:600px !important;'>" +
      "<tr>" +
      "<td style='min-width:600px; font-size:0; line-height:0;'>&nbsp;</td>" +
      "</tr>" +
      "</table>" +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td class='wrapper' style='padding:0 10px;'>" +
      "<table data-module='module-3' data-thumb='http://instampa.co/img/logo.svg' width='100%' cellpadding='0' cellspacing='0'>" +
      "<tr>" +
      "<td data-bgcolor='bg-module' bgcolor='#ffffff'>" +
      "<table class='flexible' width='600' align='center' style='margin:0 auto;' cellpadding='0' cellspacing='0'>" +
      "<tr>" +
      "<td class='img-flex' style='background-color: #8582F9; padding: 50px 150px;'>" +
      "<img src='http://instampa.co/img/mailingLogo.png'>" +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td data-bgcolor='bg-block' class='holder' style='padding:65px 60px 50px;' bgcolor='#f9f9f9'>" +
      "<table width='100%' cellpadding='0' cellspacing='0'>" +
      "<tr>" +
      "<td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:none; color:#292c34;' class='title' align='center' style='font:30px/33px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: bold;'>" +
      "¡Hola "+nombre[1]+"! Hemos recibido exitosamente tu pedido." +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td>" +
      "<table width='100%' cellpadding='0' cellspacing='0' style='margin-bottom: 25px;margin-top: 15px;'>" +
      "<tr class='title' align='center' style='font:15px/17px Arial, Helvetica, sans-serif; color:#000; font-weight: bold;'>" +
      "<td style='padding:10px 0;background-color: rgba(133, 130, 249, 0.37);'>Producto</td>" +
      "<td style='padding:10px 0;background-color: rgba(133, 130, 249, 0.37);'># fotos</td>" +
      "<td style='padding:10px 0;background-color: rgba(133, 130, 249, 0.37);'>Formato</td>" +
      "<td style='padding:10px 0;background-color: rgba(133, 130, 249, 0.37);'>Accesorios/Empaque</td>" +
      "</tr>" +
      productos +
      "</table>" +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td data-color='title' data-link-color='link title color' data-link-style='text-decoration:none; color:#292c34;' align='center' style='font:15px/17px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: normal;'>" +
      'dirección de envio:' + address +
      "</td>" +
      "<tr>" +
      "<td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:none; color:#292c34;' class='title' align='center' style='font:30px/33px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: bold;'>" +
      'Ya estamos trabajando para ti, recuerda que tu pedido llegará a tu casa dentro de 3 a 5 días hábiles.' +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='font-weight:bold; text-decoration:underline; color:#40aceb;' align='center' style='font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:0 0 21px;'>" +
      "<img src='http://instampa.co/img/mailing1.png' style='    padding-bottom: 2rem;padding-left: 2rem;' width='30%'>" +
      "<br>" +
      'Te esperamos para que realices más pedidos. Cualquier pregunta que tengas escríbenos por whatsapp al 3105612138' +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td style='padding:0 0 20px;'>" +
      "<table width='134' align='center' style='margin:0 auto;' cellpadding='0' cellspacing='0'>" +
      "<tr>" +
      "<td colspan='2' data-bgcolor='bg-button' data-size='size button' data-min='10' data-max='16' class='btn' align='center' style='font:12px/14px Arial, Helvetica, sans-serif; color:#FFFFFF; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius: 25px;' bgcolor='#25BDE7'>" +
      "<a target='_blank' style='text-decoration:none; color:#FFFFFF; display:block; padding-top:15px;' href='http://instampa.co'>IR AL SITIO WEB</a>" +
      "<br>" +
      "</td>" +
      "</tr>" +
      "<tr style='padding-top: 50px;'>" +
      "<td style='padding-top: 50px;'>" +
      "<a target='_blank' href='https://www.facebook.com/instampa.co'> <img src='http://instampa.co/img/mailingFace.png' width='30' height='50' style='padding-left: 25px;'> </a>" +
      "</td>" +
      "<td style='padding-top: 50px;'>" +
      "<a target='_blank' href='https://www.instagram.com/instampa.co'> <img src='http://instampa.co/img/mailingInta.png' width='30' height='40'> </a>" +
      "</td>" +
      "</tr>" +
      "</table>" +
      "</td>" +
      "</tr>" +
      "</table>" +
      "</td>" +
      "</tr>" +
      "<tr><td height='28'></td></tr>" +
      "</table>" +
      "</td>" +
      "</tr>" +
      "</table>" +
      "</td>" +
      "</tr>" +
      "<tr>" +
      "<td style='line-height:0;'><div style='display:none; white-space:nowrap; font:15px/1px courier;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>" +
      "</tr>" +
      "</table>" +
      "</body>" +
      "</html>"
  };
  mailgun.messages().send(data, function (error, body) {
    res.status(200)
      .json({
        status: 200,
        data: body,
        message: productos,
      });
  });
}

function pedidoEnviado(req, res) {
  var data = {
    from: 'Instampa <instampa@instampa.co>',
    to: req.query.correo,
    subject: "Tu pedido ha sido despachado.",
    html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' +
      '<html xmlns="http://www.w3.org/1999/xhtml">' +
      '	<head>' +
      '		<title>Internal_email-29</title>' +
      '		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />' +
      '		<meta name="viewport" content="width=device-width, initial-scale=1.0" />' +
      '	</head>' +
      '	<body style="margin:50px; padding:0;" bgcolor="#ffffff">' +
      '		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">' +
      '			<!-- fix for gmail -->' +
      '			<tr>' +
      '				<td class="hide">' +
      '					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">' +
      '						<tr>' +
      '							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>' +
      '						</tr>' +
      '					</table>' +
      '				</td>' +
      '			</tr>' +
      '			<tr>' +
      '				<td class="wrapper" style="padding:0 10px;">' +
      '					<!-- module 3 -->' +
      '					<table data-module="module-3" data-thumb="http://instampa.co/img/logo.svg" width="100%" cellpadding="0" cellspacing="0">' +
      '						<tr>' +
      '							<td data-bgcolor="bg-module" bgcolor="#ffffff">' +
      '								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">' +
      '									<tr>' +
      '										<td class="img-flex" style="background-color: #8582F9; padding: 50px 150px;">' +
      '											<img src="http://instampa.co/img/mailingLogo.png">' +
      '										</td>' +
      '									</tr>' +
      '									<tr>' +
      '										<td data-bgcolor="bg-block" class="holder" style="padding:65px 60px 50px;" bgcolor="#f9f9f9">' +
      '											<table width="100%" cellpadding="0" cellspacing="0">' +
      '												<tr>' +
      '													<td data-color="title" data-size="size title" data-min="20" data-max="40" data-link-color="link title color" data-link-style="text-decoration:none; color:#292c34;" class="title" align="center" style="font:30px/33px Arial, Helvetica, sans-serif; color:#000; padding:0 0 24px; font-weight: bold;">' +
      '														¡Hola ' + req.query.nombre + '! Tu pedido ya ha sido enviado. Nos comunicaremos contigo directamente para darte el número de guía de tu envío así como hacerte llegar tu factura de compra.' +
      '													</td>' +
      '												</tr>' +
      '												<tr>' +
      '													<td data-color="text" data-size="size text" data-min="10" data-max="26" data-link-color="link text color" data-link-style="font-weight:bold; text-decoration:underline; color:#40aceb;" align="center" style="font:16px/29px Arial, Helvetica, sans-serif; color:#888; padding:0 0 21px;">' +
      '														<img src="http://instampa.co/img/mailing1.png" style="    padding-bottom: 2rem;padding-left: 2rem;" width="30%">' +
      '														<br>' +
      '														Te esperamos para que realices más pedidos. Cualquier pregunta que tengas escríbenos por whatsapp al 3105612138 ¡Saludos! INSTAMPA.' +
      '													</td>' +
      '												</tr>' +
      '												<tr>' +
      '													<td style="padding:0 0 20px;">' +
      '														<table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">' +
      '															<tr>' +
      '																<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#FFFFFF; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius: 25px;" bgcolor="#25BDE7">' +
      '																	<a target="_blank" style="text-decoration:none; color:#FFFFFF; display:block; padding-top:15px;" href="http://instampa.co">IR AL SITIO WEB</a> <br>' +
      '																</td>' +
      '															</tr>' +
      '															<tr style="padding-top: 50px;">' +
      '															  <td style="padding-top: 50px;">' +
      '                                <a target="_blank" href=""> <img src="http://instampa.co/img/mailingFace.png" width="30" height="50" style="padding-left: 25px;"> </a>' +
      '                                <a target="_blank" href=""> <img src="http://instampa.co/img/mailingInta.png" width="30" height="40" style="position: relative; top: -7px; padding-left: 25px;"> </a>' +
      '															  </td>' +
      '															</tr>' +
      '														</table>' +
      '													</td>' +
      '												</tr>' +
      '											</table>' +
      '										</td>' +
      '									</tr>' +
      '									<tr><td height="28"></td></tr>' +
      '								</table>' +
      '							</td>' +
      '						</tr>' +
      '					</table>' +
      '					<!-- module 4 -->' +
      '				</td>' +
      '			</tr>' +
      '			<!-- fix for gmail -->' +
      '			<tr>' +
      '				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>' +
      '			</tr>' +
      '		</table>' +
      '	</body>' +
      '</html>'
  };
  mailgun.messages().send(data, function (error, body) {
    res.status(200)
      .json({
        status: "Succesfull",
        data: body,
        message: "succesfull"
      });
  });
}

module.exports = {
  mail1: mail1,
  nuevoPedido: nuevoPedido,
  pedidoRecibido: pedidoRecibido,
  mensajeContacto: mensajeContacto,
  pedidoEnviado: pedidoEnviado
}
