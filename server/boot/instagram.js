/**
 * Created by pipe on 17/04/17.
 */

var request = require("request");
var fs = require('fs');
var stream = fs.createWriteStream("my_file.txt");

var url = "https://api.instagram.com/oauth/";
var api = "https://api.instagram.com/v1/";
var clientID = "f9799594ff26444285823dbadd01b0c4";
var clientSecret = "1d96fd74e2494c2dae216cb8b37fa875";
var redirectUri = "http://instampa.co/api/instagram/step2";
var authRedUri = "http://instampa.co/api/instagram/token";
var responseType = "code";
var accessToken = "2137361936.f979959.f2922d5cba4444c093410dab3a71532c";


function step1Login(req, res) {
  var source = url+"authorize/?client_id="+clientID+"redirect_uri="+redirectUri+"&response_type="+responseType;
  request.get(source,
    function (err, body) {
      if(!err){
        res.status(200)
          .json({
            status: "Succesfull",
            data: body,
            message: "succesfull"
          })
      }
      else {
        console.log(err);
      }
    }
  )
}

function step2Login(req, res) {
  var source = url+"access_token";
  var data = {client_secret: clientSecret, grant_type: "authorization_code", redirect_uri: authRedUri, code: req.query.code};
  request.post({url:source, form: data}, function (err, httpResponse, body) {
    if(!err){
      stream.once('open', function(fd) {
        stream.write(body);
        stream.write(httpResponse);
        stream.end();
      });
      accessToken = body.access_token;
      res.status(200)
        .json({
          status: "Succesfull",
          data: body,
          message: "succesfull"
        })
    }
    else {
      console.log(err);
    }
  });
}

function getMedia(req, res) {
 var source = api + "users/self/media/recent/?access_token="+ accessToken;
 request.get(source,
  function (err, body) {
   if(!err){
     res.status(200)
       .json({
         status: "Succesfull",
         data: body.body
       })
   }
   else{
     console.log(err);
   }
 })
}

module.exports = {
  step1Login: step1Login,
  step2Login: step2Login,
  getMedia: getMedia
}
