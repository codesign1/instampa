var fs = require("fs");
var JSZip = require("jszip");

function zipPhoto(req, res) {
  var jsZip = new JSZip();
  var fotos = req.body.fotos;
  var nombre = req.body.referencia;
  fotos.forEach(function (foto) {
    var stream = fs.createReadStream("././"+foto.imagen);
    jsZip.file(foto.imagen.split("/")[4], stream);
  });
  jsZip
    .generateNodeStream({type:'nodebuffer',streamFiles:true})
    .pipe(fs.createWriteStream('././client/img/fotos_pedidos/fotos_pedidos/'+nombre+'.zip'))
    .on('finish', function () {
      // JSZip generates a readable stream with a "end" event,
      // but is piped here in a writable stream which emits a "finish" event.
      console.log("out.zip written.");
    });
  res.status(200)
    .json({
      status: "Succesfull",
      data: nombre
    })
}

module.exports = {
  zipPhoto: zipPhoto
};
