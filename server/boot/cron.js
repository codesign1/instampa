module.exports = function(app) {
    var loopback = require('loopback');
    var schedule = require('node-schedule');

    var ds = loopback.createDataSource({
        connector: require('loopback-component-storage'),
        provider: 'filesystem',
        root: '././client/img/fotos_pedidos',
        maxFileSize: "52428800"
    });

    var container = ds.createModel('fotos_pedidos');
    app.model(container);

    var files = app.models.fotos_pedidos;
    var fotosModel = app.models.foto;

    var archivos = [];
    var archivosABorrar = [];

    // var modelNames = Object.keys(app.models);
    // var models = [];
    // modelNames.forEach(function(m) {
    //     var modelName = app.models[m].modelName;
    //     if (models.indexOf(modelName) === -1) {
    //         models.push(modelName);
    //     }
    // });

    // Cargue de fotos_pedidos y comparacion contra archivos

    function compararFotos() {
        fotosModel.find({}, function(err, res) {
            // body...
            if (err) {
                console.log(err);
            } else {
                archivos.forEach(function(file) {
                    var dias = Math.round((new Date() - new Date(file.atime)) / (1000 * 60 * 60 * 24));
                    if (dias > 14) {
                        var existe = res.find(function(foto) {
                            return foto.imagen.split("/")[4] == file.name;
                        })
                        if (existe) {

                        } else {
                            files.removeFile("fotos_pedidos", file.name);
                        }
                    }
                })
            }
        })
    }

    // Cargue de la informacion de los archivos

    function archiv(err, files) {
        if (err) {
            console.log(err)
        } else {
            archivos = files;
            compararFotos();
        }
    }

    var j = schedule.scheduleJob({ hour: 23, minute: 59, dayOfMonth: 1 }, function() {
        files.getFiles("fotos_pedidos", {}, archiv);
    })

};
