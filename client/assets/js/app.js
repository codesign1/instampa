(function () {

  'use strict';

  angular
    .module('app', ['auth0.lock', 'angular-jwt', 'ui.router', 'ngFileUpload', 'ui.bootstrap', 'ui.toggle', 'ismobile', 'ngSanitize', 'ngMd5'])
    .config(config);

  config.$inject = ['$stateProvider', 'lockProvider', '$urlRouterProvider', 'isMobileProvider', 'jwtOptionsProvider'];

  function config($stateProvider, lockProvider, $urlRouterProvider, isMobile, jwtOptionsProvider) {

    var paqueteFotos = isMobile.phone ? 'paqueteFotosMobile.html' : 'paqueteFotos.html';
    var marcos = isMobile.phone ? 'marcosMobile.html' : 'marcos.html';
    var empaque = isMobile.phone ? 'empaqueMobile.html' : 'empaque.html';
    var checkout = isMobile.phone ? 'checkoutMobile.html' : 'checkout.html';
    var fotos = isMobile.phone ? 'fotosMobile.html' : 'fotos.html';
    var caballetes = isMobile.phone ? 'caballetesMobile.html' : 'caballetes.html';
    var albmus = isMobile.phone ? 'albumsMobile.html' : 'albums.html';
    var cuerdas = isMobile.phone ? 'cuerdasMobile.html' : 'cuerdas.html';
    var postales = isMobile.phone ? 'postalesMobile.html' : 'postales.html';
    var empaquesPostales = isMobile.phone ? 'empaquePostalesMobile.html' : 'empaquePostales.html';
    var dashboard = isMobile.phone ? 'dashboardMobile.html' : 'dashboard.html';


    $stateProvider

      .state('home', {
        url: '/home',
        controller: 'HomeController',
        templateUrl: '/templates/usuario/home.html',
        controllerAs: 'vm'
      })

      .state('productos', {
        url: '/productos',
        controller: 'HomeController',
        templateUrl: '/templates/usuario/home.html',
        controllerAs: 'vm'
      })

      .state('paqueteFotos', {
        url: '/paqueteFotos/:idtipoproducto',
        controller: 'ProductController',
        templateUrl: '/templates/usuario/flujos/paqueteFotos/'+paqueteFotos,
        controllerAs: 'vm'
      })

      .state('marcos', {
        url: '/marcos/:idtipoproducto',
        controller: 'MarcosController',
        templateUrl: '/templates/usuario/flujos/marcos/'+marcos,
        controllerAs: 'vm'
      })

      .state('empaque', {
        url: '/empaque/:idtipoproducto',
        controller: 'PackingController',
        templateUrl: '/templates/usuario/flujos/empaque/'+empaque,
        controllerAs: 'vm'
      })

      .state('fotos', {
        url: '/fotos/:idtipoproducto',
        controller: 'FotosController',
        templateUrl: '/templates/usuario/flujos/fotos/'+fotos,
        controllerAs: 'vm'
      })

      .state('checkout', {
        url: '/checkout',
        controller: 'CheckoutController',
        templateUrl: '/templates/usuario/checkout/'+checkout,
        controllerAs: 'vm'
      })

      .state('caballetes', {
        url: '/caballetes/:idtipoproducto',
        controller: 'CaballetesController',
        templateUrl: '/templates/usuario/flujos/caballetes/'+caballetes,
        controllerAs: 'vm'
      })

      .state('galeria', {
        url: '/galeria',
        controller: 'InstagramController',
        templateUrl: '/templates/usuario/galeria.html',
        controllerAs: 'vm'
      })

      .state('albumes', {
        url: '/albums/:idtipoproducto',
        controller: 'AlbumsController',
        templateUrl: '/templates/usuario/flujos/albums/'+albmus,
        controllerAs: 'vm'
      })

      .state('cuerdasPinzas', {
        url: '/cuerdas/:idtipoproducto',
        controller: 'CuerdasController',
        templateUrl: '/templates/usuario/flujos/cuerdas/'+cuerdas,
        controllerAs: 'vm'
      })

      .state('response', {
        url: '/response/:datapayu',
        controller: 'ResponseController',
        templateUrl: '/templates/usuario/checkout/payU/response.html',
        controllerAs: 'vm'
      })

      .state('postales', {
        url: '/postales/:idtipoproducto',
        controller: 'PostalesController',
        templateUrl: '/templates/usuario/flujos/postales/'+postales,
        controllerAs: 'vm'
      })

      .state('empaquePostales', {
        url: '/empaquePostales',
        controller: 'PackingPostalesController',
        templateUrl: '/templates/usuario/flujos/postales/'+empaquesPostales,
        controllerAs: 'vm'
      })

      .state('politicas', {
        url: '/politicas/:idpolitica',
        controller: 'CheckoutController',
        templateUrl: '/templates/usuario/checkout/politicas.html',
        controllerAs: 'vm'
      })

      .state('contacto', {
        url: '/contacto',
        controller: 'ContactoController',
        templateUrl: '/templates/usuario/contacto.html',
        controllerAs: 'vm'
      })

      .state('dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        templateUrl: '/templates/admin/'+dashboard,
        controllerAs: 'vm'
      })

      .state('products', {
        url: '/products',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/products.html',
        controllerAs: 'vm'
      })

      .state('createProduct', {
        url: '/createProduct',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/create.html',
        controllerAs: 'vm'
      })

      .state('editProduct', {
        url: '/editProduct/:idproducto',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/create.html',
        controllerAs: 'vm'
      })

      .state('createPacking', {
        url: '/createPacking',
        controller: 'PackingBackController',
        templateUrl: '/templates/admin/packing/create.html',
        controllerAs: 'vm'
      })

      .state('editPacking', {
        url: '/editPacking/:idproducto',
        controller: 'PackingBackController',
        templateUrl: '/templates/admin/packing/create.html',
        controllerAs: 'vm'
      })

      .state('policy', {
        url: '/policy',
        controller: 'PolicyController',
        templateUrl: '/templates/admin/policy/policy.html',
        controllerAs: 'vm'
      })

      .state('categories', {
        url: '/categories',
        controller: 'CategoriesController',
        templateUrl: '/templates/admin/categories/categories.html',
        controllerAs: 'vm'
      })
    ;

    lockProvider.init({
      clientID: AUTH0_CLIENT_ID,
      domain: AUTH0_DOMAIN
    });

    $urlRouterProvider.otherwise('/home');

    jwtOptionsProvider.config({
      tokenGetter: function () {
        return localStorage.getItem('id_token');
      }
    });
  }

})();
