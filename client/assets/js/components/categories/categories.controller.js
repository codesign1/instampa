(function () {

  'use strict';

  angular
    .module('app')
    .controller('CategoriesController', CategoriesController);

  CategoriesController.$inject = ['authService', '$scope', '$state', 'CategoriesService', '$window', 'FileService', '$timeout'];

  function CategoriesController(authService, $scope, $state, CategoriesService, $window, FileService, $timeout) {

    var vm = this;
    vm.authService = authService;
    $scope.categoriaedit = {};

    CategoriesService.getCategories().then(function (res) {
      $scope.categories = res.data;
    });

    $scope.editCategories = function (idCategory, indexCategory) {
      var categoryToEdit = $scope.categoriaedit[indexCategory];
      if (categoryToEdit) {
        if (categoryToEdit.imagen) {
          FileService.uploadAdminFoto(categoryToEdit.imagen);
          categoryToEdit.imagen = "img/fotos_pedidos/admin_fotos/" + categoryToEdit.imagen.name;
        }
        // console.log(categoryToEdit);
        CategoriesService.editCategory(idCategory, categoryToEdit).then(function (res) {
          $scope.categoriaedit[indexCategory].saved = true;
          $timeout(function () {
            $scope.categoriaedit[indexCategory].saved = false;
          }, 5000);
        });
      }

    };

  }

}());
