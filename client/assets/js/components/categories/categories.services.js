(function(){
  'use strict';

  angular
    .module('app')
    .factory('CategoriesService', CategoriesService);

  CategoriesService.$inject = ['authService', '$http'];

  function CategoriesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlCategories = "/api/tipoproductos";

    function getCategories(){
      var source = urlCategories;
      return $http.get(source);
    }

    function editCategory(idCategory, category){
      var source = urlCategories+'/'+idCategory;
      return $http.patch(source, category);
    }

    return {
      getCategories : getCategories,
      editCategory : editCategory
    }

  }

}());
