/**
 * Created by pipe on 26/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductModalController', ProductModalController);

  ProductModalController.$inject = ['$uibModalInstance', 'product', 'DashboardService', '$scope'];

  function ProductModalController($uibModalInstance, product, DashboardService, $scope) {
    var vm = this;
    vm.product = product;

    vm.createZip = function (fotos, nombre) {
      DashboardService.createZip(fotos, nombre).then(function (res) {
        console.log(res);
        DashboardService.downloadZip(res.data).then(function (data) {
          console.log(data);
        });
      });
    }

    $scope.close = function(){
      $uibModalInstance.close();
    };

  }

}());
