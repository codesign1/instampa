/**
 * Created by pipe on 25/04/17.
 */

(function(){
  'use strict';

  angular
    .module('app')
    .factory('DashboardService', DashboardService);

  DashboardService.$inject = ['authService', '$http'];

  function DashboardService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlPedidos = "/api/pedidos";
    var urlProductoPedido = "/api/productospedidos";
    var urlDescarga = "/api/fotos_pedidos/fotos_pedidos/download/";
    var urlAccesorio = "/api/accesoriosproductos";

    function getPedidos() {
      var source = urlPedidos+'?filter[order]=id%20DESC';
      return $http.get(source);
    }

    function getProductoPedidosByIdPedido(idPedido) {
      var source = urlProductoPedido + "?filter[include]=productopedido_foto&" +
        "filter[include]=productopedido_producto&"+
        "filter[where][id_pedido]="+ idPedido +"&"+
        "filter[include]=productopedido_accesorioproductopedido&" +
        "filter[include]=productopedido_empaque&" +
        "filter[include]=productopedido_pedido";
      return $http.get(source);
    }

    function getProductoPedidos() {
      var source = urlProductoPedido + "?filter[include]=productopedido_foto&" +
        "filter[include]=productopedido_producto&" +
        "filter[include]=productopedido_accesorioproductopedido&" +
        "filter[include]=productopedido_empaque&" +
        "filter[include]=productopedido_pedido";
      return $http.get(source);
    }

    function createZip(fotos, reference) {
      var source = "/api/zip/createZip";
      var body = {fotos: fotos, referencia: reference}
      return $http.post(source, body);
    }

    function downloadZip(zip) {
      var source = urlDescarga+zip+".zip";
      return $http.get(source);
    }

    function cambiarEstado(estado, id) {
      var source = urlPedidos+"/"+id;
      return $http.patch(source, estado);
    }

    function getAccesorios() {
      var source = urlAccesorio;
      return $http.get(source);
    }

    function getProductoPedido(id) {
      var source = urlPedidos+"/"+id+"/pedido_productopedido";
      return $http.get(source);
    }

    function deleteAccesorioProdPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped +"/productopedido_accesorioproductopedido";
      return $http.delete(source);
      // return $http.get(source);
    };

    function deleteFotoProdPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped +"/productopedido_foto";
      return $http.delete(source);
      // return $http.get(source);
    };

    function borrarProductoPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped;
      return $http.delete(source);
      // return $http.get(source);
    };

    function borrarPedido(id) {
      var source = urlPedidos+"/"+id;
      return $http.delete(source);
    }

    return {
      getPedidos: getPedidos,
      getProductoPedidosByIdPedido: getProductoPedidosByIdPedido,
      getProductoPedidos: getProductoPedidos,
      createZip: createZip,
      cambiarEstado: cambiarEstado,
      getAccesorios: getAccesorios,
      getProductoPedido: getProductoPedido,
      deleteAccesorioProdPedido: deleteAccesorioProdPedido,
      deleteFotoProdPedido: deleteFotoProdPedido,
      borrarProductoPedido: borrarProductoPedido,
      borrarPedido: borrarPedido,
    }

  }

}());
