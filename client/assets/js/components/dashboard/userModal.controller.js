/**
 * Created by pipe on 25/04/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .controller('UserModalController', UserModalController);

  UserModalController.$inject = ['$uibModalInstance', 'user', '$scope'];

  function UserModalController($uibModalInstance, user, $scope) {
    var vm = this;
    vm.user = user;

    $scope.close = function(){
      $uibModalInstance.close();
    };

  }

}());
