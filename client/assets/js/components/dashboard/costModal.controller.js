/**
 * Created by pipe on 26/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CostModalController', CostModalController);

  CostModalController.$inject = ['$uibModalInstance', 'cost', '$scope'];

  function CostModalController($uibModalInstance, cost, $scope) {
    var vm = this;

    vm.cost = cost;
    console.log(cost);

    $scope.close = function(){
      $uibModalInstance.close();
    };
  }

}());

