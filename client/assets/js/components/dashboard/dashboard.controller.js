/**
 * Created by pipe on 25/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('DashboardController', DashboardController)
    .filter('DateFilter', DateFilter);

  DashboardController.$inject = ['authService', '$scope', '$state', '$stateParams', '$window', 'DashboardService', '$uibModal','$http'];

  function DashboardController(authService, $scope, $state, $stateParams, $window, DashboardService, $uibModal, $http) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);

    $scope.selectedPedido = {};
    $scope.estado = "";

    $scope.todosFilter = true;
    $scope.entregadosFilter = false;
    $scope.pendientesFilter = false;
    $scope.aprobadosFilter = false;

    $scope.filterType = function (tipo) {
      if(tipo == "todos"){
        $scope.todosFilter = true;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = false;
        $scope.estado = "";
      }else if(tipo == "entregados"){
        $scope.todosFilter = false;
        $scope.entregadosFilter = true;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = false;
        $scope.estado = "Entregado";
      }
      else if(tipo == "pendientes"){
        $scope.todosFilter = false;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = true;
        $scope.aprobadosFilter = false;
        $scope.estado = "Pendiente";
      }else{
        $scope.todosFilter = false;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = true;
        $scope.estado = "Aprobado";
      }
    };

    function loadDashBoard() {
      DashboardService.getProductoPedidos()
        .then(function (res) {
          $scope.productosPedidos = res.data;
          DashboardService.getAccesorios()
            .then(function (res) {
              $scope.accesorios = res.data;
              _.each($scope.productosPedidos, function (pedido) {
                pedido.accesorios =[];
                _.each(pedido.productopedido_accesorioproductopedido, function (acc) {
                  var accesorio = _.find($scope.accesorios, function (accs) {
                    return accs.id == acc.id_accesorioproducto;
                  });
                  pedido.accesorios.push(accesorio);
                });
              });
              DashboardService.getPedidos()
                .then(function (res) {
                  $scope.pedidos = res.data;
                  _.each($scope.pedidos, function (pedido) {
                    pedido.productosPedido = _.filter($scope.productosPedidos, function (producto) {
                      return pedido.id == producto.id_pedido;
                    });
                  });
                });
            });
        });
    }

    loadDashBoard();

    $scope.selectPedido = function (pedido) {
      $scope.selectedPedido = pedido;
    };

    $scope.openUserModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/userModal.html',
        controller: 'UserModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          user: function () {
            return $scope.selectedPedido;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function () {
      });
    };

    $scope.selectProduct = function (product) {
      $scope.selectedProduct = product;
    };

    $scope.openProductModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var nombre = "Pedido-"+$scope.selectedPedido.id+"-idProducto-"+$scope.selectedProduct.id;
      DashboardService.createZip($scope.selectedProduct.productopedido_foto, nombre).then(function (res) {
      });
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/productModal.html',
        controller: 'ProductModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          product: function () {
            return $scope.selectedProduct;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function (err) {
        console.log(err);
      });
    };

    $scope.openCostModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/costModal.html',
        controller: 'CostModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          cost: function () {
            return $scope.selectedPedido;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function () {
      });
    };

    $scope.entregarPedido = function (id, email, nombre) {
      var estado = {estado: "Entregado" };
      DashboardService.cambiarEstado(estado, id)
        .then(function (res) {
          
          // sendmail to customer
          var urlSendEmailCustomer = '/api/mailing/pedidoEnviado?nombre='+nombre+'&correo='+email;
          $http.get(urlSendEmailCustomer).then(function(res){
            console.log(res.data.message);
            loadDashBoard();
          });

        });
    }

    $scope.borrarPedido = function (id) {
      var dodelete = confirm('¿Estas seguro de eliminar este pedido?, Esta acción no se puede deshacer.');
      if(dodelete){
        DashboardService.getProductoPedido(id)
        .then(function (res) {
          var prdPeds = res.data;
          prdPeds.forEach(function(prod){
            console.log(prod.id);
            DashboardService.deleteAccesorioProdPedido(prod.id)
            .then(function (acc){
              console.log(acc.data);
              DashboardService.deleteFotoProdPedido(prod.id)
              .then(function(foto){
                console.log(foto.data);
                DashboardService.borrarProductoPedido(prod.id)
                .then(function(prodped){
                  console.log(prodped.data);
                  DashboardService.borrarPedido(id).then(function(){
                    console.log('pedido borrado');
                    loadDashBoard();
                  });
                });
              });
            });
          });
        });
      }
    }

  }

  function DateFilter() {
    function filterDate(items, date) {
      var pedidos = [];
      if(!date){
        return items;
      }
      var dateI = date;
      var dateF = new Date(date.getFullYear(), date.getMonth()+1, date.getDate());
      items.forEach(function (obj) {
        var fechaCreacion = new Date(obj.fecharecepcion);
        if(fechaCreacion >= dateI && fechaCreacion < dateF) {
          pedidos.push(obj);
        }
      });
      return pedidos;
    }
    return filterDate;
  }

}());
