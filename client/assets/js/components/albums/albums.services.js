(function(){
  'use strict';

  angular
    .module('app')
    .factory('AlbumService', AlbumService);

  AlbumService.$inject = ['authService', '$http'];

  function AlbumService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos;
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios
    }

  }

}());
