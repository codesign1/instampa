(function () {

  'use strict';

  angular
    .module('app')
    .controller('AlbumsController', AlbumsController);

  AlbumsController.$inject = ['authService', '$scope', '$state', 'AlbumService', 'HomeService', '$stateParams', '$window'];

  function AlbumsController(authService, $scope, $state, AlbumService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0, 0);

    $scope.productid = $stateParams.idtipoproducto;

    var carritoText = window.localStorage.getItem('carrito');
    var carrito = JSON.parse(carritoText) ? JSON.parse(carritoText) : [];

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id ==  $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    AlbumService.getProductosByTipoproducto($scope.productid).then(function (res) {
      $scope.products = res.data;

      productColors($scope.products, $scope.colors);
      $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
      $scope.selectedProduct = $scope.productsComplete[0];
      $scope.sigProduct = $scope.productsComplete[1];
    });

    AlbumService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    function productColors(products, colors){
      var colorsArray = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.selectedColor = $scope.productsComplete[0].colors[0];
    };

    $scope.continue = function () {
      $state.go('paqueteFotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());
