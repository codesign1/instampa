(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingBackController', PackingBackController);

  PackingBackController.$inject = ['authService', '$scope', '$state', '$http', 'PackingBackService', 'FileService', '$stateParams'];

  function PackingBackController(authService, $scope, $state, $http, PackingBackService, FileService, $stateParams) {

    var vm = this;
    vm.authService = authService;

    $scope.empaque = {};
    $scope.productidURL = $stateParams.idproducto;
    $scope.stateedition = false;
    $scope.pack = {};

    if($scope.productidURL){
      $scope.stateedition = true;
      PackingBackService.getPacking($scope.productidURL).then(function(res){
        $scope.pack = res.data;

        $scope.empaque.nombre = $scope.pack.nombre;
        $scope.empaque.precio = $scope.pack.precio;
        $scope.empaque.color = $scope.pack.color;
        $scope.empaque.color_code = $scope.pack.color_code;
        $scope.empaque.descripcion = $scope.pack.descripcion;
        $scope.empaque.imagen = $scope.pack.imagen;
        $scope.empaque.tipo_empaque = $scope.pack.tipo_empaque;
        $scope.empaque.id_tipoproducto = $scope.pack.id_tipoproducto;
        $scope.empaque.id = $scope.productidURL;
      });
    }else{
      $scope.stateedition = false;
    };

    PackingBackService.getTiposProducto().then(function (res) {
      $scope.productTypes = res.data;
    });

    $scope.createPacking = function(){
      if($scope.empaque.image){
        FileService.uploadAdminFoto($scope.empaque.image[0]);
        var image = "img/fotos_pedidos/admin_fotos/"+$scope.empaque.image[0].$ngfName;
      }else{
        var image = '';
      }

      var pack = {
        "nombre": $scope.empaque.nombre,
        "precio": $scope.empaque.precio,
        "color": $scope.empaque.color,
        "color_code": $scope.empaque.color_code,
        "descripcion": $scope.empaque.descripcion,
        "imagen": image,
        "tipo_empaque": $scope.empaque.tipo_empaque,
        "id_tipoproducto": $scope.empaque.id_tipoproducto,
        "habilitado" : false
      };

      PackingBackService.createPacking(pack).then(function(res){
        $state.go('products');
      });

    };

    $scope.editPacking = function(){

      if($scope.empaque.image){
        FileService.uploadAdminFoto($scope.empaque.image[0]);
        var image = "img/fotos_pedidos/admin_fotos/"+$scope.empaque.image[0].$ngfName;
      }else{
        var image = $scope.pack.imagen;
      }

      var pack = {
        "nombre": $scope.empaque.nombre,
        "precio": $scope.empaque.precio,
        "color": $scope.empaque.color,
        "color_code": $scope.empaque.color_code,
        "descripcion": $scope.empaque.descripcion,
        "imagen": image,
        "tipo_empaque": $scope.empaque.tipo_empaque,
        "id_tipoproducto": $scope.empaque.id_tipoproducto
      };

      PackingBackService.editPacking($scope.empaque.id, pack).then(function(res){
        $state.go('products');
      });

    };

  }

}());
