(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingBackService', PackingBackService);

  PackingBackService.$inject = ['authService', '$http'];

  function PackingBackService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProducto = "/api/tipoproductos";
    var urlPacking = "/api/empaques";

    function getTiposProducto(){
      var source = urlTipoProducto;
      return $http.get(source);
    }

    function createPacking(pack){
      var source = urlPacking;
      return $http.post(source, pack);
    }

    function getPacking(idpack) {
      var source = urlPacking+'/'+idpack;
      return $http.get(source);
    }

    function editPacking(idpack, pack){
      var source = urlPacking+'/'+idpack;
      return $http.patch(source, pack);
    }

    return {
      getTiposProducto : getTiposProducto,
      createPacking : createPacking,
      getPacking : getPacking,
      editPacking : editPacking
    }

  }

}());
