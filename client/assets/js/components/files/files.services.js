(function () {

  'use strict';

  angular
    .module('app')
    .factory('FileService', FileService);

  FileService.$inject = ['authService', '$http', 'Upload'];

  function FileService(authService, $http, Upload) {

    var vm = this;
    vm.authService = authService;

    function uploadImg(foto, ind) {
      // fetch(foto.$ngfDataUrl)
      //   .then(res => res.blob())
      //   .then(function (blob) {
          //blob.name = foto.$ngfName;
          var date = new Date();
          // var file = {};
          var id =  Math.random().toString(36).substr(2, 9);
          var name = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+foto.type.split("/")[1];

          //var file = Upload.rename(foto, name);
          file.upload = Upload.upload({
            url: 'api/fotos_pedidos/fotos_pedidos/upload',
            data: {
              file: foto
            },
          }).then(function(res, err){
            console.log(err);
          });
          return file.upload;
          file.upload.then(function (response) {}, function (response) {
            if (response.status > 0)
              var errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
        // })
    }

    function uploadAdminFoto(foto) {
      // fetch(foto.$ngfDataUrl)
      //   .then(res => res.blob())
      //   .then(function (blob) {
      //blob.name = foto.$ngfName;
      var file = {};
      file.upload = Upload.upload({
        url: 'api/fotos_pedidos/admin_fotos/upload',
        data: {
          file: foto
        },
      });
      return file.upload;
      file.upload.then(function (response) {}, function (response) {
        if (response.status > 0)
          var errorMsg = response.status + ': ' + response.data;
      }, function (evt) {
        // Math.min is to fix IE which reports 200% sometimes
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });
      // })
    }


    return {
      uploadImg: uploadImg,
      uploadAdminFoto: uploadAdminFoto
    }
  }

}());
