(function () {

  'use strict';

  angular
    .module('app')
    .controller('ResponseController', ResponseController);

  ResponseController.$inject = ['authService', '$scope', '$state', '$stateParams', '$window', '$location', 'md5', 'CheckoutService', 'DashboardService', '$locale', '$http', '$httpParamSerializerJQLike'];

  function ResponseController(authService, $scope, $state, $stateParams, $window, $location, md5, CheckoutService, DashboardService, $locale, $http, $httpParamSerializerJQLike) {

    $window.scrollTo(0, 0);

    $scope.idPedido = 0;

    $scope.datapayU = $location.search();

    var payU_data = CheckoutService.getDataPayU();
    CheckoutService.getPedidoByReference($scope.datapayU.referenceCode).then(function(res){
      $scope.idPedido = res.data[0].id;
      $scope.customername = res.data[0].nombre;
      $scope.table = "";

      $scope.response = $scope.datapayU.lapTransactionState;
      $scope.title = "";
      $scope.tx_value = $scope.datapayU.TX_VALUE.toString();
      var length = $scope.tx_value.length;
      var txValue = $scope.tx_value.slice(0,length-1);
      var firma_cadena = payU_data.apikey+ "~" +$scope.datapayU.merchantId+ "~" +$scope.datapayU.referenceCode+ "~" +txValue+ "~" +$scope.datapayU.currency+ "~" +$scope.datapayU.transactionState;
      $scope.firma_creada = md5.createHash(firma_cadena || '');

      if($scope.firma_creada == $scope.datapayU.signature){
        switch($scope.response){
          case 'APPROVED':
            $scope.title = "Compra exitosa";
            // var estado = {estado: "Aprobado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'DECLINED':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'ERROR':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'EXPIRED':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'PENDING':
            $scope.title = "Transacción pendiente";
            // var estado = {estado: "Pendiente"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
        }
        
      }else{
        $scope.title = "Ocurrió un error intentelo mas tarde.";
        if($scope.datapayU.confirm == 'true'){
          DashboardService.getProductoPedidosByIdPedido($scope.idPedido).then(function (res) {
            $scope.productosPedidos = res.data;
            DashboardService.getAccesorios().then(function (res) {
                $scope.accesorios = res.data;
                _.each($scope.productosPedidos, function (pedido) {
                  pedido.accesorios =[];
                  _.each(pedido.productopedido_accesorioproductopedido, function (acc) {
                    var accesorio = _.find($scope.accesorios, function (accs) {
                      return accs.id == acc.id_accesorioproducto;
                    });
                    pedido.accesorios.push(accesorio);
                  });
                });
                DashboardService.getPedidos().then(function (res) {
                    $scope.pedidos = res.data;
                    _.each($scope.pedidos, function (pedido) {
                      pedido.productosPedido = _.filter($scope.productosPedidos, function (producto) {
                        return pedido.id == $scope.idPedido;
                      });
                    });

                    _.each($scope.productosPedidos, function (pedidoprod) {
                      var formato = pedidoprod.productopedido_foto[0].formato ? 'Cuadrado' : 'Polaroid';
                      var accesories = "";
                      _.each(pedidoprod.accesorios, function (acces, i) {
                        if(i == pedidoprod.accesorios.length-1){
                          accesories += acces.color;
                        }else{
                          accesories += acces.color+' | ';
                        }
                      });
                      $scope.table += '<tr class="title" align="center" style="font:12px/17px Arial, Helvetica, sans-serif; color:#000; height: 3rem;"><td>'+ pedidoprod.productopedido_producto.nombre +'</td><td>'+ pedidoprod.productopedido_producto.numfotos +'</td><td>'+ formato +'</td><td>'+ accesories +'</td></tr>';
                    });
                    
                    var productosPedidosSerialized = $httpParamSerializerJQLike($scope.table);
                    
                    // sendmail to admin
                    // var urlSendEmail = '/api/mailing/nuevoPedido';
                    // $http.get(urlSendEmail).then(function(res){
                    //   console.log(res.data.message);
                      // sendmail to customer
                      var urlSendEmailCustomer = '/api/mailing/pedidoRecibido?nombre='+$scope.customername+'&correo='+$scope.datapayU.buyerEmail+'&productos='+productosPedidosSerialized;
                      $http.get(urlSendEmailCustomer).then(function(resp){
                        console.log(resp.data.message);
                      });
                    // });

                  });
              });
          });
        }
      }
    });
  }

}());
