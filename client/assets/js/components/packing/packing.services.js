(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingService', PackingService);

  PackingService.$inject = ['authService', '$http'];

  function PackingService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";

    function getPaquetesByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_empaque?[filter][where][habilitado]=true&filter[order]=color%20ASC';
      return $http.get(source);
    }

    return {
      getPaquetesByTipoproducto : getPaquetesByTipoproducto
    }

  }

}());
