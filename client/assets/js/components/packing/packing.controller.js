(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingController', PackingController);

  PackingController.$inject = ['authService', '$scope', '$state', 'PackingService', 'HomeService', '$stateParams', '$window'];

  function PackingController(authService, $scope, $state, PackingService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.packingGroups = [];
    $scope.tipoProducto = $stateParams.idtipoproducto;
    $scope.productid = $stateParams.idtipoproducto;
    $scope.selectIndexGroup = 0;
    $scope.polaroid = 'image1.png';

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;
      });

      PackingService.getPaquetesByTipoproducto($stateParams.idtipoproducto).then(function (res){
        $scope.paquetes = res.data;
        agruparEmpaques();
        $scope.colorSelected = $scope.packingGroups[$scope.selectIndexGroup][0];
        $scope.packingSelected = $scope.packingGroups[$scope.selectIndexGroup][0];
        $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
        if(isMobile.phone){
          $scope.selectPackMobile(0);
        }
      });
    }

    function agruparEmpaques(){

      var caja = [];
      var sobre = [];

      for(var i = 0; i < $scope.paquetes.length; i++){
        if($scope.paquetes[i].tipo_empaque == 1){
          sobre.push($scope.paquetes[i]);
        }else{
          caja.push($scope.paquetes[i]);
        }
      }

      var imageSobre = {'groupname' : 'Sobre', 'imagemainmobile': '/img/sobreMobile.png', 'imagemain': sobre[0].imagen};
      var imageCaja = {'groupname' : 'Caja', 'imagemainmobile' : '/img/cajaMobile.png', 'imagemain' : caja[0].imagen};

      sobre.push(imageSobre);
      caja.push(imageCaja);
      $scope.packingGroups.push(sobre, caja);
    }

    $scope.selectPack = function(index, indexgroup, lengthGroup){
      $scope.packingSelected = $scope.packingGroups[indexgroup][index];
      $scope.packingGroups[indexgroup][lengthGroup] = {'imagemain' : $scope.packingGroups[indexgroup][index].imagen};
      $scope.totalprice = $scope.packingSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.selectPackMobile = function(indexgroup){
      $scope.colorSelected = $scope.packingGroups[indexgroup][0];
      $scope.packingSelected = $scope.packingGroups[indexgroup];
      $scope.selectIndexGroup = indexgroup;
      $scope.selectColor(indexgroup);
    };

    $scope.selectColor = function(color){
      $scope.colorSelected = $scope.packingSelected[color];
      $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    }

    $scope.goBack = function(tipoProducto){
      $scope.carrito.splice($scope.carrito.length-1,1);
      var carro = {
        _id: 'carrito',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('paqueteFotos', {"idtipoproducto": tipoProducto});
    };

    $scope.continue = function(paquete){
      $scope.carrito[$scope.carrito.length-1].paquete = paquete;
      $scope.carrito[$scope.carrito.length-1].preciototal = $scope.totalprice;

      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go("checkout");
    };

  }

}());
