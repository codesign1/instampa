(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['authService', '$scope', '$state', '$http', 'ProductsService', 'FileService', '$stateParams'];

  function ProductsController(authService, $scope, $state, $http, ProductsService, FileService, $stateParams) {

    var vm = this;
    vm.authService = authService;
    $scope.idSelectedType = 0;
    $scope.products = [];
    $scope.idempaques = 0;
    $scope.newproduct = {};
    $scope.newproduct.costoenvio = [];
    $scope.accesories = [];
    $scope.priceshipping = [];
    $scope.productidURL = $stateParams.idproducto;
    $scope.stateedition = false;
    $scope.toremoveacces = [];
    $scope.msg = '';
    $scope.continue = true;

    if($scope.productidURL){
      $scope.stateedition = true;
      ProductsService.getProduct($scope.productidURL).then(function(res){
        var product = res.data;
        var costoenvio = product.producto_costoenvio;
        $scope.newproduct.nombre = product.nombre;
        $scope.newproduct.descripcion = product.descripcion;
        $scope.newproduct.fotos = product.numfotos;
        $scope.newproduct.tipoproducto = product.id_tipoproducto;
        $scope.newproduct.precio = product.precio;
        $scope.newproduct.accesorio = product.producto_accesorioproducto;
        $scope.accesories = product.producto_accesorioproducto;
        $scope.priceshipping = product.producto_costoenvio;
        $scope.newproduct.costoenvio = product.producto_costoenvio;
      });
    }else{
      $scope.stateedition = false;
      $scope.newproduct.tipoproducto = 1;
      $scope.accesories= [{
        "nombre": "",
        "imagen": "",
        "color": "",
        "color_code": "",
        "detalle": "",
        "tipo": 1,
        "id_producto": $scope.idSelectedType
      }];
      $scope.priceshipping = [
        {"id" : 0,"precio" : 0,"zona" : 'bogota'},
        {"id" : 0,"precio" : 0,"zona" : 'costa'},
        {"id" : 0,"precio" : 0,"zona" : 'valledupar/Monteria'},
        {"id" : 0,"precio" : 0,"zona" : 'otra'}
      ];

    };

    ProductsService.getTiposProducto().then(function (res) {
      $scope.productTypes = res.data;
      $scope.idempaques = $scope.productTypes.length+1;
      $scope.idSelectedType = $scope.productTypes[0].id;
      $scope.selectType($scope.idSelectedType);
    });


    $scope.selectType = function(idtype){
      if(idtype == $scope.idempaques){
        $scope.idSelectedType = $scope.idempaques;
        ProductsService.getEmpaques().then(function (res){
          $scope.products = res.data;
        });
        $scope.isempaque = true;
      }else{
        $scope.idSelectedType = idtype;
        getProducts($scope.idSelectedType);
      }
    }

    function getProducts(idtype){
      ProductsService.getProductsByTipo(idtype).then(function (res){
        $scope.products = res.data;
      });
      $scope.isempaque = false;
    };

    // DELETE

    $scope.deleteProduct = function(idProduct){
      if(confirm('Realmente deseas eliminar este producto, esta acción no se puede deshacer')){
        ProductsService.deleteAccesoryByProduct(idProduct).then(function(){
          ProductsService.deleteShippingPriceByProduct(idProduct).then(function(){
            ProductsService.deleteProduct(idProduct).then(function(){
              $scope.selectType($scope.idSelectedType);
            });
          });
        });
      };
    };

    $scope.addAccesory = function(){
      var voidAccesory = [{
        "nombre": "",
        "imagen": "",
        "color": "",
        "color_code": "",
        "detalle": "",
        "tipo": 1,
        "id_producto": $scope.idSelectedType
      }];
      $scope.accesories.push(voidAccesory);
    };

    $scope.removeAccesory = function(index){
      if(confirm('Realmente deseas eliminar este accesorio, esta acción no se puede deshacer')){
        var toremove = {
          id:$scope.accesories[0].id
        };
        $scope.toremoveacces.push(toremove);
        $scope.accesories.splice(index, 1);
      };
    };

    $scope.changeEstatus = function(idProduct, habilitado, empaque){
      var product_status = {
        "habilitado": !habilitado
      };
      if(empaque){
        ProductsService.changeEmpaqueStatus(idProduct, product_status).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      }else{
        ProductsService.changeProductStatus(idProduct, product_status).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      }

    };

    // CREATE

    $scope.validateCreate = function(){
      if($scope.newproduct.accesorio){
        var countAccesorios = Object.keys($scope.newproduct.accesorio).length;
        var cuerdas = 0;
        var pinzas = 0;
        if($scope.newproduct.tipoproducto == 4){
          if(countAccesorios > 1 ){
            for (var key in $scope.newproduct.accesorio) {
              if($scope.newproduct.accesorio[key].tipo == 1){
                cuerdas += 1; 
              }else{
                pinzas += 1;
              }
            }
            if(cuerdas > 0 && pinzas > 0){
              $scope.msg = '';
              return true;
            }else{
              $scope.msg = 'Debes agregar un accesorio pinzas y uno cuerda como mínimo.';
              return false;
            }
          }else{
            $scope.msg = 'Debes agregar un accesorio pinzas y uno cuerda como mínimo.';
            return false;
          }
        }else if($scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
          if(countAccesorios > 0 ){
            $scope.msg = '';
            return true;
          }else{
            $scope.msg = 'Debes agregar un color como mínimo.';
            return false;
          }
        }else{
          $scope.msg = '';
          return true;
        }
        
      }else{
        $scope.msg = 'Debes agregar un accesorio como mínimo.';
        return false;
      }
    }

    $scope.createProduct = function(){
      $scope.continue = true;

      if($scope.newproduct.tipoproducto == 4 || $scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
        $scope.continue = $scope.validateCreate()
      }

      if($scope.continue){
        if($scope.newproduct.tipoproducto == 5){
          var image = "/img/fotos_pedidos/admin_fotos/albums/album.png";
        }else if($scope.newproduct.tipoproducto == 6){
          var image = "img/fotos_pedidos/admin_fotos/postales/postal.svg";
        }else{
          var image = "x";
        }
        var productdb = {
          "nombre": $scope.newproduct.nombre,
          "descripcion": $scope.newproduct.descripcion,
          "imagen": image,
          "numfotos": $scope.newproduct.fotos,
          "precio": $scope.newproduct.precio,
          "id_tipoproducto": $scope.newproduct.tipoproducto,
          "habilitado" : false

        }

        ProductsService.createProduct(productdb).then(function (res){
          var idproductdb = res.data.id;

          // ACCESORIOS
          _.each($scope.newproduct.accesorio, function(accesorio){
            if(accesorio.fileaccesorio){
              FileService.uploadImg(accesorio.fileaccesorio[0]);
              var imageaccesorio = "img/fotos_pedidos/admin_fotos/"+accesorio.fileaccesorio[0].$ngfName;
            }else{
              var imageaccesorio = null;
            }

            var accesorydb = {
              "nombre": accesorio.nombre,
              "imagen": imageaccesorio,
              "color": accesorio.color,
              "color_code": accesorio.color_code,
              "detalle": accesorio.detalle,
              "tipo": accesorio.tipo
            }

            ProductsService.createAccesory(idproductdb, accesorydb).then(function(res){});
          });

          // COSTOS DE ENVIO
          _.each($scope.newproduct.costoenvio, function(costo, index){
            var zonas = {0 : 'bogota', 1: 'costa', 2: 'valledupar/Monteria', 3: 'otra'};
            var shippingPrice = {
              "zona" : zonas[index],
              "precio": costo.precio
            };

            ProductsService.createShippingPrice(idproductdb, shippingPrice).then(function(res){});
          });

        });

        $state.go('products');
      }

    };

    // EDIT

    $scope.editProduct = function(){
      $scope.continue = true;

      if($scope.newproduct.tipoproducto == 4 || $scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
        $scope.continue = $scope.validateCreate()
      }

      if($scope.continue){
        if($scope.newproduct.tipoproducto == 5){
          var image = "/img/fotos_pedidos/admin_fotos/albums/album.png";
        }else if($scope.newproduct.tipoproducto == 6){
          var image = "img/fotos_pedidos/admin_fotos/postales/postal.svg";
        }else{
          var image = "x";
        }
        var editproductdb = {
          "nombre": $scope.newproduct.nombre,
          "descripcion": $scope.newproduct.descripcion,
          "imagen": image,
          "numfotos": $scope.newproduct.fotos,
          "precio": $scope.newproduct.precio,
          "id_tipoproducto": $scope.newproduct.tipoproducto
        };

        ProductsService.editProduct($scope.productidURL, editproductdb);

        // ACCESORIOS
        _.each($scope.newproduct.accesorio, function(accesorio, index){
          if(accesorio.fileaccesorio){
            FileService.uploadAdminFoto(accesorio.fileaccesorio[0]);
            var imageaccesorio = "img/fotos_pedidos/admin_fotos/"+accesorio.fileaccesorio[0].$ngfName;
          }else{
            var imageaccesorio = $scope.newproduct.accesorio[index].imagen;
          }

          var accesorydb = {
            "nombre": accesorio.nombre,
            "imagen": imageaccesorio,
            "color": accesorio.color,
            "color_code": accesorio.color_code,
            "detalle": accesorio.detalle,
            "tipo": accesorio.tipo,
            "id_producto" : $scope.productidURL
          }
          if(accesorio.id){
            ProductsService.editAccesory(accesorio.id, accesorydb).then(function(res){});
          }else{
            ProductsService.createAccesory($scope.productidURL, accesorydb).then(function(res){});
          }

        });

        _.each($scope.toremoveacces, function(accesorio){
          ProductsService.deleteAccesory(accesorio.id).then(function(res){});
        });

        // COSTOS DE ENVIO
        _.each($scope.newproduct.costoenvio, function(costo){
          var shippingPrice = {
            "zona" : costo.zona,
            "precio": costo.precio,
            "id_producto" : $scope.productidURL
          };

          ProductsService.editShippingPrice(costo.id, shippingPrice).then(function(res){});
        });

        $scope.newproduct = [];
        $state.go('products');
      }
      
    };

    // DELETE PACK

    $scope.deletePack = function(idPack){
      if(confirm('Realmente deseas eliminar este empaque, esta acción no se puede deshacer')){
        ProductsService.deletePack(idPack).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      };
    };

  }

}());
