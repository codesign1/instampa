(function(){
  'use strict';

  angular
    .module('app')
    .factory('ProductsService', ProductsService);

  ProductsService.$inject = ['authService', '$http'];

  function ProductsService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProducto = "/api/tipoproductos";
    var urlEmpaques = "/api/empaques";
    var urlProductos = "/api/productos";
    var urlAccesories = "/api/accesoriosproductos";
    var urlShippingPrice = "/api/costosenvios";

    function getTiposProducto(){
      var source = urlTipoProducto;
      return $http.get(source);
    }

    function getProductsByTipo(idType){
      var source = urlTipoProducto+'/'+idType+'/tipoproducto_producto?filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getEmpaques(){
      var source = urlEmpaques;
      return $http.get(source);
    }

    // Create

    function createProduct(product){
      var source = urlProductos;
      return $http.post(source, product);
    }

    function createAccesory(idProduct, accesory){
      var source = urlProductos+'/'+idProduct+'/producto_accesorioproducto';
      return $http.post(source, accesory);
    }

    function createShippingPrice(idProduct, costos){
      var source = urlProductos+'/'+idProduct+'/producto_costoenvio';
      return $http.post(source, costos);
    }

    // Edit

    function getProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/?filter[include]=producto_accesorioproducto&'+
      'filter[include]=producto_costoenvio';
      return $http.get(source);
    }

    function editProduct(idproduct, product){
      var source = urlProductos+'/'+idproduct;
      return $http.patch(source, product);
    }

    function editAccesory(idAccesory, accesory){
      var source = urlAccesories+'/'+idAccesory;
      return $http.patch(source, accesory);
    }

    function deleteAccesory(idAccesory){
      var source = urlAccesories+'/'+idAccesory;
      return $http.delete(source);
    }

    function editShippingPrice(idShippingPrice, ShippingPrice){
      var source = urlShippingPrice+'/'+idShippingPrice;
      return $http.put(source, ShippingPrice);
    }

    function changeProductStatus(idProduct, product){
      var source = urlProductos+'/'+idProduct;
      return $http.patch(source, product);
    }

    function changeEmpaqueStatus(idProduct, product){
      var source = urlEmpaques+'/'+idProduct;
      return $http.patch(source, product);
    }

    // DELETE

    function deleteAccesoryByProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/producto_accesorioproducto';
      return $http.delete(source);
    }

    function deleteShippingPriceByProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/producto_costoenvio';
      return $http.delete(source);
    }

    function deleteProduct(idProduct){
      var source = urlProductos+'/'+idProduct;
      return $http.delete(source);
    }

    function deletePack(idPack){
      var source = urlEmpaques+'/'+idPack;
      return $http.delete(source);
    }

    return {
      getTiposProducto : getTiposProducto,
      getProductsByTipo : getProductsByTipo,
      getEmpaques : getEmpaques,
      createProduct : createProduct,
      createAccesory : createAccesory,
      createShippingPrice : createShippingPrice,
      getProduct : getProduct,
      editProduct : editProduct,
      editAccesory : editAccesory,
      deleteAccesory : deleteAccesory,
      editShippingPrice : editShippingPrice,
      changeProductStatus : changeProductStatus,
      changeEmpaqueStatus : changeEmpaqueStatus,
      deleteAccesoryByProduct : deleteAccesoryByProduct,
      deleteShippingPriceByProduct : deleteShippingPriceByProduct,
      deleteProduct : deleteProduct,
      deletePack : deletePack

    }

  }

}());
/**
 * Created by E5-471-3541 on 22/02/2017.
 */
