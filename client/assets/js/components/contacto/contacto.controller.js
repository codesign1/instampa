(function () {

  'use strict';

  angular
    .module('app')
    .controller('ContactoController', ContactoController);

  ContactoController.$inject = ['authService', '$scope', '$state', '$http', '$stateParams', '$window'];

  function ContactoController(authService, $scope, $state, $http, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);
    $scope.contacto = {};
    $scope.sendmessage = '';
    
    $scope.send = function(){
      var urlContacto = '/api/mailing/mensajeContacto?nombre='+$scope.contacto.nombre+'&correo='+$scope.contacto.correo+'&mensaje='+$scope.contacto.mensaje;
      return $http.get(urlContacto).then(function(res){
        $scope.contacto = {};
        $scope.sendmessage = res.data.message;
      });
      
    }

  }

}());
