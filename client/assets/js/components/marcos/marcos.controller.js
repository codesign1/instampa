(function () {

  'use strict';

  angular
    .module('app')
    .controller('MarcosController', MarcosController);

  MarcosController.$inject = ['authService', '$scope', '$state', 'MarcosService', 'HomeService', '$stateParams', '$window'];

  function MarcosController(authService, $scope, $state, MarcosService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    MarcosService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;

      MarcosService.getProdAccesorry($scope.products[0].id).then(function (res) {
        $scope.colors = res.data;
        productColors($scope.products, $scope.colors);
        $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
        $scope.selectedProduct = $scope.productsComplete[0];
        $scope.sigProduct = $scope.productsComplete[1];
      });

    });

    function productColors(products, colors){
      var colorsArray = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.selectedColor = $scope.productsComplete[0].colors[0];
    };

    function newProduct(products, colors, id) {

    }

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == productsCompleteLength -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];
      $scope.selectedColor = $scope.selectedProduct.colors[0];
      getPrevNextProduct(productsCompleteLength);

    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = productsCompleteLength-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];
      $scope.selectedColor = $scope.selectedProduct.colors[0];
      getPrevNextProduct(productsCompleteLength);

    };

    function getPrevNextProduct(productsCompleteLength){
      MarcosService.getProdAccesorry($scope.selectedProduct.product.id).then(function (res) {
        $scope.colors = res.data;
        $scope.selectedProduct.colors = res.data;
        productColors($scope.products, $scope.colors);
        if($scope.idproduct == productsCompleteLength -1){
          $scope.antProduct = $scope.productsComplete[$scope.idproduct - 1];
          $scope.sigProduct = $scope.productsComplete[0];
        }else if($scope.idproduct == 0){
          $scope.antProduct = $scope.productsComplete[productsCompleteLength -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        }else{
          $scope.antProduct = $scope.productsComplete[$scope.idproduct -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        };
        $scope.selectedColor = $scope.productsComplete[$scope.idproduct].colors[0];
      });
    }

    $scope.selectPack = function(position, colorindex){
      $scope.selectedProduct = $scope.productsComplete[position];
      $scope.selectedColor = $scope.selectedProduct.colors[colorindex];
    };

    $scope.continue = function (product, color) {
      var colors = {'color' : color};
      var newcarrito = {
        'product': product,
        'color' : colors,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());
