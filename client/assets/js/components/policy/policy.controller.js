(function () {

  'use strict';

  angular
    .module('app')
    .controller('PolicyController', PolicyController);

  PolicyController.$inject = ['authService', '$scope', '$state', 'PolicyService', '$window'];

  function PolicyController(authService, $scope, $state, PolicyService, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.politicaedit = {};
    $scope.politicaedit2 = {};
    
    PolicyService.getPolicy().then(function(res){
        $scope.policy = res.data;
    });

    $scope.editPolicy = function(idPolicy, pos){
      var policytoedit = {};
      if(pos == 1){
        var policyEdit = {
          "nombre": $scope.politicaedit.nombre,
          "descripcion": $scope.politicaedit.descripcion,
          "obligatorio": $scope.policy[0].obligatorio == 1 ? true : false
        };
      }else{
        var policyEdit = {
          "nombre": $scope.politicaedit2.nombre,
          "descripcion": $scope.politicaedit2.descripcion,
          "obligatorio": $scope.policy[1].obligatorio == 1 ? true : false
        };
      }

      PolicyService.updatePolicy(idPolicy, policyEdit);
            
    };

  }

}());
