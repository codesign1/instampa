(function(){
  'use strict';

  angular
    .module('app')
    .factory('PolicyService', PolicyService);

  PolicyService.$inject = ['authService', '$http'];

  function PolicyService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlPolicy = "/api/politicas";

    function getPolicy(){
      var source = urlPolicy;
      return $http.get(source);
    }

    function updatePolicy(idPolicy, policy){
      var source = urlPolicy+'/'+idPolicy;
      return $http.patch(source, policy);
    }

    return {
      getPolicy : getPolicy,
      updatePolicy : updatePolicy
    }

  }

}());
