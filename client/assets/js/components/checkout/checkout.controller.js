(function () {

  'use strict';

  angular
    .module('app')
    .controller('CheckoutController', CheckoutController);

  CheckoutController.$inject = ['authService', '$scope', '$state', '$stateParams', 'CheckoutService', 'HomeService', 'FileService', '$window', 'md5'];

  function CheckoutController(authService, $scope, $state, $stateParams, CheckoutService, HomeService, FileService, $window, md5) {

    var vm = this;
    vm.authService = authService;
    $scope.loading = false;

    $scope.profile = JSON.parse(localStorage.getItem('profile'));
    //localStorage.removeItem('datosUsuario');
    console.log(localStorage.getItem('datosUsuario'));
    $scope.pedido = JSON.parse(localStorage.getItem('datosUsuario'));

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.carrito = doc.cart;
      $scope.doc = doc;
      $scope.$apply();
      $scope.load();
    }).catch(function (err) {
      console.log(err);
    });


    $window.scrollTo(0, 0);

    $scope.totalprice = 0;
    $scope.departaments = [];
    $scope.ciudades = [];
    $scope.disclaimer_active = false;
    $scope.shipingPrice = 0;
    $scope.costos = [];
    $scope.envioexpress = false;
    $scope.iva = 0.19;
    $scope.payUData = [];
    $scope.subtotal_payU = 0;
    $scope.ivaSubtotal = 0;
    $scope.total = 0;
    $scope.description = "";
    $scope.referenceCode = "";
    $scope.signature = "";


    $scope.idpolitica = $stateParams.idpolitica;
    $scope.politica = {};

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
    });

    CheckoutService.getPoliticas().then(function (res) {
      $scope.politicas = res.data;
    });

    if($scope.idpolitica != null){
      CheckoutService.getPoliticasById($scope.idpolitica).then(function (res) {
        $scope.politica = res.data;
      });
    }

    CheckoutService.getShippingPrice().then(function (res) {
      $scope.costosEnvios = res.data;
    });

    $scope.payUData = CheckoutService.getDataPayU();

    $scope.infoPayU = function () {
      var date = new Date();
      var reference = md5.createHash(date.toString() || '');
      var signatureStr = '';

      signatureStr = $scope.payUData.apikey + "~" + $scope.payUData.merchantId + "~" + reference + "~" + $scope.total + "~COP";

      $scope.description = "Venta Instampa " + date;
      $scope.referenceCode = reference;
      $scope.signature = md5.createHash(signatureStr || '');
    };

    $scope.saveDatos = function (pedido) {
      localStorage.setItem("datosUsuario", JSON.stringify(pedido));
    }

    $scope.departaments = CheckoutService.getDepartaments();

    $scope.changeDep = function (dep) {
      $scope.ciudades = CheckoutService.getDepartaments(dep);

      if(dep.zona != 'x'){
        $scope.carrito.forEach(function (elem) {
          var costo = $scope.costosEnvios.find(function (costo) {
            return costo.id_producto == elem.product.id && costo.zona == dep.zona;
          });
          $scope.costos.push(costo);
        });
        var mayor = _.max($scope.costos, function (cost) {
          return cost.precio;
        });

        $scope.shipingPrice = mayor.precio;

      }else{
        $scope.carrito.forEach(function (elem) {
          var costo = $scope.costosEnvios.find(function (costo) {
            return costo.id_producto == elem.product.id && costo.zona == dep.zona;
          });
          $scope.costos.push(costo);
        });

        $scope.shipingPrice = 0;

      }

      $scope.costos = [];
      $scope.subtotal_payU = $scope.totalprice + $scope.shipingPrice;
      $scope.ivaSubtotal = $scope.subtotal_payU * $scope.iva;
      $scope.total = ($scope.subtotal_payU * $scope.iva) + $scope.subtotal_payU;
      $scope.envioexpress = false;
      $scope.infoPayU();

    };

    $scope.envioExpress = function (dep) {
      if ($scope.envioexpress) {
        if (dep.zona == 'bogota') {
          $scope.shipingPrice = $scope.shipingPrice + 5000;
        } else {
          $scope.shipingPrice = $scope.shipingPrice + 10000;
        }
      } else {
        if (dep.zona == 'bogota') {
          $scope.shipingPrice = $scope.shipingPrice - 5000;
        } else {
          $scope.shipingPrice = $scope.shipingPrice - 10000;
        }
      }
      $scope.subtotal_payU = $scope.totalprice + $scope.shipingPrice;
      $scope.ivaSubtotal = $scope.subtotal_payU * $scope.iva;
      $scope.total = ($scope.subtotal_payU * $scope.iva) + $scope.subtotal_payU;
      $scope.infoPayU();
    };

    $scope.displayDisclaimer = function (state) {
      $scope.disclaimer_active = state;
    };

    $scope.delete = function (index, dep) {
      $scope.carrito.splice(index, 1);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          db.get('instampaCart').then(function (doc) {
            $scope.carrito = doc.cart;
            $scope.doc = doc;
            $scope.$apply();
          }).catch(function (err) {
            console.log(err);
          });
        }).catch(function (err) {
        console.log(err);
      });
      $scope.totalprice = 0;
      $scope.shipingPrice = 0;
      $scope.load();
      $scope.changeDep(dep);
    };

    $scope.load = function () {
      console.log($scope.carrito);
      $scope.carrito.forEach(function (prod, index, object) {
        if(prod.preciototal == 0){
          var none = {zona : 'x'};
          $scope.delete(index, none);
        }
        $scope.totalprice += prod.preciototal;
      });
      $scope.ivaSubtotal = $scope.totalprice*$scope.iva;
      $scope.subtotal_payU = $scope.ivaSubtotal;
    };

    $scope.clickPayUForm = function () {
      localStorage.removeItem('datosUsuario');
      $scope.loading = true;
      var creationDate = new Date();
      var creationDateString = creationDate.toISOString()
      var precio = $scope.totalprice + $scope.shipingPrice;
      var pedidoiva = $scope.ivaSubtotal;
      var totalpedido = precio + pedidoiva;
      var id_pedido = 0;
      var id_productopedido = 0;
      var profile = JSON.parse(localStorage.getItem('profile'));

      if($scope.pedido.bday != null){
        var bhday = $scope.pedido.bday.toString();
        // var bhday = bday.slice(4,15);
      }else{
        var bhday = "_";
      }

      var pedido_to_insert = {
        "nombre": $scope.pedido.nombre,
        "cedula": $scope.pedido.cedula,
        "direccion": $scope.pedido.direccion,
        "bday": bhday,
        "direccionenvio": $scope.pedido.direccionenvio,
        "telefono": $scope.pedido.telefono,
        "politicas": $scope.pedido.policy1 ? true : false,
        "usuarioinstagram": $scope.pedido.instagram,
        "pago": "a",
        "estado": "Pendiente",
        "fecharecepcion": creationDateString,
        "precio": precio,
        "iva": pedidoiva,
        "total": totalpedido,
        "costoenvio": $scope.shipingPrice,
        "id_user": $scope.pedido.id_user,
        "referencecode": $scope.referenceCode,
        "envioexpress": $scope.envioexpress,
        "ciudad": $scope.pedido.ciudad
      };

      CheckoutService.insertPedido(pedido_to_insert).then(function (res) {
        var id_pedido = res.data.id;
        // PRODUCTOS
        _.each($scope.carrito, function (prod, indexProd) {
          if (prod.paquete.id) {
            var empaque = prod.paquete.id;
          } else {
            var empaque = null;
          }
          var productopedido = {
            "id_producto": prod.product.id,
            "id_empaque": empaque,
            "id_pedido": id_pedido
          };
          console.log(prod);
          CheckoutService.insertProductoPedido(id_pedido, productopedido).then(function (prdped) {
            var id_productopedido = prdped.data.id;
            // ACCESORIOS
            _.each(prod.color, function (coloraccesory) {
              var accesorio = {
                "id_productopedido": id_productopedido,
                "id_accesorioproducto": coloraccesory.id
              };
              CheckoutService.insertAccesorios(id_productopedido, accesorio);
            });

            // FOTOS
            _.each(prod.fotos, function(foto, index){
              foto.id_productopedido = id_productopedido;
              if(prod.format == 'Cuadrado'){
                foto.formato = true;
              }else{
                foto.formato = false;
              }

              CheckoutService.insertFotos(foto)
                .then(function (res) {
                  if(index+1 == prod.fotos.length ){
                    if(indexProd+1 == $scope.carrito.length){
                      $scope.loading = false;
                      document.getElementById('submitcheckout').click();
                      document.getElementById('payUform').submit();
                      $scope.carrito = [];
                      var carro = {
                        _id: 'instampaCart',
                        _rev: $scope.doc._rev,
                        cart: $scope.carrito
                      };
                      db.put(carro)
                        .then(function (response) {
                          // handle response
                        }).catch(function (err) {
                        console.log(err);
                      });
                    }
                  }
                });
            });
          });
        });

      });
    };

  }

}());
