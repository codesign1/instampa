(function () {

  'use strict';

  angular
    .module('app')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['authService', '$scope', '$state', '$http', 'HomeService', '$stateParams', 'authManager'];

  function HeaderController(authService, $scope, $state, $http, HomeService, $stateParams, authManager) {

    var vm = this;
    vm.authService = authService;

    $scope.profile = vm.authService.getProfile();
    $scope.isAdmin = false;

    vm.authService.getProfileDeferred().then(function (profile) {
      vm.profile = profile;
      $scope.profile = profile;
    });

    if(authManager.isAuthenticated && JSON.parse(localStorage.getItem('profile'))){
      if (JSON.parse(localStorage.getItem('profile')).user_metadata) {
        var tipo = JSON.parse(localStorage.getItem('profile')).user_metadata.roles[0];
        if (tipo == 'admin') {
          $scope.isAdmin = true;
        }else{
          // $state.go('home');
        }
      } else {
        // $state.go('home');
      }
    }

  }

}());
