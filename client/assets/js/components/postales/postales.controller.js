(function () {

  'use strict';

  angular
    .module('app')
    .controller('PostalesController', PostalesController);

  PostalesController.$inject = ['authService', '$scope', '$state', 'PostalesService', 'HomeService', '$stateParams', '$window'];

  function PostalesController(authService, $scope, $state, PostalesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.cel = 12;

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    PostalesService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.antProduct = $scope.products[$scope.products.length-1];
      $scope.selectedProduct = $scope.products[0];
      $scope.sigProduct = $scope.products[1];
    });

    PostalesService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == productsCompleteLength -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.products[$scope.idproduct];

    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = productsCompleteLength-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.products[$scope.idproduct];

    };

    function getPrevNextProduct(productsCompleteLength){
      if($scope.idproduct == productsCompleteLength -1){
        $scope.antProduct = $scope.products[$scope.idproduct - 1];
        $scope.sigProduct = $scope.products[0];
      }else if($scope.idproduct == 0){
        $scope.antProduct = $scope.products[productsCompleteLength -1];
        $scope.sigProduct = $scope.products[$scope.idproduct + 1];
      }else{
        $scope.antProduct = $scope.products[$scope.idproduct -1];
        $scope.sigProduct = $scope.products[$scope.idproduct + 1];
      };
    }

    $scope.continue = function (product, text) {

      var newcarrito = {
        'product': product,
        'text' : text,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };
  }

}());
