(function () {

  'use strict';

  angular
    .module('app')
    .factory('HomeService', HomeService);

  HomeService.$inject = ['authService', '$http'];

  function HomeService(authService, $http) {

    var vm = this;
    vm.authService = authService;

    var url = "/api/tipoproductos";

    function getTiposProducto() {
      var source = url;
      return $http.get(source);
    }

    return{
      getTiposProducto: getTiposProducto
    }
  }

}());
