(function () {

  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['authService', '$scope', '$state', '$http', 'HomeService', '$stateParams', '$window', '$anchorScroll', '$location'];

  function HomeController(authService, $scope, $state, $http, HomeService, $stateParams, $window, $anchorScroll, $location) {

    var vm = this;
    vm.authService = authService;
    $scope.img = true;
    $scope.isFirefox == false;

    if($state.current.name == "productos"){
      $scope.img = false;
    }else{
      $window.scrollTo(0,0);
    }

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;

        // validate webBrowser
        var userAgent = window.navigator.userAgent;

        var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };

        var mybrowser = [];

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                mybrowser.push(key);
            }
        };

        if (mybrowser.length == 1) {
            if (mybrowser[0] == 'firefox' || mybrowser[0] == 'safari') {
                $scope.isFirefox = true;
            } else {
                $scope.isFirefox = false;
            }
        }

        if( /Android|iPhone|iPad|iPod/i.test(window.navigator.userAgent) ) {
            $scope.isFirefox = true;
        } else {
            $scope.isFirefox = false;
        }

      })
    }

    $scope.keydown = function(){
      $location.hash('products2');
      $anchorScroll();
    };

    $scope.showtooltip = function(event, item){
      var screenX = event.screenX;
      var screenY = event.screenY;

      angular.element(document.querySelector("#producto"+item)).css("left", screenX+'px');
      angular.element(document.querySelector("#producto"+item)).css("top", screenY-30+'px');

    };

    $scope.closeUserChrome = function(){
      $scope.isFirefox = false;
    };

    load();

  }

}());
