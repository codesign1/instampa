(function () {

  'use strict';

  angular
    .module('app')
    .controller('CaballetesController', CaballetesController);

  CaballetesController.$inject = ['authService', '$scope', '$state', 'CaballetesService', 'HomeService', '$stateParams', '$window'];

  function CaballetesController(authService, $scope, $state, CaballetesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.cel = 12;

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        $scope.fotos.push(i)
      }
    };

    CaballetesService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;

      CaballetesService.getProdAccesorry($scope.products[0].id).then(function (res) {
        $scope.colors = res.data;
        $scope.crearCantidadFotos(0);
        productColors($scope.products, $scope.colors);
        $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
        $scope.selectedProduct = $scope.productsComplete[0];
        $scope.sigProduct = $scope.productsComplete[1];
      });
    });

    CaballetesService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    function productColors(products, colors){
      var colorsArray = [];
      $scope.selectedColor = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.fotos.forEach(function (foto) {
        $scope.selectedColor.push($scope.productsComplete[0].colors[0]);
      });
    };

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == $scope.products.length -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };
      $scope.crearCantidadFotos($scope.idproduct);
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];

      if($scope.selectedProduct.product.numfotos < 5){
        $scope.cel = 12/$scope.selectedProduct.product.numfotos;
      }else{
        $scope.cel = 2;
      }

      $scope.selectedColor[idProduct+1] = $scope.selectedProduct.colors[0];
    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = $scope.products.length-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      $scope.crearCantidadFotos($scope.idproduct);
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];

      if($scope.selectedProduct.product.numfotos < 5){
        $scope.cel = 12/$scope.selectedProduct.product.numfotos;
      }else{
        $scope.cel = 2;
      }

      $scope.selectedColor[idProduct+1] = $scope.selectedProduct.colors[0];
    };

    function getPrevNextProduct(productsCompleteLength){
      CaballetesService.getProdAccesorry($scope.selectedProduct.product.id).then(function (res) {
        $scope.colors = res.data;
        $scope.selectedProduct.colors = res.data;
        productColors($scope.products, $scope.colors);
        if($scope.idproduct == productsCompleteLength -1){
          $scope.antProduct = $scope.productsComplete[$scope.idproduct - 1];
          $scope.sigProduct = $scope.productsComplete[0];
        }else if($scope.idproduct == 0){
          $scope.antProduct = $scope.productsComplete[productsCompleteLength -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        }else{
          $scope.antProduct = $scope.productsComplete[$scope.idproduct -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        };
      });

    }

    $scope.selectPack = function(position, colorindex){
      $scope.selectedColor[position] = $scope.selectedProduct.colors[colorindex];
    };

    $scope.continue = function (product, color) {

      var newcarrito = {
        'product': product,
        'color' : color,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());
