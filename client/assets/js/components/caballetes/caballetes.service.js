(function(){
  'use strict';

  angular
    .module('app')
    .factory('CaballetesService', CaballetesService);

  CaballetesService.$inject = ['authService', '$http'];

  function CaballetesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";
    var urlProductos = "/api/productos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos+'?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    function getProdAccesorry(id) {
      var source = urlProductos + "/"+id+"/producto_accesorioproducto?filter[order]=nombre%20ASC";
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios,
      getProdAccesorry :getProdAccesorry
    }

  }

}());
