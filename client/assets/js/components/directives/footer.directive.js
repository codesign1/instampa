/**
 * Created by pipe on 21/02/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .directive('footerDirective', footerDirective);

  footerDirective.$inject = [];

  function footerDirective() {
    return{
      templateUrl: 'templates/footer.html'
    }
  }

}());
