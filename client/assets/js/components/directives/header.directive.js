/**
 * Created by pipe on 21/02/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .directive('headerDirective', headerDirective);

  headerDirective.$inject = [];

  function headerDirective() {
    return{
      templateUrl: 'templates/header.html'
    }
  }

}());
