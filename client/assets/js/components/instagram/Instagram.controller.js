(function () {

  'use strict';

  angular
    .module('app')
    .controller('InstagramController', InstagramController);

  InstagramController.$inject = ['authService', '$scope', '$state', '$http', 'InstagramService', '$stateParams', '$window'];

  function InstagramController(authService, $scope, $state, $http, InstagramService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);

    function load() {
      InstagramService.getFotos().then(function (res) {
        $scope.fotos = JSON.parse(res.data.data).data;
        console.log($scope.fotos);
      })
    }

    load();

  }

}());
