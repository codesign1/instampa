(function () {

  'use strict';

  angular
    .module('app')
    .factory('InstagramService', InstagramService);

  InstagramService.$inject = ['authService', '$http'];

  function InstagramService(authService, $http) {

    var vm = this;
    vm.authService = authService;

    var url = "/api/instagram";

    function getFotos() {
      var source = url+"/getselfmedia";
      return $http.get(source);
    }

    return{
      getFotos: getFotos
    }
  }

}());
