(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingPostalesController', PackingPostalesController);

  PackingPostalesController.$inject = ['authService', '$scope', '$state', 'PackingPostalesService', 'HomeService', '$stateParams', '$window'];

  function PackingPostalesController(authService, $scope, $state, PackingPostalesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.packingGroups = [];
    $scope.tipoProducto = $stateParams.idtipoproducto;
    $scope.selectIndexGroup = 0;

    var carritoText = window.localStorage.getItem('carrito');
    $scope.carritoJSON = JSON.parse(carritoText);

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;
      });

      PackingPostalesService.getPaquetesByTipoproducto($scope.carrito[$scope.carrito.length-1].product.id_tipoproducto).then(function (res){
        $scope.paquetes = res.data;
        $scope.selectColor($scope.paquetes[0]);

      }).catch(function (err) {
        console.log(err);
      });
    }

    $scope.selectColor = function(color){
      $scope.packingSelected = color;
      $scope.colorSelected = color;
      $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.goBack = function(){
      var carritoText = JSON.stringify($scope.carritoJSON);
      window.localStorage.setItem('carrito', carritoText);
      $state.go('fotos');
    };

    $scope.continue = function(paquete){
      $scope.carrito[$scope.carrito.length-1].paquete = paquete;
      $scope.carrito[$scope.carrito.length-1].preciototal = $scope.totalprice;

      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go("checkout");
    };

  }

}());
