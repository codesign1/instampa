(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingPostalesService', PackingPostalesService);

  PackingPostalesService.$inject = ['authService', '$http'];

  function PackingPostalesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";

    function getPaquetesByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_empaque?[filter][where][habilitado]=true&filter[order]=color%20ASC';

      return $http.get(source);
    }

    return {
      getPaquetesByTipoproducto : getPaquetesByTipoproducto
    }

  }

}());
