(function () {

  'use strict';

  angular
    .module('app')
    .controller('FotosController', FotosController);

  FotosController.$inject = ['authService', '$scope', '$state', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function FotosController(authService, $scope, $state, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $scope.producttype = $stateParams.idtipoproducto;
    $scope.files = [];
    $scope.fotosUser = [];
    $scope.cel = 2;
    $scope.polaroid = 'image1.png';
    let formato = 'Polaroid';

    $window.scrollTo(0, 0);

    $scope.changeFormato = function(formatcx){
      if(formatcx == true){
        $scope.polaroid = 'marco_foto.png';
      }else{
        $scope.polaroid = 'image1.png';
      }
      $scope.format = formatcx;
    }

    function load() {
      $scope.product = $scope.carrito[$scope.carrito.length-1].product;
      crearCantidadFotos();
      if($scope.producttype == 6){
        $scope.polaroid = 'marco_foto.png';
      }
    }

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
    });

    function crearCantidadFotos () {
      $scope.fotos = [];
      for (var i = 0; i < $scope.product.numfotos; i++) {
        $scope.fotos.push(i)
      }
      if($scope.product.numfotos < 5){
        $scope.cel = 12/$scope.product.numfotos;
      }else{
        $scope.cel = 2;
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
      $scope.$apply();
    };

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    vm.upload = function () {
      $scope.loading = true;
      
      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];

              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
      $scope.subidas--;
    };

    $scope.goBack = function(){
      $scope.carrito.splice($scope.carrito.length-1,1);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go($scope.tipos[$scope.product.id_tipoproducto-1].slug, {"idtipoproducto": $scope.product.id_tipoproducto});
    };

    $scope.continue = function (format, precio) {
      
      if(format){
        formato = 'Cuadrado';
      }else{
        formato = 'Polaroid';
      }

      if($scope.producttype == 6){
        formato = 'Cuadrado';
      }

      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": $scope.product.id_tipoproducto == 6 ? $scope.carrito[$scope.carrito.length-1].text : "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == $scope.product.numfotos){
        $scope.carrito[$scope.carrito.length-1].fotos = fotos;
        $scope.carrito[$scope.carrito.length-1].format = formato;
        $scope.carrito[$scope.carrito.length-1].preciototal = precio;

        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        console.log($scope.carrito);
        db.put(carro)
          .then(function (response) {
            // handle response
            console.log(response);
          }).catch(function (err) {
          console.log(err);
        });

        if($scope.product.id_tipoproducto == 6){
          $state.go("empaquePostales", {"idtipoproducto": $scope.product.id_tipoproducto});
        }else{
          $state.go("checkout");
        }
      }else{
        alert("Debes cargar todas las fotos");
      }
    };

  }

}());
