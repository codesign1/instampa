(function () {

  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['$q', 'lock', 'authManager', '$state', '$window', '$timeout'];

  function authService($q, lock, authManager, $state, $window, $timeout) {

    function login() {
      //save the current state on localstorage
      localStorage.setItem('redirectUrl', $state.current.name);
      lock.show();
    }

    var userProfile = JSON.parse(localStorage.getItem('profile')) || null;
    var deferredProfile = $q.defer();

    // Logging out just requires removing the user's
    // id_token and profile
    function logout() {
      localStorage.removeItem('id_token');
      localStorage.removeItem('profile');
      authManager.unauthenticate();
    }

    // Set up the logic for when a user authenticates
    // This method is called from app.run.js
    function registerAuthenticationListener() {
      lock.on('authenticated', function (authResult, profile) {
        lock.getProfile(authResult.idToken, function(error, profile) {
          if (error) {
            // Handle error
            return;
          }
          localStorage.setItem('idToken', authResult.idToken);
          localStorage.setItem('profile', JSON.stringify(profile));

          // Hide the login modal
          lock.hide();

          // Get current state before login.
          var redirectUrl = localStorage.getItem('redirectUrl');

          // Redirect the user
          if (redirectUrl) {
            $state.go(redirectUrl);
            // setTimeout(function(){
            //     $window.location.reload();
            // }, 900);
            $timeout(function () {
              $window.location.reload();
            }, 900)
          }
        });
        authManager.authenticate();
      });
    }

    function getProfile() {
      if(authManager.isAuthenticated){
        return JSON.parse(localStorage.getItem('profile'));
      }
    }

    if (userProfile) {
      authManager.authenticate();
      deferredProfile.resolve(userProfile);
    }

    function getProfileDeferred() {
      return deferredProfile.promise;
    }


    return {
      login: login,
      logout: logout,
      registerAuthenticationListener: registerAuthenticationListener,
      getProfile: getProfile,
      getProfileDeferred: getProfileDeferred
    }
  }
})();
