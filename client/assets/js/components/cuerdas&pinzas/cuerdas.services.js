/**
 * Created by E5-471-3541 on 22/02/2017.
 */

(function(){
  'use strict';

  angular
    .module('app')
    .factory('CuerdasService', CuerdasService);

  CuerdasService.$inject = ['authService', '$http'];

  function CuerdasService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlProductosAccesorios = "/api/productos/";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesoriesByProduct(id_producto){
      var source = urlProductosAccesorios + id_producto + '/producto_accesorioproducto?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesoriesByProduct : getAccesoriesByProduct
    }

  }

}());
