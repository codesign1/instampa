(function () {

  'use strict';

  angular
    .module('app')
    .controller('CuerdasController', CuerdasController);

  CuerdasController.$inject = ['authService', '$scope', '$state', 'CuerdasService', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function CuerdasController(authService, $scope, $state, CuerdasService, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;

    $scope.carrito = {};
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.tipoproductoid = $stateParams.idtipoproducto;

    $scope.files = [];
    $scope.fotosUser = [];

    $scope.cuerdas = [];
    $scope.pinzas = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;
    $scope.polaroid = 'image1.png';

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.tipoproductoid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    CuerdasService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.crearCantidadFotos(0);
    });

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        $scope.fotos.push(i)
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
      $scope.cuerdas = [];
      $scope.pinzas = [];
      $scope.productid = $scope.products[id].id;
      getAccesories($scope.productid);
    };

    function getAccesories(idProduct){
      CuerdasService.getAccesoriesByProduct(idProduct).then(function (res){
        $scope.accesories = res.data;
        for (var i = 0; i < $scope.accesories.length; i++){
          if($scope.accesories[i].tipo == 1){
            $scope.cuerdas.push($scope.accesories[i]);
          }else{
            $scope.pinzas.push($scope.accesories[i]);
          }

        }

        if($scope.selectedCuerdas == null && $scope.selectedPinza == null){
          $scope.selectCuerda($scope.cuerdas[0]);
          $scope.selectPinza($scope.pinzas[0]);
        }else{
          for (var i = 0; i < $scope.cuerdas.length; i++){
            if($scope.selectedCuerdas.color_code == $scope.cuerdas[i].color_code){
              $scope.selectCuerda($scope.cuerdas[i]);
              break;
            }else{
              if(i == $scope.cuerdas.length-1){
                $scope.selectCuerda($scope.cuerdas[0]);
              }
            }
          }
          for (var i = 0; i < $scope.pinzas.length; i++){
            if($scope.selectedPinza.color_code == $scope.pinzas[i].color_code){
              $scope.selectPinza($scope.pinzas[i]);
              break;
            }else{
              if(i == $scope.pinzas.length-1){
                $scope.selectPinza($scope.pinzas[0]);
              }
            }
          }
        }

      });
    }

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    $scope.selectCuerda = function(cuerda){
      $scope.selectedCuerdas = cuerda;
    }

    $scope.selectPinza = function(pinza){
      $scope.selectedPinza = pinza;
    }

    vm.upload = function () {
      $scope.loading = true;

      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];

              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    }

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
    };

    $scope.continue = function (product, format) {
      var formato = format ? 'Cuadrado' : 'Polaroid';
      var colors = {'cuerdas' : $scope.selectedCuerdas , 'pinzas' : $scope.selectedPinza};
      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == product.numfotos){
        var newcarrito = {
          'product': product,
          'color': colors,
          'format': formato,
          'fotos': fotos,
          'paquete': '',
          'preciototal': product.precio
        };
        $scope.carrito.push(newcarrito);
        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        db.put(carro)
          .then(function (response) {
            // handle response
            console.log(response);
          }).catch(function (err) {
          console.log(err);
        });
        $state.go("checkout");
      }else{
        alert("Debes cargar todas las fotos");
      }
    };

  }

}());
