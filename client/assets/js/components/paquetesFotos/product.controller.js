(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductController', ProductController);

  ProductController.$inject = ['authService', '$scope', '$state', 'ProductService', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function ProductController(authService, $scope, $state, ProductService, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = [];
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      console.log(doc.cart);
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.files = [];
    $scope.fotosUser = [];
    $scope.productid = $stateParams.idtipoproducto;
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;
    $scope.polaroid = 'image1.png';
    $scope.loading = false;
    $scope.cel = 2;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }

    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    ProductService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.crearCantidadFotos(0);
    });

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        var toPush = {height: '',width: ''};
        $scope.fotos.push(toPush);
      }
      if($scope.products[id].numfotos < 5){
        $scope.cel = 12/$scope.products[id].numfotos;
      }else{
        $scope.cel = 2;
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
    };

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    vm.upload = function () {
      $scope.loading = true;
      // here might be the problem
      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;
              console.log(file);
              var id = 0;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];
              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    };

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
    };

    $scope.continue = function (product, format) {
      var formato = format ? 'Cuadrado' : 'Polaroid';
      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == product.numfotos){
        var newcarrito = {
          'product': product,
          'color': '',
          'format': formato,
          'fotos': fotos,
          'paquete': '',
          'preciototal': product.precio
        };
        console.log($scope.carrito);
        $scope.carrito.push(newcarrito);
        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        db.put(carro)
          .then(function (response) {
          // handle response
            console.log(response);
        }).catch(function (err) {
          console.log(err);
        });
        if($stateParams.idtipoproducto == 1){
          $state.go('empaque', {"idtipoproducto": $stateParams.idtipoproducto});
        }
        else{
          $state.go('checkout');
        }
      }else{
        alert("Debes cargar todas las fotos");
      }

    };

  }

}());
