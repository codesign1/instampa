(function(){
  'use strict';

  angular
    .module('app')
    .factory('ProductService', ProductService);

  ProductService.$inject = ['authService', '$http'];

  function ProductService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlProductosPedidos = "/api/productospedidos/";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto
    }

  }

}());/**
 * Created by E5-471-3541 on 22/02/2017.
 */
