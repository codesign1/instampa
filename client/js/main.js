(function () {

  'use strict';

  angular
    .module('app', ['auth0.lock', 'angular-jwt', 'ui.router', 'ngFileUpload', 'ui.bootstrap', 'ui.toggle', 'ismobile', 'ngSanitize', 'ngMd5'])
    .config(config);

  config.$inject = ['$stateProvider', 'lockProvider', '$urlRouterProvider', 'isMobileProvider', 'jwtOptionsProvider'];

  function config($stateProvider, lockProvider, $urlRouterProvider, isMobile, jwtOptionsProvider) {

    var paqueteFotos = isMobile.phone ? 'paqueteFotosMobile.html' : 'paqueteFotos.html';
    var marcos = isMobile.phone ? 'marcosMobile.html' : 'marcos.html';
    var empaque = isMobile.phone ? 'empaqueMobile.html' : 'empaque.html';
    var checkout = isMobile.phone ? 'checkoutMobile.html' : 'checkout.html';
    var fotos = isMobile.phone ? 'fotosMobile.html' : 'fotos.html';
    var caballetes = isMobile.phone ? 'caballetesMobile.html' : 'caballetes.html';
    var albmus = isMobile.phone ? 'albumsMobile.html' : 'albums.html';
    var cuerdas = isMobile.phone ? 'cuerdasMobile.html' : 'cuerdas.html';
    var postales = isMobile.phone ? 'postalesMobile.html' : 'postales.html';
    var empaquesPostales = isMobile.phone ? 'empaquePostalesMobile.html' : 'empaquePostales.html';
    var dashboard = isMobile.phone ? 'dashboardMobile.html' : 'dashboard.html';


    $stateProvider

      .state('home', {
        url: '/home',
        controller: 'HomeController',
        templateUrl: '/templates/usuario/home.html',
        controllerAs: 'vm'
      })

      .state('productos', {
        url: '/productos',
        controller: 'HomeController',
        templateUrl: '/templates/usuario/home.html',
        controllerAs: 'vm'
      })

      .state('paqueteFotos', {
        url: '/paqueteFotos/:idtipoproducto',
        controller: 'ProductController',
        templateUrl: '/templates/usuario/flujos/paqueteFotos/'+paqueteFotos,
        controllerAs: 'vm'
      })

      .state('marcos', {
        url: '/marcos/:idtipoproducto',
        controller: 'MarcosController',
        templateUrl: '/templates/usuario/flujos/marcos/'+marcos,
        controllerAs: 'vm'
      })

      .state('empaque', {
        url: '/empaque/:idtipoproducto',
        controller: 'PackingController',
        templateUrl: '/templates/usuario/flujos/empaque/'+empaque,
        controllerAs: 'vm'
      })

      .state('fotos', {
        url: '/fotos/:idtipoproducto',
        controller: 'FotosController',
        templateUrl: '/templates/usuario/flujos/fotos/'+fotos,
        controllerAs: 'vm'
      })

      .state('checkout', {
        url: '/checkout',
        controller: 'CheckoutController',
        templateUrl: '/templates/usuario/checkout/'+checkout,
        controllerAs: 'vm'
      })

      .state('caballetes', {
        url: '/caballetes/:idtipoproducto',
        controller: 'CaballetesController',
        templateUrl: '/templates/usuario/flujos/caballetes/'+caballetes,
        controllerAs: 'vm'
      })

      .state('galeria', {
        url: '/galeria',
        controller: 'InstagramController',
        templateUrl: '/templates/usuario/galeria.html',
        controllerAs: 'vm'
      })

      .state('albumes', {
        url: '/albums/:idtipoproducto',
        controller: 'AlbumsController',
        templateUrl: '/templates/usuario/flujos/albums/'+albmus,
        controllerAs: 'vm'
      })

      .state('cuerdasPinzas', {
        url: '/cuerdas/:idtipoproducto',
        controller: 'CuerdasController',
        templateUrl: '/templates/usuario/flujos/cuerdas/'+cuerdas,
        controllerAs: 'vm'
      })

      .state('response', {
        url: '/response/:datapayu',
        controller: 'ResponseController',
        templateUrl: '/templates/usuario/checkout/payU/response.html',
        controllerAs: 'vm'
      })

      .state('postales', {
        url: '/postales/:idtipoproducto',
        controller: 'PostalesController',
        templateUrl: '/templates/usuario/flujos/postales/'+postales,
        controllerAs: 'vm'
      })

      .state('empaquePostales', {
        url: '/empaquePostales',
        controller: 'PackingPostalesController',
        templateUrl: '/templates/usuario/flujos/postales/'+empaquesPostales,
        controllerAs: 'vm'
      })

      .state('politicas', {
        url: '/politicas/:idpolitica',
        controller: 'CheckoutController',
        templateUrl: '/templates/usuario/checkout/politicas.html',
        controllerAs: 'vm'
      })

      .state('contacto', {
        url: '/contacto',
        controller: 'ContactoController',
        templateUrl: '/templates/usuario/contacto.html',
        controllerAs: 'vm'
      })

      .state('dashboard', {
        url: '/dashboard',
        controller: 'DashboardController',
        templateUrl: '/templates/admin/'+dashboard,
        controllerAs: 'vm'
      })

      .state('products', {
        url: '/products',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/products.html',
        controllerAs: 'vm'
      })

      .state('createProduct', {
        url: '/createProduct',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/create.html',
        controllerAs: 'vm'
      })

      .state('editProduct', {
        url: '/editProduct/:idproducto',
        controller: 'ProductsController',
        templateUrl: '/templates/admin/products/create.html',
        controllerAs: 'vm'
      })

      .state('createPacking', {
        url: '/createPacking',
        controller: 'PackingBackController',
        templateUrl: '/templates/admin/packing/create.html',
        controllerAs: 'vm'
      })

      .state('editPacking', {
        url: '/editPacking/:idproducto',
        controller: 'PackingBackController',
        templateUrl: '/templates/admin/packing/create.html',
        controllerAs: 'vm'
      })

      .state('policy', {
        url: '/policy',
        controller: 'PolicyController',
        templateUrl: '/templates/admin/policy/policy.html',
        controllerAs: 'vm'
      })

      .state('categories', {
        url: '/categories',
        controller: 'CategoriesController',
        templateUrl: '/templates/admin/categories/categories.html',
        controllerAs: 'vm'
      })
    ;

    lockProvider.init({
      clientID: AUTH0_CLIENT_ID,
      domain: AUTH0_DOMAIN
    });

    $urlRouterProvider.otherwise('/home');

    jwtOptionsProvider.config({
      tokenGetter: function () {
        return localStorage.getItem('id_token');
      }
    });
  }

})();

(function () {

  'use strict';

  angular
    .module('app')
    .run(run);

  run.$inject = ['$rootScope', 'authService', 'lock', 'authManager'];

  function run($rootScope, authService, lock, authManager) {
    // Put the authService on $rootScope so its methods
    // can be accessed from the nav bar
    $rootScope.authService = authService;

    // Register the authentication listener that is
    // set up in auth.service.js
    authService.registerAuthenticationListener();

    // Register the synchronous hash parser
    lock.interceptHash();

    // refreshed and maintain authentication
    authManager.checkAuthOnRefresh();
  }

})();

var AUTH0_CLIENT_ID='Md3lR7lH8yoppNuiWGHM1wc8ljCVyIlD';
var AUTH0_DOMAIN='carolinajaramillo.auth0.com';
var AUTH0_CALLBACK_URL=location.href;

(function () {

  'use strict';

  angular
    .module('app')
    .controller('AlbumsController', AlbumsController);

  AlbumsController.$inject = ['authService', '$scope', '$state', 'AlbumService', 'HomeService', '$stateParams', '$window'];

  function AlbumsController(authService, $scope, $state, AlbumService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0, 0);

    $scope.productid = $stateParams.idtipoproducto;

    var carritoText = window.localStorage.getItem('carrito');
    var carrito = JSON.parse(carritoText) ? JSON.parse(carritoText) : [];

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id ==  $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    AlbumService.getProductosByTipoproducto($scope.productid).then(function (res) {
      $scope.products = res.data;

      productColors($scope.products, $scope.colors);
      $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
      $scope.selectedProduct = $scope.productsComplete[0];
      $scope.sigProduct = $scope.productsComplete[1];
    });

    AlbumService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    function productColors(products, colors){
      var colorsArray = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.selectedColor = $scope.productsComplete[0].colors[0];
    };

    $scope.continue = function () {
      $state.go('paqueteFotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('AlbumService', AlbumService);

  AlbumService.$inject = ['authService', '$http'];

  function AlbumService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos;
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .service('authService', authService);

  authService.$inject = ['$q', 'lock', 'authManager', '$state', '$window', '$timeout'];

  function authService($q, lock, authManager, $state, $window, $timeout) {

    function login() {
      //save the current state on localstorage
      localStorage.setItem('redirectUrl', $state.current.name);
      lock.show();
    }

    var userProfile = JSON.parse(localStorage.getItem('profile')) || null;
    var deferredProfile = $q.defer();

    // Logging out just requires removing the user's
    // id_token and profile
    function logout() {
      localStorage.removeItem('id_token');
      localStorage.removeItem('profile');
      authManager.unauthenticate();
    }

    // Set up the logic for when a user authenticates
    // This method is called from app.run.js
    function registerAuthenticationListener() {
      lock.on('authenticated', function (authResult, profile) {
        lock.getProfile(authResult.idToken, function(error, profile) {
          if (error) {
            // Handle error
            return;
          }
          localStorage.setItem('idToken', authResult.idToken);
          localStorage.setItem('profile', JSON.stringify(profile));

          // Hide the login modal
          lock.hide();

          // Get current state before login.
          var redirectUrl = localStorage.getItem('redirectUrl');

          // Redirect the user
          if (redirectUrl) {
            $state.go(redirectUrl);
            // setTimeout(function(){
            //     $window.location.reload();
            // }, 900);
            $timeout(function () {
              $window.location.reload();
            }, 900)
          }
        });
        authManager.authenticate();
      });
    }

    function getProfile() {
      if(authManager.isAuthenticated){
        return JSON.parse(localStorage.getItem('profile'));
      }
    }

    if (userProfile) {
      authManager.authenticate();
      deferredProfile.resolve(userProfile);
    }

    function getProfileDeferred() {
      return deferredProfile.promise;
    }


    return {
      login: login,
      logout: logout,
      registerAuthenticationListener: registerAuthenticationListener,
      getProfile: getProfile,
      getProfileDeferred: getProfileDeferred
    }
  }
})();

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CaballetesController', CaballetesController);

  CaballetesController.$inject = ['authService', '$scope', '$state', 'CaballetesService', 'HomeService', '$stateParams', '$window'];

  function CaballetesController(authService, $scope, $state, CaballetesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.cel = 12;

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        $scope.fotos.push(i)
      }
    };

    CaballetesService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;

      CaballetesService.getProdAccesorry($scope.products[0].id).then(function (res) {
        $scope.colors = res.data;
        $scope.crearCantidadFotos(0);
        productColors($scope.products, $scope.colors);
        $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
        $scope.selectedProduct = $scope.productsComplete[0];
        $scope.sigProduct = $scope.productsComplete[1];
      });
    });

    CaballetesService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    function productColors(products, colors){
      var colorsArray = [];
      $scope.selectedColor = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.fotos.forEach(function (foto) {
        $scope.selectedColor.push($scope.productsComplete[0].colors[0]);
      });
    };

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == $scope.products.length -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };
      $scope.crearCantidadFotos($scope.idproduct);
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];

      if($scope.selectedProduct.product.numfotos < 5){
        $scope.cel = 12/$scope.selectedProduct.product.numfotos;
      }else{
        $scope.cel = 2;
      }

      $scope.selectedColor[idProduct+1] = $scope.selectedProduct.colors[0];
    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = $scope.products.length-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      $scope.crearCantidadFotos($scope.idproduct);
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];

      if($scope.selectedProduct.product.numfotos < 5){
        $scope.cel = 12/$scope.selectedProduct.product.numfotos;
      }else{
        $scope.cel = 2;
      }

      $scope.selectedColor[idProduct+1] = $scope.selectedProduct.colors[0];
    };

    function getPrevNextProduct(productsCompleteLength){
      CaballetesService.getProdAccesorry($scope.selectedProduct.product.id).then(function (res) {
        $scope.colors = res.data;
        $scope.selectedProduct.colors = res.data;
        productColors($scope.products, $scope.colors);
        if($scope.idproduct == productsCompleteLength -1){
          $scope.antProduct = $scope.productsComplete[$scope.idproduct - 1];
          $scope.sigProduct = $scope.productsComplete[0];
        }else if($scope.idproduct == 0){
          $scope.antProduct = $scope.productsComplete[productsCompleteLength -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        }else{
          $scope.antProduct = $scope.productsComplete[$scope.idproduct -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        };
      });

    }

    $scope.selectPack = function(position, colorindex){
      $scope.selectedColor[position] = $scope.selectedProduct.colors[colorindex];
    };

    $scope.continue = function (product, color) {

      var newcarrito = {
        'product': product,
        'color' : color,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('CaballetesService', CaballetesService);

  CaballetesService.$inject = ['authService', '$http'];

  function CaballetesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";
    var urlProductos = "/api/productos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos+'?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    function getProdAccesorry(id) {
      var source = urlProductos + "/"+id+"/producto_accesorioproducto?filter[order]=nombre%20ASC";
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios,
      getProdAccesorry :getProdAccesorry
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CategoriesController', CategoriesController);

  CategoriesController.$inject = ['authService', '$scope', '$state', 'CategoriesService', '$window', 'FileService', '$timeout'];

  function CategoriesController(authService, $scope, $state, CategoriesService, $window, FileService, $timeout) {

    var vm = this;
    vm.authService = authService;
    $scope.categoriaedit = {};

    CategoriesService.getCategories().then(function (res) {
      $scope.categories = res.data;
    });

    $scope.editCategories = function (idCategory, indexCategory) {
      var categoryToEdit = $scope.categoriaedit[indexCategory];
      if (categoryToEdit) {
        if (categoryToEdit.imagen) {
          FileService.uploadAdminFoto(categoryToEdit.imagen);
          categoryToEdit.imagen = "img/fotos_pedidos/admin_fotos/" + categoryToEdit.imagen.name;
        }
        // console.log(categoryToEdit);
        CategoriesService.editCategory(idCategory, categoryToEdit).then(function (res) {
          $scope.categoriaedit[indexCategory].saved = true;
          $timeout(function () {
            $scope.categoriaedit[indexCategory].saved = false;
          }, 5000);
        });
      }

    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('CategoriesService', CategoriesService);

  CategoriesService.$inject = ['authService', '$http'];

  function CategoriesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlCategories = "/api/tipoproductos";

    function getCategories(){
      var source = urlCategories;
      return $http.get(source);
    }

    function editCategory(idCategory, category){
      var source = urlCategories+'/'+idCategory;
      return $http.patch(source, category);
    }

    return {
      getCategories : getCategories,
      editCategory : editCategory
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CheckoutController', CheckoutController);

  CheckoutController.$inject = ['authService', '$scope', '$state', '$stateParams', 'CheckoutService', 'HomeService', 'FileService', '$window', 'md5'];

  function CheckoutController(authService, $scope, $state, $stateParams, CheckoutService, HomeService, FileService, $window, md5) {

    var vm = this;
    vm.authService = authService;
    $scope.loading = false;

    $scope.profile = JSON.parse(localStorage.getItem('profile'));
    //localStorage.removeItem('datosUsuario');
    console.log(localStorage.getItem('datosUsuario'));
    $scope.pedido = JSON.parse(localStorage.getItem('datosUsuario'));

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.carrito = doc.cart;
      $scope.doc = doc;
      $scope.$apply();
      $scope.load();
    }).catch(function (err) {
      console.log(err);
    });


    $window.scrollTo(0, 0);

    $scope.totalprice = 0;
    $scope.departaments = [];
    $scope.ciudades = [];
    $scope.disclaimer_active = false;
    $scope.shipingPrice = 0;
    $scope.costos = [];
    $scope.envioexpress = false;
    $scope.iva = 0.19;
    $scope.payUData = [];
    $scope.subtotal_payU = 0;
    $scope.ivaSubtotal = 0;
    $scope.total = 0;
    $scope.description = "";
    $scope.referenceCode = "";
    $scope.signature = "";


    $scope.idpolitica = $stateParams.idpolitica;
    $scope.politica = {};

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
    });

    CheckoutService.getPoliticas().then(function (res) {
      $scope.politicas = res.data;
    });

    if($scope.idpolitica != null){
      CheckoutService.getPoliticasById($scope.idpolitica).then(function (res) {
        $scope.politica = res.data;
      });
    }

    CheckoutService.getShippingPrice().then(function (res) {
      $scope.costosEnvios = res.data;
    });

    $scope.payUData = CheckoutService.getDataPayU();

    $scope.infoPayU = function () {
      var date = new Date();
      var reference = md5.createHash(date.toString() || '');
      var signatureStr = '';

      signatureStr = $scope.payUData.apikey + "~" + $scope.payUData.merchantId + "~" + reference + "~" + $scope.total + "~COP";

      $scope.description = "Venta Instampa " + date;
      $scope.referenceCode = reference;
      $scope.signature = md5.createHash(signatureStr || '');
    };

    $scope.saveDatos = function (pedido) {
      localStorage.setItem("datosUsuario", JSON.stringify(pedido));
    }

    $scope.departaments = CheckoutService.getDepartaments();

    $scope.changeDep = function (dep) {
      $scope.ciudades = CheckoutService.getDepartaments(dep);

      if(dep.zona != 'x'){
        $scope.carrito.forEach(function (elem) {
          var costo = $scope.costosEnvios.find(function (costo) {
            return costo.id_producto == elem.product.id && costo.zona == dep.zona;
          });
          $scope.costos.push(costo);
        });
        var mayor = _.max($scope.costos, function (cost) {
          return cost.precio;
        });

        $scope.shipingPrice = mayor.precio;

      }else{
        $scope.carrito.forEach(function (elem) {
          var costo = $scope.costosEnvios.find(function (costo) {
            return costo.id_producto == elem.product.id && costo.zona == dep.zona;
          });
          $scope.costos.push(costo);
        });

        $scope.shipingPrice = 0;

      }

      $scope.costos = [];
      $scope.subtotal_payU = $scope.totalprice + $scope.shipingPrice;
      $scope.ivaSubtotal = $scope.subtotal_payU * $scope.iva;
      $scope.total = ($scope.subtotal_payU * $scope.iva) + $scope.subtotal_payU;
      $scope.envioexpress = false;
      $scope.infoPayU();

    };

    $scope.envioExpress = function (dep) {
      if ($scope.envioexpress) {
        if (dep.zona == 'bogota') {
          $scope.shipingPrice = $scope.shipingPrice + 5000;
        } else {
          $scope.shipingPrice = $scope.shipingPrice + 10000;
        }
      } else {
        if (dep.zona == 'bogota') {
          $scope.shipingPrice = $scope.shipingPrice - 5000;
        } else {
          $scope.shipingPrice = $scope.shipingPrice - 10000;
        }
      }
      $scope.subtotal_payU = $scope.totalprice + $scope.shipingPrice;
      $scope.ivaSubtotal = $scope.subtotal_payU * $scope.iva;
      $scope.total = ($scope.subtotal_payU * $scope.iva) + $scope.subtotal_payU;
      $scope.infoPayU();
    };

    $scope.displayDisclaimer = function (state) {
      $scope.disclaimer_active = state;
    };

    $scope.delete = function (index, dep) {
      $scope.carrito.splice(index, 1);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          db.get('instampaCart').then(function (doc) {
            $scope.carrito = doc.cart;
            $scope.doc = doc;
            $scope.$apply();
          }).catch(function (err) {
            console.log(err);
          });
        }).catch(function (err) {
        console.log(err);
      });
      $scope.totalprice = 0;
      $scope.shipingPrice = 0;
      $scope.load();
      $scope.changeDep(dep);
    };

    $scope.load = function () {
      console.log($scope.carrito);
      $scope.carrito.forEach(function (prod, index, object) {
        if(prod.preciototal == 0){
          var none = {zona : 'x'};
          $scope.delete(index, none);
        }
        $scope.totalprice += prod.preciototal;
      });
      $scope.ivaSubtotal = $scope.totalprice*$scope.iva;
      $scope.subtotal_payU = $scope.ivaSubtotal;
    };

    $scope.clickPayUForm = function () {
      localStorage.removeItem('datosUsuario');
      $scope.loading = true;
      var creationDate = new Date();
      var creationDateString = creationDate.toISOString()
      var precio = $scope.totalprice + $scope.shipingPrice;
      var pedidoiva = $scope.ivaSubtotal;
      var totalpedido = precio + pedidoiva;
      var id_pedido = 0;
      var id_productopedido = 0;
      var profile = JSON.parse(localStorage.getItem('profile'));

      if($scope.pedido.bday != null){
        var bhday = $scope.pedido.bday.toString();
        // var bhday = bday.slice(4,15);
      }else{
        var bhday = "_";
      }

      var pedido_to_insert = {
        "nombre": $scope.pedido.nombre,
        "cedula": $scope.pedido.cedula,
        "direccion": $scope.pedido.direccion,
        "bday": bhday,
        "direccionenvio": $scope.pedido.direccionenvio,
        "telefono": $scope.pedido.telefono,
        "politicas": $scope.pedido.policy1 ? true : false,
        "usuarioinstagram": $scope.pedido.instagram,
        "pago": "a",
        "estado": "Pendiente",
        "fecharecepcion": creationDateString,
        "precio": precio,
        "iva": pedidoiva,
        "total": totalpedido,
        "costoenvio": $scope.shipingPrice,
        "id_user": $scope.pedido.id_user,
        "referencecode": $scope.referenceCode,
        "envioexpress": $scope.envioexpress,
        "ciudad": $scope.pedido.ciudad
      };

      CheckoutService.insertPedido(pedido_to_insert).then(function (res) {
        var id_pedido = res.data.id;
        // PRODUCTOS
        _.each($scope.carrito, function (prod, indexProd) {
          if (prod.paquete.id) {
            var empaque = prod.paquete.id;
          } else {
            var empaque = null;
          }
          var productopedido = {
            "id_producto": prod.product.id,
            "id_empaque": empaque,
            "id_pedido": id_pedido
          };
          console.log(prod);
          CheckoutService.insertProductoPedido(id_pedido, productopedido).then(function (prdped) {
            var id_productopedido = prdped.data.id;
            // ACCESORIOS
            _.each(prod.color, function (coloraccesory) {
              var accesorio = {
                "id_productopedido": id_productopedido,
                "id_accesorioproducto": coloraccesory.id
              };
              CheckoutService.insertAccesorios(id_productopedido, accesorio);
            });

            // FOTOS
            _.each(prod.fotos, function(foto, index){
              foto.id_productopedido = id_productopedido;
              if(prod.format == 'Cuadrado'){
                foto.formato = true;
              }else{
                foto.formato = false;
              }

              CheckoutService.insertFotos(foto)
                .then(function (res) {
                  if(index+1 == prod.fotos.length ){
                    if(indexProd+1 == $scope.carrito.length){
                      $scope.loading = false;
                      document.getElementById('submitcheckout').click();
                      document.getElementById('payUform').submit();
                      $scope.carrito = [];
                      var carro = {
                        _id: 'instampaCart',
                        _rev: $scope.doc._rev,
                        cart: $scope.carrito
                      };
                      db.put(carro)
                        .then(function (response) {
                          // handle response
                        }).catch(function (err) {
                        console.log(err);
                      });
                    }
                  }
                });
            });
          });
        });

      });
    };

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .factory('CheckoutService', CheckoutService);

  CheckoutService.$inject = ['authService', '$http'];

  function CheckoutService(authService, $http) {

    var vm = this;
    vm.authService = authService;

    var urlPoliticas = "api/politicas";
    var urlProducts = "api/productos/";
    var urlPedidos = "api/pedidos";
    var urlFotos = "api/fotos";
    var urlCostoEnvio = "/api/costosenvios";
    var urlProductosPedidos = "api/productospedidos";

    function getPoliticas() {
      var source = urlPoliticas;
      return $http.get(source);
    }

    function getPoliticasById(idpolicy) {
      var source = urlPoliticas+'/'+idpolicy;
      return $http.get(source);
    }

    var departamentos = [
        {
            "id": '',
            "departamento":"Selecciona un departamento",
            "zona":"x",
            "ciudades":[
                "---",
            ]
        },
        {
            "id":1,
            "departamento":"Amazonas",
            "zona":"otra",
            "ciudades":[
                "Leticia",
                "Puerto Nariño"
            ]
        },
        {
            "id":2,
            "departamento":"Antioquia",
            "zona":"otra",
            "ciudades":[
                "Abejorral",
                "Abriaquí",
                "Alejandría",
                "Amagá",
                "Amalfi",
                "Andes",
                "Angelópolis",
                "Angostura",
                "Anorí",
                "Anzá",
                "Apartadó",
                "Arboletes",
                "Argelia",
                "Armenia",
                "Barbosa",
                "Bello",
                "Belmira",
                "Betania",
                "Betulia",
                "Briceño",
                "Buriticá",
                "Cáceres",
                "Caicedo",
                "Caldas",
                "Campamento",
                "Cañasgordas",
                "Caracolí",
                "Caramanta",
                "Carepa",
                "Carolina del Príncipe",
                "Caucasia",
                "Chigorodó",
                "Cisneros",
                "Ciudad Bolívar",
                "Cocorná",
                "Concepción",
                "Concordia",
                "Copacabana",
                "Dabeiba",
                "Donmatías",
                "Ebéjico",
                "El Bagre",
                "El Carmen de Viboral",
                "El Peñol",
                "El Retiro",
                "El Santuario",
                "Entrerríos",
                "Envigado",
                "Fredonia",
                "Frontino",
                "Giraldo",
                "Girardota",
                "Gómez Plata",
                "Granada",
                "Guadalupe",
                "Guarne",
                "Guatapé",
                "Heliconia",
                "Hispania",
                "Itaguí",
                "Ituango",
                "Jardín",
                "Jericó",
                "La Ceja",
                "La Estrella",
                "La Pintada",
                "La Unión",
                "Liborina",
                "Maceo",
                "Marinilla",
                "Medellín",
                "Montebello",
                "Murindó",
                "Mutatá",
                "Nariño",
                "Nechí",
                "Necoclí",
                "Olaya",
                "Peque",
                "Pueblorrico",
                "Puerto Berrío",
                "Puerto Nare",
                "Puerto Triunfo",
                "Remedios",
                "Rionegro",
                "Sabanalarga",
                "Sabaneta",
                "Salgar",
                "San Andrés de Cuerquia",
                "San Carlos",
                "San Francisco",
                "San Jerónimo",
                "San José de la Montaña",
                "San Juan de Urabá",
                "San Luis",
                "San Pedro de Urabá",
                "San Pedro de los Milagros",
                "San Rafael",
                "San Roque",
                "San Vicente",
                "Santa Bárbara",
                "Santa Fe de Antioquia",
                "Santa Rosa de Osos",
                "Santo Domingo",
                "Segovia",
                "Sonsón",
                "Sopetrán",
                "Támesis",
                "Tarazá",
                "Tarso",
                "Titiribí",
                "Toledo",
                "Turbo",
                "Uramita",
                "Urrao",
                "Valdivia",
                "Valparaíso",
                "Vegachí",
                "Venecia",
                "Vigía del Fuerte",
                "Yalí",
                "Yarumal",
                "Yolombó",
                "Yondó",
                "Zaragoza"
            ]
        },
        {
            "id":3,
            "departamento":"Arauca",
            "zona":"otra",
            "ciudades":[
                "Arauca",
                "Arauquita",
                "Cravo Norte",
                "Fortul",
                "Puerto Rondón",
                "Saravena",
                "Tame"
            ]
        },
        {
            "id":4,
            "departamento":"Atlántico",
            "zona":"costa",
            "ciudades":[
                "Baranoa",
                "Barranquilla",
                "Campo de la Cruz",
                "Candelaria",
                "Galapa",
                "Juan de Acosta",
                "Luruaco",
                "Malambo",
                "Manatí",
                "Palmar de Varela",
                "Piojó",
                "Polonuevo",
                "Ponedera",
                "Puerto Colombia",
                "Repelón",
                "Sabanagrande",
                "Sabanalarga",
                "Santa Lucía",
                "Santo Tomás",
                "Soledad",
                "Suán",
                "Tubará",
                "Usiacurí"
            ]
        },
        {
            "id":5,
            "departamento":"Bolívar",
            "zona":"costa",
            "ciudades":[
                "Achí",
                "Altos del Rosario",
                "Arenal",
                "Arjona",
                "Arroyohondo",
                "Barranco de Loba",
                "Brazuelo de Papayal",
                "Calamar",
                "Cantagallo",
                "Cartagena de Indias",
                "Cicuco",
                "Clemencia",
                "Córdoba",
                "El Carmen de Bolívar",
                "El Guamo",
                "El Peñón",
                "Hatillo de Loba",
                "Magangué",
                "Mahates",
                "Margarita",
                "María la Baja",
                "Mompós",
                "Montecristo",
                "Morales",
                "Norosí",
                "Pinillos",
                "Regidor",
                "Río Viejo",
                "San Cristóbal",
                "San Estanislao",
                "San Fernando",
                "San Jacinto del Cauca",
                "San Jacinto",
                "San Juan Nepomuceno",
                "San Martín de Loba",
                "San Pablo",
                "Santa Catalina",
                "Santa Rosa",
                "Santa Rosa del Sur",
                "Simití",
                "Soplaviento",
                "Talaigua Nuevo",
                "Tiquisio",
                "Turbaco",
                "Turbaná",
                "Villanueva",
                "Zambrano"
            ]
        },
        {
            "id":6,
            "departamento":"Boyacá",
            "zona":"otra",
            "ciudades":[
                "Almeida",
                "Aquitania",
                "Arcabuco",
                "Belén",
                "Berbeo",
                "Betéitiva",
                "Boavita",
                "Boyacá",
                "Briceño",
                "Buenavista",
                "Busbanzá",
                "Caldas",
                "Campohermoso",
                "Cerinza",
                "Chinavita",
                "Chiquinquirá",
                "Chíquiza",
                "Chiscas",
                "Chita",
                "Chitaraque",
                "Chivatá",
                "Chivor",
                "Ciénega",
                "Cómbita",
                "Coper",
                "Corrales",
                "Covarachía",
                "Cubará",
                "Cucaita",
                "Cuítiva",
                "Duitama",
                "El Cocuy",
                "El Espino",
                "Firavitoba",
                "Floresta",
                "Gachantivá",
                "Gámeza",
                "Garagoa",
                "Guacamayas",
                "Guateque",
                "Guayatá",
                "Guicán",
                "Iza",
                "Jenesano",
                "Jericó",
                "La Capilla",
                "La Uvita",
                "La Victoria",
                "Labranzagrande",
                "Macanal",
                "Maripí",
                "Miraflores",
                "Mongua",
                "Monguí",
                "Moniquirá",
                "Motavita",
                "Muzo",
                "Nobsa",
                "Nuevo Colón",
                "Oicatá",
                "Otanche",
                "Pachavita",
                "Páez",
                "Paipa",
                "Pajarito",
                "Panqueba",
                "Pauna",
                "Paya",
                "Paz del Río",
                "Pesca",
                "Pisba",
                "Puerto Boyacá",
                "Quípama",
                "Ramiriquí",
                "Ráquira",
                "Rondón",
                "Saboyá",
                "Sáchica",
                "Samacá",
                "San Eduardo",
                "San José de Pare",
                "San Luis de Gaceno",
                "San Mateo",
                "San Miguel de Sema",
                "San Pablo de Borbur",
                "Santa María",
                "Santa Rosa de Viterbo",
                "Santa Sofía",
                "Santana",
                "Sativanorte",
                "Sativasur",
                "Siachoque",
                "Soatá",
                "Socha",
                "Socotá",
                "Sogamoso",
                "Somondoco",
                "Sora",
                "Soracá",
                "Sotaquirá",
                "Susacón",
                "Sutamarchán",
                "Sutatenza",
                "Tasco",
                "Tenza",
                "Tibaná",
                "Tibasosa",
                "Tinjacá",
                "Tipacoque",
                "Toca",
                "Toguí",
                "Tópaga",
                "Tota",
                "Tunja",
                "Tununguá",
                "Turmequé",
                "Tuta",
                "Tutazá",
                "Úmbita",
                "Ventaquemada",
                "Villa de Leyva",
                "Viracachá",
                "Zetaquira"
            ]
        },
        {
            "id":33,
            "departamento":'Bogotá D.C',
            "zona":"bogota",
            "ciudades":[
                "Bogotá D.C"
            ]
        },
        {
            "id":7,
            "departamento":"Caldas",
            "zona":"otra",
            "ciudades":[
                "Aguadas",
                "Anserma",
                "Aranzazu",
                "Belalcázar",
                "Chinchiná",
                "Filadelfia",
                "La Dorada",
                "La Merced",
                "Manizales",
                "Manzanares",
                "Marmato",
                "Marquetalia",
                "Marulanda",
                "Neira",
                "Norcasia",
                "Pácora",
                "Palestina",
                "Pensilvania",
                "Riosucio",
                "Risaralda",
                "Salamina",
                "Samaná",
                "San José",
                "Supía",
                "Victoria",
                "Villamaría",
                "Viterbo"
            ]
        },
        {
            "id":8,
            "departamento":"Caquetá",
            "zona":"otra",
            "ciudades":[
                "Albania",
                "Belén de los Andaquíes",
                "Cartagena del Chairá",
                "Curillo",
                "El Doncello",
                "El Paujil",
                "Florencia",
                "La Montañita",
                "Milán",
                "Morelia",
                "Puerto Rico",
                "San José del Fragua",
                "San Vicente del Caguán",
                "Solano",
                "Solita",
                "Valparaíso"
            ]
        },
        {
            "id":9,
            "departamento":"Casanare",
            "zona":"otra",
            "ciudades":[
                "Aguazul",
                "Chámeza",
                "Hato Corozal",
                "La Salina",
                "Maní",
                "Monterrey",
                "Nunchía",
                "Orocué",
                "Paz de Ariporo",
                "Pore",
                "Recetor",
                "Sabanalarga",
                "Sácama",
                "San Luis de Palenque",
                "Támara",
                "Tauramena",
                "Trinidad",
                "Villanueva",
                "Yopal"
            ]
        },
        {
            "id":10,
            "departamento":"Cauca",
            "zona":"otra",
            "ciudades":[
                "Almaguer",
                "Argelia",
                "Balboa",
                "Bolívar",
                "Buenos Aires",
                "Cajibío",
                "Caldono",
                "Caloto",
                "Corinto",
                "El Tambo",
                "Florencia",
                "Guachené",
                "Guapí",
                "Inzá",
                "Jambaló",
                "La Sierra",
                "La Vega",
                "López de Micay",
                "Mercaderes",
                "Miranda",
                "Morales",
                "Padilla",
                "Páez",
                "Patía",
                "Piamonte",
                "Piendamó",
                "Popayán",
                "Puerto Tejada",
                "Puracé",
                "Rosas",
                "San Sebastián",
                "Santa Rosa",
                "Santander de Quilichao",
                "Silvia",
                "Sotará",
                "Suárez",
                "Sucre",
                "Timbío",
                "Timbiquí",
                "Toribío",
                "Totoró",
                "Villa Rica"
            ]
        },
        {
            "id":11,
            "departamento":"Cesar",
            "zona":"valledupar/Monteria",
            "ciudades":[
                "Aguachica",
                "Agustín Codazzi",
                "Astrea",
                "Becerril",
                "Bosconia",
                "Chimichagua",
                "Chiriguaná",
                "Curumaní",
                "El Copey",
                "El Paso",
                "Gamarra",
                "González",
                "La Gloria (Cesar)",
                "La Jagua de Ibirico",
                "La Paz",
                "Manaure Balcón del Cesar",
                "Pailitas",
                "Pelaya",
                "Pueblo Bello",
                "Río de Oro",
                "San Alberto",
                "San Diego",
                "San Martín",
                "Tamalameque",
                "Valledupar"
            ]
        },
        {
            "id":12,
            "departamento":"Chocó",
            "zona":"otra",
            "ciudades":[
                "Acandí",
                "Alto Baudó",
                "Bagadó",
                "Bahía Solano",
                "Bajo Baudó",
                "Bojayá",
                "Cantón de San Pablo",
                "Cértegui",
                "Condoto",
                "El Atrato",
                "El Carmen de Atrato",
                "El Carmen del Darién",
                "Istmina",
                "Juradó",
                "Litoral de San Juan",
                "Lloró",
                "Medio Atrato",
                "Medio Baudó",
                "Medio San Juan",
                "Nóvita",
                "Nuquí",
                "Quibdó",
                "Río Iró",
                "Río Quito",
                "Riosucio",
                "San José del Palmar",
                "Sipí",
                "Tadó",
                "Unión Panamericana",
                "Unguía"
            ]
        },
        {
            "id":13,
            "departamento":"Cundinamarca",
            "zona":"otra",
            "ciudades":[
                "Agua de Dios",
                "Albán",
                "Anapoima",
                "Anolaima",
                "Apulo",
                "Arbeláez",
                "Beltrán",
                "Bituima",
                "Bojacá",
                "Cabrera",
                "Cachipay",
                "Cajicá",
                "Caparrapí",
                "Cáqueza",
                "Carmen de Carupa",
                "Chaguaní",
                "Chía",
                "Chipaque",
                "Choachí",
                "Chocontá",
                "Cogua",
                "Cota",
                "Cucunubá",
                "El Colegio",
                "El Peñón",
                "El Rosal",
                "Facatativá",
                "Fómeque",
                "Fosca",
                "Funza",
                "Fúquene",
                "Fusagasugá",
                "Gachalá",
                "Gachancipá",
                "Gachetá",
                "Gama",
                "Girardot",
                "Granada",
                "Guachetá",
                "Guaduas",
                "Guasca",
                "Guataquí",
                "Guatavita",
                "Guayabal de Síquima",
                "Guayabetal",
                "Gutiérrez",
                "Jerusalén",
                "Junín",
                "La Calera",
                "La Mesa",
                "La Palma",
                "La Peña",
                "La Vega",
                "Lenguazaque",
                "Machetá",
                "Madrid",
                "Manta",
                "Medina",
                "Mosquera",
                "Nariño",
                "Nemocón",
                "Nilo",
                "Nimaima",
                "Nocaima",
                "Pacho",
                "Paime",
                "Pandi",
                "Paratebueno",
                "Pasca",
                "Puerto Salgar",
                "Pulí",
                "Quebradanegra",
                "Quetame",
                "Quipile",
                "Ricaurte",
                "San Antonio del Tequendama",
                "San Bernardo",
                "San Cayetano",
                "San Francisco",
                "San Juan de Rioseco",
                "Sasaima",
                "Sesquilé",
                "Sibaté",
                "Silvania",
                "Simijaca",
                "Soacha",
                "Sopó",
                "Subachoque",
                "Suesca",
                "Supatá",
                "Susa",
                "Sutatausa",
                "Tabio",
                "Tausa",
                "Tena",
                "Tenjo",
                "Tibacuy",
                "Tibirita",
                "Tocaima",
                "Tocancipá",
                "Topaipí",
                "Ubalá",
                "Ubaque",
                "Ubaté",
                "Une",
                "Útica",
                "Venecia",
                "Vergara",
                "Vianí",
                "Villagómez",
                "Villapinzón",
                "Villeta",
                "Viotá",
                "Yacopí",
                "Zipacón",
                "Zipaquirá"
            ]
        },
        {
            "id":14,
            "departamento":"Córdoba",
            "zona":"valledupar/Monteria",
            "ciudades":[
                "Ayapel",
                "Buenavista",
                "Canalete",
                "Cereté",
                "Chimá",
                "Chinú",
                "Ciénaga de Oro",
                "Cotorra",
                "La Apartada",
                "Lorica",
                "Los Córdobas",
                "Momil",
                "Montelíbano",
                "Montería",
                "Moñitos",
                "Planeta Rica",
                "Pueblo Nuevo",
                "Puerto Escondido",
                "Puerto Libertador",
                "Purísima",
                "Sahagún",
                "San Andrés de Sotavento",
                "San Antero",
                "San Bernardo del Viento",
                "San Carlos",
                "San José de Uré",
                "San Pelayo",
                "Tierralta",
                "Tuchín",
                "Valencia"
            ]
        },
        {
            "id":15,
            "departamento":"Guainía",
            "zona":"otra",
            "ciudades":[
                "Inírida"
            ]
        },
        {
            "id":16,
            "departamento":"Guaviare",
            "zona":"otra",
            "ciudades":[
                "Calamar",
                "El Retorno",
                "Miraflores",
                "San José del Guaviare"
            ]
        },
        {
            "id":17,
            "departamento":"Huila",
            "zona":"otra",
            "ciudades":[
                "Acevedo",
                "Agrado",
                "Aipe",
                "Algeciras",
                "Altamira",
                "Baraya",
                "Campoalegre",
                "Colombia",
                "El Pital",
                "Elías",
                "Garzón",
                "Gigante",
                "Guadalupe",
                "Hobo",
                "Íquira",
                "Isnos",
                "La Argentina",
                "La Plata",
                "Nátaga",
                "Neiva",
                "Oporapa",
                "Paicol",
                "Palermo",
                "Palestina",
                "Pitalito",
                "Rivera",
                "Saladoblanco",
                "San Agustín",
                "Santa María",
                "Suaza",
                "Tarqui",
                "Tello",
                "Teruel",
                "Tesalia",
                "Timaná",
                "Villavieja",
                "Yaguará"
            ]
        },
        {
            "id":18,
            "departamento":"La Guajira",
            "zona":"costa",
            "ciudades":[
                "Albania",
                "Barrancas",
                "Dibulla",
                "Distracción",
                "El Molino",
                "Fonseca",
                "Hatonuevo",
                "La Jagua del Pilar",
                "Maicao",
                "Manaure",
                "Riohacha",
                "San Juan del Cesar",
                "Uribia",
                "Urumita",
                "Villanueva"
            ]
        },
        {
            "id":19,
            "departamento":"Magdalena",
            "zona":"costa",
            "ciudades":[
                "Algarrobo",
                "Aracataca",
                "Ariguaní",
                "Cerro de San Antonio",
                "Chibolo",
                "Chibolo",
                "Ciénaga",
                "Concordia",
                "El Banco",
                "El Piñón",
                "El Retén",
                "Fundación",
                "Guamal",
                "Nueva Granada",
                "Pedraza",
                "Pijiño del Carmen",
                "Pivijay",
                "Plato",
                "Pueblo Viejo",
                "Remolino",
                "Sabanas de San Ángel",
                "Salamina",
                "San Sebastián de Buenavista",
                "San Zenón",
                "Santa Ana",
                "Santa Bárbara de Pinto",
                "Santa Marta",
                "Sitionuevo",
                "Tenerife",
                "Zapayán",
                "Zona Bananera"
            ]
        },
        {
            "id":20,
            "departamento":"Meta",
            "zona":"otra",
            "ciudades":[
                "Acacías",
                "Barranca de Upía",
                "Cabuyaro",
                "Castilla la Nueva",
                "Cubarral",
                "Cumaral",
                "El Calvario",
                "El Castillo",
                "El Dorado",
                "Fuente de Oro",
                "Granada",
                "Guamal",
                "La Macarena",
                "La Uribe",
                "Lejanías",
                "Mapiripán",
                "Mesetas",
                "Puerto Concordia",
                "Puerto Gaitán",
                "Puerto Lleras",
                "Puerto López",
                "Puerto Rico",
                "Restrepo",
                "San Carlos de Guaroa",
                "San Juan de Arama",
                "San Juanito",
                "San Martín",
                "Villavicencio",
                "Vista Hermosa"
            ]
        },
        {
            "id":21,
            "departamento":"Nariño",
            "zona":"otra",
            "ciudades":[
                "Aldana",
                "Ancuyá",
                "Arboleda",
                "Barbacoas",
                "Belén",
                "Buesaco",
                "Chachaguí",
                "Colón",
                "Consacá",
                "Contadero",
                "Córdoba",
                "Cuaspud",
                "Cumbal",
                "Cumbitara",
                "El Charco",
                "El Peñol",
                "El Rosario",
                "El Tablón",
                "El Tambo",
                "Francisco Pizarro",
                "Funes",
                "Guachucal",
                "Guaitarilla",
                "Gualmatán",
                "Iles",
                "Imués",
                "Ipiales",
                "La Cruz",
                "La Florida",
                "La Llanada",
                "La Tola",
                "La Unión",
                "Leiva",
                "Linares",
                "Los Andes",
                "Maguí Payán",
                "Mallama",
                "Mosquera",
                "Nariño",
                "Olaya Herrera",
                "Ospina",
                "Pasto",
                "Policarpa",
                "Potosí",
                "Providencia",
                "Puerres",
                "Pupiales",
                "Ricaurte",
                "Roberto Payán",
                "Samaniego",
                "San Bernardo",
                "San José de Albán",
                "San Lorenzo",
                "San Pablo",
                "San Pedro de Cartago",
                "Sandoná",
                "Santa Bárbara",
                "Santacruz",
                "Sapuyes",
                "Taminango",
                "Tangua",
                "Tumaco",
                "Túquerres",
                "Yacuanquer"
            ]
        },
        {
            "id":22,
            "departamento":"Norte de Santander",
            "zona":"otra",
            "ciudades":[
                "Ábrego",
                "Arboledas",
                "Bochalema",
                "Bucarasica",
                "Cáchira",
                "Cácota",
                "Chinácota",
                "Chitagá",
                "Convención",
                "Cúcuta",
                "Cucutilla",
                "Duranía",
                "El Carmen",
                "El Tarra",
                "El Zulia",
                "Gramalote",
                "Hacarí",
                "Herrán",
                "La Esperanza",
                "La Playa de Belén",
                "Labateca",
                "Los Patios",
                "Lourdes",
                "Mutiscua",
                "Ocaña",
                "Pamplona",
                "Pamplonita",
                "Puerto Santander",
                "Ragonvalia",
                "Salazar de Las Palmas",
                "San Calixto",
                "San Cayetano",
                "Santiago",
                "Santo Domingo de Silos",
                "Sardinata",
                "Teorama",
                "Tibú",
                "Toledo",
                "Villa Caro",
                "Villa del Rosario"
            ]
        },
        {
            "id":23,
            "departamento":"Putumayo",
            "zona":"otra",
            "ciudades":[
                "Colón",
                "Mocoa",
                "Orito",
                "Puerto Asís",
                "Puerto Caicedo",
                "Puerto Guzmán",
                "Puerto Leguízamo",
                "San Francisco",
                "San Miguel",
                "Santiago",
                "Sibundoy",
                "Valle del Guamuez",
                "Villagarzón"
            ]
        },
        {
            "id":24,
            "departamento":"Quindío",
            "zona":"otra",
            "ciudades":[
                "Armenia",
                "Buenavista",
                "Calarcá",
                "Circasia",
                "Córdoba",
                "Filandia",
                "Génova",
                "La Tebaida",
                "Montenegro",
                "Pijao",
                "Quimbaya",
                "Salento"
            ]
        },
        {
            "id":25,
            "departamento":"Risaralda",
            "zona":"otra",
            "ciudades":[
                "Apía",
                "Balboa",
                "Belén de Umbría",
                "Dosquebradas",
                "Guática",
                "La Celia",
                "La Virginia",
                "Marsella",
                "Mistrató",
                "Pereira",
                "Pueblo Rico",
                "Quinchía",
                "Santa Rosa de Cabal",
                "Santuario"
            ]
        },
        {
            "id":26,
            "departamento":"San Andrés y Providencia",
            "zona":"otra",
            "ciudades":[
                "Providencia y Santa Catalina Islas",
                "San Andrés"
            ]
        },
        {
            "id":27,
            "departamento":"Santander",
            "zona":"otra",
            "ciudades":[
                "Aguada",
                "Albania",
                "Aratoca",
                "Barbosa",
                "Barichara",
                "Barrancabermeja",
                "Betulia",
                "Bolívar",
                "Bucaramanga",
                "Cabrera",
                "California",
                "Capitanejo",
                "Carcasí",
                "Cepitá",
                "Cerrito",
                "Charalá",
                "Charta",
                "Chima",
                "Chipatá",
                "Cimitarra",
                "Concepción",
                "Confines",
                "Contratación",
                "Coromoro",
                "Curití",
                "El Carmen de Chucurí",
                "El Guacamayo",
                "El Peñón",
                "El Playón",
                "El Socorro",
                "Encino",
                "Enciso",
                "Florián",
                "Floridablanca",
                "Galán",
                "Gámbita",
                "Girón",
                "Guaca",
                "Guadalupe",
                "Guapotá",
                "Guavatá",
                "Guepsa",
                "Hato",
                "Jesús María",
                "Jordán",
                "La Belleza",
                "La Paz",
                "Landázuri",
                "Lebrija",
                "Los Santos",
                "Macaravita",
                "Málaga",
                "Matanza",
                "Mogotes",
                "Molagavita",
                "Ocamonte",
                "Oiba",
                "Onzaga",
                "Palmar",
                "Palmas del Socorro",
                "Páramo",
                "Piedecuesta",
                "Pinchote",
                "Puente Nacional",
                "Puerto Parra",
                "Puerto Wilches",
                "Rionegro",
                "Sabana de Torres",
                "San Andrés",
                "San Benito",
                "San Gil",
                "San Joaquín",
                "San José de Miranda",
                "San Miguel",
                "San Vicente de Chucurí",
                "Santa Bárbara",
                "Santa Helena del Opón",
                "Simacota",
                "Suaita",
                "Sucre",
                "Suratá",
                "Tona",
                "Valle de San José",
                "Vélez",
                "Vetas",
                "Villanueva",
                "Zapatoca"
            ]
        },
        {
            "id":28,
            "departamento":"Sucre",
            "zona":"otra",
            "ciudades":[
                "Buenavista",
                "Caimito",
                "Chalán",
                "Colosó",
                "Corozal",
                "Coveñas",
                "El Roble",
                "Galeras",
                "Guaranda",
                "La Unión",
                "Los Palmitos",
                "Majagual",
                "Morroa",
                "Ovejas",
                "Sampués",
                "San Antonio de Palmito",
                "San Benito Abad",
                "San Juan de Betulia",
                "San Marcos",
                "San Onofre",
                "San Pedro",
                "Sincé",
                "Sincelejo",
                "Sucre",
                "Tolú",
                "Tolú Viejo"
            ]
        },
        {
            "id":29,
            "departamento":"Tolima",
            "zona":"otra",
            "ciudades":[
                "Alpujarra",
                "Alvarado",
                "Ambalema",
                "Anzoátegui",
                "Armero",
                "Ataco",
                "Cajamarca",
                "Carmen de Apicalá",
                "Casabianca",
                "Chaparral",
                "Coello",
                "Coyaima",
                "Cunday",
                "Dolores",
                "El Espinal",
                "Falán",
                "Flandes",
                "Fresno",
                "Guamo",
                "Herveo",
                "Honda",
                "Ibagué",
                "Icononzo",
                "Lérida",
                "Líbano",
                "Mariquita",
                "Melgar",
                "Murillo",
                "Natagaima",
                "Ortega",
                "Palocabildo",
                "Piedras",
                "Planadas",
                "Prado",
                "Purificación",
                "Rioblanco",
                "Roncesvalles",
                "Rovira",
                "Saldaña",
                "San Antonio",
                "San Luis",
                "Santa Isabel",
                "Suárez",
                "Valle de San Juan",
                "Venadillo",
                "Villahermosa",
                "Villarrica"
            ]
        },
        {
            "id":30,
            "departamento":"Valle del Cauca",
            "zona":"otra",
            "ciudades":[
                "Alcalá",
                "Andalucía",
                "Ansermanuevo",
                "Argelia",
                "Bolívar",
                "Buenaventura",
                "Buga",
                "Bugalagrande",
                "Caicedonia",
                "Cali",
                "Calima",
                "Candelaria",
                "Cartago",
                "Dagua",
                "El Águila",
                "El Cairo",
                "El Cerrito",
                "El Dovio",
                "Florida",
                "Ginebra",
                "Guacarí",
                "Jamundí",
                "La Cumbre",
                "La Unión",
                "La Victoria",
                "Obando",
                "Palmira",
                "Pradera",
                "Restrepo",
                "Riofrío",
                "Roldanillo",
                "San Pedro",
                "Sevilla",
                "Toro",
                "Trujillo",
                "Tuluá",
                "Ulloa",
                "Versalles",
                "Vijes",
                "Yotoco",
                "Yumbo",
                "Zarzal"
            ]
        },
        {
            "id":31,
            "departamento":"Vaupés",
            "zona":"otra",
            "ciudades":[
                "Carurú",
                "Mitú",
                "Taraira"
            ]
        },
        {
            "id":32,
            "departamento":"Vichada",
            "zona":"otra",
            "ciudades":[
                "Cumaribo",
                "La Primavera",
                "Puerto Carreño",
                "Santa Rosalía"
            ]
        }
    ];

    function getDepartaments(dep){
      var toReturn = [];
      if(angular.isUndefined(dep)){
        toReturn = departamentos;
      }else{
        for(var i = 0;i < departamentos.length-1; i++){
          if(dep.departamento == departamentos[i].departamento){
            toReturn = departamentos[i].ciudades;
          }
        }
      }
      return toReturn;
    }

    function getShippingPriceByProduct(idProduct){
        var source = urlProducts+idProduct+'/producto_costoenvio';
        return $http.get(source);
    }

    function getShippingPrice(){
      var source = urlCostoEnvio;
      return $http.get(source);
    }

    function getDataPayU(){
    var state = true;
    // pruebas : false,produccion : true
    var payUdata = [];
      if (state) {
        // produccion
        payUdata = {
          "test": 0,
          "merchantId": 654045,
          "accountId": 656552,
          "apikey": 'Z772ESk1227Rm3S1AR7tNBYloF',
          "action": "https://gateway.payulatam.com/ppp-web-gateway/"
        }
      } else {
        // Pruebas
        payUdata = {
          "test": 1,
          "merchantId": 508029,
          "accountId": 512321,
          "apikey": '4Vj8eK4rloUd272L48hsrarnUA',
          "action": "https://sandbox.gateway.payulatam.com/ppp-web-gateway/"
        }
      }
      return payUdata;
    }

    function insertPedido(pedido) {
        console.log(pedido);
      var source = urlPedidos;
      return $http.post(source, pedido);
    }

    function insertProductoPedido(id_pedido, productopedido) {
      var source = urlPedidos+'/'+id_pedido+'/pedido_productopedido';
      return $http.post(source, productopedido);
    }

    function insertFotos(foto) {
      var source = urlFotos;
      return $http.post(source, foto);
    }

    function insertAccesorios(id_productopedido, acc) {
      var source = urlProductosPedidos+'/'+id_productopedido+'/productopedido_accesorioproductopedido';
      return $http.post(source, acc);
    }

    function getPedidoByReference(reference) {
      var source = urlPedidos+"/?filter[where][referencecode]="+reference+"&filter[limit]=1";
      return $http.get(source);
    }

    function cambiarEstado(estado, id) {
        console.log(estado);
      var source = urlPedidos+"/"+id;
      return $http.patch(source, estado);
    }

    return{
      getPoliticas: getPoliticas,
      getPoliticasById: getPoliticasById,
      getDepartaments: getDepartaments,
      getShippingPriceByProduct: getShippingPriceByProduct,
      getDataPayU : getDataPayU,
      insertPedido: insertPedido,
      insertProductoPedido: insertProductoPedido,
      insertFotos: insertFotos,
      insertAccesorios: insertAccesorios,
      getShippingPrice : getShippingPrice,
      getPedidoByReference :getPedidoByReference,
      cambiarEstado : cambiarEstado
    }
  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ContactoController', ContactoController);

  ContactoController.$inject = ['authService', '$scope', '$state', '$http', '$stateParams', '$window'];

  function ContactoController(authService, $scope, $state, $http, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);
    $scope.contacto = {};
    $scope.sendmessage = '';
    
    $scope.send = function(){
      var urlContacto = '/api/mailing/mensajeContacto?nombre='+$scope.contacto.nombre+'&correo='+$scope.contacto.correo+'&mensaje='+$scope.contacto.mensaje;
      return $http.get(urlContacto).then(function(res){
        $scope.contacto = {};
        $scope.sendmessage = res.data.message;
      });
      
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CuerdasController', CuerdasController);

  CuerdasController.$inject = ['authService', '$scope', '$state', 'CuerdasService', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function CuerdasController(authService, $scope, $state, CuerdasService, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;

    $scope.carrito = {};
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.tipoproductoid = $stateParams.idtipoproducto;

    $scope.files = [];
    $scope.fotosUser = [];

    $scope.cuerdas = [];
    $scope.pinzas = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;
    $scope.polaroid = 'image1.png';

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.tipoproductoid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    CuerdasService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.crearCantidadFotos(0);
    });

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        $scope.fotos.push(i)
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
      $scope.cuerdas = [];
      $scope.pinzas = [];
      $scope.productid = $scope.products[id].id;
      getAccesories($scope.productid);
    };

    function getAccesories(idProduct){
      CuerdasService.getAccesoriesByProduct(idProduct).then(function (res){
        $scope.accesories = res.data;
        for (var i = 0; i < $scope.accesories.length; i++){
          if($scope.accesories[i].tipo == 1){
            $scope.cuerdas.push($scope.accesories[i]);
          }else{
            $scope.pinzas.push($scope.accesories[i]);
          }

        }

        if($scope.selectedCuerdas == null && $scope.selectedPinza == null){
          $scope.selectCuerda($scope.cuerdas[0]);
          $scope.selectPinza($scope.pinzas[0]);
        }else{
          for (var i = 0; i < $scope.cuerdas.length; i++){
            if($scope.selectedCuerdas.color_code == $scope.cuerdas[i].color_code){
              $scope.selectCuerda($scope.cuerdas[i]);
              break;
            }else{
              if(i == $scope.cuerdas.length-1){
                $scope.selectCuerda($scope.cuerdas[0]);
              }
            }
          }
          for (var i = 0; i < $scope.pinzas.length; i++){
            if($scope.selectedPinza.color_code == $scope.pinzas[i].color_code){
              $scope.selectPinza($scope.pinzas[i]);
              break;
            }else{
              if(i == $scope.pinzas.length-1){
                $scope.selectPinza($scope.pinzas[0]);
              }
            }
          }
        }

      });
    }

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    $scope.selectCuerda = function(cuerda){
      $scope.selectedCuerdas = cuerda;
    }

    $scope.selectPinza = function(pinza){
      $scope.selectedPinza = pinza;
    }

    vm.upload = function () {
      $scope.loading = true;

      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];

              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    }

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
    };

    $scope.continue = function (product, format) {
      var formato = format ? 'Cuadrado' : 'Polaroid';
      var colors = {'cuerdas' : $scope.selectedCuerdas , 'pinzas' : $scope.selectedPinza};
      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == product.numfotos){
        var newcarrito = {
          'product': product,
          'color': colors,
          'format': formato,
          'fotos': fotos,
          'paquete': '',
          'preciototal': product.precio
        };
        $scope.carrito.push(newcarrito);
        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        db.put(carro)
          .then(function (response) {
            // handle response
            console.log(response);
          }).catch(function (err) {
          console.log(err);
        });
        $state.go("checkout");
      }else{
        alert("Debes cargar todas las fotos");
      }
    };

  }

}());

/**
 * Created by E5-471-3541 on 22/02/2017.
 */

(function(){
  'use strict';

  angular
    .module('app')
    .factory('CuerdasService', CuerdasService);

  CuerdasService.$inject = ['authService', '$http'];

  function CuerdasService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlProductosAccesorios = "/api/productos/";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesoriesByProduct(id_producto){
      var source = urlProductosAccesorios + id_producto + '/producto_accesorioproducto?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesoriesByProduct : getAccesoriesByProduct
    }

  }

}());

/**
 * Created by pipe on 26/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('CostModalController', CostModalController);

  CostModalController.$inject = ['$uibModalInstance', 'cost', '$scope'];

  function CostModalController($uibModalInstance, cost, $scope) {
    var vm = this;

    vm.cost = cost;
    console.log(cost);

    $scope.close = function(){
      $uibModalInstance.close();
    };
  }

}());


/**
 * Created by pipe on 25/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('DashboardController', DashboardController)
    .filter('DateFilter', DateFilter);

  DashboardController.$inject = ['authService', '$scope', '$state', '$stateParams', '$window', 'DashboardService', '$uibModal','$http'];

  function DashboardController(authService, $scope, $state, $stateParams, $window, DashboardService, $uibModal, $http) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);

    $scope.selectedPedido = {};
    $scope.estado = "";

    $scope.todosFilter = true;
    $scope.entregadosFilter = false;
    $scope.pendientesFilter = false;
    $scope.aprobadosFilter = false;

    $scope.filterType = function (tipo) {
      if(tipo == "todos"){
        $scope.todosFilter = true;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = false;
        $scope.estado = "";
      }else if(tipo == "entregados"){
        $scope.todosFilter = false;
        $scope.entregadosFilter = true;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = false;
        $scope.estado = "Entregado";
      }
      else if(tipo == "pendientes"){
        $scope.todosFilter = false;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = true;
        $scope.aprobadosFilter = false;
        $scope.estado = "Pendiente";
      }else{
        $scope.todosFilter = false;
        $scope.entregadosFilter = false;
        $scope.pendientesFilter = false;
        $scope.aprobadosFilter = true;
        $scope.estado = "Aprobado";
      }
    };

    function loadDashBoard() {
      DashboardService.getProductoPedidos()
        .then(function (res) {
          $scope.productosPedidos = res.data;
          DashboardService.getAccesorios()
            .then(function (res) {
              $scope.accesorios = res.data;
              _.each($scope.productosPedidos, function (pedido) {
                pedido.accesorios =[];
                _.each(pedido.productopedido_accesorioproductopedido, function (acc) {
                  var accesorio = _.find($scope.accesorios, function (accs) {
                    return accs.id == acc.id_accesorioproducto;
                  });
                  pedido.accesorios.push(accesorio);
                });
              });
              DashboardService.getPedidos()
                .then(function (res) {
                  $scope.pedidos = res.data;
                  _.each($scope.pedidos, function (pedido) {
                    pedido.productosPedido = _.filter($scope.productosPedidos, function (producto) {
                      return pedido.id == producto.id_pedido;
                    });
                  });
                });
            });
        });
    }

    loadDashBoard();

    $scope.selectPedido = function (pedido) {
      $scope.selectedPedido = pedido;
    };

    $scope.openUserModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/userModal.html',
        controller: 'UserModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          user: function () {
            return $scope.selectedPedido;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function () {
      });
    };

    $scope.selectProduct = function (product) {
      $scope.selectedProduct = product;
    };

    $scope.openProductModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var nombre = "Pedido-"+$scope.selectedPedido.id+"-idProducto-"+$scope.selectedProduct.id;
      DashboardService.createZip($scope.selectedProduct.productopedido_foto, nombre).then(function (res) {
      });
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/productModal.html',
        controller: 'ProductModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          product: function () {
            return $scope.selectedProduct;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function (err) {
        console.log(err);
      });
    };

    $scope.openCostModal = function (size, parentSelector) {
      var parentElem = parentSelector ?
        angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'templates/admin/modals/costModal.html',
        controller: 'CostModalController',
        controllerAs: 'vm',
        size: size,
        appendTo: parentElem,
        resolve: {
          cost: function () {
            return $scope.selectedPedido;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        vm.selected = selectedItem;
      }, function () {
      });
    };

    $scope.entregarPedido = function (id, email, nombre) {
      var estado = {estado: "Entregado" };
      DashboardService.cambiarEstado(estado, id)
        .then(function (res) {
          
          // sendmail to customer
          var urlSendEmailCustomer = '/api/mailing/pedidoEnviado?nombre='+nombre+'&correo='+email;
          $http.get(urlSendEmailCustomer).then(function(res){
            console.log(res.data.message);
            loadDashBoard();
          });

        });
    }

    $scope.borrarPedido = function (id) {
      var dodelete = confirm('¿Estas seguro de eliminar este pedido?, Esta acción no se puede deshacer.');
      if(dodelete){
        DashboardService.getProductoPedido(id)
        .then(function (res) {
          var prdPeds = res.data;
          prdPeds.forEach(function(prod){
            console.log(prod.id);
            DashboardService.deleteAccesorioProdPedido(prod.id)
            .then(function (acc){
              console.log(acc.data);
              DashboardService.deleteFotoProdPedido(prod.id)
              .then(function(foto){
                console.log(foto.data);
                DashboardService.borrarProductoPedido(prod.id)
                .then(function(prodped){
                  console.log(prodped.data);
                  DashboardService.borrarPedido(id).then(function(){
                    console.log('pedido borrado');
                    loadDashBoard();
                  });
                });
              });
            });
          });
        });
      }
    }

  }

  function DateFilter() {
    function filterDate(items, date) {
      var pedidos = [];
      if(!date){
        return items;
      }
      var dateI = date;
      var dateF = new Date(date.getFullYear(), date.getMonth()+1, date.getDate());
      items.forEach(function (obj) {
        var fechaCreacion = new Date(obj.fecharecepcion);
        if(fechaCreacion >= dateI && fechaCreacion < dateF) {
          pedidos.push(obj);
        }
      });
      return pedidos;
    }
    return filterDate;
  }

}());

/**
 * Created by pipe on 25/04/17.
 */

(function(){
  'use strict';

  angular
    .module('app')
    .factory('DashboardService', DashboardService);

  DashboardService.$inject = ['authService', '$http'];

  function DashboardService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlPedidos = "/api/pedidos";
    var urlProductoPedido = "/api/productospedidos";
    var urlDescarga = "/api/fotos_pedidos/fotos_pedidos/download/";
    var urlAccesorio = "/api/accesoriosproductos";

    function getPedidos() {
      var source = urlPedidos+'?filter[order]=id%20DESC';
      return $http.get(source);
    }

    function getProductoPedidosByIdPedido(idPedido) {
      var source = urlProductoPedido + "?filter[include]=productopedido_foto&" +
        "filter[include]=productopedido_producto&"+
        "filter[where][id_pedido]="+ idPedido +"&"+
        "filter[include]=productopedido_accesorioproductopedido&" +
        "filter[include]=productopedido_empaque&" +
        "filter[include]=productopedido_pedido";
      return $http.get(source);
    }

    function getProductoPedidos() {
      var source = urlProductoPedido + "?filter[include]=productopedido_foto&" +
        "filter[include]=productopedido_producto&" +
        "filter[include]=productopedido_accesorioproductopedido&" +
        "filter[include]=productopedido_empaque&" +
        "filter[include]=productopedido_pedido";
      return $http.get(source);
    }

    function createZip(fotos, reference) {
      var source = "/api/zip/createZip";
      var body = {fotos: fotos, referencia: reference}
      return $http.post(source, body);
    }

    function downloadZip(zip) {
      var source = urlDescarga+zip+".zip";
      return $http.get(source);
    }

    function cambiarEstado(estado, id) {
      var source = urlPedidos+"/"+id;
      return $http.patch(source, estado);
    }

    function getAccesorios() {
      var source = urlAccesorio;
      return $http.get(source);
    }

    function getProductoPedido(id) {
      var source = urlPedidos+"/"+id+"/pedido_productopedido";
      return $http.get(source);
    }

    function deleteAccesorioProdPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped +"/productopedido_accesorioproductopedido";
      return $http.delete(source);
      // return $http.get(source);
    };

    function deleteFotoProdPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped +"/productopedido_foto";
      return $http.delete(source);
      // return $http.get(source);
    };

    function borrarProductoPedido(id_prodped){
      var source = urlProductoPedido+"/"+ id_prodped;
      return $http.delete(source);
      // return $http.get(source);
    };

    function borrarPedido(id) {
      var source = urlPedidos+"/"+id;
      return $http.delete(source);
    }

    return {
      getPedidos: getPedidos,
      getProductoPedidosByIdPedido: getProductoPedidosByIdPedido,
      getProductoPedidos: getProductoPedidos,
      createZip: createZip,
      cambiarEstado: cambiarEstado,
      getAccesorios: getAccesorios,
      getProductoPedido: getProductoPedido,
      deleteAccesorioProdPedido: deleteAccesorioProdPedido,
      deleteFotoProdPedido: deleteFotoProdPedido,
      borrarProductoPedido: borrarProductoPedido,
      borrarPedido: borrarPedido,
    }

  }

}());

/**
 * Created by pipe on 26/04/17.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductModalController', ProductModalController);

  ProductModalController.$inject = ['$uibModalInstance', 'product', 'DashboardService', '$scope'];

  function ProductModalController($uibModalInstance, product, DashboardService, $scope) {
    var vm = this;
    vm.product = product;

    vm.createZip = function (fotos, nombre) {
      DashboardService.createZip(fotos, nombre).then(function (res) {
        console.log(res);
        DashboardService.downloadZip(res.data).then(function (data) {
          console.log(data);
        });
      });
    }

    $scope.close = function(){
      $uibModalInstance.close();
    };

  }

}());

/**
 * Created by pipe on 25/04/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .controller('UserModalController', UserModalController);

  UserModalController.$inject = ['$uibModalInstance', 'user', '$scope'];

  function UserModalController($uibModalInstance, user, $scope) {
    var vm = this;
    vm.user = user;

    $scope.close = function(){
      $uibModalInstance.close();
    };

  }

}());

/**
 * Created by pipe on 21/02/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .directive('footerDirective', footerDirective);

  footerDirective.$inject = [];

  function footerDirective() {
    return{
      templateUrl: 'templates/footer.html'
    }
  }

}());

/**
 * Created by pipe on 21/02/17.
 */
(function () {

  'use strict';

  angular
    .module('app')
    .directive('headerDirective', headerDirective);

  headerDirective.$inject = [];

  function headerDirective() {
    return{
      templateUrl: 'templates/header.html'
    }
  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .factory('FileService', FileService);

  FileService.$inject = ['authService', '$http', 'Upload'];

  function FileService(authService, $http, Upload) {

    var vm = this;
    vm.authService = authService;

    function uploadImg(foto, ind) {
      // fetch(foto.$ngfDataUrl)
      //   .then(res => res.blob())
      //   .then(function (blob) {
          //blob.name = foto.$ngfName;
          var date = new Date();
          // var file = {};
          var id =  Math.random().toString(36).substr(2, 9);
          var name = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+foto.type.split("/")[1];

          //var file = Upload.rename(foto, name);
          file.upload = Upload.upload({
            url: 'api/fotos_pedidos/fotos_pedidos/upload',
            data: {
              file: foto
            },
          }).then(function(res, err){
            console.log(err);
          });
          return file.upload;
          file.upload.then(function (response) {}, function (response) {
            if (response.status > 0)
              var errorMsg = response.status + ': ' + response.data;
          }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
        // })
    }

    function uploadAdminFoto(foto) {
      // fetch(foto.$ngfDataUrl)
      //   .then(res => res.blob())
      //   .then(function (blob) {
      //blob.name = foto.$ngfName;
      var file = {};
      file.upload = Upload.upload({
        url: 'api/fotos_pedidos/admin_fotos/upload',
        data: {
          file: foto
        },
      });
      return file.upload;
      file.upload.then(function (response) {}, function (response) {
        if (response.status > 0)
          var errorMsg = response.status + ': ' + response.data;
      }, function (evt) {
        // Math.min is to fix IE which reports 200% sometimes
        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
      });
      // })
    }


    return {
      uploadImg: uploadImg,
      uploadAdminFoto: uploadAdminFoto
    }
  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('FotosController', FotosController);

  FotosController.$inject = ['authService', '$scope', '$state', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function FotosController(authService, $scope, $state, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $scope.producttype = $stateParams.idtipoproducto;
    $scope.files = [];
    $scope.fotosUser = [];
    $scope.cel = 2;
    $scope.polaroid = 'image1.png';
    let formato = 'Polaroid';

    $window.scrollTo(0, 0);

    $scope.changeFormato = function(formatcx){
      if(formatcx == true){
        $scope.polaroid = 'marco_foto.png';
      }else{
        $scope.polaroid = 'image1.png';
      }
      $scope.format = formatcx;
    }

    function load() {
      $scope.product = $scope.carrito[$scope.carrito.length-1].product;
      crearCantidadFotos();
      if($scope.producttype == 6){
        $scope.polaroid = 'marco_foto.png';
      }
    }

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
    });

    function crearCantidadFotos () {
      $scope.fotos = [];
      for (var i = 0; i < $scope.product.numfotos; i++) {
        $scope.fotos.push(i)
      }
      if($scope.product.numfotos < 5){
        $scope.cel = 12/$scope.product.numfotos;
      }else{
        $scope.cel = 2;
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
      $scope.$apply();
    };

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    vm.upload = function () {
      $scope.loading = true;
      
      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];

              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
      $scope.subidas--;
    };

    $scope.goBack = function(){
      $scope.carrito.splice($scope.carrito.length-1,1);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go($scope.tipos[$scope.product.id_tipoproducto-1].slug, {"idtipoproducto": $scope.product.id_tipoproducto});
    };

    $scope.continue = function (format, precio) {
      
      if(format){
        formato = 'Cuadrado';
      }else{
        formato = 'Polaroid';
      }

      if($scope.producttype == 6){
        formato = 'Cuadrado';
      }

      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": $scope.product.id_tipoproducto == 6 ? $scope.carrito[$scope.carrito.length-1].text : "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == $scope.product.numfotos){
        $scope.carrito[$scope.carrito.length-1].fotos = fotos;
        $scope.carrito[$scope.carrito.length-1].format = formato;
        $scope.carrito[$scope.carrito.length-1].preciototal = precio;

        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        console.log($scope.carrito);
        db.put(carro)
          .then(function (response) {
            // handle response
            console.log(response);
          }).catch(function (err) {
          console.log(err);
        });

        if($scope.product.id_tipoproducto == 6){
          $state.go("empaquePostales", {"idtipoproducto": $scope.product.id_tipoproducto});
        }else{
          $state.go("checkout");
        }
      }else{
        alert("Debes cargar todas las fotos");
      }
    };

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['authService', '$scope', '$state', '$http', 'HomeService', '$stateParams', 'authManager'];

  function HeaderController(authService, $scope, $state, $http, HomeService, $stateParams, authManager) {

    var vm = this;
    vm.authService = authService;

    $scope.profile = vm.authService.getProfile();
    $scope.isAdmin = false;

    vm.authService.getProfileDeferred().then(function (profile) {
      vm.profile = profile;
      $scope.profile = profile;
    });

    if(authManager.isAuthenticated && JSON.parse(localStorage.getItem('profile'))){
      if (JSON.parse(localStorage.getItem('profile')).user_metadata) {
        var tipo = JSON.parse(localStorage.getItem('profile')).user_metadata.roles[0];
        if (tipo == 'admin') {
          $scope.isAdmin = true;
        }else{
          // $state.go('home');
        }
      } else {
        // $state.go('home');
      }
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  HomeController.$inject = ['authService', '$scope', '$state', '$http', 'HomeService', '$stateParams', '$window', '$anchorScroll', '$location'];

  function HomeController(authService, $scope, $state, $http, HomeService, $stateParams, $window, $anchorScroll, $location) {

    var vm = this;
    vm.authService = authService;
    $scope.img = true;
    $scope.isFirefox == false;

    if($state.current.name == "productos"){
      $scope.img = false;
    }else{
      $window.scrollTo(0,0);
    }

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;

        // validate webBrowser
        var userAgent = window.navigator.userAgent;

        var browsers = { chrome: /chrome/i, safari: /safari/i, firefox: /firefox/i, ie: /internet explorer/i };

        var mybrowser = [];

        for (var key in browsers) {
            if (browsers[key].test(userAgent)) {
                mybrowser.push(key);
            }
        };

        if (mybrowser.length == 1) {
            if (mybrowser[0] == 'firefox' || mybrowser[0] == 'safari') {
                $scope.isFirefox = true;
            } else {
                $scope.isFirefox = false;
            }
        }

        if( /Android|iPhone|iPad|iPod/i.test(window.navigator.userAgent) ) {
            $scope.isFirefox = true;
        } else {
            $scope.isFirefox = false;
        }

      })
    }

    $scope.keydown = function(){
      $location.hash('products2');
      $anchorScroll();
    };

    $scope.showtooltip = function(event, item){
      var screenX = event.screenX;
      var screenY = event.screenY;

      angular.element(document.querySelector("#producto"+item)).css("left", screenX+'px');
      angular.element(document.querySelector("#producto"+item)).css("top", screenY-30+'px');

    };

    $scope.closeUserChrome = function(){
      $scope.isFirefox = false;
    };

    load();

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .factory('HomeService', HomeService);

  HomeService.$inject = ['authService', '$http'];

  function HomeService(authService, $http) {

    var vm = this;
    vm.authService = authService;

    var url = "/api/tipoproductos";

    function getTiposProducto() {
      var source = url;
      return $http.get(source);
    }

    return{
      getTiposProducto: getTiposProducto
    }
  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('InstagramController', InstagramController);

  InstagramController.$inject = ['authService', '$scope', '$state', '$http', 'InstagramService', '$stateParams', '$window'];

  function InstagramController(authService, $scope, $state, $http, InstagramService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;

    $window.scrollTo(0,0);

    function load() {
      InstagramService.getFotos().then(function (res) {
        $scope.fotos = JSON.parse(res.data.data).data;
        console.log($scope.fotos);
      })
    }

    load();

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .factory('InstagramService', InstagramService);

  InstagramService.$inject = ['authService', '$http'];

  function InstagramService(authService, $http) {

    var vm = this;
    vm.authService = authService;

    var url = "/api/instagram";

    function getFotos() {
      var source = url+"/getselfmedia";
      return $http.get(source);
    }

    return{
      getFotos: getFotos
    }
  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingController', PackingController);

  PackingController.$inject = ['authService', '$scope', '$state', 'PackingService', 'HomeService', '$stateParams', '$window'];

  function PackingController(authService, $scope, $state, PackingService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.packingGroups = [];
    $scope.tipoProducto = $stateParams.idtipoproducto;
    $scope.productid = $stateParams.idtipoproducto;
    $scope.selectIndexGroup = 0;
    $scope.polaroid = 'image1.png';

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;
      });

      PackingService.getPaquetesByTipoproducto($stateParams.idtipoproducto).then(function (res){
        $scope.paquetes = res.data;
        agruparEmpaques();
        $scope.colorSelected = $scope.packingGroups[$scope.selectIndexGroup][0];
        $scope.packingSelected = $scope.packingGroups[$scope.selectIndexGroup][0];
        $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
        if(isMobile.phone){
          $scope.selectPackMobile(0);
        }
      });
    }

    function agruparEmpaques(){

      var caja = [];
      var sobre = [];

      for(var i = 0; i < $scope.paquetes.length; i++){
        if($scope.paquetes[i].tipo_empaque == 1){
          sobre.push($scope.paquetes[i]);
        }else{
          caja.push($scope.paquetes[i]);
        }
      }

      var imageSobre = {'groupname' : 'Sobre', 'imagemainmobile': '/img/sobreMobile.png', 'imagemain': sobre[0].imagen};
      var imageCaja = {'groupname' : 'Caja', 'imagemainmobile' : '/img/cajaMobile.png', 'imagemain' : caja[0].imagen};

      sobre.push(imageSobre);
      caja.push(imageCaja);
      $scope.packingGroups.push(sobre, caja);
    }

    $scope.selectPack = function(index, indexgroup, lengthGroup){
      $scope.packingSelected = $scope.packingGroups[indexgroup][index];
      $scope.packingGroups[indexgroup][lengthGroup] = {'imagemain' : $scope.packingGroups[indexgroup][index].imagen};
      $scope.totalprice = $scope.packingSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.selectPackMobile = function(indexgroup){
      $scope.colorSelected = $scope.packingGroups[indexgroup][0];
      $scope.packingSelected = $scope.packingGroups[indexgroup];
      $scope.selectIndexGroup = indexgroup;
      $scope.selectColor(indexgroup);
    };

    $scope.selectColor = function(color){
      $scope.colorSelected = $scope.packingSelected[color];
      $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    }

    $scope.goBack = function(tipoProducto){
      $scope.carrito.splice($scope.carrito.length-1,1);
      var carro = {
        _id: 'carrito',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('paqueteFotos', {"idtipoproducto": tipoProducto});
    };

    $scope.continue = function(paquete){
      $scope.carrito[$scope.carrito.length-1].paquete = paquete;
      $scope.carrito[$scope.carrito.length-1].preciototal = $scope.totalprice;

      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go("checkout");
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingService', PackingService);

  PackingService.$inject = ['authService', '$http'];

  function PackingService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";

    function getPaquetesByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_empaque?[filter][where][habilitado]=true&filter[order]=color%20ASC';
      return $http.get(source);
    }

    return {
      getPaquetesByTipoproducto : getPaquetesByTipoproducto
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('MarcosController', MarcosController);

  MarcosController.$inject = ['authService', '$scope', '$state', 'MarcosService', 'HomeService', '$stateParams', '$window'];

  function MarcosController(authService, $scope, $state, MarcosService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    MarcosService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;

      MarcosService.getProdAccesorry($scope.products[0].id).then(function (res) {
        $scope.colors = res.data;
        productColors($scope.products, $scope.colors);
        $scope.antProduct = $scope.productsComplete[$scope.productsComplete.length-1];
        $scope.selectedProduct = $scope.productsComplete[0];
        $scope.sigProduct = $scope.productsComplete[1];
      });

    });

    function productColors(products, colors){
      var colorsArray = [];
      for (var i = 0; i < products.length; i++) {
        var combo = {product: products[i], colors: ''};
        for (var o = 0; o < colors.length; o++){
          if(colors[o].id_producto == products[i].id){
            colorsArray.push(colors[o]);
          }
        };
        combo.colors = colorsArray;
        $scope.productsComplete.push(combo);
        colorsArray = [];
      };
      $scope.selectedColor = $scope.productsComplete[0].colors[0];
    };

    function newProduct(products, colors, id) {

    }

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == productsCompleteLength -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];
      $scope.selectedColor = $scope.selectedProduct.colors[0];
      getPrevNextProduct(productsCompleteLength);

    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = productsCompleteLength-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      $scope.selectedProduct = $scope.productsComplete[$scope.idproduct];
      $scope.selectedColor = $scope.selectedProduct.colors[0];
      getPrevNextProduct(productsCompleteLength);

    };

    function getPrevNextProduct(productsCompleteLength){
      MarcosService.getProdAccesorry($scope.selectedProduct.product.id).then(function (res) {
        $scope.colors = res.data;
        $scope.selectedProduct.colors = res.data;
        productColors($scope.products, $scope.colors);
        if($scope.idproduct == productsCompleteLength -1){
          $scope.antProduct = $scope.productsComplete[$scope.idproduct - 1];
          $scope.sigProduct = $scope.productsComplete[0];
        }else if($scope.idproduct == 0){
          $scope.antProduct = $scope.productsComplete[productsCompleteLength -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        }else{
          $scope.antProduct = $scope.productsComplete[$scope.idproduct -1];
          $scope.sigProduct = $scope.productsComplete[$scope.idproduct + 1];
        };
        $scope.selectedColor = $scope.productsComplete[$scope.idproduct].colors[0];
      });
    }

    $scope.selectPack = function(position, colorindex){
      $scope.selectedProduct = $scope.productsComplete[position];
      $scope.selectedColor = $scope.selectedProduct.colors[colorindex];
    };

    $scope.continue = function (product, color) {
      var colors = {'color' : color};
      var newcarrito = {
        'product': product,
        'color' : colors,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('MarcosService', MarcosService);

  MarcosService.$inject = ['authService', '$http'];

  function MarcosService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";
    var urlProductos = "/api/productos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos+'?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    function getProdAccesorry(id) {
      var source = urlProductos + "/"+id+"/producto_accesorioproducto?filter[order]=nombre%20ASC";
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios,
      getProdAccesorry :getProdAccesorry
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingBackController', PackingBackController);

  PackingBackController.$inject = ['authService', '$scope', '$state', '$http', 'PackingBackService', 'FileService', '$stateParams'];

  function PackingBackController(authService, $scope, $state, $http, PackingBackService, FileService, $stateParams) {

    var vm = this;
    vm.authService = authService;

    $scope.empaque = {};
    $scope.productidURL = $stateParams.idproducto;
    $scope.stateedition = false;
    $scope.pack = {};

    if($scope.productidURL){
      $scope.stateedition = true;
      PackingBackService.getPacking($scope.productidURL).then(function(res){
        $scope.pack = res.data;

        $scope.empaque.nombre = $scope.pack.nombre;
        $scope.empaque.precio = $scope.pack.precio;
        $scope.empaque.color = $scope.pack.color;
        $scope.empaque.color_code = $scope.pack.color_code;
        $scope.empaque.descripcion = $scope.pack.descripcion;
        $scope.empaque.imagen = $scope.pack.imagen;
        $scope.empaque.tipo_empaque = $scope.pack.tipo_empaque;
        $scope.empaque.id_tipoproducto = $scope.pack.id_tipoproducto;
        $scope.empaque.id = $scope.productidURL;
      });
    }else{
      $scope.stateedition = false;
    };

    PackingBackService.getTiposProducto().then(function (res) {
      $scope.productTypes = res.data;
    });

    $scope.createPacking = function(){
      if($scope.empaque.image){
        FileService.uploadAdminFoto($scope.empaque.image[0]);
        var image = "img/fotos_pedidos/admin_fotos/"+$scope.empaque.image[0].$ngfName;
      }else{
        var image = '';
      }

      var pack = {
        "nombre": $scope.empaque.nombre,
        "precio": $scope.empaque.precio,
        "color": $scope.empaque.color,
        "color_code": $scope.empaque.color_code,
        "descripcion": $scope.empaque.descripcion,
        "imagen": image,
        "tipo_empaque": $scope.empaque.tipo_empaque,
        "id_tipoproducto": $scope.empaque.id_tipoproducto,
        "habilitado" : false
      };

      PackingBackService.createPacking(pack).then(function(res){
        $state.go('products');
      });

    };

    $scope.editPacking = function(){

      if($scope.empaque.image){
        FileService.uploadAdminFoto($scope.empaque.image[0]);
        var image = "img/fotos_pedidos/admin_fotos/"+$scope.empaque.image[0].$ngfName;
      }else{
        var image = $scope.pack.imagen;
      }

      var pack = {
        "nombre": $scope.empaque.nombre,
        "precio": $scope.empaque.precio,
        "color": $scope.empaque.color,
        "color_code": $scope.empaque.color_code,
        "descripcion": $scope.empaque.descripcion,
        "imagen": image,
        "tipo_empaque": $scope.empaque.tipo_empaque,
        "id_tipoproducto": $scope.empaque.id_tipoproducto
      };

      PackingBackService.editPacking($scope.empaque.id, pack).then(function(res){
        $state.go('products');
      });

    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingBackService', PackingBackService);

  PackingBackService.$inject = ['authService', '$http'];

  function PackingBackService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProducto = "/api/tipoproductos";
    var urlPacking = "/api/empaques";

    function getTiposProducto(){
      var source = urlTipoProducto;
      return $http.get(source);
    }

    function createPacking(pack){
      var source = urlPacking;
      return $http.post(source, pack);
    }

    function getPacking(idpack) {
      var source = urlPacking+'/'+idpack;
      return $http.get(source);
    }

    function editPacking(idpack, pack){
      var source = urlPacking+'/'+idpack;
      return $http.patch(source, pack);
    }

    return {
      getTiposProducto : getTiposProducto,
      createPacking : createPacking,
      getPacking : getPacking,
      editPacking : editPacking
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('PackingPostalesController', PackingPostalesController);

  PackingPostalesController.$inject = ['authService', '$scope', '$state', 'PackingPostalesService', 'HomeService', '$stateParams', '$window'];

  function PackingPostalesController(authService, $scope, $state, PackingPostalesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
      load();
    }).catch(function (err) {
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.packingGroups = [];
    $scope.tipoProducto = $stateParams.idtipoproducto;
    $scope.selectIndexGroup = 0;

    var carritoText = window.localStorage.getItem('carrito');
    $scope.carritoJSON = JSON.parse(carritoText);

    function load() {
      HomeService.getTiposProducto().then(function (res) {
        $scope.tipos = res.data;
      });

      PackingPostalesService.getPaquetesByTipoproducto($scope.carrito[$scope.carrito.length-1].product.id_tipoproducto).then(function (res){
        $scope.paquetes = res.data;
        $scope.selectColor($scope.paquetes[0]);

      }).catch(function (err) {
        console.log(err);
      });
    }

    $scope.selectColor = function(color){
      $scope.packingSelected = color;
      $scope.colorSelected = color;
      $scope.totalprice = $scope.colorSelected.precio + $scope.carrito[$scope.carrito.length-1].product.precio;
    };

    $scope.goBack = function(){
      var carritoText = JSON.stringify($scope.carritoJSON);
      window.localStorage.setItem('carrito', carritoText);
      $state.go('fotos');
    };

    $scope.continue = function(paquete){
      $scope.carrito[$scope.carrito.length-1].paquete = paquete;
      $scope.carrito[$scope.carrito.length-1].preciototal = $scope.totalprice;

      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go("checkout");
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('PackingPostalesService', PackingPostalesService);

  PackingPostalesService.$inject = ['authService', '$http'];

  function PackingPostalesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";

    function getPaquetesByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_empaque?[filter][where][habilitado]=true&filter[order]=color%20ASC';

      return $http.get(source);
    }

    return {
      getPaquetesByTipoproducto : getPaquetesByTipoproducto
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductController', ProductController);

  ProductController.$inject = ['authService', '$scope', '$state', 'ProductService', 'HomeService', '$stateParams', '$window', '$timeout', 'FileService'];

  function ProductController(authService, $scope, $state, ProductService, HomeService, $stateParams, $window, $timeout, FileService) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = [];
    $scope.doc ={};
    $scope.subidas = 0;

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      console.log(doc.cart);
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.files = [];
    $scope.fotosUser = [];
    $scope.productid = $stateParams.idtipoproducto;
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;
    $scope.polaroid = 'image1.png';
    $scope.loading = false;
    $scope.cel = 2;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length-1; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }

    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    ProductService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.crearCantidadFotos(0);
    });

    $scope.crearCantidadFotos = function (id) {
      $scope.fotos = [];
      for (var i = 0; i < $scope.products[id].numfotos; i++) {
        var toPush = {height: '',width: ''};
        $scope.fotos.push(toPush);
      }
      if($scope.products[id].numfotos < 5){
        $scope.cel = 12/$scope.products[id].numfotos;
      }else{
        $scope.cel = 2;
      }
      if ($scope.fotos.length < $scope.fotosUser.length) {
        verificarFotos();
      }
    };

    function verificarFotos() {
      if ($scope.fotos.length < $scope.fotosUser.length) {
        var aEliminar = $scope.fotosUser.length - $scope.fotos.length;
        $scope.fotosUser.splice($scope.fotos.length, aEliminar);
        verificarFotos();
      }
    };

    vm.upload = function () {
      $scope.loading = true;
      // here might be the problem
      if($scope.files.length == 0){
        $scope.loading = false;
      }else{
        $timeout(function(){
          $scope.files.forEach(function (file, ind) {
            if ($scope.fotos.length > $scope.fotosUser.length) {

              file.height = file.$ngfHeight;
              file.width = file.$ngfWidth;
              console.log(file);
              var id = 0;

              var date = new Date();
              var id =  Math.random().toString(36).substr(2, 9);
              file.ngfName = (date.getMonth()+1)+"-"+date.getDay()+"-"+date.getYear()+"-"+id+"-"+ind+"."+file.type.split("/")[1];
              $scope.fotosUser.push(file);
              FileService.uploadImg(file, ind).then(function (res) {
                $scope.subidas++;
                console.log($scope.subidas, $scope.fotosUser.length);
                if($scope.fotosUser.length == $scope.subidas){
                  $scope.loading = false;
                }
              })
            }
            else {
              if(!$scope.complet){
                $scope.complet = true;
                if ($scope.fotosUser.length > 1) {
                  alert("Enviaste más fotos de las necesarias, usaremos las primeras "+ $scope.fotos.length);
                }else{
                  alert("Enviaste más fotos de las necesarias, usaremos la primera foto");
                }
                $scope.loading = false;
              }
            }
          });
        }, 500);
      }
    };

    $scope.changeFormato = function(format){
      if(format){
        $scope.polaroid = 'image1.png';
      }else{
        $scope.polaroid = 'marco_foto.png';
      }
    };

    $scope.delete = function(index){
      $scope.fotosUser.splice(index,1);
    };

    $scope.continue = function (product, format) {
      var formato = format ? 'Cuadrado' : 'Polaroid';
      var fotos = [];
      $scope.fotosUser.forEach(function (foto) {
        var fotodb = {
          "imagen": "client/img/fotos_pedidos/fotos_pedidos/"+foto.ngfName,
          "texto": "a",
          "color": "a",
          "formato": formato
        };
        fotos.push(fotodb);
      });
      if($scope.fotosUser.length == product.numfotos){
        var newcarrito = {
          'product': product,
          'color': '',
          'format': formato,
          'fotos': fotos,
          'paquete': '',
          'preciototal': product.precio
        };
        console.log($scope.carrito);
        $scope.carrito.push(newcarrito);
        var carro = {
          _id: 'instampaCart',
          _rev: $scope.doc._rev,
          cart: $scope.carrito
        };
        db.put(carro)
          .then(function (response) {
          // handle response
            console.log(response);
        }).catch(function (err) {
          console.log(err);
        });
        if($stateParams.idtipoproducto == 1){
          $state.go('empaque', {"idtipoproducto": $stateParams.idtipoproducto});
        }
        else{
          $state.go('checkout');
        }
      }else{
        alert("Debes cargar todas las fotos");
      }

    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('ProductService', ProductService);

  ProductService.$inject = ['authService', '$http'];

  function ProductService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlProductosPedidos = "/api/productospedidos/";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto
    }

  }

}());/**
 * Created by E5-471-3541 on 22/02/2017.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('PolicyController', PolicyController);

  PolicyController.$inject = ['authService', '$scope', '$state', 'PolicyService', '$window'];

  function PolicyController(authService, $scope, $state, PolicyService, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.politicaedit = {};
    $scope.politicaedit2 = {};
    
    PolicyService.getPolicy().then(function(res){
        $scope.policy = res.data;
    });

    $scope.editPolicy = function(idPolicy, pos){
      var policytoedit = {};
      if(pos == 1){
        var policyEdit = {
          "nombre": $scope.politicaedit.nombre,
          "descripcion": $scope.politicaedit.descripcion,
          "obligatorio": $scope.policy[0].obligatorio == 1 ? true : false
        };
      }else{
        var policyEdit = {
          "nombre": $scope.politicaedit2.nombre,
          "descripcion": $scope.politicaedit2.descripcion,
          "obligatorio": $scope.policy[1].obligatorio == 1 ? true : false
        };
      }

      PolicyService.updatePolicy(idPolicy, policyEdit);
            
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('PolicyService', PolicyService);

  PolicyService.$inject = ['authService', '$http'];

  function PolicyService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlPolicy = "/api/politicas";

    function getPolicy(){
      var source = urlPolicy;
      return $http.get(source);
    }

    function updatePolicy(idPolicy, policy){
      var source = urlPolicy+'/'+idPolicy;
      return $http.patch(source, policy);
    }

    return {
      getPolicy : getPolicy,
      updatePolicy : updatePolicy
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('PostalesController', PostalesController);

  PostalesController.$inject = ['authService', '$scope', '$state', 'PostalesService', 'HomeService', '$stateParams', '$window'];

  function PostalesController(authService, $scope, $state, PostalesService, HomeService, $stateParams, $window) {

    var vm = this;
    vm.authService = authService;
    $scope.carrito = {};
    $scope.doc ={};

    var db = new PouchDB('instampaCart');
    db.get('instampaCart').then(function (doc) {
      $scope.doc = doc;
      $scope.carrito = doc.cart;
      $scope.$apply();
    }).catch(function (err) {
      $scope.carrito = [];
      console.log(err);
    });

    $window.scrollTo(0, 0);

    $scope.cel = 12;

    $scope.productid = $stateParams.idtipoproducto;

    $scope.productsComplete = [];
    $scope.idproduct = 0;
    $scope.colors = [];
    $scope.disclaimer = [];
    $scope.disclaimer_active = false;

    HomeService.getTiposProducto().then(function (res) {
      $scope.tipos = res.data;
      for(var i = 0; i < $scope.tipos.length; i++){
        if($scope.tipos[i].id == $scope.productid){
          $scope.disclaimer = $scope.tipos[i];
        }
      }
    });

    $scope.displayDisclaimer = function(state){
      $scope.disclaimer_active = state;
    };

    PostalesService.getProductosByTipoproducto($stateParams.idtipoproducto).then(function (res) {
      $scope.products = res.data;
      $scope.antProduct = $scope.products[$scope.products.length-1];
      $scope.selectedProduct = $scope.products[0];
      $scope.sigProduct = $scope.products[1];
    });

    PostalesService.getAccesorios().then(function (res) {
      $scope.colors = res.data;
    });

    $scope.nextProduct = function(idProduct, productsCompleteLength){
      if(idProduct == productsCompleteLength -1){
        $scope.idproduct = 0;
      } else {
        $scope.idproduct = idProduct+1;
      };
      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.products[$scope.idproduct];

    };

    $scope.prevProduct = function(idProduct, productsCompleteLength){
      if(idProduct == 0) {
        $scope.idproduct = productsCompleteLength-1;
      } else {
        $scope.idproduct = idProduct-1;
      };

      getPrevNextProduct(productsCompleteLength);

      $scope.selectedProduct = $scope.products[$scope.idproduct];

    };

    function getPrevNextProduct(productsCompleteLength){
      if($scope.idproduct == productsCompleteLength -1){
        $scope.antProduct = $scope.products[$scope.idproduct - 1];
        $scope.sigProduct = $scope.products[0];
      }else if($scope.idproduct == 0){
        $scope.antProduct = $scope.products[productsCompleteLength -1];
        $scope.sigProduct = $scope.products[$scope.idproduct + 1];
      }else{
        $scope.antProduct = $scope.products[$scope.idproduct -1];
        $scope.sigProduct = $scope.products[$scope.idproduct + 1];
      };
    }

    $scope.continue = function (product, text) {

      var newcarrito = {
        'product': product,
        'text' : text,
        'format': '',
        'fotos': '',
        'paquete': '',
        'preciototal': 0
      };
      $scope.carrito.push(newcarrito);
      var carro = {
        _id: 'instampaCart',
        _rev: $scope.doc._rev,
        cart: $scope.carrito
      };
      db.put(carro)
        .then(function (response) {
          // handle response
          console.log(response);
        }).catch(function (err) {
        console.log(err);
      });
      $state.go('fotos', {"idtipoproducto": $stateParams.idtipoproducto});
    };
  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('PostalesService', PostalesService);

  PostalesService.$inject = ['authService', '$http'];

  function PostalesService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProductos = "/api/tipoproductos/";
    var urlAccesoriosProductos = "api/accesoriosproductos";

    function getProductosByTipoproducto(id_tipoproducto){
      var source = urlTipoProductos + id_tipoproducto + '/tipoproducto_producto?[filter][where][habilitado]=true&filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getAccesorios(){
      var source = urlAccesoriosProductos+'?filter[order]=nombre%20ASC';
      return $http.get(source);
    }

    return {
      getProductosByTipoproducto : getProductosByTipoproducto,
      getAccesorios : getAccesorios
    }

  }

}());

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['authService', '$scope', '$state', '$http', 'ProductsService', 'FileService', '$stateParams'];

  function ProductsController(authService, $scope, $state, $http, ProductsService, FileService, $stateParams) {

    var vm = this;
    vm.authService = authService;
    $scope.idSelectedType = 0;
    $scope.products = [];
    $scope.idempaques = 0;
    $scope.newproduct = {};
    $scope.newproduct.costoenvio = [];
    $scope.accesories = [];
    $scope.priceshipping = [];
    $scope.productidURL = $stateParams.idproducto;
    $scope.stateedition = false;
    $scope.toremoveacces = [];
    $scope.msg = '';
    $scope.continue = true;

    if($scope.productidURL){
      $scope.stateedition = true;
      ProductsService.getProduct($scope.productidURL).then(function(res){
        var product = res.data;
        var costoenvio = product.producto_costoenvio;
        $scope.newproduct.nombre = product.nombre;
        $scope.newproduct.descripcion = product.descripcion;
        $scope.newproduct.fotos = product.numfotos;
        $scope.newproduct.tipoproducto = product.id_tipoproducto;
        $scope.newproduct.precio = product.precio;
        $scope.newproduct.accesorio = product.producto_accesorioproducto;
        $scope.accesories = product.producto_accesorioproducto;
        $scope.priceshipping = product.producto_costoenvio;
        $scope.newproduct.costoenvio = product.producto_costoenvio;
      });
    }else{
      $scope.stateedition = false;
      $scope.newproduct.tipoproducto = 1;
      $scope.accesories= [{
        "nombre": "",
        "imagen": "",
        "color": "",
        "color_code": "",
        "detalle": "",
        "tipo": 1,
        "id_producto": $scope.idSelectedType
      }];
      $scope.priceshipping = [
        {"id" : 0,"precio" : 0,"zona" : 'bogota'},
        {"id" : 0,"precio" : 0,"zona" : 'costa'},
        {"id" : 0,"precio" : 0,"zona" : 'valledupar/Monteria'},
        {"id" : 0,"precio" : 0,"zona" : 'otra'}
      ];

    };

    ProductsService.getTiposProducto().then(function (res) {
      $scope.productTypes = res.data;
      $scope.idempaques = $scope.productTypes.length+1;
      $scope.idSelectedType = $scope.productTypes[0].id;
      $scope.selectType($scope.idSelectedType);
    });


    $scope.selectType = function(idtype){
      if(idtype == $scope.idempaques){
        $scope.idSelectedType = $scope.idempaques;
        ProductsService.getEmpaques().then(function (res){
          $scope.products = res.data;
        });
        $scope.isempaque = true;
      }else{
        $scope.idSelectedType = idtype;
        getProducts($scope.idSelectedType);
      }
    }

    function getProducts(idtype){
      ProductsService.getProductsByTipo(idtype).then(function (res){
        $scope.products = res.data;
      });
      $scope.isempaque = false;
    };

    // DELETE

    $scope.deleteProduct = function(idProduct){
      if(confirm('Realmente deseas eliminar este producto, esta acción no se puede deshacer')){
        ProductsService.deleteAccesoryByProduct(idProduct).then(function(){
          ProductsService.deleteShippingPriceByProduct(idProduct).then(function(){
            ProductsService.deleteProduct(idProduct).then(function(){
              $scope.selectType($scope.idSelectedType);
            });
          });
        });
      };
    };

    $scope.addAccesory = function(){
      var voidAccesory = [{
        "nombre": "",
        "imagen": "",
        "color": "",
        "color_code": "",
        "detalle": "",
        "tipo": 1,
        "id_producto": $scope.idSelectedType
      }];
      $scope.accesories.push(voidAccesory);
    };

    $scope.removeAccesory = function(index){
      if(confirm('Realmente deseas eliminar este accesorio, esta acción no se puede deshacer')){
        var toremove = {
          id:$scope.accesories[0].id
        };
        $scope.toremoveacces.push(toremove);
        $scope.accesories.splice(index, 1);
      };
    };

    $scope.changeEstatus = function(idProduct, habilitado, empaque){
      var product_status = {
        "habilitado": !habilitado
      };
      if(empaque){
        ProductsService.changeEmpaqueStatus(idProduct, product_status).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      }else{
        ProductsService.changeProductStatus(idProduct, product_status).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      }

    };

    // CREATE

    $scope.validateCreate = function(){
      if($scope.newproduct.accesorio){
        var countAccesorios = Object.keys($scope.newproduct.accesorio).length;
        var cuerdas = 0;
        var pinzas = 0;
        if($scope.newproduct.tipoproducto == 4){
          if(countAccesorios > 1 ){
            for (var key in $scope.newproduct.accesorio) {
              if($scope.newproduct.accesorio[key].tipo == 1){
                cuerdas += 1; 
              }else{
                pinzas += 1;
              }
            }
            if(cuerdas > 0 && pinzas > 0){
              $scope.msg = '';
              return true;
            }else{
              $scope.msg = 'Debes agregar un accesorio pinzas y uno cuerda como mínimo.';
              return false;
            }
          }else{
            $scope.msg = 'Debes agregar un accesorio pinzas y uno cuerda como mínimo.';
            return false;
          }
        }else if($scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
          if(countAccesorios > 0 ){
            $scope.msg = '';
            return true;
          }else{
            $scope.msg = 'Debes agregar un color como mínimo.';
            return false;
          }
        }else{
          $scope.msg = '';
          return true;
        }
        
      }else{
        $scope.msg = 'Debes agregar un accesorio como mínimo.';
        return false;
      }
    }

    $scope.createProduct = function(){
      $scope.continue = true;

      if($scope.newproduct.tipoproducto == 4 || $scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
        $scope.continue = $scope.validateCreate()
      }

      if($scope.continue){
        if($scope.newproduct.tipoproducto == 5){
          var image = "/img/fotos_pedidos/admin_fotos/albums/album.png";
        }else if($scope.newproduct.tipoproducto == 6){
          var image = "img/fotos_pedidos/admin_fotos/postales/postal.svg";
        }else{
          var image = "x";
        }
        var productdb = {
          "nombre": $scope.newproduct.nombre,
          "descripcion": $scope.newproduct.descripcion,
          "imagen": image,
          "numfotos": $scope.newproduct.fotos,
          "precio": $scope.newproduct.precio,
          "id_tipoproducto": $scope.newproduct.tipoproducto,
          "habilitado" : false

        }

        ProductsService.createProduct(productdb).then(function (res){
          var idproductdb = res.data.id;

          // ACCESORIOS
          _.each($scope.newproduct.accesorio, function(accesorio){
            if(accesorio.fileaccesorio){
              FileService.uploadImg(accesorio.fileaccesorio[0]);
              var imageaccesorio = "img/fotos_pedidos/admin_fotos/"+accesorio.fileaccesorio[0].$ngfName;
            }else{
              var imageaccesorio = null;
            }

            var accesorydb = {
              "nombre": accesorio.nombre,
              "imagen": imageaccesorio,
              "color": accesorio.color,
              "color_code": accesorio.color_code,
              "detalle": accesorio.detalle,
              "tipo": accesorio.tipo
            }

            ProductsService.createAccesory(idproductdb, accesorydb).then(function(res){});
          });

          // COSTOS DE ENVIO
          _.each($scope.newproduct.costoenvio, function(costo, index){
            var zonas = {0 : 'bogota', 1: 'costa', 2: 'valledupar/Monteria', 3: 'otra'};
            var shippingPrice = {
              "zona" : zonas[index],
              "precio": costo.precio
            };

            ProductsService.createShippingPrice(idproductdb, shippingPrice).then(function(res){});
          });

        });

        $state.go('products');
      }

    };

    // EDIT

    $scope.editProduct = function(){
      $scope.continue = true;

      if($scope.newproduct.tipoproducto == 4 || $scope.newproduct.tipoproducto == 3 || $scope.newproduct.tipoproducto == 2){
        $scope.continue = $scope.validateCreate()
      }

      if($scope.continue){
        if($scope.newproduct.tipoproducto == 5){
          var image = "/img/fotos_pedidos/admin_fotos/albums/album.png";
        }else if($scope.newproduct.tipoproducto == 6){
          var image = "img/fotos_pedidos/admin_fotos/postales/postal.svg";
        }else{
          var image = "x";
        }
        var editproductdb = {
          "nombre": $scope.newproduct.nombre,
          "descripcion": $scope.newproduct.descripcion,
          "imagen": image,
          "numfotos": $scope.newproduct.fotos,
          "precio": $scope.newproduct.precio,
          "id_tipoproducto": $scope.newproduct.tipoproducto
        };

        ProductsService.editProduct($scope.productidURL, editproductdb);

        // ACCESORIOS
        _.each($scope.newproduct.accesorio, function(accesorio, index){
          if(accesorio.fileaccesorio){
            FileService.uploadAdminFoto(accesorio.fileaccesorio[0]);
            var imageaccesorio = "img/fotos_pedidos/admin_fotos/"+accesorio.fileaccesorio[0].$ngfName;
          }else{
            var imageaccesorio = $scope.newproduct.accesorio[index].imagen;
          }

          var accesorydb = {
            "nombre": accesorio.nombre,
            "imagen": imageaccesorio,
            "color": accesorio.color,
            "color_code": accesorio.color_code,
            "detalle": accesorio.detalle,
            "tipo": accesorio.tipo,
            "id_producto" : $scope.productidURL
          }
          if(accesorio.id){
            ProductsService.editAccesory(accesorio.id, accesorydb).then(function(res){});
          }else{
            ProductsService.createAccesory($scope.productidURL, accesorydb).then(function(res){});
          }

        });

        _.each($scope.toremoveacces, function(accesorio){
          ProductsService.deleteAccesory(accesorio.id).then(function(res){});
        });

        // COSTOS DE ENVIO
        _.each($scope.newproduct.costoenvio, function(costo){
          var shippingPrice = {
            "zona" : costo.zona,
            "precio": costo.precio,
            "id_producto" : $scope.productidURL
          };

          ProductsService.editShippingPrice(costo.id, shippingPrice).then(function(res){});
        });

        $scope.newproduct = [];
        $state.go('products');
      }
      
    };

    // DELETE PACK

    $scope.deletePack = function(idPack){
      if(confirm('Realmente deseas eliminar este empaque, esta acción no se puede deshacer')){
        ProductsService.deletePack(idPack).then(function(){
          $scope.selectType($scope.idSelectedType);
        });
      };
    };

  }

}());

(function(){
  'use strict';

  angular
    .module('app')
    .factory('ProductsService', ProductsService);

  ProductsService.$inject = ['authService', '$http'];

  function ProductsService(authService, $http){

    var vm = this;
    vm.authService = authService;

    var urlTipoProducto = "/api/tipoproductos";
    var urlEmpaques = "/api/empaques";
    var urlProductos = "/api/productos";
    var urlAccesories = "/api/accesoriosproductos";
    var urlShippingPrice = "/api/costosenvios";

    function getTiposProducto(){
      var source = urlTipoProducto;
      return $http.get(source);
    }

    function getProductsByTipo(idType){
      var source = urlTipoProducto+'/'+idType+'/tipoproducto_producto?filter[order]=numfotos%20ASC';
      return $http.get(source);
    }

    function getEmpaques(){
      var source = urlEmpaques;
      return $http.get(source);
    }

    // Create

    function createProduct(product){
      var source = urlProductos;
      return $http.post(source, product);
    }

    function createAccesory(idProduct, accesory){
      var source = urlProductos+'/'+idProduct+'/producto_accesorioproducto';
      return $http.post(source, accesory);
    }

    function createShippingPrice(idProduct, costos){
      var source = urlProductos+'/'+idProduct+'/producto_costoenvio';
      return $http.post(source, costos);
    }

    // Edit

    function getProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/?filter[include]=producto_accesorioproducto&'+
      'filter[include]=producto_costoenvio';
      return $http.get(source);
    }

    function editProduct(idproduct, product){
      var source = urlProductos+'/'+idproduct;
      return $http.patch(source, product);
    }

    function editAccesory(idAccesory, accesory){
      var source = urlAccesories+'/'+idAccesory;
      return $http.patch(source, accesory);
    }

    function deleteAccesory(idAccesory){
      var source = urlAccesories+'/'+idAccesory;
      return $http.delete(source);
    }

    function editShippingPrice(idShippingPrice, ShippingPrice){
      var source = urlShippingPrice+'/'+idShippingPrice;
      return $http.put(source, ShippingPrice);
    }

    function changeProductStatus(idProduct, product){
      var source = urlProductos+'/'+idProduct;
      return $http.patch(source, product);
    }

    function changeEmpaqueStatus(idProduct, product){
      var source = urlEmpaques+'/'+idProduct;
      return $http.patch(source, product);
    }

    // DELETE

    function deleteAccesoryByProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/producto_accesorioproducto';
      return $http.delete(source);
    }

    function deleteShippingPriceByProduct(idProduct){
      var source = urlProductos+'/'+idProduct+'/producto_costoenvio';
      return $http.delete(source);
    }

    function deleteProduct(idProduct){
      var source = urlProductos+'/'+idProduct;
      return $http.delete(source);
    }

    function deletePack(idPack){
      var source = urlEmpaques+'/'+idPack;
      return $http.delete(source);
    }

    return {
      getTiposProducto : getTiposProducto,
      getProductsByTipo : getProductsByTipo,
      getEmpaques : getEmpaques,
      createProduct : createProduct,
      createAccesory : createAccesory,
      createShippingPrice : createShippingPrice,
      getProduct : getProduct,
      editProduct : editProduct,
      editAccesory : editAccesory,
      deleteAccesory : deleteAccesory,
      editShippingPrice : editShippingPrice,
      changeProductStatus : changeProductStatus,
      changeEmpaqueStatus : changeEmpaqueStatus,
      deleteAccesoryByProduct : deleteAccesoryByProduct,
      deleteShippingPriceByProduct : deleteShippingPriceByProduct,
      deleteProduct : deleteProduct,
      deletePack : deletePack

    }

  }

}());
/**
 * Created by E5-471-3541 on 22/02/2017.
 */

(function () {

  'use strict';

  angular
    .module('app')
    .controller('ResponseController', ResponseController);

  ResponseController.$inject = ['authService', '$scope', '$state', '$stateParams', '$window', '$location', 'md5', 'CheckoutService', 'DashboardService', '$locale', '$http', '$httpParamSerializerJQLike'];

  function ResponseController(authService, $scope, $state, $stateParams, $window, $location, md5, CheckoutService, DashboardService, $locale, $http, $httpParamSerializerJQLike) {

    $window.scrollTo(0, 0);

    $scope.idPedido = 0;

    $scope.datapayU = $location.search();

    var payU_data = CheckoutService.getDataPayU();
    CheckoutService.getPedidoByReference($scope.datapayU.referenceCode).then(function(res){
      $scope.idPedido = res.data[0].id;
      $scope.customername = res.data[0].nombre;
      $scope.table = "";

      $scope.response = $scope.datapayU.lapTransactionState;
      $scope.title = "";
      $scope.tx_value = $scope.datapayU.TX_VALUE.toString();
      var length = $scope.tx_value.length;
      var txValue = $scope.tx_value.slice(0,length-1);
      var firma_cadena = payU_data.apikey+ "~" +$scope.datapayU.merchantId+ "~" +$scope.datapayU.referenceCode+ "~" +txValue+ "~" +$scope.datapayU.currency+ "~" +$scope.datapayU.transactionState;
      $scope.firma_creada = md5.createHash(firma_cadena || '');

      if($scope.firma_creada == $scope.datapayU.signature){
        switch($scope.response){
          case 'APPROVED':
            $scope.title = "Compra exitosa";
            // var estado = {estado: "Aprobado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'DECLINED':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'ERROR':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'EXPIRED':
            $scope.title = "Transacción rechazada";
            // var estado = {estado: "Rechazado"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
          case 'PENDING':
            $scope.title = "Transacción pendiente";
            // var estado = {estado: "Pendiente"};
            // CheckoutService.cambiarEstado(estado, $scope.idPedido);
            break;
        }
        
      }else{
        $scope.title = "Ocurrió un error intentelo mas tarde.";
        if($scope.datapayU.confirm == 'true'){
          DashboardService.getProductoPedidosByIdPedido($scope.idPedido).then(function (res) {
            $scope.productosPedidos = res.data;
            DashboardService.getAccesorios().then(function (res) {
                $scope.accesorios = res.data;
                _.each($scope.productosPedidos, function (pedido) {
                  pedido.accesorios =[];
                  _.each(pedido.productopedido_accesorioproductopedido, function (acc) {
                    var accesorio = _.find($scope.accesorios, function (accs) {
                      return accs.id == acc.id_accesorioproducto;
                    });
                    pedido.accesorios.push(accesorio);
                  });
                });
                DashboardService.getPedidos().then(function (res) {
                    $scope.pedidos = res.data;
                    _.each($scope.pedidos, function (pedido) {
                      pedido.productosPedido = _.filter($scope.productosPedidos, function (producto) {
                        return pedido.id == $scope.idPedido;
                      });
                    });

                    _.each($scope.productosPedidos, function (pedidoprod) {
                      var formato = pedidoprod.productopedido_foto[0].formato ? 'Cuadrado' : 'Polaroid';
                      var accesories = "";
                      _.each(pedidoprod.accesorios, function (acces, i) {
                        if(i == pedidoprod.accesorios.length-1){
                          accesories += acces.color;
                        }else{
                          accesories += acces.color+' | ';
                        }
                      });
                      $scope.table += '<tr class="title" align="center" style="font:12px/17px Arial, Helvetica, sans-serif; color:#000; height: 3rem;"><td>'+ pedidoprod.productopedido_producto.nombre +'</td><td>'+ pedidoprod.productopedido_producto.numfotos +'</td><td>'+ formato +'</td><td>'+ accesories +'</td></tr>';
                    });
                    
                    var productosPedidosSerialized = $httpParamSerializerJQLike($scope.table);
                    
                    // sendmail to admin
                    // var urlSendEmail = '/api/mailing/nuevoPedido';
                    // $http.get(urlSendEmail).then(function(res){
                    //   console.log(res.data.message);
                      // sendmail to customer
                      var urlSendEmailCustomer = '/api/mailing/pedidoRecibido?nombre='+$scope.customername+'&correo='+$scope.datapayU.buyerEmail+'&productos='+productosPedidosSerialized;
                      $http.get(urlSendEmailCustomer).then(function(resp){
                        console.log(resp.data.message);
                      });
                    // });

                  });
              });
          });
        }
      }
    });
  }

}());
