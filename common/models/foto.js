'use strict';

module.exports = function(Foto) {

  Foto.getFotosPedido = function (id_pedido, cb) {
    var filtro =  {
      where: {
        id_productopedido: id_pedido
      }
    }
    Foto.find(filtro, function (err, res) {
      var fotos = [];
      res.forEach(function (foto) {
        fotos.push(foto.imagen);
      });

      cb(null, fotos);
    });
  }

  Foto.remoteMethod('getFotosPedido',{
    http: {
      path: '/FotosPedido',
      verb: 'get'
    },
    description:'Fotos de un pediod especificado por id',
    accepts: [
      {
        arg: 'id_pedido',
        type: 'number',
        description: 'El id del pedido del cual quiero las fotos'
      }
    ],
    returns: [
      {
        arg: 'fotos',
        type: 'array'
      }
    ]
  })

};
