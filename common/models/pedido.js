'use strict';

module.exports = function(Pedido) {

  var Md5 = require('md5');
  Pedido.confirmacion = function (confirmacion, cb) {
    var filter = {where: {id: confirmacion.reference_sale}};
    Pedido.findOne(filter, function (err, pedido) {
      if(err){
        cb(null, err);
      }
      else{
        var idPedido = pedido.id;
        var table = '';
        var apiKey = '4Vj8eK4rloUd272L48hsrarnUA';
        var merchantId = confirmacion.merchant_id;
        var referenceSale = confirmacion.reference_sale;
        var value = confirmacion.value;

        var txValue = value.toString();
        var length = txValue.length;
        var newValue = txValue.slice(0, length - 1);

        var currency = confirmacion.currency;
        var statePol = confirmacion.state_pol;

        var firmaCadena = apiKey + '~' + merchantId + '~' + referenceSale + '~' + newValue + '~' + currency + '~' + statePol;
        var firmaCreada = Md5(firmaCadena || '');

        if (firmaCreada == confirmacion.sign) {
          switch (statePol) {
            case 4:
              pedido.updateAttributes({estado: 'Aprobado'}, function (err, res) {
                if(!err){
                  cb(null, "Actualizado");
                }
              });
              break;
            case 6:
              pedido.updateAttributes({estado: 'Rechazado'}, function (err, res) {
                if(!err){
                  cb(null, "Actualizado");
                }
              });
              break;
            case 5:
              pedido.updateAttributes({estado: 'Rechazado'}, function (err, res) {
                if(!err){
                  cb(null, "Actualizado");
                }
              });
              break;
          }
        }
        else{
          cb(null, "Rechazado");
        }
      }
    });
  };

  Pedido.remoteMethod("confirmacion", {
    accepts: {arg: 'confirmacion', type: 'object', http: { source: 'body' }},
    returns: {arg: 'confirmacion', type: 'string'}
  })
};
